Ci-joint l�ensemble du code source d�EVAsion : 
- Dans EVAsion.R_src.zip le projet Unity3D (2017.3.1)
- Dans parseval_libs.zip les projets parseval permettant de g�n�rer des librairies dynamiques contenant le code de parseval (.SO et .DLL)

Une fois les .SO o� .DLL recompil�, il vous faudra les copier dans le dossier correspondant dans le projet Unity :
 Assets/Plugins/� (remplacer les fichiers existants)

NB : Nous avons utilis� Visual Studio Community 2017 pour la DLL et Android studio pour le SO.
