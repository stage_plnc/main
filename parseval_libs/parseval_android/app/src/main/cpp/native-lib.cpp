#include <jni.h>
#include <string>
#include <iostream>
#include <cstdint>
#include "parseval_srcs/lpnc_context3.h"
#include "parseval_srcs/lpnc_parseval3_MESR.h"
#define DllExport __declspec (dllexport)
#define DEBUG true


extern "C"
{
    /*
    Creating some structure that can be serialized and casted throw C++ to C#
    */
    struct LPNC_DataExport_s
    {
        int d01;
        int d02;
        int d03;
        uintptr_t toFree;
    }typedef LPNC_DataExport;

    struct LPNC_Core_s
    {
        uintptr_t contextPtr;
        uintptr_t MESRPtr;
        uintptr_t toFree;

    }typedef LPNC_Core;

    JNIEXPORT LPNC_Core JNICALL InitCore(float winArg, int nbDimention1, int nbDimention2, int nbDimention3)
    {
        if (DEBUG)
        {
            freopen("debug.log", "a", stdout); // redirect cout in logfile
            std::cout << "[PARSVAL]----------START------------" << std::endl;
            std::cout << "[PARSVAL]- " << "Init Core : with : " << winArg << " %." << std::endl;
        }
        //Creating Core & Context
        LPNC_Core						*_exp = new LPNC_Core();
        LPNC_Context3<double>			*_context = new LPNC_Context3<double>
                (LPNC_Coord3(nbDimention1, nbDimention2, nbDimention3), 0.2, winArg);
        LPNC_Parseval3_MESR<double>		*_parseval = new LPNC_Parseval3_MESR<double>(*_context);

        _exp->contextPtr = reinterpret_cast<uintptr_t>(_context);
        _exp->MESRPtr = reinterpret_cast<uintptr_t>(_parseval);
        _parseval->init();
        return *_exp;
    }

    JNIEXPORT LPNC_DataExport JNICALL GetNextExercise(LPNC_Core_s core)
    {
        LPNC_Parseval3_MESR<double>* _parseval = (LPNC_Parseval3_MESR<double>*)(core.MESRPtr);
        LPNC_Coord3 l_exo = _parseval->getNextExercise();

        if (DEBUG)
            std::cout << "[PARSVAL]- " << "new exo =>  X : " << l_exo.getX() << ", Y : " << l_exo.getY() << ", Z : " << l_exo.getZ() << std::endl;
        LPNC_DataExport *expDT = (LPNC_DataExport*)malloc(sizeof(LPNC_DataExport));
        expDT->d01 = l_exo.getX();
        expDT->d02 = l_exo.getY();
        expDT->d03 = l_exo.getZ();
        expDT->toFree = reinterpret_cast<uintptr_t>(expDT);
        return *expDT;
    }

    JNIEXPORT void JNICALL SetResult(LPNC_Core_s core, LPNC_DataExport E, bool isSucces)
    {
        LPNC_Parseval3_MESR<double>* _parseval = (LPNC_Parseval3_MESR<double>*)(core.MESRPtr);
        LPNC_Coord3* l_exo;
        if (E.toFree == NULL)
        {
            l_exo = new LPNC_Coord3();
            l_exo->setX(E.d01);
            l_exo->setY(E.d02);
            l_exo->setZ(E.d03);
            _parseval->pushTrialResult(isSucces, *l_exo);
            _parseval->updatePerformanceEstimation();
            delete l_exo;
        }
        else
        {
            l_exo = (LPNC_Coord3*)(E.toFree);
            _parseval->pushTrialResult(isSucces, *l_exo);
            _parseval->updatePerformanceEstimation();
        }
    }

    JNIEXPORT void JNICALL FreeData(LPNC_DataExport_s E)
    {
        free((void*)E.toFree);
        if (DEBUG)
            std::cout << "[PARSVAL]- " << "FreeData" << std::endl;
    }

    JNIEXPORT LPNC_DataExport_s JNICALL GetUserPerformanceLevel(LPNC_Core_s core)
    {
		LPNC_DataExport_s *exp = (LPNC_DataExport*)malloc(sizeof(LPNC_DataExport));
		LPNC_Parseval3_MESR<double>* _parseval = (LPNC_Parseval3_MESR<double>*)(core.MESRPtr);
		LPNC_Coord3 coord = _parseval->getK();
		exp->d01 = coord.getX();
		exp->d02 = coord.getY();
		exp->d03 = coord.getZ();
		exp->toFree = reinterpret_cast<uintptr_t>(exp);
		if (DEBUG)
			std::cout << "[PARSVAL]- " << "Getting user perf : " + exp->d01 << std::endl;

		return *exp;
    }

    JNIEXPORT LPNC_DataExport JNICALL GetUserPerformanceSpace(LPNC_Core_s core)
    {
        LPNC_Context3<double>	*_context = (LPNC_Context3<double>*)(core.contextPtr);
        LPNC_Coord3 coords = _context->getPerformanceSpaceDimension();

        LPNC_DataExport_s *exp = (LPNC_DataExport*)malloc(sizeof(LPNC_DataExport));
        exp->d01 = coords.getX();
        exp->d02 = coords.getY();
        exp->d03 = coords.getZ();
        exp->toFree = reinterpret_cast<uintptr_t>(exp);
        return *exp;
    }

    JNIEXPORT void FreeLPNC_Core(LPNC_Core_s core)
    {
        if (core.toFree != NULL) {
            delete (LPNC_Context3<double> *) core.contextPtr;
            delete (LPNC_Parseval3_MESR<double> *) core.MESRPtr;
            delete (LPNC_Core *) core.toFree;
        }
    }
}

