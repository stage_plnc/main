/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_parseval3.h
* Version        :  1.0
* Author         :  Thierry PHENIX  / thierry.phenix@upmf-grenoble.fr
*                :   Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/


#ifndef LPNC_PARSEVAL3_H
#define LPNC_PARSEVAL3_H

#include <list>
#include "lpnc_context3.h"
#include "lpnc_matrix2.h"
#include "lpnc_matrix3.h"
#include "lpnc_step3.h"

/*! @\brief
 *
 */

template <class Real>
class LPNC_Parseval3
{
public:
    /*! @name Constructor / Destructor */
    //@{
    //!
    explicit LPNC_Parseval3(const LPNC_Context3<Real>& p_context);
    //@}

    /*! @name Accessors */
    //@{
    //! reading acces only
    //! access to the current context
    inline const LPNC_Context3<Real>& getContext() const {return m_currentContext;}

    //! reading acces only
    //! access to the current matrix of P(E|K) for each couple of (E,K)
    inline const LPNC_Matrix2<Real>& getExerciseLikelihoodMatrix() const
    {return m_likelihoodE;}

    //! reading acces only
    //! access to the matrix of P(S|E K)
    inline const LPNC_Matrix2<Real>& getSigmoidMatrix() const
    {return m_sigmoid;}

    //! reading acces only
    //! access to the current matrix of P(KT|KT-1) for each couple of (KT,KT-1)
    inline const LPNC_Matrix2<Real>& getPerformanceLikelihoodMatrix()const
    {return m_likelihoodK;}

    //! reading access only
    //! access to the current distribution of E at time T
    //! knowing values of E and S at times 0:T-1
    inline const LPNC_Matrix3<Real>& getDistribE_prevEandS() const
    {return m_pE_prevEandS;}

    //! reading access only
    //! access to the current distribution of K at time T
    //! knowing values of E and S at times 0:T-1
    inline const LPNC_Matrix3<Real>& getDistribK_prevEandS() const
    {return m_pK_prevEandS;}

    //! reading access only
    //! access to the current distribution of K at time T
    //! knowing values of E and S at times 0:T
    inline const LPNC_Matrix3<Real>& getDistribK_EandS() const
    {return m_pK_EandS;}

    //! reading access only
    //! access to the current step used by Parseval
    inline const LPNC_Step3<Real>* getCurrentStep() const
    {return mp_currentStep;}

    //! reading access only
    //! access to the history of the game
    inline const std::list<LPNC_Step3<Real>>& getHistory() const
    {return m_gameHistory;}

    //@}

    /*! @name Runtime methods */
    //@{
    //! launch this method at the init phase of the program.
    //! it computes matrix according parameters in the stored context.
    void init();

    //! update the internal computer and provide a new exercise
    LPNC_Coord3 getNextExercise();

    //! store the result
    void pushTrialResult(bool p_success);

    //! this method free the array conatining all the previous steps
    //! but keep the current step to enable dynamic clearing
    //! @warning final size is one
    void deleteHistory();

   //@}


protected:

    /*! @name Data */
    //@{
    //! Store the context needed by Parseval3 to run
    //! TODO: maybe it must not allow on the fly modification...
    LPNC_Context3<Real>         m_currentContext;

    //! This 2D matrix contains all values for P(E|K)
    //! when E and K describe all the performance space.
    //! A column 'e' of this matrix contains the distribution of P([E=e]|K)
    LPNC_Matrix2<Real>          m_likelihoodE;

    //! This 2D matrix contains all values for P(Ki|Ki-1)
    //! when Ki and Ki-1 describe all the performance space.
    //! A column 'k' of this matrix contains the distribution of P([Ki=k]|Ki-1)
    LPNC_Matrix2<Real>          m_likelihoodK;

    //! this 2D matrix contains all values for P(S|E K) (S = true)
    //! when E and K describe all the performance space
    //! A line 'E' correspond to P(S|E K) for all values of K, known E
    LPNC_Matrix2<Real>          m_sigmoid;

    //! store the successive steps of the game.
    //! the size of the list is store in the context.
    //! when the list is full, append a new step bring automatically to the removal of the first element.
    std::list<LPNC_Step3<Real>> m_gameHistory;

    //! refernce to the current step used in Parseval
    LPNC_Step3<Real>           *mp_currentStep;

    //! This is the ditribution used to estimate the performance
    LPNC_Matrix3<Real>          m_pK_prevEandS;

    //! This is the distribution used to select the next exercise
    LPNC_Matrix3<Real>          m_pE_prevEandS;

    //! This is working matrix representing the distribution of K at time T
    //! knowing E at times 0:T and S at times 0:T
    LPNC_Matrix3<Real>          m_pK_EandS;

    //! The first exercise is provided without the knowledge of previous success
    //! so, we have to make a special case.
    bool                        m_isFirstExercise;
    //@}
};

#endif // LPNC_PARSEVAL3_H
