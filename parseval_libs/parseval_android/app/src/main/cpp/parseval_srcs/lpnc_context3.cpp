/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_context3.cpp
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
* 1.1            :  04 2014
*                   - Add slope for each dimension in the context
*                   - Add a sigma value used to select new exercises
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#include "lpnc_context3.h"
#include <iostream>

template<class Real>
LPNC_Context3<Real>::LPNC_Context3( const LPNC_Coord3&  p_dimension,
                                    Real                p_learningLikelihoodVariance,
                                    Real                p_userPerformanceLevel,
                                    Real                p_chanceLevel,
                                    Real                p_sigma,
                                    Real                p_slopeX,          
                                    Real                p_slopeY,
                                    Real                p_slopeZ ) :
    m_performanceSpaceDimension(p_dimension),
    m_LearningLikelihoodVariance(p_learningLikelihoodVariance),
    m_userPerformanceLevel(p_userPerformanceLevel),
    m_chanceLevel(p_chanceLevel),
    m_sigma(p_sigma),
    m_slopeX(p_slopeX),
    m_slopeY(p_slopeY),
    m_slopeZ(p_slopeZ)
{
}

template<class Real>
bool LPNC_Context3<Real>::operator==(const LPNC_Context3& p_context) const
{
    return (m_performanceSpaceDimension  == p_context.m_performanceSpaceDimension  &&
            m_LearningLikelihoodVariance == p_context.m_LearningLikelihoodVariance &&            
            m_userPerformanceLevel       == p_context.m_userPerformanceLevel       &&
            m_chanceLevel                == p_context.m_chanceLevel                &&
            m_sigma                      == p_context.m_sigma                      &&
            m_slopeX                     == p_context.m_slopeX                     &&
            m_slopeY                     == p_context.m_slopeY                     &&
            m_slopeZ                     == p_context.m_slopeZ                     );
}

// this force the compiler to instanciate the three classes
// and enable the developer to code in .cpp file instead of in header file
template class LPNC_Context3<float>;
template class LPNC_Context3<double>;
template class LPNC_Context3<long double>;
