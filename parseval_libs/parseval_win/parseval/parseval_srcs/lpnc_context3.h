/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_context3.h
* Version        :  1.1
* Author         :  Thierry PHENIX  / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
* 1.1            :  04 2014
*                   - Add slope for each dimension in the context
*                   - Add a sigma value used to select new exercises
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#ifndef LPNC_CONTEXT3_H
#define LPNC_CONTEXT3_H

#include "lpnc_coord3.h"

/*! @\brief data structure used in Parseval Algorithm
 *  This class contains all the parameters used by the PARSEVAL algorithm.
 *  An instance of this class is required to instanciate the algorithm.
 *  Default values are provided. They have been tested for the specific performance space (6,6,6).
 *  All still probably be good for other performance spaces.
 *  Nevertheless, there's no default value for the performance space.
 */

template<class Real>
class LPNC_Context3
{
public:
    LPNC_Context3(const LPNC_Coord3&    p_dimension,
                        Real            p_learningLikelihoodVariance    = .2,
                        Real            p_userPerformanceLevel          = .75,
                        Real            p_chanceLevel                   = 0.,
                        Real            p_sigma                         = 0.05,
                        Real            p_slopeX                        = 2.5,
                        Real            p_slopeY                        = 2.5,
                        Real            p_slopeZ                        = 2.5);


    /*! @name Accessors */
    //@{
    //! reading acces only
    //! return the LPNC_Coord3 wich contains the size of each dimension of the performance space.
    inline const LPNC_Coord3& getPerformanceSpaceDimension() const {return m_performanceSpaceDimension;}

    //! reading acces only
    //! return the LPNC_Coord3 wich contains the size of each dimension of the performance space.
    inline Real getLearningLikelihoodVariance() const {return m_LearningLikelihoodVariance;}

    //!
    inline Real getUserPerformanceLevel() const {return m_userPerformanceLevel;}

    //!
    inline Real getSlopeX() const {return m_slopeX;}
    inline Real getSlopeY() const {return m_slopeY;}
    inline Real getSlopeZ() const {return m_slopeZ;}

    //!
    inline Real getChanceLevel() const {return m_chanceLevel;}
    
    //!
    inline Real getSigma() const {return m_sigma;}
    //@}

    /*! @name Comparison and assignment operators */
    //@{
    //! Comparison operator
    //! @param the context to be compared to
    //! return true if each field of the parameter is equal to the same field in this
    bool operator==(const LPNC_Context3&) const;
    //@}

private:
    /*! @name Data */
    //@{
    //!
    //! this array contains the number of steps available for each of the 3 dimensions.
    //! They can be all different.
    LPNC_Coord3 m_performanceSpaceDimension;

    //!
    Real m_LearningLikelihoodVariance;

    //! This value correspond to the value percent of succes that can reaches the user
    Real m_userPerformanceLevel;

    //!
    Real m_slopeX, m_slopeY, m_slopeZ;

    //!
    Real m_chanceLevel;
    
    //!
    Real m_sigma;

    //@}


};

#endif // LPNC_CONTEXT3_H
