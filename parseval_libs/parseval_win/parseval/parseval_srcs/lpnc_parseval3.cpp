/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_parseval3.cpp
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :   Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#include "lpnc_parseval3.h"
#include "lpnc_coord3.h"
#include <iostream>
#include <stdlib.h>
#include <valarray>

template <class Real>
void printMatrix(const std::string& p_message, const std::valarray<Real> p_matrix, int p_size1D);

template <class Real>
LPNC_Parseval3<Real>::LPNC_Parseval3(const LPNC_Context3<Real> &p_context) :
    m_currentContext(p_context),
    mp_currentStep(0),
    m_pK_prevEandS(LPNC_Matrix3<Real>(  p_context.getPerformanceSpaceDimension().getX(),
                                        p_context.getPerformanceSpaceDimension().getY(),
                                        p_context.getPerformanceSpaceDimension().getZ())),
    m_pE_prevEandS(LPNC_Matrix3<Real>(  p_context.getPerformanceSpaceDimension().getX(),
                                        p_context.getPerformanceSpaceDimension().getY(),
                                        p_context.getPerformanceSpaceDimension().getZ())),
    m_pK_EandS(LPNC_Matrix3<Real>(  p_context.getPerformanceSpaceDimension().getX(),
                                    p_context.getPerformanceSpaceDimension().getY(),
                                    p_context.getPerformanceSpaceDimension().getZ(),
                                    1.)),
    m_isFirstExercise(true)
{
}

template <class Real>
void LPNC_Parseval3<Real>::init()
{
    // Initialzation of the Matrix of Exercices Selecting Strategy : P(E|K)
    m_likelihoodE.initWithModifiedSigmoid(m_currentContext.getPerformanceSpaceDimension(),
                                          m_currentContext.getSlopeX(),
                                          m_currentContext.getSlopeY(),
                                          m_currentContext.getSlopeZ(),
                                          m_currentContext.getChanceLevel(),
                                          m_currentContext.getUserPerformanceLevel() );

    // Initialization of the matrix of learning model : P(Kt|Kt-1)
    m_likelihoodK.initWithNormal3DDistribution(m_currentContext.getPerformanceSpaceDimension(),
                                               m_currentContext.getLearningLikelihoodVariance());

    //
    m_sigmoid.initWithSigmoidFunction(m_currentContext.getPerformanceSpaceDimension(),
                                      m_currentContext.getSlopeX(),
                                      m_currentContext.getSlopeY(),
                                      m_currentContext.getSlopeZ(),
                                      m_currentContext.getChanceLevel());

}

template <class Real>
LPNC_Coord3 LPNC_Parseval3<Real>::getNextExercise()
{
    if (m_isFirstExercise)
    {
        // Build the distribution of P(K0|E0 S0)
        // and store the result in m_pK_EandS
        // Nothing to do here if we consider this distribution as uniform
        // because the matrix is created with 1.0 as default value in constructor
        // and the normalisation do the rest.

        m_isFirstExercise = false;
    } else
    {
        // compute P(KT-1|E0:T-1 S0:T-1) and store the new part in current step
        updatePK_EandS(m_pK_EandS,
                       m_pK_prevEandS,
                       m_likelihoodE,
                       m_sigmoid,
                       mp_currentStep);
    }

    // compute P(KT|E0:T-1 S0:T-1) based on P(KT-1|E0:T-1 S0:T-1)
    m_pK_prevEandS.matrix() = 0.;
    updatePK_prevEandS(m_pK_prevEandS,
                       m_pK_EandS,
                       m_likelihoodK);


    // compute P(ET|E0:T-1 S0:T-1) based on P(KT|E0:T-1 S0:T-1)
    m_pE_prevEandS.matrix() = 0.;
    updatePE_prevEandS(m_pE_prevEandS,
                       m_pK_prevEandS,
                       m_likelihoodE);

    // create new step and reference it in m_currentStep;
    m_gameHistory.push_back(LPNC_Step3<Real>());
    mp_currentStep = &m_gameHistory.back();

    // select the next exercise and store it in the current step
    mp_currentStep->exercise() = drawExercise(m_pE_prevEandS);

    // finaly return the selected exercise
    return mp_currentStep->exercise();
}


template <class Real>
void LPNC_Parseval3<Real>::pushTrialResult(bool p_success)
{
    mp_currentStep->trials().push_back(p_success);
}

template <class Real>
void LPNC_Parseval3<Real>::deleteHistory()
{
    typename std::list<LPNC_Step3<Real>>::iterator l_start, l_end;
    l_start = m_gameHistory.begin();
    l_end   = m_gameHistory.end();
    l_end--;
    m_gameHistory.erase(l_start, l_end);
}


// ****************************
// PRIVATE METHODS
// ****************************



//! This is the first step for computing the ditribution used to draw the next exercise
//! It computes Ct = P(Kt|E0:t S0:t) knowing E and the list of trial results:
//! for each value of Kt,calculate the product P(Et|Kt)*P(S1T|ET KT)*...*P(SnT|ET KT)
//! then multiply by Bt
//! @param p_K_prevEandS the new value P(Kt|E0:t-1 S0:t-1) for t = current step
//! @param p_K_EandS the value of P(Kt-1|E0:t-1 S0:t-1), computed during the previous step
//! @param p_likelyhoodK P(Kt|Kt-1), Kt and Kt-1 describing all the performance space.
template <class Real>
void updatePK_EandS(      LPNC_Matrix3<Real>& p_K_EandS,
                    const LPNC_Matrix3<Real>& p_K_prevEandS,
                    const LPNC_Matrix2<Real>& p_likelyhoodE,
                    const LPNC_Matrix2<Real>& p_sigmoid,
                    const LPNC_Step3<Real>*   p_step)
{
    // extract P(E|K) for the known E and all values of K
    // means extracting the line E from the likelyhoodE matrix
    int l_row=  p_step->exercise().getX()*p_K_EandS.getSizeY()*p_K_EandS.getSizeZ()+
                p_step->exercise().getY()*p_K_EandS.getSizeZ()+
                p_step->exercise().getZ();
    int l_size= p_K_EandS.size();
    std::valarray<Real> l_pE_K= p_likelyhoodE.matrix()[std::slice(l_row*l_size,l_size,1)];

    // extract P(S|E K) for the known E and all values of K
    std::valarray<Real> l_pSuccess_EandK= p_sigmoid.matrix()[std::slice(l_row*l_size,l_size,1)];

    std::valarray<Real> l_pFailure_EandK(1., l_size);
    l_pFailure_EandK -= l_pSuccess_EandK;
    std::valarray<Real> l_allP_EandK(1., l_size);
    for(auto l_it= begin(p_step->trials()); l_it!= end(p_step->trials()); ++l_it)
    {
         if (*l_it) {
            l_allP_EandK *= l_pSuccess_EandK;
        } else {
            l_allP_EandK *= l_pFailure_EandK;
        }
    }

    // compute final value of P(K|E S)
//    p_K_EandS.matrix() = l_pE_K;
    p_K_EandS.matrix()= l_allP_EandK;
    p_K_EandS.matrix()*= p_K_prevEandS.matrix();

    // normalization
    Real l_sum= 0.;
    for(auto l_it= begin(p_K_EandS.matrix()); l_it!= end(p_K_EandS.matrix()); ++l_it)
    {
        l_sum+= *l_it;
    }
    p_K_EandS.matrix()/= l_sum;
    if(l_sum<10e-9)
        std::cout << "DIVISION BY ZERO : " << l_sum << " IN updatePK_EandS\n";
}


//! This is the second step for computing the ditribution used to draw the next exercise
//! It computes Bt = P(Kt|E0:t-1 S0:t-1) with the formula:
//! for each value of Kt, sum over Kt-1 P(Kt|Kt-1)*P(Kt-1|E0:t-1 S0:t-1)
//! @param p_K_prevEandS the new value P(Kt|E0:t-1 S0:t-1) for t = current step
//! @param p_K_EandS the value of P(Kt-1|E0:t-1 S0:t-1), computed during the previous step
//! @param p_likelyhoodK P(Kt|Kt-1), Kt and Kt-1 describing all the performance space.
template <class Real>
void updatePK_prevEandS(      LPNC_Matrix3<Real>& p_K_prevEandS,
                        const LPNC_Matrix3<Real>& p_K_EandS,
                        const LPNC_Matrix2<Real>& p_likelyhoodK)
{
    Real l_sum   = 0.0;
    int  l_i     = 0;
    int  l_size  = p_K_prevEandS.size();
    std::valarray<Real>& l_matrixK_ES= p_K_prevEandS.matrix();
    for(auto l_it= begin(l_matrixK_ES); l_it!=end(l_matrixK_ES); ++l_it)
    {
        // extract the line corresponding to P(K=kt|Kt-1)
        std::valarray<Real> l_line= p_likelyhoodK.matrix()[std::slice(l_i*l_size,l_size,1)];
        std::valarray<Real> l_product = l_line * p_K_EandS.matrix();

        for(auto l_jt= begin(l_product); l_jt!=end(l_product); ++l_jt)
        {
            p_K_prevEandS(l_i) += *l_jt;
        }
        l_sum += p_K_prevEandS(l_i);
        l_i++;
    }
    // normalisation of P(E0)
    p_K_prevEandS.matrix() /= l_sum;
    if(l_sum<10e-6)
        std::cout << "DIVISION BY ZERO : " << l_sum << "  IN updatePK_prevEandS\n";
}


//! This is the finally step for computing the ditribution used to draw the next exercise
//! It computes At = P(Et|E0:t-1 S0:t-1) with the formula:
//! for each value of Et, sum over Kt P(Et|Kt)*Bt
//! @param p_E_prevEandS the new value P(Et|E0:t-1 S0:t-1)
//! knowing previous values of E and S at times 0:t-1
//! @param p_K_prevEandS previous computed value
//! @param p_likelyhoodE P(Et|Kt), Et and Kt describing all the performance space.
template <class Real>
void updatePE_prevEandS(      LPNC_Matrix3<Real>& p_E_prevEandS,
                        const LPNC_Matrix3<Real>& p_K_prevEandS,
                        const LPNC_Matrix2<Real>& p_likelyhoodE)
{
    Real l_sum = 0.0;
    int  l_i     = 0;
    int  l_size = p_E_prevEandS.size();
    std::valarray<Real>& l_matrixE_ES= p_E_prevEandS.matrix();
    for(auto l_it= begin(l_matrixE_ES); l_it!=end(l_matrixE_ES); ++l_it)
    {
        // extract the line corresponding to P(K=kl_i|Kt-1)
        std::valarray<Real> l_line= p_likelyhoodE.matrix()[std::slice(l_i*l_size,l_size,1)];
        std::valarray<Real> l_product = l_line * p_K_prevEandS.matrix();
        for(auto l_jt= begin(l_product); l_jt!=end(l_product); ++l_jt)
        {
            p_E_prevEandS(l_i) += *l_jt;
        }
        l_sum += p_E_prevEandS(l_i);
        l_i++;
    }
    // normalisation of P(E0)
    std::valarray<Real>& l_matrix = p_E_prevEandS.matrix();
    l_matrix /= l_sum;
    if(l_sum<10e-6)
        std::cout << "DIVISION BY ZERO : " << l_sum << "  IN updatePE_prevEandS\n";
}

//! Method that draws an exercise knowing the current distribution over exercises
//! This method is classical, special thaks to Julien Diard for the explanations
//! We can use an other method like the max value that must be arround our estimated
//! performance value, but this choice brings to a loos of information.
template <class Real>
LPNC_Coord3 drawExercise(const LPNC_Matrix3<Real> p_pE)
{
    // random number between 0 and 1
    Real l_random= (Real)(rand()/(RAND_MAX + 1.0));
    // find the interval containing the random value
    int l_i     = -1;
    Real l_sum  = 0.0;
    const std::valarray<Real>& l_matrixE= p_pE.matrix();
    for(auto l_it=begin(l_matrixE);(l_random>l_sum && l_it!=end(l_matrixE));++l_it)
    {
        l_sum += *l_it;
        ++l_i;
    }
    // return corresponding coordinates in space of exercises
    if(l_i<0) l_i= 0;
    int l_sizeZ = p_pE.getSizeZ();
    int l_sizeYZ= p_pE.getSizeY()*l_sizeZ;
    return LPNC_Coord3(l_i/l_sizeYZ, l_i%l_sizeYZ/l_sizeZ, l_i%l_sizeYZ%l_sizeZ);
}


template <class Real>
LPNC_Coord3 drawExercise2(const LPNC_Matrix3<Real> p_pE)
{
    int                         l_iMax      = -1;
    int                         l_i         = 0;
    Real                        l_value     = 0;
    const std::valarray<Real>&  l_matrixE   = p_pE.matrix();

    for(auto l_it= begin(l_matrixE); l_it!= end(l_matrixE); ++l_it)
    {
        if(l_value< *l_it)
       {
            l_value = *l_it;
            l_iMax= l_i++;
        }
    }
    // return corresponding coordinates in space of exercises
    int l_sizeZ = p_pE.getSizeZ();
    int l_sizeYZ= p_pE.getSizeY()*l_sizeZ;
    return LPNC_Coord3(l_iMax/l_sizeYZ, l_iMax%l_sizeYZ/l_sizeZ, l_iMax%l_sizeYZ%l_sizeZ);
}


//! ****** UNUSED ******
//! Compute the distribution of E0 by summing over K0 P(E0|K0)
template <class Real>
void BuildE0Distribution(LPNC_Matrix3<Real>& p_pE, const LPNC_Matrix2<Real>& p_pE_K)
{
    Real l_sum = 0.0;
    int  l_i     = 0;
    int  l_size = p_pE.size();
    const std::valarray<Real>& l_matrixE_K= p_pE_K.matrix();
    // sum over K of P(E|K) (means sum each line of p_pE_K)
    for(auto l_it= begin(l_matrixE_K); l_it!=end(l_matrixE_K); ++l_it)
    {
        p_pE(l_i++/l_size)+= *l_it;
        l_sum += *l_it;
    }
    // normalisation of P(E0)
    std::valarray<Real>& l_matrixE = p_pE.matrix();
    l_matrixE /= l_sum;
}

template <class Real>
void printMatrix(const std::string& p_message, const std::valarray<Real> p_matrix, int p_size1D)
{
    int l_count = 0;
    std::cout << "\n *** " << p_message << " SIZE: " << p_matrix.size() << " ***\n";
    for(auto l_it=begin(p_matrix);l_it!= end(p_matrix);++l_it)
    {
        std::cout << ((*l_it<10e-9)?0.000:*l_it) << "\t|";
        l_count++;
        if(!(l_count % p_size1D)) std::cout << "\n";
    }
    std::cout << "\n\n";
}


// this force the compiler to instanciate the three classes
// and enable the developer to code in .cpp file instead of in header file
template class LPNC_Parseval3<float>;
template class LPNC_Parseval3<double>;
template class LPNC_Parseval3<long double>;
