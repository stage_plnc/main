/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_matrix2.cpp
* Version        :  1.1
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
* 1.1            :  04 2014
*                   - Modification of implementations to take in account of slopes
*                   - Implementation of method initWithModifiedSigmoid2
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#include "lpnc_matrix2.h"
#include <iostream>
#include <cmath>

using namespace std;

template<class Real>
LPNC_Matrix2<Real>::LPNC_Matrix2(int p_sizeX,
                                 int p_sizeY)
{
    m_sizeX = p_sizeX;
    m_sizeY = p_sizeY;
    if (m_sizeX != 0 && m_sizeY != 0) {
        m_data.resize(p_sizeX*p_sizeY);
    }
}

template<class Real>
LPNC_Matrix2<Real>::LPNC_Matrix2(int p_sizeX,
                                 int p_sizeY,
                                 Real p_value)
{
    m_sizeX = p_sizeX;
    m_sizeY = p_sizeY;
    // no default value... hope sizes not equal 0... anyway it works!
    m_data.resize(p_sizeX*p_sizeY, p_value);
}

template<class Real>
void  LPNC_Matrix2<Real>::initWithNormal3DDistribution(const LPNC_Coord3 &p_dimension,
                                                       Real p_variance)
{
    // check the size of the matrix and update if needed
    int l_sizeX = p_dimension.getX();
    int l_sizeY = p_dimension.getY();
    int l_sizeZ = p_dimension.getZ();
    int l_size = l_sizeX * l_sizeY * l_sizeZ;
    if(m_sizeX != l_size || m_sizeY != l_size)
    {
        m_data.resize(l_size*l_size);
        m_sizeX = l_size;
        m_sizeY = l_size;
    }
    // used to normalized each column so that it correspond to a distribution (somme = 1)
    valarray<Real> l_sums(l_size);

    // build the matrix of the distribution of the variable X|Y
    // Given a value of X, its column correspond to the distribution for each value of Y
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        int l_row    = l_i/l_size;
        int l_column = l_i%l_size;
        // identify each coordinate of the point in the 3D space associated to X
        int l_xi = l_row/(l_sizeY*l_sizeZ);
        int l_yi = l_row%(l_sizeY*l_sizeZ)/l_sizeZ;
        int l_zi = l_row%(l_sizeY*l_sizeZ)%l_sizeZ;
        // identify each coordinate of the point in the 3D space associated to Y
        int l_xj = l_column/(l_sizeY*l_sizeZ);
        int l_yj = (l_column%(l_sizeY*l_sizeZ))/l_sizeZ;
        int l_zj = (l_column%(l_sizeY*l_sizeZ))%l_sizeZ;

        Real l_value = (Real)((l_xi-l_xj)*(l_xi-l_xj) + (l_yi-l_yj)*(l_yi-l_yj) + (l_zi-l_zj)*(l_zi-l_zj));
        l_value      = exp((- 1./(2.*p_variance*p_variance)) * l_value);
        m_data[l_i] = l_value;
        l_sums[l_column] += l_value;
    }

    // Normalized each column of the matrix
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        m_data[l_i] /= l_sums[l_i%l_size];
    }
}

template<class Real>
void LPNC_Matrix2<Real>::initWithSigmoidFunction(const LPNC_Coord3& p_dimension,
                                                 Real               p_slopeX,
                                                 Real               p_slopeY,
                                                 Real               p_slopeZ,
                                                 Real               p_chanceLevel)
{
    // check the size of the matrix and update if needed
    int l_sizeX = p_dimension.getX();
    int l_sizeY = p_dimension.getY();
    int l_sizeZ = p_dimension.getZ();
    int l_size = l_sizeX * l_sizeY * l_sizeZ;
    if(m_sizeX != l_size || m_sizeY != l_size)
    {
        m_data.resize(l_size*l_size);
        m_sizeX = l_size;
        m_sizeY = l_size;
    }
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        int l_row    = l_i/l_size;
        int l_column = l_i%l_size;
        // identify each coordinate of the point in the 3D space associated to X
        int l_xi = l_row/(l_sizeY*l_sizeZ);
        int l_yi = l_row%(l_sizeY*l_sizeZ)/l_sizeZ;
        int l_zi = l_row%(l_sizeY*l_sizeZ)%l_sizeZ;
        // identify each coordinate of the point in the 3D space associated to Y
        int l_xj = l_column/(l_sizeY*l_sizeZ);
        int l_yj = (l_column%(l_sizeY*l_sizeZ))/l_sizeZ;
        int l_zj = (l_column%(l_sizeY*l_sizeZ))%l_sizeZ;
        m_data[l_i] =   (1.0 + exp(p_slopeX * (l_xi - l_xj)))*
                        (1.0 + exp(p_slopeY * (l_yi - l_yj)))*
                        (1.0 + exp(p_slopeZ * (l_zi - l_zj)));
        m_data[l_i]= (p_chanceLevel+ (1-p_chanceLevel)*pow(m_data[l_i], - 0.3333333));
    }
}

template<class Real>
void LPNC_Matrix2<Real>::initWithModifiedSigmoid(const  LPNC_Coord3&    p_dimension,
                                                 Real                   p_slopeX,
                                                 Real                   p_slopeY,
                                                 Real                   p_slopeZ,
                                                 Real                   p_chanceLevel,
                                                 Real                   p_performanceLevel)
{
    // check the size of the matrix and update if needed
    int l_sizeX = p_dimension.getX();
    int l_sizeY = p_dimension.getY();
    int l_sizeZ = p_dimension.getZ();
    int l_size = l_sizeX * l_sizeY * l_sizeZ;
    if(m_sizeX != l_size || m_sizeY != l_size)
    {
        m_data.resize(l_size*l_size);
        m_sizeX = l_size;
        m_sizeY = l_size;
    }
    int l_count1= 0, l_count2= 0, l_count3= 0;
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        int l_row    = l_i/l_size;
        int l_column = l_i%l_size;
        // identify each coordinate of the point in the 3D space associated to X
        int l_xi = l_row/(l_sizeY*l_sizeZ);
        int l_yi = l_row%(l_sizeY*l_sizeZ)/l_sizeZ;
        int l_zi = l_row%(l_sizeY*l_sizeZ)%l_sizeZ;
        // identify each coordinate of the point in the 3D space associated to Y
        int l_xj = l_column/(l_sizeY*l_sizeZ);
        int l_yj = (l_column%(l_sizeY*l_sizeZ))/l_sizeZ;
        int l_zj = (l_column%(l_sizeY*l_sizeZ))%l_sizeZ;
        // compute the value of sigmoid curve
        Real l_temp =   (1.0 + exp(p_slopeX * (l_xi - l_xj)))*
                        (1.0 + exp(p_slopeY * (l_yi - l_yj)))*
                        (1.0 + exp(p_slopeZ * (l_zi - l_zj)));
        l_temp= (p_chanceLevel+ (1-p_chanceLevel)*pow(l_temp, - 0.3333333));
        m_data[l_i]= l_temp;
        // select the best fit position
        if (fabs(l_temp-p_performanceLevel)<.05) {
            m_data[l_i]= 1.;
            l_count1++;
        } else if (fabs(l_temp-p_performanceLevel)<0.1) {
            m_data[l_i]= .1;
            l_count2++;
        }  else if (fabs(l_temp-p_performanceLevel)<0.15) {
            m_data[l_i]= .01;
            l_count3++;
        } else {
            m_data[l_i]= 0.001;
        }
    }
    // for each value of K, create the distribution
    
    int l_sum = l_count1+l_count2+l_count3;
    valarray<Real> l_sums(l_size);
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        if (m_data[l_i]== .5) {
            m_data[l_i]= m_data[l_i]*l_count1/l_count2;
        } else if (m_data[l_i]== .25) {
            m_data[l_i]= m_data[l_i]*l_count1/l_count3;
        } else if (m_data[l_i]== .05) {
            m_data[l_i]= m_data[l_i]*l_count1/(m_data.size()-l_sum);
        }
        int l_column = l_i%l_size;
        l_sums[l_column] += m_data[l_i];
    }
    
    // Normalized each column of the matrix
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        m_data[l_i] /= l_sums[l_i%l_size];
    }
}

template<class Real>
void LPNC_Matrix2<Real>::initWithModifiedSigmoid2(const LPNC_Coord3&    p_dimension,
                                                  Real                  p_slopeX,
                                                  Real                  p_slopeY,
                                                  Real                  p_slopeZ,
                                                  Real                  p_sigma,
                                                  Real                  p_chanceLevel,
                                                  Real                  p_performanceLevel)
{
    // check the size of the matrix and update if needed
    int l_sizeX = p_dimension.getX();
    int l_sizeY = p_dimension.getY();
    int l_sizeZ = p_dimension.getZ();
    int l_size = l_sizeX * l_sizeY * l_sizeZ;
    if(m_sizeX != l_size || m_sizeY != l_size)
    {
        m_data.resize(l_size*l_size);
        m_sizeX = l_size;
        m_sizeY = l_size;
    }
    valarray<Real> l_sums(l_size); // array containing the sum of elements of each column.
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        int l_row    = l_i/l_size;
        int l_column = l_i%l_size;
        // identify each coordinate of the point in the 3D space associated to X
        int l_xi = l_row/(l_sizeY*l_sizeZ);
        int l_yi = l_row%(l_sizeY*l_sizeZ)/l_sizeZ;
        int l_zi = l_row%(l_sizeY*l_sizeZ)%l_sizeZ;
        // identify each coordinate of the point in the 3D space associated to Y
        int l_xj = l_column/(l_sizeY*l_sizeZ);
        int l_yj = (l_column%(l_sizeY*l_sizeZ))/l_sizeZ;
        int l_zj = (l_column%(l_sizeY*l_sizeZ))%l_sizeZ;
        // compute the value of sigmoid curve
        Real l_temp =   (1.0 + exp(p_slopeX * (l_xi - l_xj)))*
                        (1.0 + exp(p_slopeY * (l_yi - l_yj)))*
                        (1.0 + exp(p_slopeZ * (l_zi - l_zj)));
        l_temp= (p_chanceLevel+ (1-p_chanceLevel)*pow(l_temp, - 0.3333333));
        m_data[l_i]= exp(-pow(l_temp-p_performanceLevel, 2)/(2*pow(p_sigma, 2)));
        l_sums[l_column] += m_data[l_i];
    }

    // Normalized each column of the matrix
    for (int l_i = 0, iMax= (int)m_data.size(); l_i < iMax; ++l_i)
    {
        m_data[l_i] /= l_sums[l_i%l_size];
    }
}

template<class Real>
LPNC_Matrix2<Real>& LPNC_Matrix2<Real>::operator=(const LPNC_Matrix2& p_matrix)
{
    m_sizeX = p_matrix.m_sizeX;
    m_sizeY = p_matrix.m_sizeY;
    m_data.resize(m_sizeX*m_sizeY);
    for (int i = 0, iMax = m_sizeX*m_sizeY; i < iMax; ++i) {
        m_data[i] = p_matrix.m_data[i];
    }
    return *this;
}

template<class Real>
void LPNC_Matrix2<Real>::assign(Real p_value)
{
    for (unsigned i=0, iMax= m_sizeX*m_sizeY; i<iMax; i++)
    {
        // at() check the bound and signals if the requested position is out of range.
        m_data[i] = p_value;
    }
}

template<class Real>
std::string LPNC_Matrix2<Real>::toMathematica() const
{
    string l_currentSTR;
    l_currentSTR.append("{");
    for (int l_i = 0; l_i < m_sizeX*m_sizeY; ++l_i)
    {
        l_currentSTR.append(to_string((m_data[l_i])));
        l_currentSTR.append(",");
        l_currentSTR.append(to_string((m_data[1])));
        l_currentSTR.append(",");
        l_currentSTR.append(to_string((m_data[2])));
        l_currentSTR.append("}");
    }
    return l_currentSTR;
}


// this force the compiler to instanciate the three classes
// and enable the developer to code in .cpp file instead of in header file
template class LPNC_Matrix2<float>;
template class LPNC_Matrix2<double>;
template class LPNC_Matrix2<long double>;
