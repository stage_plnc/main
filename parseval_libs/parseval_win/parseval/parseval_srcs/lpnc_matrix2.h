/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_matrix2.h
* Version        :  1.1
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
* 1.1            :  04 2014
*                   - Modification of signatures of certain methods to take in account of slopes
*                   - Add method initWithModifiedSigmoid2
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#ifndef LPNC_MATRIX2_H
#define LPNC_MATRIX2_H

#include <valarray>
#include "lpnc_coord3.h"

/*! @\brief
 *
 */


template<class Real>
class LPNC_Matrix2
{
public:
    /*! @name Constructors and Initialisation*/
    //@{
    //! Default constructor
    //! @warning if the size is not precised, the matrix is empty and no space is reserved
    LPNC_Matrix2( int p_sizeX= 0, int p_sizeY= 0);

    //! constructor with value, and no default size
    //! @param p_value is the value assign to each element of the matrix.
    LPNC_Matrix2( int p_sizeX, int p_sizeY, Real p_value);

    //! constructor by copy
    //! @param p_matrix2 is the 2D matrix that will be copied in.
    LPNC_Matrix2(const LPNC_Matrix2& p_matrix2) {(*this) = p_matrix2;}

    //! Calculates the value of the 3D normal distribution followed by X|Y
    //! knowing that for each couple (X, Y), the mean of the normal distribution is Y
    //! @param p_dimension contains the size of each dimension of the performance space.
    //! @param p_variance contains the variance of the normal distribution.
    //! @warning in this method we consider that the variance is the same along the 3 dimension.
    void initWithNormal3DDistribution(const LPNC_Coord3& p_dimension, Real p_variance);

    //! This matrix correspond to the sigmoid function evaluate for all the possible
    //! values of E and K.
    //! E is represented by the lines and K by the coolumn
    void initWithSigmoidFunction(const LPNC_Coord3& p_dimension,
                                 Real               p_slopeX,
                                 Real               p_slopeY,
                                 Real               p_slopeZ,
                                 Real               p_chanceLevel);
    
    //! This matrix correspond to the sigmoid function evaluate for all the possible
    //! values of E and K. Then modify the values i function of performances
    //! E is represented by the lines and K by the coolumn
    void initWithModifiedSigmoid(const LPNC_Coord3& p_dimension,
                                 Real               p_slopeX,
                                 Real               p_slopeY,
                                 Real               p_slopeZ,
                                 Real               p_chanceLevel,
                                 Real               p_performanceLevel);
    
    //! This matrix correspond to the sigmoid function evaluate for all the possible
    //! values of E and K. Then modify the values i function of performances
    //! E is represented by the lines and K by the coolumn
    void initWithModifiedSigmoid2(const LPNC_Coord3&    p_dimension,
                                  Real                  p_slopeX,
                                  Real                  p_slopeY,
                                  Real                  p_slopeZ,
                                  Real                  p_sigma,
                                  Real                  p_chanceLevel,
                                  Real                  p_performanceLevel);
    //@}


    /*! @name Accessors */
    //@{
    //! Reading access to the (i,j) component value.
    //! @warning No check is made on the range of validity of the arguments.
    inline const Real& operator()(int p_i, int p_j) const
    {return m_data[p_i*m_sizeY + p_j];}

    //! Writing access to the (i,j) component value.
    //! @warning No check is made on the range of validity of the arguments.
    inline Real& operator()(int p_i, int p_j)
    {return m_data[p_i*m_sizeY + p_j];}

    //! Getters
    inline int getSizeX() const
    {return m_sizeX;}

    inline int getSizeY() const
    {return m_sizeY;}

    inline const std::valarray<Real>& matrix() const
    {return m_data;}
    //@}

    /*! @name Assignment operators */
    //@{
    //! Assignment operator: paste the element of the given LPNC_Matrix2 in this
    //! @param p_matrix is the 2D matrix that will be copied in.
    //! @return the current element
    LPNC_Matrix2& operator=(const LPNC_Matrix2& p_matrix);

    //! Fill the matrix with Real p_value.
    void assign(Real p_value);
    //@}


    /*! @name I/O */
    //@{
    //! @return returned string format : xx,yy,zz
    //std::string toString() const;

    //! @return a string formated for mathematica
    std::string toMathematica() const;

    //! @param p_string must be formatd like this: "%d,%d,%d"
    //bool fromString(const std::string& p_string);
    //@}


protected:
    /*! @name Data */
    //@{
    //! number of items on the first dimension of the matrix
    int m_sizeX;

    //! number of items on the second dimension of the matrix
    int m_sizeY;

    //! the matrix 2D
    std::valarray<Real> m_data;
    //@}
};

#endif // LPNC_MATRIX2_H
