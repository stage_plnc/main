/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_step3.h
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#ifndef LPNC_STEP3_H
#define LPNC_STEP3_H

#include <vector>
#include "lpnc_coord3.h"


template<class Real>
class LPNC_Step3
{
public:
    //! default constructor
    LPNC_Step3() {}

    //! constructor with parameters
    //! @param p_E is the exercise corresponding of the current step
    LPNC_Step3(const LPNC_Coord3 &p_E);

    // constructor by copy
    inline explicit LPNC_Step3(const LPNC_Step3& p_step)
    {(*this)= p_step;}

    /*! @name Accessors */
    //@{
    //! Exercise
    //! Reading access only
    inline const LPNC_Coord3& exercise() const {return m_E;}
    //! Writting access
    inline LPNC_Coord3& exercise()  {return m_E;}

    //! list of trial
    //! Reading access only
    inline const std::vector<bool>& trials() const {return m_S;}
    //! Writting access
    inline std::vector<bool>& trials() {return m_S;}
    //! number of elemet in the liste of trials
    inline size_t numberOfTrial() const {return m_S.size();}
    //! reading access to first element
    inline bool trial() const {return m_S.front();}



    //! append a new trial result to the list
    inline void appendNewResult(bool p_result) {m_S.push_back(p_result);}
    //@}

    /*! @name Assignment operator */
    //@{
    //! Assignment operator: paste the element of the given LPNC_Step3 in this LPNC_Step3.
        LPNC_Step3& operator=(const LPNC_Step3& p_step);
    //@}

    /*! @name I/O */
    //@{
    //! @return returned string format : "Ex,Ey,Ez;Kx,Ky,Kz;TrialSize,0,1,0,1\n"
    std::string toString() const;

    //! @param p_string must be formatd like this: "Ex,Ey,Ez;Kx,Ky,Kz;TrialSize,0,1,0,1\n"
    bool fromString(const std::string& p_string);
    //@}

protected:
    /*! @name Data */
    //@{
    //! The position in the performance space of the selected exercise
    LPNC_Coord3 m_E;

    //! The list of trial results : 'true' means success
    std::vector<bool> m_S;
    //@}
};

#endif // LPNC_STEP3_H
