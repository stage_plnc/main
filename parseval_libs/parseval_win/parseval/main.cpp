#include <iostream>
#include "parseval_srcs\lpnc_context3.h"
#include "parseval_srcs\lpnc_parseval3_MESR.h"
#include <cstdint>
#define DllExport __declspec (dllexport)
#define DEBUG true



extern "C"
{
	/*
	Creating some structure that can be serialized and casted throw C++ to C#
	*/
	struct LPNC_DataExport_s
	{
		int d01;
		int d02;
		int d03;
		uintptr_t	toFree;
	}typedef LPNC_DataExport;

	struct LPNC_Core_s
	{
		uintptr_t contextPtr;
		uintptr_t MESRPtr;
		uintptr_t toFree;

	}typedef LPNC_Core;

	DllExport LPNC_Core InitCore(float winArg, int nbDimention1, int nbDimention2, int nbDimention3)
	{
		if (DEBUG)
		{

			freopen("PARSEVAL.log", "a", stdout); // redirect cout in logfile 
			std::cout << "[PARSVAL]----------START------------ C++ v : " << __cplusplus << std::endl;
			std::cout << "[PARSVAL]- " << "Init Core : with : " << winArg << " %." << std::endl;
		}
		//Creating Core & Context
		LPNC_Core						*_exp = new LPNC_Core();
		LPNC_Context3<double>			*_context = new LPNC_Context3<double>
			(LPNC_Coord3(nbDimention1, nbDimention2, nbDimention3), 0.2, winArg);
		LPNC_Parseval3_MESR<double>		*_parseval = new LPNC_Parseval3_MESR<double>(*_context);

		_exp->contextPtr = reinterpret_cast<uintptr_t>(_context);
		_exp->MESRPtr = reinterpret_cast<uintptr_t>(_parseval);
		_exp->toFree = reinterpret_cast<uintptr_t>(_exp);
		_parseval->init();
		return *_exp;
	}


	DllExport LPNC_DataExport GetNextExercise(LPNC_Core core)
	{
		LPNC_Parseval3_MESR<double>* _parseval = (LPNC_Parseval3_MESR<double>*)(core.MESRPtr);
		LPNC_Coord3 l_exo = _parseval->getNextExercise();

		if (DEBUG)
			std::cout << "[PARSVAL]- " << "new exo =>  X : " << l_exo.getX() << ", Y : " << l_exo.getY() << ", Z : " << l_exo.getZ() << std::endl;
		LPNC_DataExport *expDT = (LPNC_DataExport*)malloc(sizeof(LPNC_DataExport));
		expDT->d01 = l_exo.getX();
		expDT->d02 = l_exo.getY();
		expDT->d03 = l_exo.getZ();
		expDT->toFree = reinterpret_cast<uintptr_t>(expDT);
		return *expDT;
	}

	DllExport void SetResult(LPNC_Core core, LPNC_DataExport E, bool isSucces)
	{
		LPNC_Parseval3_MESR<double>* _parseval = (LPNC_Parseval3_MESR<double>*)(core.MESRPtr);
		LPNC_Coord3* l_exo;

		if (DEBUG)
			std::cout << "[PARSVAL]- " << "Setting result : " << isSucces << "(" << E.d01 << "," << E.d02 << "," << E.d03 << ")" << std::endl;

		if (E.toFree == NULL)
		{
			l_exo = new LPNC_Coord3();
			l_exo->setX(E.d01);
			l_exo->setY(E.d02);
			l_exo->setZ(E.d03);
			_parseval->pushTrialResult(isSucces, *l_exo);
			_parseval->updatePerformanceEstimation();
			delete l_exo;
		}
		else
		{
			l_exo = (LPNC_Coord3*)(E.toFree);
			_parseval->pushTrialResult(isSucces, *l_exo);
			_parseval->updatePerformanceEstimation();
		}
	}


	DllExport void FreeData(LPNC_DataExport E)
	{
		free((void*)E.toFree);
		if (DEBUG)
			std::cout << "[PARSVAL]- " << "FreeData" << std::endl;
	}

	DllExport LPNC_DataExport_s GetUserPerformanceLevel(LPNC_Core core)
	{
		LPNC_DataExport_s *exp = (LPNC_DataExport*)malloc(sizeof(LPNC_DataExport));
		LPNC_Parseval3_MESR<double>* _parseval = (LPNC_Parseval3_MESR<double>*)(core.MESRPtr);
		LPNC_Coord3 coord = _parseval->getK();

		exp->d01 = coord.getX();
		exp->d02 = coord.getY();
		exp->d03 = coord.getZ();
		exp->toFree = reinterpret_cast<uintptr_t>(exp);
		if (DEBUG)
			std::cout << "[PARSVAL]- " << "Getting user perf : " + exp->d01 << std::endl;

		return *exp;
	}

	DllExport void FreeLPNC_Core(LPNC_Core core)
	{
		if (core.toFree != NULL)
		{
			delete (LPNC_Context3<double>*)core.contextPtr;
			delete (LPNC_Parseval3_MESR<double>*)core.MESRPtr;
			delete (LPNC_Core*)core.toFree;
		}
	}


}