﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class MapTest
{

	[Test]
	public void MapTestSimplePasses() {
        Map map = new Map(3, 3, 3, Vector3.zero, 50, 50);
        MapCell c = map.GetCell(2, 2, 2);
        c.IsVisible = true;
        // Assert that we can't see now
        Assert.IsFalse(map.IsCellVisible(2, 2, 3));
        Assert.IsTrue(map.IsCellVisible(2, 2, 1));

        // Assert that we can see if we remove the content
        c.IsVisible = false;
        Assert.IsTrue(map.IsCellVisible(2, 2, 3));

        // Test isCase
        Assert.IsTrue(map.IsCell(0, 0, 0));
        Assert.IsFalse(map.IsCell(-1, 0, 0));

        // Test getAdjacent
        List<MapCell> adjacentCases = map.GetAdjacentCell(new Vector3(0, 0, 0));
        Assert.IsTrue(adjacentCases.Count == 3);
        Assert.IsTrue(adjacentCases[0].MapPosition == new Vector3(0, 1, 0));
    }
}
