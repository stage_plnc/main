﻿/**********************************************************************
* Puppet2D_AngleLimitNode.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using System.Collections;

[System.Serializable]
public class AngleLimitNode
{
	public Transform Transform;
	public float min;
	public float max;
}