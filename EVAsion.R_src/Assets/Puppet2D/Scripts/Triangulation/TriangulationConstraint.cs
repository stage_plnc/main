﻿/**********************************************************************
* TriangulationConstraint.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;


namespace Poly2Tri
{
    public class Edge
    {
        protected Point2D mP = null;
        protected Point2D mQ = null;

        public Point2D EdgeStart { get { return mP; } set { mP= value;} }
        public Point2D EdgeEnd { get { return mQ; } set { mQ = value; } }

        public Edge() { mP = null; mQ = null; }
        public Edge(Point2D edgeStart, Point2D edgeEnd)
        {
            mP = edgeStart;
            mQ = edgeEnd;
        }
    }

 
    public class TriangulationConstraint : Edge
    {
        private uint mContraintCode = 0;

        public TriangulationPoint P
        {
            get { return mP as TriangulationPoint; } 
            set 
            {
                // Note:  intentionally use != instead of !Equals() because we
                // WANT to compare pointer values here rather than VertexCode values
                if (value != null && mP != value)
                {
                    mP = value;
                    CalculateContraintCode();
                }
            }
        }
        public TriangulationPoint Q
        {
            get { return mQ as TriangulationPoint; }
            set
            {
                // Note:  intentionally use != instead of !Equals() because we
                // WANT to compare pointer values here rather than VertexCode values
                if (value != null && mQ != value)
                {
                    mQ = value;
                    CalculateContraintCode();
                }
            }
        }
        public uint ConstraintCode { get { return mContraintCode; } }


        /// <summary>
        /// Give two points in any order. Will always be ordered so
        /// that q.y > p.y and q.x > p.x if same y value 
        /// </summary>
        public TriangulationConstraint(TriangulationPoint p1, TriangulationPoint p2)
        {
            mP = p1;
            mQ = p2;
            if (p1.Y > p2.Y)
            {
                mQ = p1;
                mP = p2;
            }
            else if (p1.Y == p2.Y)
            {
                if (p1.X > p2.X)
                {
                    mQ = p1;
                    mP = p2;
                }
                else if (p1.X == p2.X)
                {
                    //                logger.info( "Failed to create constraint {}={}", p1, p2 );
                    //                throw new DuplicatePointException( p1 + "=" + p2 );
                    //                return;
                }
            }
            CalculateContraintCode();
        }


        public override string ToString()
        {
            return "[P=" + P.ToString() + ", Q=" + Q.ToString() + " : {" + mContraintCode.ToString() + "}]";
        }

        
        public void CalculateContraintCode()
        {
            mContraintCode = TriangulationConstraint.CalculateContraintCode(P, Q);
        }


        public static uint CalculateContraintCode(TriangulationPoint p, TriangulationPoint q)
        {
            if (p == null || p == null)
            {
                throw new ArgumentNullException();
            }

            uint constraintCode = MathUtil.Jenkins32Hash(BitConverter.GetBytes(p.VertexCode), 0);
            constraintCode = MathUtil.Jenkins32Hash(BitConverter.GetBytes(q.VertexCode), constraintCode);

            return constraintCode;
        }

    }
}
