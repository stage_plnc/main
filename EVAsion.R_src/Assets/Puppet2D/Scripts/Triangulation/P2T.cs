/**********************************************************************
* P2T.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Diagnostics;


namespace Poly2Tri
{
    public static class P2T
    {
        private static TriangulationAlgorithm _defaultAlgorithm = TriangulationAlgorithm.DTSweep;

        public static void Triangulate(PolygonSet ps)
        {
            foreach (Polygon p in ps.Polygons)
            {
                Triangulate(p);
            }
        }


        public static void Triangulate(Polygon p)
        {
            Triangulate(_defaultAlgorithm, p);
        }

        
        public static void Triangulate(ConstrainedPointSet cps)
        {
            Triangulate(_defaultAlgorithm, cps);
        }

        public static void Triangulate(PointSet ps)
        {
            Triangulate(_defaultAlgorithm, ps);
        }

        
        public static TriangulationContext CreateContext(TriangulationAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case TriangulationAlgorithm.DTSweep:
                default:
                    return new DTSweepContext();
            }
        }

        
        public static void Triangulate(TriangulationAlgorithm algorithm, ITriangulatable t)
        {
            TriangulationContext tcx;

			#if !UNITY_METRO
            	System.Console.WriteLine("Triangulating " + t.FileName);
			#endif
            //        long time = System.nanoTime();
            tcx = CreateContext(algorithm);
            tcx.PrepareTriangulation(t);
            Triangulate(tcx);
            //        logger.info( "Triangulation of {} points [{}ms]", tcx.getPoints().size(), ( System.nanoTime() - time ) / 1e6 );
        }

        
        public static void Triangulate(TriangulationContext tcx)
        {
            switch (tcx.Algorithm)
            {
                case TriangulationAlgorithm.DTSweep:
                default:
                    DTSweep.Triangulate((DTSweepContext)tcx);
                    break;
            }
        }


        /// <summary>
        /// Will do a warmup run to let the JVM optimize the triangulation code -- or would if this were Java --MM
        /// </summary>
        public static void Warmup()
        {
#if false
			/*
			 * After a method is run 10000 times, the Hotspot compiler will compile
			 * it into native code. Periodically, the Hotspot compiler may recompile
			 * the method. After an unspecified amount of time, then the compilation
			 * system should become quiet.
			 */
			Polygon poly = PolygonGenerator.RandomCircleSweep2(50, 50000);
			TriangulationProcess process = new TriangulationProcess();
			process.triangulate(poly);
#endif
        }
    }
}
