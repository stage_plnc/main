﻿/**********************************************************************
* TriangulationDebugContext.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


namespace Poly2Tri
{
    public abstract class TriangulationDebugContext
    {
        protected TriangulationContext _tcx;

        public TriangulationDebugContext(TriangulationContext tcx)
        {
            _tcx = tcx;
        }

        public abstract void Clear();
    }
}
