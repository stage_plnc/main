﻿/**********************************************************************
* DTSweepConstraint.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
//using System.Collections.Generic;
using System.Diagnostics;
//using System.Linq;


namespace Poly2Tri
{
    public class DTSweepConstraint : TriangulationConstraint
    {
        /// <summary>
        /// Give two points in any order. Will always be ordered so
        /// that q.y > p.y and q.x > p.x if same y value 
        /// </summary>
        public DTSweepConstraint(TriangulationPoint p1, TriangulationPoint p2)
            : base(p1, p2)
        {
            Q.AddEdge(this);
        }
    }
}
