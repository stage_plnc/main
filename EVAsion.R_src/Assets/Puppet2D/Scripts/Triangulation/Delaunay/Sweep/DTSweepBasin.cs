﻿/**********************************************************************
* DTSweepBasin.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


namespace Poly2Tri
{
    public class DTSweepBasin
    {
        public AdvancingFrontNode leftNode;
        public AdvancingFrontNode bottomNode;
        public AdvancingFrontNode rightNode;
        public double width;
        public bool leftHighest;
    }
}
