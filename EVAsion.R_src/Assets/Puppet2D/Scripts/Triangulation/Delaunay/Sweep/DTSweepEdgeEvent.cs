﻿/**********************************************************************
* DTSweepEdgeEvent.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


/// Changes from the Java version
///   Turned DTSweepEdgeEvent into a value type

namespace Poly2Tri
{
    public class DTSweepEdgeEvent
    {
        public DTSweepConstraint ConstrainedEdge;
        public bool Right;
    }
}
