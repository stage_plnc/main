﻿/**********************************************************************
* PolygonSet.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


/// Changes from the Java version
///   Replaced getPolygons with attribute
/// Future possibilities
///   Replace Add(Polygon) with exposed container?
///   Replace entire class with HashSet<Polygon> ?

using System.Collections.Generic;

namespace Poly2Tri
{
    public class PolygonSet
    {
        protected List<Polygon> _polygons = new List<Polygon>();

        public PolygonSet() { }

        public PolygonSet(Polygon poly)
        {
            _polygons.Add(poly);
        }

        public void Add(Polygon p)
        {
            _polygons.Add(p);
        }

        public IEnumerable<Polygon> Polygons { get { return _polygons; } }
    }
}
