﻿/**********************************************************************
* PolygonPoint.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


/// Changes from the Java version
///   Replaced get/set Next/Previous with attributes
/// Future possibilities
///   Documentation!

namespace Poly2Tri
{
    public class PolygonPoint : TriangulationPoint
    {
        public PolygonPoint(double x, double y) : base(x, y) { }

        public PolygonPoint Next { get; set; }
        public PolygonPoint Previous { get; set; }

        public static Point2D ToBasePoint(PolygonPoint p)
        {
            return (Point2D)p;
        }

        public static TriangulationPoint ToTriangulationPoint(PolygonPoint p)
        {
            return (TriangulationPoint)p;
        }
    }
}
