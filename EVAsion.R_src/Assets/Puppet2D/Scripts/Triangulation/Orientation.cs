﻿/**********************************************************************
* Orientation.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


namespace Poly2Tri
{
    public enum Orientation
    {
        CW,
        CCW,
        Collinear
    }
}
