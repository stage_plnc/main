/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_coord3.h
* Version        :  1.0
* Author         :  Thierry PHENIX  / thierry.phenix@upmf-grenoble.fr
*                :   Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#ifndef LPNC_COORD3_H
#define LPNC_COORD3_H

#include <string>


/*! @\brief Base class for 3D coordinates point in a discret space.

    'x' correspond to first dimension, 'y' to second dimension and 'z' to the third.
    You can access to each dimension by using the [] operator.
    This class provide classical comparison operator
    and basic I/O functions to string.

 */

class LPNC_Coord3
{
public:
    // default constructor
    inline LPNC_Coord3(int p_x= 0, int p_y= 0, int p_z=0)
    {
        m_data[0] = p_x;
        m_data[1] = p_y;
        m_data[2] = p_z;
    }

    // constructor by copy
    inline LPNC_Coord3(const LPNC_Coord3& p_coord3)
    {(*this)= p_coord3;}

    /*! @name Accessors */
    //@{
    //! Reading access to the "p_i"th component value.
    //! @warning p_i value ranges from 0 to 2 included. No check is made.
    inline int operator[](int p_i) const { return m_data[p_i]; }

    //! Writing access to the "p_i"th component value.
    //! @warning p_i value ranges from 0 to 2 included. No check is made.
    inline int& operator[](int p_i) {return m_data[p_i];}

    //! current setter
    inline void setX(int p_x) {m_data[0]= p_x;}
    inline void setY(int p_y) {m_data[1]= p_y;}
    inline void setZ(int p_z) {m_data[2]= p_z;}

    //! current getter
    inline int getX() const {return m_data[0];}
    inline int getY() const {return m_data[1];}
    inline int getZ() const {return m_data[2];}
    //@}

    /*! @name Comparison and assignment operators */
    //@{
    inline bool operator==(const LPNC_Coord3& p_coord) const
    {
        return (m_data[0] == p_coord.m_data[0] &&
                m_data[1] == p_coord.m_data[1] &&
                m_data[2] == p_coord.m_data[2] );
    }

    inline bool operator!=(const LPNC_Coord3& p_coord) const
    {
        return !((*this) == p_coord);
    }

    //! Assignment operator: paste the element of the given LPNC_Coord3 in this LPNC_Coord3.
        inline LPNC_Coord3& operator=(const LPNC_Coord3& p_coord)
    {
        m_data[0] = p_coord.m_data[0];
        m_data[1] = p_coord.m_data[1];
        m_data[2] = p_coord.m_data[2];
        return *this;
    }
    //@}

    /*! @name I/O */
    //@{
    //! @return returned string format : xx,yy,zz
    std::string toString() const;

    //! @return a string formated for mathematica
    std::string toMathematica() const;

    //! @param p_string must be formatd like this: "%d,%d,%d"
    bool fromString(const std::string& p_string);
    //@}

protected:
    /*! @name Data */
    //@{
    //! three dimensional integer array
    int m_data[3];
    //@}

};

#endif // LPNC_COORD3_H
