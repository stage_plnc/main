/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_matrix3.cpp
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#include "lpnc_matrix3.h"
#include <math.h>

using namespace std;

template<class Real>
LPNC_Matrix3<Real>::LPNC_Matrix3(int p_sizeX, int p_sizeY, int p_sizeZ):
    m_sizeX(p_sizeX),
    m_sizeY(p_sizeY),
    m_sizeZ(p_sizeZ)
{
    if (m_sizeX != 0 && m_sizeY != 0 && m_sizeZ!=0) {
        m_data.resize(m_sizeX*m_sizeY*m_sizeZ);
    }
}

template<class Real>
LPNC_Matrix3<Real>::LPNC_Matrix3( int p_sizeX,
                                  int p_sizeY,
                                  int p_sizeZ,
                                  Real p_value):
    m_sizeX(p_sizeX),
    m_sizeY(p_sizeY),
    m_sizeZ(p_sizeZ)
{
    m_data.resize(m_sizeX*m_sizeY*m_sizeZ, p_value);
}

template<class Real>
LPNC_Matrix3<Real>& LPNC_Matrix3<Real>::operator=(const LPNC_Matrix3& p_matrix)
{
    m_sizeX = p_matrix.m_sizeX;
    m_sizeY = p_matrix.m_sizeY;
    m_sizeZ = p_matrix.m_sizeZ;
    m_data.resize(m_sizeX*m_sizeY*m_sizeZ);
    for (int i = 0, iMax = m_sizeX*m_sizeY*m_sizeZ; i<iMax; ++i) {
        m_data[i] = p_matrix.m_data[i];
    }
    return *this;
}

template<class Real>
void LPNC_Matrix3<Real>::assign(Real p_value)
{
    for (unsigned i=0, iMax= m_sizeX*m_sizeY*m_sizeZ; i<iMax; i++)
    {
        // at() check the bound and signals if the requested position is out of range.
        m_data[i] = p_value;
    }
}


template<class Real>
Real LPNC_Matrix3<Real>::getEntropy() const
{
    Real l_sum= 0.;
    for(auto l_it= begin(m_data); l_it!= end(m_data); ++l_it)
    {
        l_sum+= (*l_it)*log2(*l_it);
    }
    return -l_sum;
}


// this force the compiler to instanciate the three classes
// and enable the developer to code in .cpp file instead of in header file
template class LPNC_Matrix3<float>;
template class LPNC_Matrix3<double>;
template class LPNC_Matrix3<long double>;
