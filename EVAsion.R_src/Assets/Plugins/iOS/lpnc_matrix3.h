/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_matrix3.h
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#ifndef LPNC_MATRIX3_H
#define LPNC_MATRIX3_H

#include <valarray>

/*! @\brief
 *
 */

template<class Real>
class LPNC_Matrix3
{
public:
    /*! @name Constructors and Initialisation*/
    //@{
    //! Default constructor
    //! @warning if the size is not precised, the matrix is empty and no space is reserved
    LPNC_Matrix3(int p_sizeX= 0, int p_sizeY= 0, int p_sizeZ= 0);

    //! constructor with value, and no default size
    //! @param p_value is the value assign to each element of the matrix.
    LPNC_Matrix3(int p_sizeX, int p_sizeY, int p_sizeZ, Real p_value);

    //! constructor by copy
    //! @param p_matrix3 is the 3D matrix that will be copied in.
    LPNC_Matrix3(const LPNC_Matrix3& p_matrix3){(*this) = p_matrix3;}
    //@}


    /*! @name Accessors */
    //@{
    //! Reading access to the (i,j,k) component value.
    //! @warning No check is made on the range of validity of the arguments.
    inline const Real& operator()(int p_i, int p_j, int p_k) const
    {return m_data[p_i*m_sizeY*m_sizeZ + p_j*m_sizeZ + p_k];}

    //! Writing access into the (i,j,k) component value.
    //! @warning No check is made on the range of validity of the arguments.
    inline Real& operator()(int p_i, int p_j, int p_k)
    {return m_data[p_i*m_sizeY*m_sizeZ + p_j*m_sizeZ + p_k];}


    inline Real& operator()(int p_i)
    {return m_data[p_i];}

    //! Direct writing access to the array.
    inline std::valarray<Real>& matrix()
    {return m_data;}

    inline const std::valarray<Real>& matrix() const
    {return m_data;}

    //! Getters
    inline int getSizeX() const
    {return m_sizeX;}

    inline int getSizeY() const
    {return m_sizeY;}

    inline int getSizeZ() const
    {return m_sizeZ;}

    inline int size() const
    {return (int)m_sizeX*m_sizeY*m_sizeZ;}

    //@}

    /*! @name Assignment operators */
    //@{
    //! Assignment operator: paste the element of the given LPNC_Matrix2 in this
    //! @param p_matrix is the 2D matrix that will be copied in.
    //! @return the current element
    LPNC_Matrix3& operator=(const LPNC_Matrix3& p_matrix);

    //! Fill the matrix with Real p_value.
    void assign(Real p_value);
    //@}


    //!
    Real getEntropy() const;


protected:
    /*! @name Data */
    //@{
    //! number of items on the first dimension of the matrix
    int m_sizeX;

    //! number of items on the second dimension of the matrix
    int m_sizeY;

    //! number of items on the third dimension of the matrix
    int m_sizeZ;

    //! the matrix 3D
    std::valarray<Real> m_data;
    //@}
};

#endif // LPNC_MATRIX3_H
