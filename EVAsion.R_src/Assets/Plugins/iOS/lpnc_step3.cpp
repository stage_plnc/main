/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_step3.cpp
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :   Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/
#include "lpnc_step3.h"

using namespace std;

template<class Real>
LPNC_Step3<Real>::LPNC_Step3(const LPNC_Coord3 &p_E) :
    m_E(p_E)
{
}

template<class Real>
LPNC_Step3<Real>& LPNC_Step3<Real>::operator=(const LPNC_Step3<Real>& p_step)
{
    m_E     = p_step.m_E;
    m_S.assign(p_step.m_S.begin(), p_step.m_S.end());
    return *this;
}

template<class Real>
string LPNC_Step3<Real>::toString() const
{
    return 0;
}

template<class Real>
bool  LPNC_Step3<Real>::fromString(const string &p_string)
{
    return true;
}

// this force the compiler to instanciate the three classes
// and enable the developer to code in .cpp file instead of in header file
template class LPNC_Step3<float>;
template class LPNC_Step3<double>;
template class LPNC_Step3<long double>;
