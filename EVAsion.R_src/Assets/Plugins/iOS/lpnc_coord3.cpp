/*=============================================================================
* Product        :  PARSEVAL
* File           :  lpnc_coord3.cpp
* Version        :  1.0
* Author         :  Thierry PHENIX / thierry.phenix@upmf-grenoble.fr
*                :  Julien DIARD   / julien.diard@upmf-grenoble.fr

* Creation       :  01 2014
*
*=============================================================================
*     (c) Copyright 2014, Centre National de la Recherche Scientifique,
*                              all rights reserved
*=============================================================================
*/

#include "lpnc_coord3.h"

using namespace std;

string LPNC_Coord3::toString() const
{
    string l_currentSTR;
    l_currentSTR.append(to_string((m_data[0])));
    l_currentSTR.append(" ; ");
    l_currentSTR.append(to_string((m_data[1])));
    l_currentSTR.append(" ; ");
    l_currentSTR.append(to_string((m_data[2])));

    return l_currentSTR;
}

std::string LPNC_Coord3::toMathematica() const
{
    string l_currentSTR;
    l_currentSTR.append("{");
    l_currentSTR.append(to_string((m_data[0])));
    l_currentSTR.append(" ");
    l_currentSTR.append(to_string((m_data[1])));
    l_currentSTR.append(" ");
    l_currentSTR.append(to_string((m_data[2])));
    l_currentSTR.append("}");

    return l_currentSTR;
}

bool  LPNC_Coord3::fromString(const string &p_string)
{
    string l_str = string(p_string);

    return true;
}
