﻿/**********************************************************************
* PRSVL_Player.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public struct ParsevalTrace
{
    public int d01;
    public int d02;
    public int d03;
    public float success;
    public float surfaceDiff;
    public int timestamp;

    public static int GetTimestampOf(DateTime time)
    {
        DateTime date1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        int timespamp = (int)(time.Subtract(date1970).TotalSeconds) - 3600;

        return timespamp;

    }

    public ParsevalTrace(LPNC_DataExport parsevalValues, float success, float surfaceDiff, DateTime time)
    {
        this.d01 = parsevalValues.d01;
        this.d02 = parsevalValues.d02;
        this.d03 = parsevalValues.d03;
        this.success = success;
        this.timestamp = GetTimestampOf(time);
        this.surfaceDiff = surfaceDiff;
    }

    public LPNC_DataExport ToPNLC_DataExport()
    {
        LPNC_DataExport lpnc_dataExport = new LPNC_DataExport()
        {
            d01 = this.d01,
            d02 = this.d02,
            d03 = this.d03, 
            toFree = System.UIntPtr.Zero
            
        };
        return lpnc_dataExport;
    }

    public new string ToString()
    {
        return string.Format("d1:{0}; d2:{1}, d3:{2}, succ = {3}", d01, d02, d03, success);
    }
};

[SerializeField]
public class PRSVL_Player
{

    /*
     *  This class is self aware/handling of saves and past experiencies since it loading a profil by name
     */

    private string _name;
    private string _groupId;
    private string _filename;

    private Dictionary<string, List<ParsevalTrace>> _traces;
    
    public Dictionary<string, PRSVL_Ctrl> _parsevalInstances;

    private string _currentGame;
    public LPNC_DataExport LastExercice;

    public string Name
    {
        get { return _name; }
    }

    public PRSVL_Player(string groupId, string playerId, float UPL)
    {
        _parsevalInstances = new Dictionary<string, PRSVL_Ctrl>();

        _name = string.Format("Group{0}_Id{1}", groupId, playerId);
        _filename = _name + "_Save.json";

        _currentGame = string.Empty;

        LoadFile(_filename);
    }

    public void LoadGame(string game, float UPL)
    {
        _parsevalInstances[game] = new PRSVL_Ctrl(UPL);
        _currentGame = game;

        if (_traces.ContainsKey(game))
        {
            foreach (ParsevalTrace trace in _traces[game])
            {
                _parsevalInstances[game].SetResult(trace);
            }
        }
    }

    public void AddResult(string game, ParsevalTrace trace)
    {
        if (_parsevalInstances.ContainsKey(game))
        {
            _parsevalInstances[game].SetResult(trace);

            _traces[game].Add(trace);

            SaveProfile();
        }
        else
        {
            Debug.LogError("The " + game + " is not defined in the parsevalInstances dictionary");
        }
    }


    public LPNC_DataExport GetPerformanceLevel()
    {
        if (_currentGame == string.Empty)
        {
            Debug.LogError("GetPerformanceLevel in PRSVL_Player : no game defined");
        }

        return _parsevalInstances[_currentGame].GetUserPerformanceLevel();
    }

    public void AddCurrentResult(LPNC_DataExport parsevalValues, string game, float success)
    {
        if (_parsevalInstances.ContainsKey(game))
        {
            LPNC_DataExport perf = _parsevalInstances[game].GetUserPerformanceLevel();
            float surfaceDiff = perf.d01 * 100 + perf.d02 * 10 + perf.d03;
            ParsevalTrace trace = new ParsevalTrace(parsevalValues, success, surfaceDiff, DateTime.Now);

            _parsevalInstances[game].SetResult(trace);
            _traces[game].Add(trace);

            SaveProfile();

            if (DataSender.Instance != null)
            {

                Debug.Log("[AddNewEvent] success ? " + (success == 1.0f) + ". K = " + surfaceDiff);
                Debug.Log("d01 = " + parsevalValues.d01 + ", d02 = " + parsevalValues.d02 + ", d03 = " + parsevalValues.d03);

                DataSender.Instance.AddNewEvent(
                    parsevalValues,
                    surfaceDiff,
                    success == 1.0f);
            }
        }
        else
        {
            Debug.LogError("The " + game + " is not defined in the parsevalInstances dictionary");
        }
    }

    public LPNC_DataExport GetExercise()
    {
        if (_currentGame == string.Empty)
        {
            Debug.LogError("GetExercise in PRSVL_Player : no game defined");
        }

        LastExercice = _parsevalInstances[_currentGame].GetExercise();
        return LastExercice;
    }

    public void CleanParsevalInstances()
    {
        foreach (PRSVL_Ctrl parsevalInstance in _parsevalInstances.Values)
        {
            parsevalInstance.Clean();
        }
    }

    #region loading & Saving

    public bool LoadFile(string filename)
    {
        try
        {
            string path = string.Format("{0}/Players/{1}", Application.persistentDataPath, filename);
            string txtFile = File.ReadAllText(path);

            if (txtFile.Length > 0)
            {
                LoadProfileFromJSON(new JSONObject(txtFile));

                return true;
            }
        }
        catch (System.Exception e)
        {
            _traces = new Dictionary<string, List<ParsevalTrace>>();

            foreach (string Game in PRSVL_PlayerMgnt.Instance.GamesNames)
                _traces[Game] = new List<ParsevalTrace>();

            Debug.Log("No save for : " + Name + ", sys : " + e.Message);

            ElapsedTimeMgmt.Instance.Reset();

            return false;
        }

        return false;
    }

    private void LoadProfileFromJSON(JSONObject JSONSave)
    {
        _name = JSONSave.GetField("name").str;
        _filename = JSONSave.GetField("filename").str;

        JSONObject tracesList = JSONSave.GetField("traces");
        JSONObject elapsedTimeList = JSONSave.GetField("elapsedTimes");

        _traces = new Dictionary<string, List<ParsevalTrace>>();

        foreach (string Game in PRSVL_PlayerMgnt.Instance.GamesNames)
            _traces[Game] = new List<ParsevalTrace>();

        if (tracesList.ToString() != "null")
        {
            foreach (string game in Constants.GameNamesId)
            {
                // It can happen if the player hasnt played this game yet
                if (tracesList.HasField(game))
                {
                    List<JSONObject> tracesGame = tracesList.GetField(game).list;

                    Debug.Log("So the game " + game + " has " + tracesGame.Count + " traces");

                    for (int idx = 0; idx < tracesGame.Count; idx++)
                    {
                        ParsevalTrace tmpT = new ParsevalTrace()
                        {
                            d01 = (int)tracesGame[idx].GetField("d01").f,
                            d02 = (int)tracesGame[idx].GetField("d02").f,
                            d03 = (int)tracesGame[idx].GetField("d03").f,
                            success = tracesGame[idx].GetField("success").f,
                            surfaceDiff = tracesGame[idx].GetField("surfaceDiff").f, 
                            timestamp = (int) tracesGame[idx].GetField("time").f
                        };

                        _traces[game].Add(tmpT);
                    }

                    Debug.Log("After parsing, we have " + _traces[game].Count);
                }
            }
        }

        if (elapsedTimeList != null)
        {
            ElapsedTimeMgmt.Instance.SetElapsedTimes(elapsedTimeList);
        }
    }

    public void SaveProfile()
    {
        JSONObject JSONSave = new JSONObject();
        JSONSave.AddField("name", Name);
        JSONSave.AddField("filename", _filename);
        JSONSave.AddField("traces", GetTraceJSON());
        JSONSave.AddField("elapsedTimes", ElapsedTimeMgmt.Instance.GetElapsedTimesJSON());

        string directoryPath = string.Format("{0}/Players", Application.persistentDataPath);
        string path = string.Format("{0}/{1}", directoryPath, _filename);

        if (Directory.Exists(directoryPath) == false)
        {
            Directory.CreateDirectory(directoryPath);
        }

        File.WriteAllText(path, JSONSave.ToString(true));
    }

    public JSONObject GetTraceJSON()
    {
        JSONObject traces = new JSONObject();
        List<string> filter = new List<string>();

        foreach (string Game in Constants.GameNamesId)
        {
            JSONObject GameTrace = new JSONObject();
            int idxBeginning = 0;
            int maxSaves = 0;

            // If there is no trace, we dont bother to write anything (just null in the file text)
            if (_traces[Game].Count > 0)
            {

                maxSaves = DifficultyMgmt.Instance.MaxSaves(Game);

                if (_traces[Game].Count > maxSaves)
                {
                    idxBeginning = _traces[Game].Count - maxSaves;
                }

                for (int idx = idxBeginning; idx < _traces[Game].Count; idx++)
                {
                    JSONObject tmpJSO = new JSONObject();
                    tmpJSO.AddField("d01", _traces[Game][idx].d01);
                    tmpJSO.AddField("d02", _traces[Game][idx].d02);
                    tmpJSO.AddField("d03", _traces[Game][idx].d03);
                    tmpJSO.AddField("success", _traces[Game][idx].success);
                    tmpJSO.AddField("surfaceDiff", _traces[Game][idx].surfaceDiff);
                    tmpJSO.AddField("time", _traces[Game][idx].timestamp);

                    //avoiding duplicate traces
                    //if (!filter.Contains(_traces[Game][idx].timestamp.ToString()))
                    //{
                    //    filter.Add(_traces[Game][idx].timestamp.ToString());
                    //    GameTrace.Add(tmpJSO);
                    //}
                    //else
                    //    Debug.LogWarning("PRSLV_Player : removing duplicate trace");

                    GameTrace.Add(tmpJSO);
                }
                traces.AddField(Game, GameTrace);
            }
        }
        return traces;
    }
    #endregion
}
