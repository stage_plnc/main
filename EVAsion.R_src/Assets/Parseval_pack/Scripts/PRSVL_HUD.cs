﻿/**********************************************************************
* PRSVL_HUD.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PRSVL_HUD : MonoBehaviour
{
    public GameObject OpenPanelButton;
    public UITweener PanelTween;

    public UILabel UPLLabel;
    public UILabel VersionLabel;
    public UILabel K1Label;
    public UILabel K2Label;
    public UILabel K3Label;

    [Header("Parseval Graph values")]
    public UIPanel GraphPanel;
    public UIGrid GraphGrid;

    public GameObject PointPrefab;
    public GameObject CellPrefab;

    #region Private
    private bool _isPanelOpen = false;

    private int _cycleCounter = 0;

    private Vector2[] _previousPointPosition;
    private Vector2[] _newPointPosition;

    private Color[] _colorsPerDimension = { Color.yellow, Color.magenta, Color.blue };

    private UISprite[,] _cells;
    private int _sizeCell;

    private List<GameObject> _lines;
    #endregion

    //public void Awake()
    //{
    //    _previousPointPosition = new Vector2[PRSVL_Ctrl.PARSEVAL_NBR_DIMENSIONS];
    //    _newPointPosition = new Vector2[PRSVL_Ctrl.PARSEVAL_NBR_DIMENSIONS];

    //    _lines = new List<GameObject>();
    //}

    //// Use this for initialization
    //void Start()
    //{
    //    UIEventListener.Get(OpenPanelButton).onClick = TooglePanel;

    //    CreateGrid();
    //    VersionLabel.text = "v." + Application.version;
    //}

    //public void ClearAll()
    //{
    //    // Actually, the gameobject is not destroyed, but this is called when we're changing the scene. So, we reset the all the values
    //    for (int i = 0; i < _lines.Count; i++)
    //    {
    //        Destroy(_lines[i]);
    //    }

    //    _lines.Clear();

    //    for (int i = 0; i < PRSVL_Ctrl.PARSEVAL_DEPTH; i++)
    //    {
    //        for (int j = 0; j < _cycleCounter; j++)
    //        {
    //            _cells[i, j].color = Color.white;
    //        }
    //    }

    //    GraphPanel.transform.localPosition = Vector2.zero;

    //    _cycleCounter = 0;
    //}

    //public void CreateGrid()
    //{
    //    _sizeCell = (int)(GraphPanel.height / PRSVL_Ctrl.PARSEVAL_DEPTH);

    //    GraphGrid.cellHeight = GraphGrid.cellWidth = _sizeCell;
    //    GraphGrid.maxPerLine = Constants.NbrTotalCycles;

    //    _cells = new UISprite[PRSVL_Ctrl.PARSEVAL_DEPTH, GraphGrid.maxPerLine];

    //    for (int i = 0; i < PRSVL_Ctrl.PARSEVAL_DEPTH; i++)
    //    {
    //        for (int j = 0; j < GraphGrid.maxPerLine; j++)
    //        {
    //            GameObject tile = NGUITools.AddChild(GraphGrid.gameObject, CellPrefab);

    //            UISprite tileSprite = tile.GetComponent<UISprite>();
    //            tileSprite.width = tileSprite.height = (int)_sizeCell;

    //            _cells[i, j] = tileSprite;
    //        }
    //    }

    //    GraphGrid.Reposition();
    //}


    //void TooglePanel(GameObject go = null)
    //{
    //    if (!_isPanelOpen)
    //    {
    //        PanelTween.PlayForward();
    //        _isPanelOpen = true;
    //    }
    //    else
    //    {
    //        PanelTween.Play(false);
    //        _isPanelOpen = false;
    //    }
    //}

    //public void UpdateDatas(int diff1, int diff2, int diff3, float upl, LPNC_DataExport k)
    //{
    //    int offsetPanelX = Mathf.Max((int)((_cycleCounter * _sizeCell) + GraphGrid.transform.localPosition.x), 0);

    //    UpdateGraph(new int[3] { diff1, diff2, diff3 });

    //    GraphPanel.clipOffset = new Vector2(offsetPanelX, GraphPanel.clipOffset.y);
    //    GraphPanel.transform.localPosition = new Vector3(-offsetPanelX, GraphPanel.transform.localPosition.y, 0.0f);

    //    UPLLabel.text = String.Format("UPL : {0} %", (upl * 100.0f).ToString("0.00"));
    //    K1Label.text = "K(1) : "+ k.d01;
    //    K2Label.text = "K(2) : " + k.d02;
    //    K3Label.text = "K(3) : " + k.d03;
    //    _cycleCounter++;
    //}

    //public void UpdateGridSuccess(bool success)
    //{
    //    if (_cycleCounter >= 1)
    //    {
    //        for (int i = 0; i < PRSVL_Ctrl.PARSEVAL_DEPTH; i++)
    //        {
    //            _cells[i, _cycleCounter - 1].color = success == true ? new Color(0.0f, 1.0f, 0.0f, 0.75f) : new Color(1.0f, 0.0f, 0.0f, 0.75f);
    //        }
    //    }
    //}

    //private void UpdateGraph(int[] values)
    //{
    //    for (int i = 0; i < PRSVL_Ctrl.PARSEVAL_NBR_DIMENSIONS; i++)
    //    {
    //        _newPointPosition[i] = new Vector2(_cycleCounter * _sizeCell, values[i] * _sizeCell);

    //        if (_cycleCounter != 0)
    //        {
    //            GameObject Line = NGUITools.AddChild(GraphGrid.gameObject, PointPrefab);

    //            float oppose = _newPointPosition[i].y - _previousPointPosition[i].y;
    //            float adj = _newPointPosition[i].x - _previousPointPosition[i].x;

    //            // Draw line according to previous point
    //            float angle = Mathf.Atan(oppose / adj) * Mathf.Rad2Deg;

    //            // We're adding a little shift (6pixels to the top) to clearly see all the lines
    //            Line.transform.localPosition = _previousPointPosition[i] + Vector2.up * 6 * i - new Vector2(0.0f, 320.0f);
    //            Line.transform.Rotate(0.0f, 0.0f, angle);

    //            UISprite sprite = Line.GetComponent<UISprite>();
    //            sprite.color = _colorsPerDimension[i];

    //            sprite.width = (int)Mathf.Sqrt((float)(oppose * oppose + adj * adj));

    //            _lines.Add(Line);

    //        }

    //        _previousPointPosition[i] = _newPointPosition[i];
    //    }
    //}


}
