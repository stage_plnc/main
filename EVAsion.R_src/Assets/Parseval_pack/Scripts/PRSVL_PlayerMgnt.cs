﻿/**********************************************************************
* PRSVL_PlayerMgnt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PRSVL_PlayerMgnt : MonoBehaviour
{
    private static PRSVL_PlayerMgnt _instance;
    public static PRSVL_PlayerMgnt Instance
    {
        get { return _instance; }
    }

    public PRSVL_Player CurrentPlayer
    {
        get { return _currentPlayer; }
    }
    private float _currentUPL = 0.75f;
    public float UPL
    {
        get
        {
            return _currentUPL;
        }
        set
        {
            _currentUPL = value;
        }
    }

    public bool IsPlayerLoaded
    {
        get { return _currentPlayer != null; }
    }

    public string[] GamesNames;

    private List<PRSVL_Player> _players;
    private PRSVL_Player _currentPlayer = null;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            _players = new List<PRSVL_Player>();

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void OnApplicationQuit()
    {
        foreach (PRSVL_Player player in _players)
        {
            player.CleanParsevalInstances();
        }
    }

    // Instanciating PRSVL_Player will search for saves with PlayerName
    public void LoadPlayer(string groupId, string playerId)
    {
        PRSVL_Player player = _players.Find(x => x.Name == playerId);

        if (player == null)
        {
            PRSVL_Player newP = new PRSVL_Player(groupId, playerId, _currentUPL);
            _currentPlayer = newP;

            _players.Add(newP);
        }
        else
        {
            _currentPlayer = player;
        }
    }
}
