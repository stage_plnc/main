﻿/**********************************************************************
* PRSVL_Ctrl.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using System;

/*
 * 
 * This class communicate with C++ .dll or .so containing Parseval Algorithm
 * 
*/

public struct LPNC_DataExport
{
    public int d01;
    public int d02;
    public int d03;
    public System.UIntPtr toFree;
};

public struct LPNC_Core
{
    public System.UIntPtr contextPtr;
    public System.UIntPtr MESRPtr;
    public System.UIntPtr toFree;

};

public class PRSVL_Ctrl
{

#if UNITY_IOS
    // Importing function from DLL
    [DllImport ("__Internal")]
    private static extern LPNC_Core InitCore(float val, int nbDimention1, int nbDimention2, int nbDimention3);
    [DllImport ("__Internal")]
    private static extern LPNC_DataExport GetNextExercise(LPNC_Core core);
    [DllImport ("__Internal")]
    private static extern void FreeData(LPNC_DataExport E);
    [DllImport ("__Internal")]
    private static extern void SetResult(LPNC_Core core, LPNC_DataExport E, bool isSucces);
    [DllImport("__Internal")]
    private static extern LPNC_DataExport GetUserPerformanceLevel(LPNC_Core core);
    [DllImport("__Internal")]
    private static extern void FreeLPNC_Core(LPNC_Core core);

#else
    // Importing function from DLL
    [DllImport("parseval")]
    private static extern LPNC_Core InitCore(float val, int nbDimention1, int nbDimention2, int nbDimention3);
    [DllImport("parseval")]
    private static extern LPNC_DataExport GetNextExercise(LPNC_Core core);
    [DllImport("parseval")]
    private static extern void FreeData(LPNC_DataExport E);
    [DllImport("parseval")]
    private static extern void SetResult(LPNC_Core core, LPNC_DataExport E, bool isSucces);
    [DllImport("parseval")]
    private static extern LPNC_DataExport GetUserPerformanceLevel(LPNC_Core core);
    [DllImport("parseval")]
    private static extern void FreeLPNC_Core(LPNC_Core core);
#endif

    public static int PARSEVAL_NBR_DIMENSIONS = 3;
    public static int PARSEVAL_DEPTH = 6;

    private LPNC_Core _core;
    private float _currentDifficulty;
    private LPNC_DataExport _currentExercise;

    public PRSVL_Ctrl(float difficulty)
    {
        _currentExercise.toFree = System.UIntPtr.Zero;

        _currentDifficulty = difficulty;
        _core = InitCore(_currentDifficulty, PARSEVAL_DEPTH, PARSEVAL_DEPTH, PARSEVAL_DEPTH);
    }

    public LPNC_DataExport GetExercise()
    {
        if (_currentExercise.toFree != System.UIntPtr.Zero)
        {
            FreeData(_currentExercise);
        };

        _currentExercise = GetNextExercise(_core);

        return _currentExercise;
    }

    public void SetCurrentResult(float result)
    {
        //Its ready to be change with variable succes if parseval evolve
        if (result > _currentDifficulty)
        {
            SetResult(_core, _currentExercise, true);
        }
        else
        {
            SetResult(_core, _currentExercise, false);
        }
    }

    public void SetResult(ParsevalTrace parsevalTrace)
    {
        //Its ready to be change with variable succes if parseval evolve
        if (parsevalTrace.success > _currentDifficulty)
        {
            SetResult(_core, parsevalTrace.ToPNLC_DataExport(), true);
        } 
        else
        {
            SetResult(_core, parsevalTrace.ToPNLC_DataExport(), false);
        }
    }


    public void ResetCore(float difficulty)
    {
        if (_currentExercise.toFree != System.UIntPtr.Zero)
        {
            FreeData(_currentExercise);
        };
        _currentExercise.toFree = System.UIntPtr.Zero;
        _currentDifficulty = 0.0f;
        _core = InitCore(_currentDifficulty, PARSEVAL_DEPTH, PARSEVAL_DEPTH, PARSEVAL_DEPTH);
    }

    public LPNC_DataExport GetUserPerformanceLevel()
    {
        LPNC_DataExport tmpDt = GetUserPerformanceLevel(_core);
        LPNC_DataExport retDT = new LPNC_DataExport();
        retDT.d01 = tmpDt.d01;
        retDT.d02 = tmpDt.d02;
        retDT.d03 = tmpDt.d03;
        retDT.toFree = System.UIntPtr.Zero;

        FreeData(tmpDt);

        return retDT;
    }

    public void Clean()
    {
        FreeLPNC_Core(_core);
        _core.contextPtr = UIntPtr.Zero;
        _core.MESRPtr = UIntPtr.Zero;
        _core.toFree =  UIntPtr.Zero;
    }
}
