﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLetters : MonoBehaviour
{
    public int DestinationX;

    private UITweener[] _tweens;

    private void Awake()
    {
        _tweens = GetComponents<UITweener>();
    }

    void Start ()
    {
        TweenPosition tweenPosition = GetComponent<TweenPosition>();
        tweenPosition.to = new Vector3(UnityEngine.Random.Range (-DestinationX, DestinationX), tweenPosition.to.y);
    }

    public void PlayTweens()
    {
        gameObject.SetActive(true);
        foreach (UITweener tween in _tweens)
        {
            tween.PlayForward();
        }
    }
}
