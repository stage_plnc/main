﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempPlayTweens : MonoBehaviour
{
    public GameObject Label;

	// Use this for initialization
	void Start ()
    {
        UIEventListener.Get(gameObject).onClick = PlayTweens;
		
	}

    private void PlayTweens(GameObject go)
    {
        UITweener[] tweens = Label.GetComponents<UITweener>();
        Label.SetActive(true);

        foreach (UITweener tween in tweens)
        {
            tween.PlayForward();
        }
    }
}
