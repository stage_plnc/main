﻿/**********************************************************************
* IslandsTranslation.cs
* Created at 11-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandsTranslation : MonoBehaviour
{
    public int PositionY;
    public int StartX;
    public int EndX;
    public float DurationMovement;
}
