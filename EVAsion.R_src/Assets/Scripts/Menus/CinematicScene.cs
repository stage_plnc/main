﻿
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

// Double click
// http://answers.unity3d.com/questions/957874/how-to-detect-double-click-in-c.html

public class CinematicScene : MonoBehaviour
{
    public AudioSource AudioSource;
    public VideoPlayer TheVideoPlayer;
    public string SceneToLoadName;

    public float MaxTimeToClick = 0.60f;
    public float MinTimeToClick = 0.05f;

    private float _minCurrentTime;
    private float _maxCurrentTime;

    public GameObject PassButton;

    private bool _isLoading = false;

    void Awake()
    {
        TheVideoPlayer.loopPointReached += OnVideoOver;

        AudioSource.volume = PlayerPrefs.GetFloat("MusicVolume", 1.0f);

        _minCurrentTime = MinTimeToClick;
        _maxCurrentTime = MaxTimeToClick;

        Time.timeScale = 1.0f;

        UIEventListener.Get(PassButton).onClick = LoadNextScene;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (DoubleClick())
        //    {
        //        LoadNextScene();
        //    }
        //}
    }

    private void OnVideoOver(VideoPlayer vp)
    {
        LoadNextScene();
    }

    private void LoadNextScene(GameObject go = null)
    {
        if (_isLoading == false)
        {
            _isLoading = true;
            SceneManager.LoadScene(SceneToLoadName);
        }
    }

    public bool DoubleClick()
    {
        if (Time.time >= _minCurrentTime && Time.time <= _maxCurrentTime)
        {
            return true;
        }

        _minCurrentTime = Time.time + MinTimeToClick;
        _maxCurrentTime = Time.time + MaxTimeToClick;

        return false;
    }
}