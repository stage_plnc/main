﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InactivityPopup : MonoBehaviour
{
    [Header("Front circle")]
    public GameObject FrontCircle;

    [Header("Tween scales")]
    public TweenScale[] TweenScalesApparition;
    public TweenScale[] TweenScalesDisparition;

    [Header("Z letters Tween alpha")]
    public TweenAlpha[] ZLettersTweenAlpha;

    public bool Visible = false;

    public void Awake()
    {
        
    }

    public void StartTween()
    {
        foreach (TweenScale tweenScale in TweenScalesApparition)
        {
            tweenScale.ResetToBeginning();
            tweenScale.PlayForward();
        }

        Visible = true;
    }

    public void ShowLetters()
    {
        foreach (TweenAlpha tweenAlpha in ZLettersTweenAlpha)
        {
            tweenAlpha.ResetToBeginning();
        }

        // This will active the next ones
        ZLettersTweenAlpha[0].PlayForward();
    }

    private void HildeAllContent()
    {
        foreach (TweenScale tweenScale in TweenScalesDisparition)
        {
            tweenScale.ResetToBeginning();
            tweenScale.PlayForward();
        }

        Visible = false;
    }

    public void Hide()
    {
        HildeAllContent();
    }
}
