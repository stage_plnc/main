﻿/**********************************************************************
* CinematicScene.cs
* Created at 16-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
using System.Collections;

// Double click
// http://answers.unity3d.com/questions/957874/how-to-detect-double-click-in-c.html

public class TutorialScene : MonoBehaviour
{
    [System.Serializable]
    public struct TutoVideo
    {
        public string GameName;
        public VideoClip Video;
    };

    public TutoVideo[] TutosVideos;

    public AudioSource AudioSource;
    public VideoPlayer TheVideoPlayer;
    public string SceneToLoadName;

    public float MaxTimeToClick = 0.60f;
    public float MinTimeToClick = 0.05f;

    [Header("Button")]
    public GameObject PassButton;

    private float _minCurrentTime;
    private float _maxCurrentTime;

    private int _indexGame = -1;
    private bool _isLoading = false;
    private string _nextSceneName;

    void Awake()
    {
        TheVideoPlayer.loopPointReached += OnVideoOver;

        AudioSource.volume = PlayerPrefs.GetFloat("MusicVolume", 1.0f);
    }

    // Thanks Internet! Didn't keep the link, sorry
    void Start()
    {
        _nextSceneName = PlayerPrefs.GetString("NextSceneName", string.Empty);

        if (_nextSceneName == string.Empty)
        {
            _nextSceneName = "MainMenu";
        }
        else
        {
            _indexGame = Array.FindIndex(TutosVideos, x => x.GameName == _nextSceneName);

            if (_indexGame != -1)
            {
                if (TutosVideos[_indexGame].Video != null)
                {
                    TheVideoPlayer.clip = TutosVideos[_indexGame].Video;

                    TheVideoPlayer.Play();
                }
                else
                {
                    LoadNextScene();
                }
            }
            else
            {
                Debug.LogError("Didnt found " + _nextSceneName);
            }
        }

        UIEventListener.Get(PassButton).onClick = LoadNextScene;
    }

    private IEnumerator LoadNextSceneCoroutine(string nextSceneName)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(nextSceneName);
        async.allowSceneActivation = true;

        while (!async.isDone)
        {
            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (DoubleClick())
        //    {
        //        TheVideoPlayer.Pause();

        //        LoadNextScene();
        //    }
        //}
    }

    private void OnVideoOver(VideoPlayer vp)
    {
        TheVideoPlayer.Stop();
        TheVideoPlayer.Play();
    }

    private void LoadNextScene(GameObject go = null)
    {
        if (_isLoading == false)
        {
            _isLoading = true;
            StartCoroutine(LoadNextSceneCoroutine(_nextSceneName));
        }
        // SceneManager.LoadScene(SceneToLoadName);
    }

    public bool DoubleClick()
    {
        if (Time.time >= _minCurrentTime && Time.time <= _maxCurrentTime)
        {
            return true;
        }

        _minCurrentTime = Time.time + MinTimeToClick; _maxCurrentTime = Time.time + MaxTimeToClick;

        return false;
    }
}