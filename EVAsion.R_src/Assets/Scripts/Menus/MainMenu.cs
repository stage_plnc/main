﻿/**********************************************************************
* MainMenu.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

// This class manages the islands buttons
// IMPORTANT : The position 0 is for the BOTTOM plan of the sea, the position 3 is for the TOP plan of the sea
public class MainMenu : MonoBehaviour
{
    public bool AreIslandsPlacedByDefault = true;
    public bool AreIslandsPlacesByBand = false;

    public AudioClip Music;

    // this struct need to be Serializable because we want it to show on the inspector
    [System.Serializable]
    public struct Islandbutton
    {
        public GameObject Button;
        public string SceneName;


        [Range(0, 3)]
        public int Position;
    }

    [Header("Islands")]
    // An array with button-GameName correspondence
    public Islandbutton[] IslandsButton;
    public IslandsTranslation[] IslandsTranslation;
    public float[] ScaleByPlan;

    [Header("Panels")]
    public UIPanel[] Panels;

    [Header("Options")]
    public GameObject OpenMenuButton;
    public OptionsPopup OptionsPopup;

    [Header("Other")]
    public GameObject FirstIslandParticleSystem;
    public GameObject BlockCollisionWidget;
    public TweenAlpha ShowMenuTweenAlpha;
    public TweenAlpha HideMenuTweenAlpha;

    void Start()
    {
        ElapsedTimeMgmt.Instance.StartSession();

        if (PlayerPrefs.HasKey("learnerId"))
        {
            PRSVL_PlayerMgnt.Instance.LoadPlayer(PlayerPrefs.GetInt("groupId").ToString(), PlayerPrefs.GetInt("learnerId").ToString());
        }
        else
        {
            Debug.LogError("Didnt use connexion scene. Loading FakePlayer");
            PRSVL_PlayerMgnt.Instance.LoadPlayer("Fake", "Player");
        }

        UIEventListener.Get(OpenMenuButton).onClick = OpenMenu;
        UIEventListener.Get(OptionsPopup.CloseOptionsButton).onClick = CloseMenu;

        foreach (Islandbutton GSH in IslandsButton)
            UIEventListener.Get(GSH.Button).onClick = ButtonCB;

        if (AreIslandsPlacedByDefault)
        {
            SetIslandsOrder();
        }
        else if (AreIslandsPlacesByBand)
        {
            SetIslandsOrder(null, true);
        }
        else
        {
            //int[] order = { 0, 1, 2, 3 };

            //order = RandomShuffling.Shuffle<int>(order);

            int[] order = ElapsedTimeMgmt.Instance.LessPlayedToMostPlayedGames();

            SetIslandsOrder(order);
        }

        SoundMgmt.Instance.PlayMusic(Music, true);
    }

    private void CloseMenu(GameObject go)
    {
        AuthorizeInput();
        OptionsPopup.HidePopup();
    }

    private void OpenMenu(GameObject go)
    {
        if (OptionsPopup.Visible == false)
        {
            ForbidInput();
            OptionsPopup.ShowPopup();
        }
    }

    private void ButtonCB(GameObject go)
    {
        foreach (Islandbutton GSH in IslandsButton)
        {
            if (GSH.Button == go)
            {
                PlayerPrefs.SetString("gamePlayed", GSH.SceneName);
                PlayerPrefs.SetString("NextSceneName", GSH.SceneName);

                HideMenuTweenAlpha.SetOnFinished(LoadLoadingScreen);
                HideMenuTweenAlpha.PlayForward();

            }
        }
    }

    public void LoadLoadingScreen()
    {
        SceneManager.LoadScene("TutorialScene", LoadSceneMode.Single);
    }

    public static int GetInitialGameOrder(string gameName)
    {
        int index = Array.FindIndex(Constants.GameNames, x => x == gameName);

        if (index == -1)
        {
            Debug.LogError("Error : the game name [" + gameName + "] doesnt match the game names inside Constants.cs");

            index = 0;
        }

        return index;
    }

    // TODO Maybe a method "GetIslandsOrder", returns a pair<gameName, order>, according to the method complexity
    // If we do not pass an array as parameter, we're choosing the initial order of the islands (see GetInitialGameOrder below)
    private void SetIslandsOrder(int[] arrayOrder = null, bool useInspectorValues = false)
    {
        for (int i = 0; i < IslandsButton.Length; i++)
        {
            if (arrayOrder == null)
            {
                if (!useInspectorValues)
                    IslandsButton[i].Position = GetInitialGameOrder(IslandsButton[i].SceneName);
            }
            else
            {
                IslandsButton[i].Position = arrayOrder[i];
            }

            IslandsTranslation islandTranslation = IslandsTranslation[IslandsButton[i].Position];
            UIWidget islandWidget = IslandsButton[i].Button.GetComponent<UIWidget>();
            islandWidget.depth = 22 + (3 - IslandsButton[i].Position) * 10;

            if (IslandsButton[i].Position == 0)
            {
                islandWidget.pivot = UIWidget.Pivot.BottomLeft;
                // UGLY
                BoxCollider boxCollider = IslandsButton[i].Button.GetComponent<BoxCollider>();
                boxCollider.center = new Vector3(islandWidget.width / 2.0f, boxCollider.center.y, 0.0f);

                // Ugly HACK
                if (IslandsButton[i].SceneName == "MatchMaker")
                {
                    boxCollider.size = new Vector3(boxCollider.size.x, 551.5f);
                }

            }
            else if (IslandsButton[i].Position == 1)
            {
                islandWidget.pivot = UIWidget.Pivot.BottomRight;
                // UGLY
                BoxCollider boxCollider = IslandsButton[i].Button.GetComponent<BoxCollider>();
                boxCollider.center = new Vector3(-islandWidget.width / 2.0f, boxCollider.center.y, 0.0f);
            }


            IslandsButton[i].Button.transform.localPosition = new Vector3(islandTranslation.StartX, islandTranslation.PositionY);
            IslandsButton[i].Button.transform.localScale = Vector3.one * ScaleByPlan[IslandsButton[i].Position];

            UIButtonScale uiButtonScale = IslandsButton[i].Button.GetComponent<UIButtonScale>();
            uiButtonScale.pressed = Vector3.one * ScaleByPlan[IslandsButton[i].Position] * 1.25f;
            uiButtonScale.hover = IslandsButton[i].Button.transform.localScale;

            TweenPosition tweenPosition = IslandsButton[i].Button.GetComponent<TweenPosition>();

            tweenPosition.from = new Vector3(islandTranslation.StartX, islandTranslation.PositionY, 0.0f);
            tweenPosition.to = new Vector3(islandTranslation.EndX, islandTranslation.PositionY, 0.0f);
            tweenPosition.duration = islandTranslation.DurationMovement;

            if (i == 0)
            {
                tweenPosition.SetOnFinished(IslandFinishedMoving);
            }

            tweenPosition.PlayForward();

            IslandsButton[i].Button.transform.SetParent(Panels[IslandsButton[i].Position].transform, false);
            IslandsButton[i].Button.GetComponent<UIWidget>().ParentHasChanged();
        }

    }

    public void IslandFinishedMoving()
    {
        AuthorizeInput();
        FirstIslandParticleSystem.SetActive(true);
    }

    public void AuthorizeInput()
    {
        BlockCollisionWidget.SetActive(false);
    }

    public void ForbidInput()
    {
        BlockCollisionWidget.SetActive(true);
    }

    public void DestroySaveFiles(GameObject go)
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath);

        foreach (FileInfo fileInfo in directoryInfo.GetFiles())
        {
            if (fileInfo.Name.Contains("Save"))
            {
                fileInfo.Delete();
            }
        }

        // SceneManager.LoadScene("SplashScreenAndCredits");
    }

}