﻿/**********************************************************************
* OptionsPopup.cs
* Created at 12-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsPopup : MonoBehaviour
{
    [Header("Game HUD")]
    public GameHUD GameHUD = null;

    [Header("Buttons")]
    public GameObject OpenOptionsButton;
    public GameObject CloseOptionsButton;
    public GameObject MainMenuButton;
    public GameObject MusicButton;
    public GameObject SoundButton;
    public GameObject DeconnexionButton;

    [Header("Sprites")]
    public string[] MusicSpriteNames;
    public string[] SoundSpriteNames;

    [Header("Circle Tween scales")]
    public TweenScale[] TweenScalesApparition;
    public TweenScale[] TweenScalesDisparition;
    public int MaxDurationScale;

    [Header("Buttons TweenAlphas")]
    public TweenAlpha[] ButtonsTweenAlpha;

    [Header("Alpha background")]
    public TweenAlpha TweenAlphaBackground;

    [Header("OnMainMenu")]
    public UILabel GroupIdLabel;
    public UILabel LearnerIdLabel;

    private bool _isPanelOpen;

    private UISprite _musicSprite;
    private UISprite _soundSprite;

    private int _currThresholdMusicVol;
    private int _currentThresholdSoundVol;

    public bool Visible = false;

    private void Awake()
    {
        _musicSprite = MusicButton.GetComponent<UISprite>();
        _soundSprite = SoundButton.GetComponent<UISprite>();
    }

    private void Start()
    {
        _currThresholdMusicVol = (int) Mathf.Round(SoundMgmt.Instance.MusicVolume * (MusicSpriteNames.Length - 1));
        _currentThresholdSoundVol = (int)Mathf.Round(SoundMgmt.Instance.SoundVolume * (SoundSpriteNames.Length - 1));

        _musicSprite.spriteName = MusicSpriteNames[_currThresholdMusicVol];
        _soundSprite.spriteName = SoundSpriteNames[_currentThresholdSoundVol];

        UIEventListener.Get(MusicButton).onClick = ChangeMusicVolume;
        UIEventListener.Get(SoundButton).onClick = ChangeSoundVolume;

        if (MainMenuButton)
            UIEventListener.Get(MainMenuButton).onClick = GoBackToMainMenu;

        TweenScalesApparition[0].SetOnFinished(ShowButtons);

        //EventDelegate.Add(TweenScalesApparition[0].onFinished, ShowButtons);
        EventDelegate.Add(TweenScalesDisparition[0].onFinished, HideButtons);

        if (DeconnexionButton)
        {
            UIEventListener.Get(DeconnexionButton).onClick = Deconnexion;
        }

        if (GroupIdLabel && LearnerIdLabel)
        {
            GroupIdLabel.text = PlayerPrefs.GetInt("groupId").ToString();
            LearnerIdLabel.text = PlayerPrefs.GetInt("learnerId").ToString();
        }
    }

    private void Deconnexion(GameObject go)
    {
        if (Constants.SendTraces)
        {
            DataSender.Instance.SendPreviousTraces();
        }

        SceneManager.LoadScene("ConnexionSceneOffline2");
    }

    public void ShowPopup()
    {
        Visible = true;

        if (Application.loadedLevelName != "MainMenu")
        {
            DataSender.Instance.AddNewAction("PAUSE");
        }

        foreach (TweenScale tweenScale in TweenScalesApparition)
        {
            tweenScale.ResetToBeginning();
            tweenScale.PlayForward();
        }

        if (TweenAlphaBackground)
        {
            TweenAlphaBackground.PlayForward();
        }

        Time.timeScale = 0.0f;
    }

    public void HidePopup()
    {
        Visible = false;

        foreach (TweenScale tweenScale in TweenScalesDisparition)
        {
            tweenScale.ResetToBeginning();
            tweenScale.PlayForward();
        }

        if (TweenAlphaBackground)
        {
            TweenAlphaBackground.PlayReverse();
        }

        Time.timeScale = 1.0f;
    }

    private void ChangeSoundVolume(GameObject go)
    {
        if (_currentThresholdSoundVol + 1 == SoundSpriteNames.Length)
        {
            _currentThresholdSoundVol = 0;
        }
        else
        {
            _currentThresholdSoundVol++;
        }

        SoundMgmt.Instance.SetSoundVolume(_currentThresholdSoundVol / (float) (SoundSpriteNames.Length - 1));

        _soundSprite.spriteName = SoundSpriteNames[_currentThresholdSoundVol];
    }

    private void ChangeMusicVolume(GameObject go)
    {
        if (_currThresholdMusicVol + 1 == MusicSpriteNames.Length)
        {
            _currThresholdMusicVol = 0;
        }
        else
        {
            _currThresholdMusicVol++;
        }

        SoundMgmt.Instance.SetMusicVolume(_currThresholdMusicVol / (float) (MusicSpriteNames.Length - 1));

        _musicSprite.spriteName = MusicSpriteNames[_currThresholdMusicVol];
    }

    private void GoBackToMainMenu(GameObject go)
    {
        GameHUD.GoBackToMainMenu();
    }

    public void ShowButtons()
    {
        foreach (TweenAlpha tweenAlpha in ButtonsTweenAlpha)
        {
            tweenAlpha.ResetToBeginning();
            tweenAlpha.PlayForward();
        }
    }

    public void HideButtons()
    {
        foreach (TweenAlpha tweenAlpha in ButtonsTweenAlpha)
        {
            //tweenAlpha.ResetToBeginning();
            tweenAlpha.PlayReverse();
        }
    }
}
