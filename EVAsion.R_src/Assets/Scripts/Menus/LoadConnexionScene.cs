﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadConnexionScene : MonoBehaviour
{
    public int DelayLoadScene = 8; 
    public string CinematicSceneName = "CinematicScene";
    public string ConnexionSceneName = "ConnexionScene";

	// Use this for initialization
	void Start ()
    {
        Invoke("Load", DelayLoadScene);
	}
	
	public void Load()
    {
        //if (Constants.OnlineVersion)
        {
            SceneManager.LoadScene(ConnexionSceneName);
        }
        //else
        //{
        //    SceneManager.LoadScene(CinematicSceneName);
        //}
    }
}
