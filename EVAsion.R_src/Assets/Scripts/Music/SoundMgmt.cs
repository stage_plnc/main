﻿/**********************************************************************
* SoundMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMgmt : MonoBehaviour
{
    public AudioSource MusicSource;
    public AudioSource FxSource;

    private float _soundVolume;
    private float _musicVolume;

    private static SoundMgmt _instance;
    public static SoundMgmt Instance
    {
        get { return _instance; }
    }

    public float SoundVolume
    {
        get
        {
            return _soundVolume;
        }
        set
        {
            _soundVolume = value;
            FxSource.volume = _soundVolume;
        }
    }

    public float MusicVolume
    {
        get
        {
            return _musicVolume;
        }
        set
        {
            _musicVolume = value;
            MusicSource.volume = _musicVolume;

        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            DontDestroyOnLoad(this.gameObject);

            LoadVolumes();
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    public void LoadVolumes()
    {
        _soundVolume = PlayerPrefs.GetFloat("SoundVolume", 1.0f);
        _musicVolume = PlayerPrefs.GetFloat("MusicVolume", 1.0f);

        MusicSource.volume = _musicVolume;
        FxSource.volume = _soundVolume;
    }

    public void PlayMusic(AudioClip audioClipMusic, bool isLooping)
    {
        MusicSource.clip = audioClipMusic;
        MusicSource.loop = isLooping;

        MusicSource.Play();
    }

    public void SetMusicVolume(float volume)
    {
        PlayerPrefs.SetFloat("MusicVolume", volume);

        MusicSource.volume = _musicVolume = volume;
    }

    public void SetSoundVolume(float volume)
    {
        PlayerPrefs.SetFloat("SoundVolume", volume);

        FxSource.volume = _soundVolume = volume;
    }

    public void PlayOneShotFXSound(AudioClip audioClipFX, float volume = 1.0f)
    {
        FxSource.PlayOneShot(audioClipFX, volume);
    }

    public void StopMusic()
    {
        MusicSource.Stop();
    }
}
