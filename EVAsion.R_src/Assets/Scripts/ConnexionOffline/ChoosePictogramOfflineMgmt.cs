﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChoosePictogramOfflineMgmt : MonoBehaviour
{
    public enum TypeChoice
    {
        Learner,
        Group
    };

    [Header("Type")]
    public TypeChoice Type;

    [Header("Mgmt")]
    public ConnexionOfflineSceneMgmt ConnexionMgmtI;
    public UnlockingPatternMgmt UnlockingPatternMgmt;

    [Header("Button")]
    public GameObject ArrowButton;

    [Header("Wheels")]
    public WheelOffline Wheel;
    public TweenAlpha TweenAlphaPictograms;

    private Dictionary<string, string> _patternByPicto;
    private string _patternToMatch = null;

    public BasePOD SelectedData { get { return Wheel.SelectedData; } }

    void Start()
    {
        if (ArrowButton != null)
        {
            UIEventListener.Get(ArrowButton).onClick = HandleArrowButton;
        }
    }

    private void HandleArrowButton(GameObject go)
    {
        if (Type == TypeChoice.Learner)
        {
     
            // Meaning we are selecting a pictogram
            if (UnlockingPatternMgmt.IsVisible == false)
            {
                Wheel.Clean();
                ConnexionMgmtI.GoBackToGroupChoice();
            }
            // Meaning we are in the unlockingPatternMgmt
            else
            {
                Wheel.Unlock();
                TweenAlphaPictograms.PlayReverse();
                UnlockingPatternMgmt.Lock();
            }
        }
    }

    public void FillGroups(List<Group> groups, bool v)
    {
        List<BasePOD> basePod = new List<BasePOD>();
        foreach (Group group in groups)
        {
            basePod.Add((BasePOD)group);
        }

        Wheel.FillDatas(basePod);
    }

    public void Hide()
    {
        GetComponent<TweenAlpha>().PlayReverse();
    }

    public void Show()
    {
        GetComponent<TweenAlpha>().PlayForward();
    }

    public void FillLearners(List<Learner> learners)
    {
        List<BasePOD> basePod = new List<BasePOD>();
        foreach (Learner learner in learners)
        {
            basePod.Add((BasePOD)learner);
        }

        Wheel.FillDatas(basePod);
    }

    private void ShowLearnersWheel(GameObject go = null)
    {
        ConnexionMgmtI.ShowLearnersWheel();
    }

    public void PictogramHasBeenClicked(BasePOD selectedData)
    {
        if (Type == TypeChoice.Learner)
        {
            // Store the id of the learner
            ConnexionMgmtI.SelectedLearnerId = selectedData.Id;
            OpenUnlockingPatternScene();
        }
        else if (Type == TypeChoice.Group)
        {
            ConnexionMgmtI.SelectedGroupId = selectedData.Id;
            ShowLearnersWheel();
        }
    }

    public void OpenUnlockingPatternScene(GameObject go = null)
    {
        Learner learner = (Learner) Wheel.SelectedData;
        _patternToMatch = learner.Password;

        // Illogical, but lets move on
        UnlockingPatternMgmt.GroupSprite.spriteName = ConnexionMgmtI.GroupPictogramMgmt.Wheel.SelectedPictogram.Sprite.spriteName;
        //UnlockingPatternMgmt.GroupSpriteBackground.spriteName = ConnexionMgmtI.GroupPictogramMgmt.Wheel.SelectedPictogram.BackgroundSprite.spriteName;
        UnlockingPatternMgmt.LearnerSprite.spriteName = Wheel.SelectedPictogram.Sprite.spriteName;
        UnlockingPatternMgmt.LearnerBackground.spriteName = Wheel.SelectedPictogram.BackgroundSprite.spriteName;

        PlayerPrefs.SetString("group_sprite_name", ConnexionMgmtI.GroupPictogramMgmt.Wheel.SelectedPictogram.Sprite.spriteName);
        PlayerPrefs.SetString("leaner_sprite_name", Wheel.SelectedPictogram.Sprite.spriteName);
        PlayerPrefs.SetString("leaner_background_sprite_name", Wheel.SelectedPictogram.BackgroundSprite.spriteName);

        UnlockingPatternMgmt.Unlock(_patternToMatch);

        Wheel.Lock();
        TweenAlphaPictograms.PlayForward();
    }

    public void HasCompletePattern()
    {
        PlayerPrefs.SetInt("learnerId", ConnexionMgmtI.SelectedLearnerId);
        PlayerPrefs.SetInt("groupId", ConnexionMgmtI.SelectedGroupId);
        SceneManager.LoadScene("CinematicScene");
    }
}
