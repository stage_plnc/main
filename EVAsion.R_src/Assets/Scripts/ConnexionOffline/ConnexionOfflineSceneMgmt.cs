﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ConnexionOfflineSceneMgmt : MonoBehaviour
{ 
    [Header("Mgmt")]
    public ChoosePictogramOfflineMgmt GroupPictogramMgmt;
    public ChoosePictogramOfflineMgmt LearnersPictogramMgmt;

    [Header("Text Assets")]
    public TextAsset GroupsJSON;
    public TextAsset LearnersJSON;

    public int SelectedGroupId;
    public int SelectedLearnerId;

    private List<Group> _groups;
    private List<Learner> _learners;

    private void Awake()
    {
        _groups = new List<Group>();
        _learners = new List<Learner>();
    }

    // Use this for initialization
    void Start()
    {
        LoadGroups();

        LoadLearners();

        GroupPictogramMgmt.FillGroups(_groups, false);
    }

    public void LoadGroups()
    {
        string groupsJsonPath = string.Format("{0}/groups.json", Application.persistentDataPath);
        string content = string.Empty;

        if (File.Exists(groupsJsonPath))
        {
            content = File.ReadAllText(groupsJsonPath);
        }
        else
        {
            content = GroupsJSON.text;

            File.WriteAllText(groupsJsonPath, content);
        }

        JSONObject jsonContent = new JSONObject(content);
        List<JSONObject> groupsJson = jsonContent.GetField("groups").list;

        foreach (JSONObject groupJson in groupsJson)
        {
            _groups.Add(new Group(groupJson));
        }
    }

    private void LoadLearners()
    {
        string learnersJsonPath = string.Format("{0}/learners.json", Application.persistentDataPath);
        string content = string.Empty;

        if (File.Exists(learnersJsonPath))
        {
            content = File.ReadAllText(learnersJsonPath);
        }
        else
        {
            content = LearnersJSON.text;

            File.WriteAllText(learnersJsonPath, content);
        }

        JSONObject jsonContent = new JSONObject(content);
        List<JSONObject> learnersJson = jsonContent.GetField("learners").list;

        foreach (JSONObject learnerJson in learnersJson)
        {
            _learners.Add(new Learner(learnerJson));
        }
    }

    internal void GoBackToGroupChoice()
    {
        GroupPictogramMgmt.Show();
        LearnersPictogramMgmt.Hide();
    }

    public void ShowLearnersWheel()
    {
        Group choosenGroup = (Group) GroupPictogramMgmt.SelectedData;
        List<Learner> choosenLearners = new List<Learner>();

        foreach (int idLearner in choosenGroup.LearnersId)
        {
            Learner l = _learners.Find(x => x.Id == idLearner);

            choosenLearners.Add(l);
        }

        LearnersPictogramMgmt.FillLearners(choosenLearners);

        LearnersPictogramMgmt.Show();
        GroupPictogramMgmt.Hide();

        SelectedGroupId = choosenGroup.Id;
    }
}
