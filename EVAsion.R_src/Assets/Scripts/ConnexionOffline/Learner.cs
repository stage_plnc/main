﻿public class Learner : BasePOD
{
    public string FirstName;
    public string Password;
    public int GroupId;

    public Learner(JSONObject learnerJson)
    {
        if (learnerJson.HasField("id"))
            Id = (int) learnerJson.GetField("id").n;

        FirstName = learnerJson.GetField("firstname").str;

        if (learnerJson.HasField("password"))
            Password = learnerJson.GetField("password").str;
        if (learnerJson.HasField("avatar"))
            Avatar = learnerJson.GetField("avatar").str;

        if (learnerJson.HasField("group"))
            GroupId = (int) learnerJson.GetField("group").n;
    }

    public Learner(int id, int groupId)
    {
        Id = id;
        GroupId = groupId;
    }

    public JSONObject ToJSON()
    {
        JSONObject json = new JSONObject();

        json.AddField("id", Id);
        json.AddField("firstname", FirstName);
        json.AddField("group", GroupId);

        return json;
    }

}