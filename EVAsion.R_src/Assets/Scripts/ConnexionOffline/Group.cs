﻿using System;
using System.Collections.Generic;

public class Group : BasePOD
{
    public string Name;
    public List<int> LearnersId = new List<int>();

    public Group(int id)
    {
        Id = id;
    }

    public Group(JSONObject groupJson)
    {
        Id = (int)groupJson.GetField("id").n;

        if (groupJson.HasField("name"))
            Name = groupJson.GetField("name").str;

        if (groupJson.HasField("avatar"))
            Avatar = groupJson.GetField("avatar").str;

        if (groupJson.HasField("learners"))
        {
            List<JSONObject> learnersJSON = groupJson.GetField("learners").list;
            LearnersId = new List<int>();

            foreach (JSONObject learnerJsonId in learnersJSON)
            {
                LearnersId.Add((int)learnerJsonId.n);
            }
        }
    }

    public void AddLearner(int idLearner)
    {
        LearnersId.Add(idLearner);
    }

    public JSONObject ToJSON()
    {
        JSONObject json = new JSONObject();

        json.AddField("id", Id);

        JSONObject learnersJSON = new JSONObject(JSONObject.Type.ARRAY);
        foreach (int learnerId in LearnersId)
        {
            learnersJSON.Add(learnerId);
        }

        json.AddField("learners", learnersJSON);

        return json;
    }
}