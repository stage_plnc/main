﻿/**********************************************************************
* ElapsedTimeMgmt.cs
* Created at 14-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using UnityEngine.SceneManagement;

public class ElapsedTimeMgmt : MonoBehaviour
{
    public Dictionary<string, float> ElapsedTimes;

    private string _runningGame = string.Empty;

    private static ElapsedTimeMgmt _instance;
    public static ElapsedTimeMgmt Instance
    {
        get { return _instance; }
    }

    public float MaxDurationSessionMinutes = 30.0f;

    private float _startSession = -1;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            Init();

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogWarning("[ElapsedTimeMgmt] Instance de ElapsedTimeMgmt already exists)");
        }
    }

    private void Init()
    {
        ElapsedTimes = new Dictionary<string, float>();

        foreach (string game in Constants.GameNamesId)
        {
            ElapsedTimes[game] = 0.0f;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void StartSession()
    {
        _startSession = Time.unscaledTime;
    }

    public void StartToPlay(string Game)
    {
        _runningGame = Game;
    }

    public void StopToPlay()
    {
        _runningGame = string.Empty;
    }

    private void Update()
    {
        if (_runningGame != string.Empty)
        {
            ElapsedTimes[_runningGame] += Time.deltaTime;
        }

        if (_startSession != -1)
        {
            if (Time.unscaledTime - _startSession > MaxDurationSessionMinutes * 60.0f)
            {
                _startSession = -1;

                SceneManager.LoadScene("ConnexionScene");
            }
        }
    }

    public JSONObject GetElapsedTimesJSON()
    {
        JSONObject elapsedTimeJSON = new JSONObject();

        foreach (string Game in ElapsedTimes.Keys)
        {
            elapsedTimeJSON.AddField(Game, ElapsedTimes[Game]);
        }

        return elapsedTimeJSON;
    }

    public void SetElapsedTimes(JSONObject elapsedTimesJSON)
    {
        Debug.Log("Setting elapsedTime " + elapsedTimesJSON.ToString(true));

        foreach (string Game in PRSVL_PlayerMgnt.Instance.GamesNames)
        {
            if (!elapsedTimesJSON.GetField(Game).IsNull)
            {
                Debug.Log("Setting " + Game + " to " + elapsedTimesJSON.GetField(Game).f);
                ElapsedTimes[Game] = elapsedTimesJSON.GetField(Game).f;
            }
            else
            {
                Debug.Log("Setting " + Game + " to 0");
                ElapsedTimes[Game] = 0.0f;
            }
        }
    }

    public int[] LessPlayedToMostPlayedGames()
    {
        string[] allGames = (string[]) PRSVL_PlayerMgnt.Instance.GamesNames.Clone();
        string[] allGamesOrdered = (string[])PRSVL_PlayerMgnt.Instance.GamesNames.Clone();
        int[] finalOrder = new int[ElapsedTimes.Count];

        List<KeyValuePair<string, float>> ElapsedTimeSorted = ElapsedTimes.ToList();

        foreach (KeyValuePair<string, float> g in ElapsedTimeSorted)
        {

        }

        ElapsedTimeSorted.Sort(
            delegate (KeyValuePair<string, float> pair1,
            KeyValuePair<string, float> pair2)
            {
                return pair1.Value.CompareTo(pair2.Value);
            }
        );

        for (int i = 0; i < allGames.Length; i++)
        {
            allGamesOrdered[i] = ElapsedTimeSorted[i].Key;
        }

        for (int i = 0; i < allGames.Length; i++)
        {
            finalOrder[i] = Array.FindIndex(allGamesOrdered, x => x == allGames[i]);
        }
        return finalOrder;
    }

    public void Reset()
    {
        foreach (string game in Constants.GameNamesId)
        {
            ElapsedTimes[game] = 0.0f;
        }
    }
}
