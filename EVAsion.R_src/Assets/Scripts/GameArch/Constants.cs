﻿/**********************************************************************
* Constants.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/

using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public static readonly string[] GameNames = { "Runner", "MatchMaker", "Chase", "TowerDefense" };
    public static readonly string[] GameNamesId = { "runner", "match_maker", "chase", "tower_defense" };

    // Will not change
    public const bool HardFeedParseval = true;

    // Can change
    public const bool OnlineVersion = false;
    public const bool SendTraces = false; 
    public const bool DebugVersionForLPNC = false; // Meaning they can change the difficulty files

    // Useless ?
    public const bool ButtonDeleteTraces = false;

    public const int WidthScreen = 2048;
    public const int HeigthScreen = 1536;

    public const int NbrOfLetterLatinAlphabet = 26;
    public const float FailureImpactOnScore = 0.33f;

    // Can be changed through the difficulty files. This variable is common to all games, but it changes when loading one game or another
    public static int NbrTotalCycles = 10;
    public static int NbrCyclesDefault = 10;

    // linear, random or parseval
    public static string DifficultyMethod = "parseval";

    public static Vector3 LinearDifficultyStartBound;
    public static Vector3 LinearDifficultyEndBound;

    public static int NbrEvents = -1;

    // In seconds. See L 188 in GameMgmt.cs
    public const float TimeWithoutActivity = 120.0f;
    // Serveur prod = https://fluence.ac-grenoble.fr/API/
    // Serveur test = https://portail-enseignant-fluence.gtnapp.com/API/

    // NEW key : NdVHcVRgyrMfj43WbXAK
    // OLD key : Bktfh56MY9F2LDWeeQk6

    public static string BaseURLApi = "https://fluence.ac-grenoble.fr/API/";

    public static readonly string[] CombinationPatterns = { "A", "#A#", "AA", "#AA#", "AAA", "#AAA#" };

    public static class HUD
    {
        public const int DurationTutorialPopUp = 3;
        public const int DurationInterludeTutorial = 6;
    }

    public static class Runner
    {
        public const int NbrOfLanes = 3;

        public const int GameStartCountdown = 3;

        public const float CycleDuration = 25.0f;
        public const float InterludeDuration = 6.0f;

        public static readonly int RunAnimationId = Animator.StringToHash("LP_COURSE");
    }

    public static class MatchMaker
    {
        public const int NbrVisibleTreesLayers = 4;
        public const int NbrTreesByLayer = 4;
        public const float HeigthBetweenLayerTrees = 96.0f;

        public const float CycleDuration = 25.0f;
        public const float InterludeDuration = 6.0f;

        public const int TreesWidthSeparation = 24;

        public const float ShiftTreesOffset = 120.0f;

        public const int HeigthMapGhosts = (int)(HeigthScreen * 0.66f);
    }

    public static class Chase
    {
        public const int NbrVisibleFloors = 3;
        public const int NbrColumns = 4;

        public const float CycleDuration = 25.0f;
        public const float InterludeDuration = 6.0f;
    }

    public static class TowerDefense
    {
        public const float CycleDuration = 25.0f;
        public const float InterludeDuration = 6.0f;

        public const int NbrViewports = 4;

        public static readonly Dictionary<UIWidget.Pivot, int> _viewportToIdx = new Dictionary<UIWidget.Pivot, int>()
        {
            { UIWidget.Pivot.BottomLeft, 0 },
            { UIWidget.Pivot.TopLeft, 1 },
            { UIWidget.Pivot.TopRight, 2 },
            { UIWidget.Pivot.BottomRight, 3 },
        };

        public static readonly UIWidget.Pivot[] _idxToViewport = {
            UIWidget.Pivot.BottomLeft,
            UIWidget.Pivot.TopLeft,
            UIWidget.Pivot.TopRight,
            UIWidget.Pivot.BottomRight
        };
    }


}

