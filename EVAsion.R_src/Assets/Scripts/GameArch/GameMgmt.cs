﻿/**********************************************************************
* GameMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/

using System;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * This class handle the game cycles with parseval algorithm
 * All Mini-games manager are inherited from it
*/
public abstract class GameMgmt : MonoBehaviour
{
    public enum GameStatus
    {
        POP_UP_TUTORIAL = 0,
        APPARITION_PLAYER = 1,
        TUTORIAL_TIME = 3,
        INTERLUDE = 4,
        GAME_CYCLE = 5,
        GAME_IS_FINISHED = 6,
    }

    [Header("Music")]
    public AudioClip Music;

    public GameHUD GameHUD = null;
    public bool GenerateOnInterludeEnd = false;

    public GameStatus Status = GameStatus.POP_UP_TUTORIAL;

    [Range(0, 1)] public float UPL;

    public delegate void AlphaLetters();
    public event AlphaLetters ShowLetters;
    public event AlphaLetters HideLetters;

    protected bool _shouldKeepRunning = true;

    protected CycleFactory _factory; // Has to be set in child class before StartCycle !

    protected PRSVL_Player _pPlayer = null;

    protected LPNC_DataExport _currentParsevalDT;
    protected CycleData _cycleData = null;
    protected CycleSuccessAndFailure _cycleSuccessAndFailure;
    protected float _cycleStartDate;
    protected string _gameName;

    protected int _nbrElapsedCycles = 0;

    protected DateTime _dateLastTouch;

    protected int _nbrTotalSuccess = 0;
    protected int _nbrTotalGoodTarget = 0;
    protected int _nbrTotalFailure = 0;

    public virtual void Start()
    {
        DifficultyMgmt.Instance.LoadDifficultyFile(
            String.Format("{0}/Games/{1}_{2}.json", Application.persistentDataPath, _gameName, Application.version), _gameName);

        ContinueLoading();
    }

    public void ContinueLoading()
    {
        // TODO Load from its own attribut
        DistractorsMgmt.Instance.LoadFromFile();

        PRSVL_PlayerMgnt.Instance.UPL = DifficultyMgmt.Instance.GetUPL(_gameName);

        if (PlayerPrefs.HasKey("learnerId"))
        {
            PRSVL_PlayerMgnt.Instance.LoadPlayer(PlayerPrefs.GetInt("groupId").ToString(), PlayerPrefs.GetInt("learnerId").ToString());
        }
        else
        {
            Debug.LogError("Didnt use connexion scene. Loading FakePlayer");
            PRSVL_PlayerMgnt.Instance.LoadPlayer("Fake", "Player");
        }

        _pPlayer = PRSVL_PlayerMgnt.Instance.CurrentPlayer;

        _pPlayer.LoadGame(_gameName, PRSVL_PlayerMgnt.Instance.UPL);

        SoundMgmt.Instance.PlayMusic(Music, true);
        _cycleSuccessAndFailure = new CycleSuccessAndFailure();

        // Useful to control the pause
        UIEventListener.Get(GameHUD.InactivityPopupI.FrontCircle).onClick = HideInactivityPopup;
        _dateLastTouch = DateTime.Now;
        ElapsedTimeMgmt.Instance.StartToPlay(_gameName);

        // Prepare the data to send at the end of the session
        DataSender.Instance.CreateNewExercice(Array.FindIndex(Constants.GameNamesId, x => x == _gameName) + 1);
    }

    public virtual void StartGame()
    {
        // We're starting with an interlude to init the first values
        StartInterlude();
    }

    public virtual void StartTutorial()
    {
        Status = GameStatus.TUTORIAL_TIME;
    }

    public abstract void EndTutorial();

    public virtual void StartInterlude()
    {
        Status = GameStatus.INTERLUDE;

        // Updating Parseval datas
        _currentParsevalDT = _pPlayer.GetExercise();

        // The factory returns a set of values according to Parseval values.
        // Like the combination of the letters to find...
        _cycleData = _factory.InitNextCycle(_currentParsevalDT);

        _nbrTotalGoodTarget += _cycleData.NbrGoodCombinations;
        if (!GenerateOnInterludeEnd)
        {
            // The factory creates the multiple elements 
            _factory.GenerateNextCycle();
        }

        if (GameHUD)
        {
            GameHUD.StartShowingInterludeData(_cycleData, _nbrElapsedCycles);
        }

        RetrieveDifficulties(_factory);

        // Invoke(EndInterlude) is called by the daughter
    }

    private void RetrieveDifficulties(CycleFactory factory)
    {
        _currentParsevalDT = factory.ParsevalDT;
    }

    public virtual void EndInterlude()
    {
        if (GenerateOnInterludeEnd)
        {
            // The factory creates the multiple elements 
            _factory.GenerateNextCycle();
        }

        StartGameCycle();
    }

    public virtual void StartGameCycle()
    {
        Status = GameStatus.GAME_CYCLE;
        _cycleStartDate = Time.time;

        // Invoke(EndGameCycle) is called by the daughter
    }

    public virtual void EndGameCycle()
    {
        float score = CycleScore();

        _nbrElapsedCycles++;

        if (!Constants.HardFeedParseval)
        {
            _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, score);
        }

        if (Constants.NbrTotalCycles != Mathf.Infinity && _nbrElapsedCycles != Constants.NbrTotalCycles)
        {
            StartInterlude();
        }
        else
        {
            GameEnded();
        }
    }

    public virtual void GameEnded()
    {
        Status = GameStatus.GAME_IS_FINISHED;

        DataSender.Instance.SetExerciceEnd();
        // 1.0f = the progress bar is full
        // GameHUD.ShowProgressBar(1.0f);

        StartAnimatiqueEnd();

        ElapsedTimeMgmt.Instance.StopToPlay();
    }

    public void StartAnimatiqueEnd()
    {
        GameHUD.TweenAlphaEndGame();

        Invoke("LoadConclusionScene", GameHUD.TweenAlphaBackground.duration);
    }

    public void LoadConclusionScene()
    {
        int numConclusion = 0;
        float ratioSuccess = (float)(_nbrTotalSuccess - (_nbrTotalFailure / Constants.FailureImpactOnScore)) / (float)_nbrTotalGoodTarget;
        if (ratioSuccess < 0)
            ratioSuccess = 0;

        if (ratioSuccess > 0.66f)
        {
            numConclusion = 3;
        }
        else if (ratioSuccess < 0.33f)
        {
            numConclusion = 1;
        }
        else
        {
            numConclusion = 2;
        }

        SoundMgmt.Instance.StopMusic();

        PlayerPrefs.SetString("gameName", _gameName);
        Debug.LogError("_nbrTotalSuccess : " + _nbrTotalSuccess + ", _nbrTotalGoodTarget : " + _nbrTotalGoodTarget);
        Debug.LogError("numConclusion : " + numConclusion + ", ratio : " + ratioSuccess);
        PlayerPrefs.SetInt("numConclusion", numConclusion);

        SceneManager.LoadScene("ConclusionScene", LoadSceneMode.Single);
    }

    // TODO Work in Progress
    public abstract float CycleScore();

    public bool GameIsNotStarted()
    {
        return Status == GameStatus.APPARITION_PLAYER || Status == GameStatus.POP_UP_TUTORIAL;
    }

    public bool IsGameRunning()
    {
        return !GameIsNotStarted();
    }

    public void HideInactivityPopup(GameObject go)
    {
        GameHUD.HideInactivityPopup();

        Time.timeScale = 1.0f;
        _dateLastTouch = DateTime.Now;
    }

    public void HideAllLetters()
    {
        if (HideLetters != null)
        {
            HideLetters();
        }
    }

    public void ShowAllLetters()
    {
        if (ShowLetters != null)
        {
            ShowLetters();
        }
    }

    public virtual void OnDestroy()
    {
        ElapsedTimeMgmt.Instance.StopToPlay();

        _pPlayer.SaveProfile();
        
        // _parsevalInstance.FreeLPNC_Core();
    }

    #if !UNITY_EDITOR && !UNITY_STANDALONE
    protected virtual void FixedUpdate()
    {
        if (Status == GameStatus.GAME_CYCLE || Status == GameStatus.INTERLUDE)
        {
            TimeSpan span = DateTime.Now - _dateLastTouch;
            float timeSinceLastTouch = (span.Ticks / TimeSpan.TicksPerMillisecond) * 0.001f;

            if (timeSinceLastTouch > Constants.TimeWithoutActivity)
            {
                GameHUD.ShowInactivityPopup();

                Time.timeScale = 0.0f;
            }
        }
    }
    #endif

    private void OnApplicationQuit()
    {
        _pPlayer.SaveProfile();
    }

    protected virtual void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
#if UNITY_EDITOR || UNITY_ANDROID
            SceneManager.LoadScene("MainMenu");
#else
            Application.Quit();
#endif
        }

        //#if !UNITY_EDITOR
        if (!GameHUD.GamePaused && (Input.GetMouseButtonDown(0) || Input.anyKeyDown))
        {
            _dateLastTouch = DateTime.Now;

            Time.timeScale = 1.0f;
        }
        //#endif

#if (UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER)
        if (Input.GetKeyUp(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
#endif
    }

    public class CycleSuccessAndFailure
    {
        public int NbrTargetSuccess = 0;
        public int NbrTargetFailure = 0;

        public void Clear()
        {
            NbrTargetSuccess = NbrTargetFailure = 0;
        }
    }

}
