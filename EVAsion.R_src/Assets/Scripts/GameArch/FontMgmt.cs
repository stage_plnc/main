﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

// Voir L217 de UIFontMaker.cs

public class FontMgmt : MonoBehaviour
{
    [System.Serializable]
    public struct PrefabsByGame
    {
        public string GameName;
        public GameObject[] Prefabs;
    };

    private static FontMgmt _instance = null;
    public static FontMgmt Instance { get { return _instance; } }

    [Header("Font")]
    public Font CurrentFont;

    [Header("Games prefab")]
    public PrefabsByGame[] Prefabs;
    public GameObject[] FeedbacksPrefab;

    public void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        //string directoryFont = string.Format("{0}/Font", Application.persistentDataPath);
        //string fontName = string.Format("{0}/tahoma.ttf", directoryFont);

        //if (Directory.Exists(directoryFont) == false)
        //{
        //    Directory.CreateDirectory(directoryFont);
        //    //File.Copy(
        //}

        foreach (PrefabsByGame prefabs in Prefabs)
        {
            foreach (GameObject prefab in prefabs.Prefabs)
            {
                UILabel[] labels = prefab.GetComponentsInChildren<UILabel>();

                foreach (UILabel label in labels)
                {
                    label.trueTypeFont = CurrentFont;
                }
            }
        }

        foreach (GameObject feedbackPrefab in FeedbacksPrefab)
        {
            TextFeedBack textFeedBackBehaviour = feedbackPrefab.GetComponent<TextFeedBack>();
            textFeedBackBehaviour.LabelFont = CurrentFont;
        }
    }

    public void UpdateInterludePopup(GameObject interludePopup)
    {
        UILabel label = interludePopup.GetComponentInChildren<UILabel>();
        label.trueTypeFont = CurrentFont;
    } 
}
