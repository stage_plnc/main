﻿/**********************************************************************
* GameHUD.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using UnityEngine;
using UnityEngine.SceneManagement;

// This class will handle all NGUI elements relative to the game
// The GameMgnt has an attribute of GameHUD

public class GameHUD : MonoBehaviour
{
    [Header("Mgmt")]
    public GameMgmt GameMgmt;

    [Header("Pop ups")]
    public OptionsPopup OptionsPopup;
    public InterludePopup InterludePopupI;
    public InactivityPopup InactivityPopupI;

    [Header("Alpha background")]
    public TweenAlpha TweenAlphaBackground;

    [Header("White Background")]
    public TweenAlpha WhiteBackgroundTweenAlpha;

    public bool GamePaused { get { return InactivityPopupI.Visible || OptionsPopup.Visible; } }

    public void Awake()
    {
        // Open and close options
        UIEventListener.Get(OptionsPopup.OpenOptionsButton).onClick = ShowOptions;
        UIEventListener.Get(OptionsPopup.CloseOptionsButton).onClick = HideOptions;
    }

    #region Options
    private void ShowOptions(GameObject go)
    {
        OptionsPopup.ShowPopup();
        TweenAlphaBackground.PlayForward();

        GameMgmt.HideAllLetters();
    }

    private void HideOptions(GameObject go)
    {
        OptionsPopup.HidePopup();
        TweenAlphaBackground.PlayReverse();

        GameMgmt.ShowAllLetters();
    }

    #endregion

    #region Inactivity Popup
    public void ShowInactivityPopup()
    {
        InactivityPopupI.StartTween();
        TweenAlphaBackground.PlayForward();

        GameMgmt.HideAllLetters();
    }

    public void HideInactivityPopup()
    {
        InactivityPopupI.Hide();
        TweenAlphaBackground.PlayReverse();

        GameMgmt.ShowAllLetters();
    }
    #endregion

    public virtual void StartShowingInterludeData(CycleData cycleData, int nbrCycles)
    {
        InterludePopupI.SetCombination(cycleData.Combination);
        InterludePopupI.UpdateProgressBar((float) nbrCycles / (float)Constants.NbrTotalCycles);

        InterludePopupI.StartTween();
    }

    public void TweenAlphaEndGame()
    {
        TweenAlphaBackground.PlayForward();
    }

    internal void GoBackToMainMenu()
    {
        WhiteBackgroundTweenAlpha.SetOnFinished(LoadMainMenu);
        WhiteBackgroundTweenAlpha.PlayReverse();
    }

    private void LoadMainMenu()
    {
        DataSender.Instance.SetExerciceEnd();

        Time.timeScale = 1.0f;

        SceneManager.LoadScene("MainMenu");
    }
}
