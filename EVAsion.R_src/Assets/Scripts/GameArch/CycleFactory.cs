﻿/**********************************************************************
* CycleFactory.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using System;
using System.Text;
using System.IO;

public abstract class CycleFactory : MonoBehaviour
{
    /*
     * Theyse class should handle the procedural generation of mini-games
     */

    protected System.Random _random = null;

    protected string[] _seeds;
    private int _currentSeedIdx = 0;

    // Path file
    private string _seedsFilePath;

    [Header("Seeds Management")]
    public bool UsePreviousSeedsFile = false;
    public string ForceThisSeed = string.Empty;

    protected LPNC_DataExport _parsevalDT;

    // Useful for linear difficulty
    protected LPNC_DataExport _initialParsevalDT;
    protected float[] _factLinearDifficultyIncrease;
    protected int _currentCycle = 0;

    protected bool _upperCaseSession;
    protected int _indexNewPattern;
    protected string _goodCombination;
    protected string _wrongCombination;
    protected string _leftPartCombination, _rightPartCombination;

    public LPNC_DataExport ParsevalDT { get { return _parsevalDT; } }

    public string CurrentSeed
    {
        get
        {
            if (ForceThisSeed == string.Empty)
            {
                return _seeds[_currentSeedIdx];
            }
            else
            {
                return ForceThisSeed;
            }
        }
    }

    protected virtual void Awake()
    {

        //_seedsFilePath = Application.persistentDataPath + "/seeds.txt";

        if (ForceThisSeed != string.Empty)
        {
            if (ForceThisSeed.Length == 8)
            {
                _random = new System.Random(ForceThisSeed.GetHashCode());
            }
            else
            {
                Debug.LogWarning("[CycleFactory] The seed in parameter is not valid... Usage of a file");
                ForceThisSeed = string.Empty;
            }
        }
        else
        {
            _seeds = LoadSeeds();
            _random = new System.Random(_seeds[0].GetHashCode());
        }

        _factLinearDifficultyIncrease = new float[3];
    }

    // Is called by the MatchMakerMgmt to determine which trees layer comes first in th screen
    public int GetRandom(int min, int max)
    {
        if (_random != null)
        {
            return _random.Next(min, max);
        }
        else
        {
            return min;
        }
    }

    private string[] LoadSeeds()
    {
        string[] Seeds = null;

        if (UsePreviousSeedsFile && File.Exists(_seedsFilePath))
        {
            try
            {
                Seeds = File.ReadAllLines(_seedsFilePath);
            }
            catch (Exception e)
            {
                Debug.LogWarning("[CycleFactory] The file exists, but we cannot read it.. Either delete it or do something else. We are creating temporary seeds : " + e.Message);

                Seeds = GenerateSeeds(200, 8);
            }
        }
        else
        {
            if (UsePreviousSeedsFile)
                Debug.LogWarning("[CycleFactory] You chose to use the previous seeds file, but there wasnt one...");

            Seeds = GenerateSeeds(200, 8);

            //File.WriteAllLines(_seedsFilePath, Seeds);
        }

        return Seeds;
    }

    private string[] GenerateSeeds(int nbSeeds, int lengthStr)
    {
        System.Random random = new System.Random();

        string[] Seeds = new string[nbSeeds];

        for (int i = 0; i < nbSeeds; i++)
        {
            StringBuilder builder = new StringBuilder();
            char ch;

            for (int j = 0; j < lengthStr; j++)
            {
                // Generate a char between A and Z
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(random.NextDouble() * ('Z' - 'A') + 'A')));

                builder.Append(ch);
            }

            Seeds[i] = builder.ToString();
        }

        return Seeds;
    }

    /**
     * The third dimension of parsevalDt has to be the combination
     */
    public virtual CycleData InitNextCycle(LPNC_DataExport parsevalDT)
    {
        //if (_currentCycle >= 1)
        //{
        //    _initialParsevalDT = _parsevalDT;
        //}

        _parsevalDT = parsevalDT;

        // Update seed
        if (ForceThisSeed == string.Empty)
        {
            _random = new System.Random(_seeds[_currentSeedIdx].GetHashCode());

            _currentSeedIdx++;

            if (_currentSeedIdx >= _seeds.Length)
                _currentSeedIdx = 0;
        }

        int rand = _random.Next(0, 2);
        _upperCaseSession = rand == 0;
        // The return value of this method is used inside the daughter classes
        return null;
    }

    public abstract void GenerateNextCycle();
}