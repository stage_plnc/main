﻿/**********************************************************************
* SwipeControler.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SwipeControler : MonoBehaviour
{
    /*
    *  Controller childs should handle player input related to the gameplay
    *  by suscribing these events
    */
    public delegate void SwipeAction();
    public event SwipeAction OnUpSwipe;
    public event SwipeAction OnLeftSwipe;
    public event SwipeAction OnDownSwipe;
    public event SwipeAction OnRightSwipe;

    public int MinDistanceForSwipe = 1;

    [Range(0, 90)]
    public int MinAngleToSwip = 45;
    private float _minRadAngleToSwip;


    protected void Awake()
    {
        _minRadAngleToSwip = MinAngleToSwip * Mathf.PI / 180.0f;
    }

    void Update()
    {
        CtrlUpdate();
    }

    protected virtual void CtrlUpdate()
    {
        CheckSwipe();
    }

    #region swipe detection

    /* 
     * this code is actualy drained from the internet :p 
     * http://answers.unity3d.com/questions/600148/detect-swipe-in-four-directions-android.html
     * No time to waste 
     */
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
#if !(UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER)

    public void CheckSwipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
                if (currentSwipe.magnitude >= MinDistanceForSwipe)
                {
                    //normalize the 2d vector
                    currentSwipe.Normalize();

                    CheckSwipeValues(); 
                }

            }
        }
    }
#else

    public void CheckSwipe()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            //save ended touch 2d point
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            if (currentSwipe.magnitude >= MinDistanceForSwipe)
            {
                //normalize the 2d vector
                currentSwipe.Normalize();

                CheckSwipeValues();
            }
        }
    }

#endif

    private void CheckSwipeValues()
    {
        float acosX = Mathf.Acos(currentSwipe.x);

        //swipe upwards
        if (currentSwipe.y > 0 && IsInHalfCircle(acosX))
        {
            //Debug.Log("Controller : Up Swipe");
            if (OnUpSwipe != null) OnUpSwipe();
        }
        //swipe down
        if (currentSwipe.y < 0 && IsInHalfCircle(acosX))
        {
            //Debug.Log("Controller : Down Swipe");
            if (OnDownSwipe != null) OnDownSwipe();
        }
        //swipe left
        if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            //Debug.Log("Controller : Left Swipe");
            if (OnLeftSwipe != null) OnLeftSwipe();
        }
        //swipe right
        if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            //Debug.Log("Controller : Right Swipe");
            if (OnRightSwipe != null) OnRightSwipe();
        }
    }

    private bool IsInHalfCircle(float acosX)
    {
        return acosX > _minRadAngleToSwip && acosX < (Mathf.PI - _minRadAngleToSwip);
    }
    #endregion
}
