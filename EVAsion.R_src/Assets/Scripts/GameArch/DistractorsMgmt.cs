﻿/**********************************************************************
* CharactersSimilarities.cs
* Created at 19-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using System.Text;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

// There are a lot of Constants.NbrOfLetterLatinAlphabet, thats the number of letters in the alphabet
class DistractorsMgmt : MonoBehaviour
{
    public char[] WrongCharaters = { '#' };

    public int NbrOfCloseWrongCharacters = 3;

    [Header("New distrctators LPNC")]
    public TextAsset[] NewDistractorsFilesMaj;
    public TextAsset[] NewDistractorsFilesMin;

    [Header("Other distractors")]
    public TextAsset UpperCaseDistractorText;
    public TextAsset LowerCaseDistractorText;


    private char[] _allCharacters;
    private float[][] _matrixRatiosSimilarities;

    // 3D array
    // First dim : for the 6 files
    // Second dim : patterns to recognize
    // Third dim : distractors
    private Dictionary<char, string[]> _distractorsOldMin;
    private Dictionary<char, string[]> _distractorsOldMaj;

    private string[][][] _distractorsMaj;
    private string[][][] _distractorsMin;

    private static DistractorsMgmt _instance;
    public static DistractorsMgmt Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            _distractorsOldMin = new Dictionary<char, string[]>();
            _distractorsOldMaj = new Dictionary<char, string[]>();

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogWarning("[CharacterSimilarities] Instance already exists, will be deleted");
            Destroy(this.gameObject);
        }
    }

    public void LoadFromFile()
    {
        LoadDistractors();
        LoadNewDistractors();
    }

    private void LoadNewDistractors()
    {
        char[] sepBackline = new char[] { '\n' };
        char[] sep = new char[] { '\t' };
        _distractorsMaj = new string[6][][];
        _distractorsMin = new string[6][][];

        // First, we have to check if there is distractors inside AppData
        string distractorsFolders = string.Format("{0}/{1}", Application.persistentDataPath, "DIS");
        bool loadingFromAppData = Directory.Exists(distractorsFolders);

        try
        {
            for (int i = 0; i < NewDistractorsFilesMaj.Length; i++)
            {
                string contentMaj, contentMin;
                string distractorsMinPath = string.Format("{0}/{1}.txt", distractorsFolders, NewDistractorsFilesMin[i].name);
                string distractorsMajPath = string.Format("{0}/{1}.txt", distractorsFolders, NewDistractorsFilesMaj[i].name);
                int nbrOccLines = -1, nbrOccCol = -1;

                if (loadingFromAppData)
                {
                    contentMin = File.ReadAllText(distractorsMinPath);
                    contentMaj = File.ReadAllText(distractorsMajPath);
                }
                else
                {
                    contentMin = NewDistractorsFilesMin[i].text;
                    contentMaj = NewDistractorsFilesMaj[i].text;

                    if (Directory.Exists(distractorsFolders) == false)
                        Directory.CreateDirectory(distractorsFolders);

                    File.WriteAllText(distractorsMinPath, NewDistractorsFilesMin[i].text);
                    File.WriteAllText(distractorsMajPath, NewDistractorsFilesMaj[i].text);
                }

                string[] linesMaj = contentMaj.Split(sepBackline, StringSplitOptions.RemoveEmptyEntries);

                nbrOccCol = linesMaj.Length;
                // -- MAJ -- 
                // Creating matrix
                for (int j = 0; j < linesMaj.Length; j++)
                {
                    string[] splitted = linesMaj[j].Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    if (nbrOccLines == -1)
                    {
                        nbrOccLines = splitted.Length;
                    }
                    else if (nbrOccLines != splitted.Length)
                    {
                        Debug.LogErrorFormat("Error in {0} : number of occurences at line {1} not coherent", distractorsMajPath, j);
                    }

                    _distractorsMaj[i] = new string[splitted.Length][];

                    for (int k = 0; k < splitted.Length; k++)
                    {
                        _distractorsMaj[i][k] = new string[nbrOccCol];
                    }
                }

                for (int j = 0; j < linesMaj.Length; j++)
                {
                    string[] splitted = linesMaj[j].Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    // Each patterns
                    for (int k = 0; k < splitted.Length; k++)
                    {
                        _distractorsMaj[i][k][j] = splitted[k].Trim();
                    }
                }

                // -- MIN --
                string[] linesMin = contentMin.Split(sepBackline, StringSplitOptions.RemoveEmptyEntries);

                for (int j = 0; j < linesMin.Length; j++)
                {
                    string[] splitted = linesMin[j].Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    _distractorsMin[i] = new string[splitted.Length][];

                    for (int k = 0; k < splitted.Length; k++)
                    {
                        _distractorsMin[i][k] = new string[7];
                    }
                }

                for (int j = 0; j < linesMin.Length; j++)
                {
                    string[] splitted = linesMin[j].Split(sep, StringSplitOptions.RemoveEmptyEntries);

                    // Each patterns
                    for (int k = 0; k < splitted.Length; k++)
                    {
                        _distractorsMin[i][k][j] = splitted[k].Trim();
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("[DistractorsMgmt::LoadNextDistractors] Error loading the new distractors : " + e.Message);
        }
    }

    private void LoadDistractors()
    {
        char[] separator = { '\n' };

        // First, we have to check if there is distractors inside AppData
        string distractorsFolders = string.Format("{0}/{1}", Application.persistentDataPath, "SetLettres");
        string distractorsMinPath = string.Format("{0}/{1}.txt", distractorsFolders, LowerCaseDistractorText.name);
        string distractorsMajPath = string.Format("{0}/{1}.txt", distractorsFolders, UpperCaseDistractorText.name);

        string contentMin, contentMaj;

        try
        {
            if (File.Exists(distractorsMinPath) && File.Exists(distractorsMajPath))
            {
                contentMin = File.ReadAllText(distractorsMinPath);
                contentMaj = File.ReadAllText(distractorsMajPath);
            }
            else
            {
                contentMin = LowerCaseDistractorText.text;
                contentMaj = UpperCaseDistractorText.text;

                Directory.CreateDirectory(distractorsFolders);

                File.WriteAllText(distractorsMinPath, LowerCaseDistractorText.text);
                File.WriteAllText(distractorsMajPath, UpperCaseDistractorText.text);
            }

            string[] eachLineMaj = contentMaj.Split(separator);

            // The file is organized this way :
            // A
            // AUOBO
            // AUOBO
            // AUOBO
            // AUOBO
            // AUOB
            // A

            // B
            // etc...

            for (int i = 0; i < eachLineMaj.Length; i += 8)
            {
                char letter = eachLineMaj[i][0];

                _distractorsOldMaj[letter] = new string[6];

                for (int j = 0; j < 6; j++)
                {
                    _distractorsOldMaj[letter][j] = eachLineMaj[i + j + 1];
                }
            }

            string[] eachLineMin = contentMin.Split(separator);

            for (int i = 0; i < eachLineMin.Length; i += 8)
            {
                char letter = eachLineMin[i][0];

                _distractorsOldMin[letter] = new string[6];

                for (int j = 0; j < 6; j++)
                {
                    _distractorsOldMin[letter][j] = eachLineMin[i + j + 1].Trim();
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("[DistractorsMgmt::LoadDistractors] Error loading the distractors : " + e.Message);
        }
    }


    public char GetDistractor(System.Random random, char letter, int difficulty)
    {
        Dictionary<char, string[]> distractors;

        if (Char.IsUpper(letter))
            distractors = _distractorsOldMaj;
        else
            distractors = _distractorsOldMin;

        string line = distractors[letter][difficulty - 1];

        line = line.Trim();

        return line[random.Next(0, line.Length)];
    }

    public string GenerateWrongCombinationRunnerTD(string goodCombination, int nbrOfLetters, int difficultyDistractor)
    {
        string wrongCombination = string.Empty;

        // Removing the # so we keep only the letters
        string refPattern = goodCombination.Replace("#", string.Empty).Trim();
        int whichDistractor = -1;

        string[][] matrixUsed = null;

        if (nbrOfLetters == 2)
        {
            whichDistractor = 0;
        }
        else if (nbrOfLetters == 3)
        {
            whichDistractor = 2;
        }

        if (Char.IsUpper(refPattern[0]))
        {
            matrixUsed = _distractorsMaj[whichDistractor];
        }
        else
        {
            matrixUsed = _distractorsMin[whichDistractor];
        }

        for (int i = 0; i < matrixUsed.Length; i++)
        {
            if (refPattern == matrixUsed[i][0])
            {
                wrongCombination = matrixUsed[i][difficultyDistractor - 1 + 1].Trim();

                break;
            }
        }

        if (goodCombination.Contains("#"))
        {
            wrongCombination = "#" + wrongCombination + "#";
        }

        return wrongCombination;
    }


    public string GenerateWrongCombinationMMAndChase(string goodCombination, int difficultySimilarity)
    {
        string[][] matrixUsed;

        if (Char.IsUpper(goodCombination[0]))
        {
            matrixUsed = _distractorsMaj[difficultySimilarity - 1];
        }
        else
        {
            matrixUsed = _distractorsMin[difficultySimilarity - 1];
        }

        string wrongCombination = string.Empty;

        for (int i = 0; i < matrixUsed.Length; i++)
        {
            if (goodCombination == matrixUsed[i][0].Trim())
            {
                wrongCombination = matrixUsed[i][difficultySimilarity - 1 + 1].Trim();

                break;
            }
        }

        return wrongCombination;
    }

};
