﻿/**********************************************************************
* SetOfLetters.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class TargetGeneratorMgmt : MonoBehaviour
{
    [Serializable]
    public struct NewLPNCFiles
    {
        public TextAsset file1;
        public TextAsset file2;
    }

    [Serializable]
    public struct NewLPNCFilesContent
    {
        public string contentFile1;
        public string contentFile2;
    }

    [Header("New Pattern")]
    public NewLPNCFiles[] NewLPNCDifficulties;

    private NewLPNCFilesContent[] _LPNCFilesContent;

    private char[] _separatorNewLine = { '\n' };

    private static TargetGeneratorMgmt _instance;
    public static TargetGeneratorMgmt Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            Init();

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogWarning("[CharacterSimilarities] Instance already exists, will be deleted");
            Destroy(this.gameObject);
        }
    }

    private void Init()
    {
        _LPNCFilesContent = new NewLPNCFilesContent[NewLPNCDifficulties.Length];

        string cransFolder = string.Format("{0}/{1}", Application.persistentDataPath, "Crans");
        bool loadingFromAppData = Directory.Exists(cransFolder);

        if (loadingFromAppData)
        {
            for (int i = 0; i < NewLPNCDifficulties.Length; i++)
            {
                string cransLeftPath = string.Format("{0}/{1}.txt", cransFolder, NewLPNCDifficulties[i].file1.name).Trim();
                string cransRightPath = string.Format("{0}/{1}.txt", cransFolder, NewLPNCDifficulties[i].file2.name).Trim();

                _LPNCFilesContent[i].contentFile1 = File.ReadAllText(cransLeftPath);
                _LPNCFilesContent[i].contentFile2 = File.ReadAllText(cransRightPath);
            }
        }
        else
        {
            for (int i = 0; i < NewLPNCDifficulties.Length; i++)
            {
                string cransLeftPath = string.Format("{0}/{1}.txt", cransFolder, NewLPNCDifficulties[i].file1.name).Trim();
                string cransRightPath = string.Format("{0}/{1}.txt", cransFolder, NewLPNCDifficulties[i].file2.name).Trim();

                _LPNCFilesContent[i].contentFile1 = NewLPNCDifficulties[i].file1.text;
                _LPNCFilesContent[i].contentFile2 = NewLPNCDifficulties[i].file2.text;

                if (Directory.Exists(cransFolder) == false)
                    Directory.CreateDirectory(cransFolder);

                File.WriteAllText(cransLeftPath, _LPNCFilesContent[i].contentFile1);
                File.WriteAllText(cransRightPath, _LPNCFilesContent[i].contentFile2);
            }
        }
    }

    public List<int> GenerateRandomIndexes(System.Random random, int lenStr, int difficulty)
    {
        // 0, 1, 2 .. lenString - 1
        List<int> indexesPossible = Enumerable.Range(0, lenStr).ToList();
        List<int> indexesCharacterDontChange = new List<int>();

        int nbrOfLetters = -1;

        if (difficulty == 4)
        {
            nbrOfLetters = lenStr - 1;
        }
        else if (difficulty == 5)
        {
            nbrOfLetters = lenStr < 3 ? 1 : 2;
        }
        else if (difficulty == 6)
        {
            nbrOfLetters = 1;
        }

        for (int i = 0; i < nbrOfLetters; i++)
        {
            int rdIdex = random.Next(0, indexesPossible.Count);
            int idx = indexesPossible[rdIdex];

            indexesCharacterDontChange.Add(idx);

            indexesPossible.Remove(idx);
        }

        return indexesCharacterDontChange;
    }

    public int GetRandomIndex(System.Random random, int difficultySimilarity)
    {
        string[] letters = _LPNCFilesContent[difficultySimilarity - 1].contentFile1.Split(_separatorNewLine, StringSplitOptions.RemoveEmptyEntries);
        int randomIndex = random.Next(0, letters.Length);

        return randomIndex;
    }

    public string[] GetCombinationAt(System.Random random, int index, int difficultyTarget)
    {
        string[] lettersFile1 = _LPNCFilesContent[difficultyTarget - 1].contentFile1.Split(_separatorNewLine, StringSplitOptions.RemoveEmptyEntries);
        string[] lettersFile2 = _LPNCFilesContent[difficultyTarget - 1].contentFile2.Split(_separatorNewLine, StringSplitOptions.RemoveEmptyEntries);

        return new string[] { lettersFile1[index], lettersFile2[index] };
    }

    public string[] GetCombinationWithout(System.Random random, int goodCombinIndex, int difficultySimilarity)
    {
        string[] lettersFile1 = _LPNCFilesContent[difficultySimilarity - 1].contentFile1.Split(_separatorNewLine, StringSplitOptions.RemoveEmptyEntries);
        string[] lettersFile2 = _LPNCFilesContent[difficultySimilarity - 1].contentFile2.Split(_separatorNewLine, StringSplitOptions.RemoveEmptyEntries);

        int index = goodCombinIndex;
        while (index == goodCombinIndex)
        {
            index = random.Next(0, lettersFile1.Length);
        }

        return new string[] { lettersFile1[index], lettersFile2[index] };
        
    }
}
