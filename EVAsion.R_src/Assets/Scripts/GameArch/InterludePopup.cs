﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterludePopup : MonoBehaviour
{
    [System.Serializable]
    public struct ColorCircles
    {
        public string Game;
        public Color ColorFront;
        public Color ColorMiddle;
        public Color ColorBack;
    };

    [Header("Values")]
    public int DurationProgressBar;

    [Header("Objects")]
    public UIProgressBar GameProgressBar;
    public UISprite CircleProgress;
    public UILabel CombinationLabel;

    [Header("Tween scales")]
    public TweenScale[] TweenScalesApparition;
    public TweenScale[] TweenScalesDisparition;

    [Header("Label and ProgressBar Tween Alpha")]
    public TweenAlpha LabelTweenAlpha;
    public TweenAlpha ProgressBarTweenAlpha;

    [Header("Colors")]
    public ColorCircles[] ColorCirclesByGames;

    private float _progressBarValue;
    private bool _isProgressBarStarted;

    private float _elapsedTime = 0.0f;

    public void StartProgressBar()
    {
        _isProgressBarStarted = true;

        LabelTweenAlpha.ResetToBeginning();
        LabelTweenAlpha.PlayForward();

        ProgressBarTweenAlpha.ResetToBeginning();
        ProgressBarTweenAlpha.PlayForward();

        _elapsedTime = 0.0f;
    }

    void Start()
    {
        GameProgressBar.numberOfSteps = Constants.NbrTotalCycles;

        string game = PlayerPrefs.GetString("gamePlayed", string.Empty);
        Debug.Log("Name  = " + game);

        if (game != string.Empty)
        {
            ColorCircles colorCircles = Array.Find(ColorCirclesByGames, x => x.Game == game);

            TweenScalesApparition[0].GetComponent<UISprite>().color = colorCircles.ColorFront;
            TweenScalesApparition[1].GetComponent<UISprite>().color = colorCircles.ColorMiddle;
            TweenScalesApparition[2].GetComponent<UISprite>().color = colorCircles.ColorBack;

            GameProgressBar.GetComponent<UISprite>().color = 
                new Color(colorCircles.ColorMiddle.r, colorCircles.ColorMiddle.g, colorCircles.ColorMiddle.b, 0.0f);
        }

        FontMgmt.Instance.UpdateInterludePopup(gameObject);
    }

    public void StartTween()
    {
        foreach (TweenScale tweenScale in TweenScalesApparition)
        {
            tweenScale.ResetToBeginning();
            tweenScale.PlayForward();
        }
    }

    private void Update()
    {
        if (_isProgressBarStarted && Time.timeScale > 0)
        {
            _elapsedTime += Time.deltaTime;

            if (_elapsedTime > DurationProgressBar)
            {
                HildeAllContent();

                _isProgressBarStarted = false;
            }

            CircleProgress.fillAmount = 1 - _elapsedTime / DurationProgressBar;
        }
    }

    public void SetCombination(string combination)
    {
        CombinationLabel.text = combination;
    }

    private void HildeAllContent()
    {
        foreach (TweenScale tweenScale in TweenScalesDisparition)
        {
            tweenScale.ResetToBeginning();
            tweenScale.PlayForward();
        }
    }

    internal void UpdateProgressBar(float progressBarValue)
    {
        GameProgressBar.value = progressBarValue;
    }
}
