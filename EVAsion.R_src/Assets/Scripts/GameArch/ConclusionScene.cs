﻿/**********************************************************************
* ConclusionScene.cs
* Created at 6-7-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Video;

public class ConclusionScene : MonoBehaviour
{
    public AudioSource AudioSource;
    public VideoPlayer VideoPlayer;

    [Header("Next scene")]
    public string NextSceneName;

    public TweenAlpha WhiteBackgroundTweenAlpha;

    public float MaxTimeToClick = 0.60f;
    public float MinTimeToClick = 0.05f;

    private float _minCurrentTime;
    private float _maxCurrentTime;

    // Use this for initialization
    void Awake()
    {
        string gameName = PlayerPrefs.GetString("gameName");
        int numConclusion = PlayerPrefs.GetInt("numConclusion", 1);

        //ConclusionVideo conclusionVideo = Array.Find(ConclusionVideos, x => x.GameName == gameName);

        string path = String.Format("Conclusions/{0}/Conclusion0{1}", gameName, numConclusion);
        VideoClip clip = Resources.Load<VideoClip>(path);

        if (clip != null)
        {
            VideoPlayer.clip = clip;

            AudioSource.volume = PlayerPrefs.GetFloat("MusicVolume", 1.0f);

            VideoPlayer.loopPointReached += OnVideoOver;

            VideoPlayer.Play();
            AudioSource.Play();
        }
        else
        {
            Debug.LogError("Error : the clip is null");

            LoadNextScene();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (DoubleClick())
            {
                VideoPlayer.Pause();

                LoadNextScene();
            }
        }
    }

    private void OnVideoOver(VideoPlayer source)
    {
        WhiteBackgroundTweenAlpha.SetOnFinished(LoadNextScene);
        WhiteBackgroundTweenAlpha.PlayForward();
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(NextSceneName);
    }

    public bool DoubleClick()
    {
        if (Time.time >= _minCurrentTime && Time.time <= _maxCurrentTime)
        {
            return true;
        }

        _minCurrentTime = Time.time + MinTimeToClick; _maxCurrentTime = Time.time + MaxTimeToClick;

        return false;
    }
}
