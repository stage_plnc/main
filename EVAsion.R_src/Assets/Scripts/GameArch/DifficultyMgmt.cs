﻿/**********************************************************************
* DifficultyMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class DifficultyMgmt : MonoBehaviour
{
    public TextAsset[] JSONFiles;

    // Associate the name of the game to the gameInfo
    private Dictionary<string, GameDifficultyInfo> _gamesInfos;
    private Dictionary<string, int> _maxSaves;

    private static DifficultyMgmt _instance;

    public static DifficultyMgmt Instance
    {
        get { return _instance; }

    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            _gamesInfos = new Dictionary<string, GameDifficultyInfo>();

            _maxSaves = new Dictionary<string, int>();

            if (!Constants.DebugVersionForLPNC)
            {
                LoadMaxSaves();
            }

            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Debug.LogWarning("[DifficultyMgmt] Instance already exists, destroying it");

            Destroy(this.gameObject);
        }
    }

    private void LoadMaxSaves()
    {
        foreach (string gameName in Constants.GameNamesId)
        {
            TextAsset JSONFile = Array.Find(JSONFiles, x => x.name == gameName);
            string txtFile = JSONFile.text;

            if (txtFile.Length > 0)
            {
                JSONObject content = new JSONObject(txtFile);

                List<JSONObject> games = content.GetField("games").list;

                foreach (JSONObject game in games)
                {
                    _maxSaves[gameName] = (int)game.GetField("max_saves").n;
                }

            }
        }
    }

    /**
     * \brief Use the json file given to load into _gamesInfo informations about the dimensions of the games.
     * \param path The path to a json file.
     */
    public void LoadDifficultyFile(string path, string name)
    {
        string txtFile = string.Empty;

        if (File.Exists(path))
        {
            txtFile = File.ReadAllText(path);

            if (txtFile.Length > 0)
            {
                // If the game is already loaded, we reload it, if the scientists did change the JSON file
                if (_gamesInfos.ContainsKey(name))
                {
                    _gamesInfos.Remove(name);
                }

                LoadDifficultiesFromJSON(new JSONObject(txtFile));
            }
            else
            {
                PrintToScreen.Print("Difficulty Manager : Failed to read difficulty file from System");
            }
        }
        else
        {
            TextAsset JSONFile = Array.Find(JSONFiles, x => x.name == name);
            txtFile = JSONFile.text;

            if (txtFile.Length > 0)
            {
                string directory = string.Format("{0}/Games", Application.persistentDataPath);

                if (_gamesInfos.ContainsKey(name))
                {
                    _gamesInfos.Remove(name);
                }

                LoadDifficultiesFromJSON(new JSONObject(txtFile));

                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }

                File.WriteAllText(path, txtFile);
            }
            else
            {
                PrintToScreen.Print("Difficulty Manager : Failed to read difficulty file from Unity resources...");
            }
        }
        DestroyPotentialOldFiles(name);
    }

    private void DestroyPotentialOldFiles(string gameName)
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] filesInfo = directoryInfo.GetFiles();

        string upToDateFilename = String.Format("{0}_{1}.json", gameName, Application.version);

        foreach (FileInfo file in filesInfo)
        {
            if (file.Name.StartsWith(gameName) && file.Name != upToDateFilename)
            {
                File.Delete(file.FullName);
            }
        }
    }

    /**
     * return The value of associated with the given difficulty from the given dimension for a given game.
     */
    public string GetDifficultyValue(string gameName, string dimensionName, int difficultyLevel)
    {
        if (!_gamesInfos.ContainsKey(gameName))
        {
            PrintToScreen.Print("Difficulty Manager : The game " + gameName + " was not saved.");
        }
        else if (!_gamesInfos[gameName].dimensions.ContainsKey(dimensionName))
        {
            PrintToScreen.Print("Difficulty Manager : The dimensions " + dimensionName + " was not saved with the game " + gameName + ".");
        }
        else if (!_gamesInfos[gameName].dimensions[dimensionName].valuesByDifficulty.ContainsKey(difficultyLevel))
        {
            PrintToScreen.Print("Difficulty Manager : The difficulty level " + difficultyLevel + " was not saved for the dimension " + dimensionName + " with the game " + gameName + ".");
        }
        return _gamesInfos[gameName].dimensions[dimensionName].valuesByDifficulty[difficultyLevel];
    }

    public float GetUPL(string gameName)
    {
        if (_gamesInfos.ContainsKey(gameName))
        {
            return _gamesInfos[gameName].UPL;
        }
        else
        {
            PrintToScreen.Print("Difficulty Manager : Error : UPL not defined for " + gameName + ".");
        }

        return 0.0f;
    }

    public string GetDifficulty(string gameName)
    {
        if (_gamesInfos.ContainsKey(gameName))
        {
            return _gamesInfos[gameName].difficulty;
        }
        else
        {
            PrintToScreen.Print("Difficulty Manager : Error : UPL not defined for " + gameName + ".");
        }

        return "parseval";
    }

    public int MaxSaves(string game)
    {
        int maxSaves = 50;

        if (_maxSaves.ContainsKey(game))
            maxSaves = _maxSaves[game];

        return maxSaves;
    }
    /**
     * brief Parse the json file and save infos in _gamesInfo.
     */
    private void LoadDifficultiesFromJSON(JSONObject JSONFile)
    {
        List<JSONObject> games = JSONFile.GetField("games").list;

        // Build a GameDifficultyInfo from each game of the file
        try
        {
            foreach (JSONObject game in games)
            {
                GameDifficultyInfo gdi = new GameDifficultyInfo()
                {
                    name = game.GetField("game_name").str,
                    UPL = game.GetField("UPL").f,
                    maxSaves = (int)game.GetField("max_saves").n,
                };

                if (game.HasField("nbrCycles"))
                {
                    gdi.nbrCycles = (int)game.GetField("nbrCycles").n;
                    Constants.NbrTotalCycles = gdi.nbrCycles;
                }
                else
                {
                    gdi.nbrCycles = Constants.NbrTotalCycles = Constants.NbrCyclesDefault;
                }

                if (game.HasField("difficulty"))
                {
                    gdi.difficulty = game.GetField("difficulty").str;
                    Constants.DifficultyMethod = gdi.difficulty;
                }
                else
                {
                    gdi.difficulty = "parseval";
                }

                // Checking if the difficulty is linear to set the bounds
                if (gdi.difficulty == "linear")
                {
                    if (game.HasField("startBound") && game.HasField("endBound"))
                    {
                        char[] sepComma = new char[] { ',' };
                        string startBoundStr = game.GetField("startBound").str;
                        string endBoundStr = game.GetField("endBound").str;

                        string[] valuesStartBound = startBoundStr.Split(sepComma, StringSplitOptions.RemoveEmptyEntries);
                        Constants.LinearDifficultyStartBound =
                            new Vector3(int.Parse(valuesStartBound[0]), int.Parse(valuesStartBound[1]), int.Parse(valuesStartBound[2]));

                        string[] valuesEndBound = endBoundStr.Split(sepComma, StringSplitOptions.RemoveEmptyEntries);
                        Constants.LinearDifficultyEndBound =
                            new Vector3(int.Parse(valuesEndBound[0]), int.Parse(valuesEndBound[1]), int.Parse(valuesEndBound[2]));
                    }
                    else
                    {
                        Debug.LogError("Difficulty is linear, but startBound and endBound are not set..");
                        // PrintToScreen.Instance.Print("Difficulty is linear, but startBound and endBound are not set...");
                    }
                }

                List<JSONObject> dimensions = game.GetField("dimensions").list;

                // Build a Dimension from each dimension of the game
                foreach (JSONObject dimension in dimensions)
                {
                    Dimension d = new Dimension()
                    {
                        name = dimension.GetField("dimension_name").str,
                        valuesByDifficulty = new Dictionary<int, string>()
                    };

                    List<JSONObject> dimensionValues = dimension.GetField("dimension_values").list;

                    // Assign difficulty values of the dimension
                    foreach (JSONObject difficultyValue in dimensionValues)
                    {
                        int difficultyLevel = (int)difficultyValue.GetField("difficulty_level").f;
                        string value = difficultyValue.GetField("value").str;
                        d.valuesByDifficulty.Add(difficultyLevel, value);
                    }
                    gdi.dimensions.Add(d.name, d);
                }
                _gamesInfos.Add(gdi.name, gdi);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("[DifficultyMgmt.LoadDifficultiesFromJSON] Exception : " + e.Message);
        }
    }

    // Check if the gamename is loaded. Useful inside GameMgmt, when we don't want the same game to be loaded twice (could cause some duplication error)
    public bool IsGameLoaded(string gameName)
    {
        if (_gamesInfos.Count > 0)
        {
            return _gamesInfos.ContainsKey(gameName);
        }

        return false;
    }


    private class GameDifficultyInfo
    {
        /**
         * \brief Data representation of info about the difficulty of a game.
         */
        public string name;
        public float UPL;
        public int maxSaves;
        public int nbrCycles;
        //public int? nbrEvents;

        // Difficulty can be : "random", "linear", "parseval"
        public string difficulty;

        public Dictionary<string, Dimension> dimensions;

        public GameDifficultyInfo()
        {
            name = "unnamed";
            dimensions = new Dictionary<string, Dimension>();
        }

        public override string ToString()
        {
            string s = "[ " + name;
            s += "Dimensions [";
            foreach (Dimension d in dimensions.Values)
            {
                s += d.ToString();
                s += ", ";
            }
            s += "]]";
            return s;
        }
    }

    private class Dimension
    {
        /**
         * \brief Data reprensentation of a dimension.
         * \example The size of the text for the runner game.
         */
        public string name;
        public Dictionary<int, string> valuesByDifficulty;
        public Dimension()
        {
            name = "unnamed";
            valuesByDifficulty = new Dictionary<int, string>();
        }

        public override string ToString()
        {
            string s = "[ " + name;
            s += "Difficulties : [";
            foreach (KeyValuePair<int, string> pair in valuesByDifficulty)
            {
                s += pair.Key + " : " + pair.Value;
                s += ",";
            }
            s += "]]";
            return s;
        }
    }


}
