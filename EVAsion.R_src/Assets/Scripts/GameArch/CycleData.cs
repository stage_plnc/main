﻿/**********************************************************************
* CycleData.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycleData {

    public string Combination;
    public int NbrGoodCombinations;

    public CycleData(string combination, int nbrGoodCombinations)
    {
        this.Combination = combination;
        this.NbrGoodCombinations = nbrGoodCombinations;
    }
}
