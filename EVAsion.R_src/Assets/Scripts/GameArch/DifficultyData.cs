﻿using System.Linq;

public class DifficultyData
{
    public int DifficultyTarget;
    public int DifficultyDistractor;

    public DifficultyData(int difficultyTarget, int difficultyDistractor)
    {
        DifficultyTarget = difficultyTarget;
        DifficultyDistractor = difficultyDistractor;
    }

    public int GetNumberOfLetters()
    {
        return Constants.CombinationPatterns[DifficultyTarget - 1].Count(x => x == 'A');
    }
}
