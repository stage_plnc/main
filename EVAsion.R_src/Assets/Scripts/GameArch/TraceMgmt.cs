﻿/**********************************************************************
* TraceMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceMgmt : MonoBehaviour
{


    /*
     *  Soon
     */
     
    private static TraceMgmt _instance;
    public static TraceMgmt Instance
    {
        get { return _instance; }
    }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = null;
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
