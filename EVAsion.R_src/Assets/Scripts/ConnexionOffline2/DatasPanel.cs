﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DatasPanel : MonoBehaviour
{
    public enum TypeData
    {
        Learners,
        Groups
    };

    [Header("Type")]
    public TypeData Type;

    [Header("Mgmt")]
    public ConnexionOffline2Mgmt ConnexionOfflineMgmt;

    [Header("Elements")]
    public UILabel SelectedDataLabel;

    public UIButton DeleteButton;
    public UIButton CreateButton;
    public UIButton SelectButton;

    public DataSelector DataSelectorI;

    [Header("Text Assets")]
    public TextAsset DatasJSON;

    [Header("Data")]
    public BasePOD SelectedData;

    private List<BasePOD> _datas;
    private List<GameObject> _datasSelectionGo;

    private JSONObject _datasJsonContent;

    private int _selectedDataId;
    private int _nextDataId;
    private string _datasJsonPath;

    private void Awake()
    {
        _datas = new List<BasePOD>();
        _datasJsonPath = string.Format("{0}/{1}.json", Application.persistentDataPath, Type.ToString());

        LoadDatas();

        _datasSelectionGo = new List<GameObject>();
    }

    private void Start()
    {
        DeleteButton.GetComponent<BoxCollider>().enabled = false;

        UIEventListener.Get(DeleteButton.gameObject).onClick = ClickOnDeleteSelection;
        UIEventListener.Get(CreateButton.gameObject).onClick = ClickOnCreate;
        UIEventListener.Get(SelectButton.gameObject).onClick = ClickOnSelection;
        //EventDelegate.Add(DatasPopupList.onChange, PopupListOnChanged);
    }

    #region Common

    public void LoadDatas()
    {
        string content = string.Empty;

        if (File.Exists(_datasJsonPath))
        {
            content = File.ReadAllText(_datasJsonPath);
        }
        else
        {
            content = DatasJSON.text;

            File.WriteAllText(_datasJsonPath, content);
        }

        _datasJsonContent = new JSONObject(content);

        if (Type == TypeData.Groups)
        {
            List<JSONObject> groupsJson = _datasJsonContent.GetField("groups").list;

            foreach (JSONObject groupJson in groupsJson)
            {
                Group group = new Group(groupJson);
                _datas.Add(group);
            }
        }
        else if (Type == TypeData.Learners)
        {
            List<JSONObject> learnersJson = _datasJsonContent.GetField("learners").list;

            foreach (JSONObject learnerJson in learnersJson)
            {
                Learner learner = new Learner(learnerJson);
                _datas.Add(learner);
            }
        }
        if (_datas.Count == 0)
        {
            //DataSelectorI.SetEnabled(false);
            DeleteButton.isEnabled = false;
            SelectButton.isEnabled = false;
        }
        else
        {
            _datas.Sort(delegate (BasePOD x, BasePOD y)
            {
                return x.Id.CompareTo(y.Id);
            });

            foreach (BasePOD basePod in _datas)
                DataSelectorI.AddItem(basePod.Id.ToString());

            //if (Type == TypeData.Groups)
            //    DataSelectorI.SetEnabled(false);
        }
    }

    private void ClickOnSelection(GameObject go)
    {
        DataSelectorI.SetEnabled(!DataSelectorI.IsEnabled);
    }

    public void SelectionUpdated(int selectedId)
    {
        _selectedDataId = selectedId;

        DeleteButton.isEnabled = true;

        SelectedData = _datas.Find(x => x.Id == _selectedDataId);
        SelectedDataLabel.text = _selectedDataId.ToString();

        if (Type == TypeData.Groups)
        {
            Group selectedGroup = (Group)SelectedData;
            ConnexionOfflineMgmt.SetLearnersFromGroup(selectedGroup);
        }
        else
        {
            Learner selectedLearner = (Learner)SelectedData;

            ConnexionOfflineMgmt.SetConnexionButtonEnabled(true);
        }
    }

    private void DeleteSelection(GameObject go = null)
    {
        _datas.Remove(_datas.Find(x => x.Id == _selectedDataId));
        DataSelectorI.RemoveItem(_selectedDataId.ToString());

        if (Type == TypeData.Groups)
        {
            ConnexionOfflineMgmt.DeleteAllLearnersFromGroup(_selectedDataId);
        }
        else if (Type == TypeData.Learners)
        {
            ConnexionOfflineMgmt.DeleteLearnerInCurrentGroup(_selectedDataId);

            DeleteSaveFile(ConnexionOfflineMgmt.GroupsDatasPanel.SelectedData.Id, _selectedDataId);
        }

        DataSelectorI.SetEnabled(false);
        _selectedDataId = -1;

        DeleteButton.isEnabled = false;

        if (Type == TypeData.Groups)
        {
            ConnexionOfflineMgmt.LearnersDatasPanel.ClearAndLockEverything();
        }

        if (DataSelectorI.ItemsCount == 0)
        {
            SelectButton.isEnabled = false;
        }

        SelectedDataLabel.text = string.Empty;

        ConnexionOfflineMgmt.SetConnexionButtonEnabled(false);

        UpdateJSONFile();
    }

    private void ClickOnDeleteSelection(GameObject go)
    {
        string text =
            string.Format("Voulez-vous supprimer {0} avec ID = {1}", Type == TypeData.Groups ? "le groupe" : "l'élève", _selectedDataId);

        EventDelegate.Callback yesCallback = delegate () { DeleteSelection(); };

        ConnexionOfflineMgmt.ShowConfirmationPopup(text, yesCallback, DeleteButton.transform.localPosition);
    }

    private void CreateData()
    {
        DataSelectorI.AddItem(_nextDataId.ToString());

        if (Type == TypeData.Groups)
        {
            _datas.Add(new Group(_nextDataId));
        }
        else
        {
            _datas.Add(new Learner(_nextDataId, ConnexionOfflineMgmt.GroupsDatasPanel.SelectedData.Id));

            ConnexionOfflineMgmt.AddLearnerInCurrentGroup(_nextDataId);
        }

        // Meaning there were no datas, there is one now
        if (DataSelectorI.ItemsCount == 1)
        {
            SelectButton.isEnabled = true;
        }

        UpdateJSONFile();
    }

    private void ClickOnCreate(GameObject go)
    {
        _nextDataId = 1;

        if (_datas.Count == 1)
        {
            _nextDataId = _datas[0].Id + 1;
        }
        else if (_datas.Count >= 2)
        {
            _datas.Sort(delegate (BasePOD x, BasePOD y)
            {
                return y.Id.CompareTo(x.Id);
            });

            _nextDataId = _datas[0].Id + 1;
        }

        string text =
    string.Format("Voulez-vous créer {0} avec ID = {1}", Type == TypeData.Groups ? "un nouveau groupe" : "un nouvel élève", _nextDataId);

        EventDelegate.Callback yesCallback = delegate () { CreateData(); };

        ConnexionOfflineMgmt.ShowConfirmationPopup(text, yesCallback, CreateButton.transform.localPosition);
    }

    #endregion Common

    public void ClearAndLockEverything()
    {
        DataSelectorI.ClearItems();

        SelectButton.isEnabled = false;
        DeleteButton.isEnabled = false;
        CreateButton.isEnabled = false;
    }

    public void SetLearnersFromGroup(Group group)
    {
        DataSelectorI.ClearItems();
        SelectedDataLabel.text = string.Empty;

        if (group.LearnersId.Count > 0)
        {
            foreach (int idLearner in group.LearnersId)
            {
                BasePOD basePod = _datas.Find(x => x.Id == idLearner);

                DataSelectorI.AddItem(basePod.Id.ToString());
            }

            SelectButton.isEnabled = true;
        }
        else
        {
            SelectButton.isEnabled = false;
            DeleteButton.isEnabled = false;
            SelectedDataLabel.text = string.Empty;

            ConnexionOfflineMgmt.SetConnexionButtonEnabled(false);
        }

        DataSelectorI.SetEnabled(false);
        CreateButton.isEnabled = true;
    }

    public void AddLearnerInCurrentGroup(int learnerId)
    {
        List<BasePOD> newDatas = new List<BasePOD>();

        foreach (BasePOD data in _datas)
        {
            Group group = (Group)data;

            if (group.Id == _selectedDataId)
            {
                group.LearnersId.Add(learnerId);
            }

            newDatas.Add(group);
        }

        _datas = newDatas;

        UpdateJSONFile();
    }

    public void Func()
    {
    }

    public void DeleteLearnerInGroup(int learnerId)
    {
        List<BasePOD> newDatas = new List<BasePOD>();

        foreach (BasePOD data in _datas)
        {
            Group group = (Group)data;

            if (group.LearnersId.Contains(learnerId))
            {
                group.LearnersId.Remove(learnerId);
            }

            newDatas.Add(group);
        }

        DataSelectorI.SetEnabled(false);

        _datas = newDatas;

        UpdateJSONFile();
    }

    public void DeleteLearnersFromGroups(int idGroup)
    {
        List<BasePOD> newDatas = new List<BasePOD>();

        foreach (BasePOD data in _datas)
        {
            Learner learner = (Learner)data;

            if (learner.GroupId == idGroup)
            {
                DeleteSaveFile(idGroup, learner.Id);
            }
            else
            {
                newDatas.Add(data);
            }
        }

        DataSelectorI.SetEnabled(false);
        SelectedDataLabel.text = string.Empty;

        _datas = newDatas;

        UpdateJSONFile();
    }

    #region Files

    private void UpdateJSONFile()
    {
        File.WriteAllText(_datasJsonPath, DatasToJSON(_datas, Type).ToString(true));
    }

    private void DeleteSaveFile(int groupId, int learnerId)
    {
        string directoryPath = string.Format("{0}/Players", Application.persistentDataPath);

        if (Directory.Exists(directoryPath))
        {
            string name = string.Format("Group{0}_Id{1}", groupId, learnerId);
            string path = string.Format("{0}/{1}_Save.json", directoryPath, name);

            Debug.Log("Deleting " + path);

            File.Delete(path);
        }
    }

    private static JSONObject DatasToJSON(List<BasePOD> datas, TypeData type)
    {
        JSONObject jsonObject = new JSONObject(JSONObject.Type.OBJECT);

        if (type == TypeData.Groups)
        {
            JSONObject groupsJson = new JSONObject(JSONObject.Type.ARRAY);

            foreach (Group group in datas)
            {
                groupsJson.Add(group.ToJSON());
            }

            jsonObject.AddField("groups", groupsJson);
        }
        else if (type == TypeData.Learners)
        {
            JSONObject learnersJson = new JSONObject(JSONObject.Type.ARRAY);

            foreach (Learner learner in datas)
            {
                learnersJson.Add(learner.ToJSON());
            }

            jsonObject.AddField("learners", learnersJson);
        }
        return jsonObject;
    }

    #endregion Files
}