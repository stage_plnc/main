﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSelector : MonoBehaviour
{
    [Header("Mgmt")]
    public DatasPanel DatasPanelI;

    [Header("Elements")]
    public UIGrid Grid;

    [Header("Prefab")]
    public GameObject NumberGoPrefab;

    private List<GameObject> _items = new List<GameObject>();

    public int ItemsCount { get { return _items.Count; } }
    public string FirstItem
    {
        get
        {
            if (_items.Count > 0)
                return _items[0].GetComponentInChildren<UILabel>().text;
            return string.Empty;
        }
    }

    public bool IsEnabled { get { return GetComponent<UIWidget>().alpha == 1.0f; } }

    public void Fill(List<BasePOD> datas)
    {
        for (int i = 0; i < datas.Count; i++)
        {
            AddItem(datas[i].Id.ToString());
        }
    }

    private void ClickOnSelection(GameObject go)
    {
        int numId = int.Parse(go.GetComponentInChildren<UILabel>().text);

        DatasPanelI.SelectionUpdated(numId);

        SetEnabled(false);
    }

    public void SetEnabled(bool enabled)
    {
        GetComponent<UIWidget>().alpha = enabled ? 1.0f : 0.0f;
    }

    public void AddItem(string item)
    {
        GameObject go = NGUITools.AddChild(Grid.gameObject, NumberGoPrefab);
        go.GetComponentInChildren<UILabel>().text = item;

        _items.Add(go);

        UIEventListener.Get(go).onClick = ClickOnSelection;

        Grid.Reposition();
    }

    public void RemoveItem(string itemStr)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            string label = _items[i].GetComponentInChildren<UILabel>().text;

            if (label == itemStr)
            {
                NGUITools.Destroy(_items[i]);
                _items.Remove(_items[i]);
            }
        }

        Grid.Reposition();
    }

    public void ClearItems()
    {
        while (Grid.transform.childCount > 0)
            NGUITools.Destroy(Grid.GetChild(0).gameObject);

        _items.Clear();
    }
}
