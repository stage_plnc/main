﻿using UnityEngine;

public class ConfirmationPopup : MonoBehaviour
{
    [Header("Elements")]
    public UIWidget RootWidget;

    public UILabel ContentLabel;
    public GameObject NoButton;
    public GameObject YesButton;
    public GameObject CirclesRoot;

    [Header("Values")]
    public int YShifting = 150;

    private EventDelegate.Callback _clickOnYesCallback = null;

    private void Start()
    {
        EventDelegate.Callback myDelegate = delegate () { Hide(); };

        EventDelegate.Add(YesButton.GetComponent<UIButton>().onClick, myDelegate);
        UIEventListener.Get(NoButton).onClick = Hide;
    }

    public void Show(string text, EventDelegate.Callback clickOnYesCallback, Vector3 pos)
    {
        if (_clickOnYesCallback != null)
            EventDelegate.Remove(YesButton.GetComponent<UIButton>().onClick, _clickOnYesCallback);

        RootWidget.alpha = 1.0f;
        ContentLabel.text = text;

        _clickOnYesCallback = clickOnYesCallback;
        EventDelegate.Add(YesButton.GetComponent<UIButton>().onClick, _clickOnYesCallback);

        CirclesRoot.transform.localPosition = pos + YShifting * (pos.y > 0 ? Vector3.down : Vector3.up);
    }

    public void Hide(GameObject go = null)
    {
        RootWidget.alpha = 0.0f;
    }
}