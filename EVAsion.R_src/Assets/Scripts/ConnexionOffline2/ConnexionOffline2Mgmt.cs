﻿using UnityEngine;
using UnityEngine.SceneManagement;

public delegate void ConfirmationYesBtnDelegate(GameObject go);

public class ConnexionOffline2Mgmt : MonoBehaviour
{
    [Header("Panels")]
    public DatasPanel GroupsDatasPanel;

    public DatasPanel LearnersDatasPanel;

    [Header("Popup")]
    public ConfirmationPopup ConfirmationPopupI;

    [Header("Buttons")]
    public GameObject ConnexionButton;

    private UIButton _connexionButtonUIButton;

    private void Awake()
    {
        _connexionButtonUIButton = ConnexionButton.GetComponent<UIButton>();
    }

    private void Start()
    {
        UIEventListener.Get(ConnexionButton).onClick = ClickOnConnexion;

        SetConnexionButtonEnabled(false);
    }

    public void SetLearnersFromGroup(Group group)
    {
        LearnersDatasPanel.SetLearnersFromGroup(group);
    }

    public void GroupHasBeenDeleted()
    {
        LearnersDatasPanel.ClearAndLockEverything();
    }

    internal void DeleteAllLearnersFromGroup(int idGroup)
    {
        LearnersDatasPanel.ClearAndLockEverything();

        LearnersDatasPanel.DeleteLearnersFromGroups(idGroup);
    }

    public void DeleteLearnerInCurrentGroup(int learnerId)
    {
        GroupsDatasPanel.DeleteLearnerInGroup(learnerId);
    }

    public void AddLearnerInCurrentGroup(int learnerId)
    {
        GroupsDatasPanel.AddLearnerInCurrentGroup(learnerId);
    }

    public void ShowConfirmationPopup(string content, EventDelegate.Callback yesFunction, Vector3 pos)
    {
        ConfirmationPopupI.Show(content, yesFunction, pos);
    }

    private void ClickOnConnexion(GameObject go)
    {
        int groupId = GroupsDatasPanel.SelectedData.Id;
        int learnerId = LearnersDatasPanel.SelectedData.Id;

        PlayerPrefs.SetInt("groupId", groupId);
        PlayerPrefs.SetInt("learnerId", learnerId);

        Debug.LogError("GroupId = " + groupId);
        Debug.LogError("LearnerId = " + learnerId);

        SceneManager.LoadScene("CinematicScene");
    }

    public void SetConnexionButtonEnabled(bool enabled)
    {
        if (_connexionButtonUIButton.isEnabled != enabled)
            _connexionButtonUIButton.isEnabled = enabled;
    }
}