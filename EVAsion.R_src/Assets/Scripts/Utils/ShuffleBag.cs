﻿/**********************************************************************
* ShuffleBag.cs
* Created at 16-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A bag that can give you a random element of it, but never one that was already given before, until all elements were given.
/// Inspired by https://gamedevelopment.tutsplus.com/tutorials/shuffle-bags-making-random-feel-more-random--gamedev-1249
/// </summary>
/// <typeparam name="T">The type of the data contained ine the bag. </typeparam>
public class ShuffleBag<T>
{
    /// <summary>
    /// Create a bag containing all elements of the given list.
    /// Use a default random generator.
    /// </summary>
    /// <param name="listOfData">The list from which take elements.</param>
    public ShuffleBag(IList<T> listOfData)
    {
        list = new List<T>(listOfData);

        currentPosition = list.Count - 1;

        random = new System.Random();
    }

    /// <summary>
    /// Create a bag containing all elements of the given list.
    /// Use the given random generator to randomly choose.
    /// </summary>
    /// <param name="listOfData">The list from which take elements.</param>
    /// <param name="randomGenerator">The random generator to use.</param>
    public ShuffleBag(IList<T> listOfData, System.Random randomGenerator) : this(listOfData)
    {
        random = randomGenerator;
    }

    private System.Random random = new System.Random(); //< The random generator to use
    private T currentItem; //< The last item returned by Next
    private int currentPosition = -1; //< The position of the last item returned by Nets
    private IList<T> list; //< The list containing all elements.

    /// <summary>
    /// Returns a random element contained in the bag.
    /// The element will be different from the elements returned before, 
    /// until all elements of the bag are returned.
    /// </summary>
    /// <param name="randomGenerator">The random generator to use.</param>
    /// <returns></returns>
    public T Next(System.Random randomGenerator)
    {
        if (currentPosition < 1)
        {
            currentPosition = list.Count - 1;
            currentItem = list[currentPosition];

            return currentItem;
        }

        int pos = randomGenerator.Next(currentPosition);

        currentItem = list[pos];
        list[pos] = list[currentPosition];
        list[currentPosition] = currentItem;
        currentPosition--;

        return currentItem;
    }

    /// <summary>
    /// Returns a random element contained in the bag.
    /// The element will be different from the elements returned before, 
    /// until all elements of the bag are returned.
    /// Use a default random generator.
    /// </summary>
    public T Next()
    {
        return Next(random);
    }
}
