﻿/**********************************************************************
* ShadeVariation.cs
* Created at 29-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShadeVariation : MonoBehaviour
{
    [Range(0, 255)]
    public int Variation;
    public bool NegativeVariation;


    void Start()
    {
        UITexture texture = this.GetComponent<UITexture>();
        UISprite sprite = this.GetComponent<UISprite>();

        if (texture == null && sprite == null)
            Debug.LogError("ShadeVariation : " + this.name + " sprite or texture not found.");
        else if (texture != null && sprite != null)
            Debug.LogError("ShadeVariation : " + this.name + " sprite AND texture are both found.");
        else
        {
            Color32 baseColor;
            if (sprite != null)
                baseColor = sprite.color;
            else
                baseColor = texture.color;
            int varRand = Random.Range(0, Variation);

            if (NegativeVariation)
                varRand *= -1;

            baseColor.r -= (byte)varRand;
            baseColor.g -= (byte)varRand;
            baseColor.b -= (byte)varRand;

            if (sprite != null)
                sprite.color = baseColor;
            else
                texture.color = baseColor;
        }

    }
}
