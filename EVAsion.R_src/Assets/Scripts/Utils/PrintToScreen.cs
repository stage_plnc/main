﻿/**********************************************************************
* PrintToScreen.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintToScreen : MonoBehaviour
{
    private static PrintToScreen _instance;
    public static PrintToScreen Instance
    {
        get { return _instance; }
    }

    public UILabel MessageLabel;
    public UITweener AppatitionTween;
    public float MessageDuration = 6.0f;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogError("PrintToScreen : trying to recreate an instance.");
        }
    }

    void OnDestroy()
    {
        _instance = null;
    }

    public static void Print(string msg, float duration = 0.0f)
    {
        if (_instance != null)
        {
            PrintToScreen.Instance.MessageLabel.text = msg;
            PrintToScreen.Instance.CancelInvoke("EndTween");
            if (duration != 0)
            {
                PrintToScreen.Instance.Invoke("EndTween", duration);
            }
            else
            {
                PrintToScreen.Instance.Invoke("EndTween", PrintToScreen.Instance.MessageDuration);
            }
            PrintToScreen.Instance.AppatitionTween.PlayForward();
        }
        else
            Debug.LogError("PrintToScreen : not instanciated, message : " + msg);
    }

    private void EndTween()
    {
        AppatitionTween.PlayReverse();
    }
}
