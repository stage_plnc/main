﻿/**********************************************************************
* SetRenderQueue.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SetRenderQueue : MonoBehaviour
{
    /// <summary>
    /// Allow to change the render queue of the entire GameObject (children are affected).
    /// </summary>
    /// <remarks>
    /// Create new materials at start to not alter original materials.
    /// </remarks>

    private int _renderQueue;

    [Tooltip("The value of the render queue.")]
    public int RenderQueue = 3000; // To allow change the value in the inspector 

    private List<Material> m_MaterialList = null;

    private void Awake()
    {
        m_MaterialList = new List<Material>();
    }


    private void Start()
    {
        LoadMaterial();
        LoadChildrenMaterial();
    }

    private void Update()
    {
        // Change the Render Queue value only if the it was changed
        if (_renderQueue != RenderQueue)
        {
            _renderQueue = RenderQueue;
            for (int i = 0; i < m_MaterialList.Count; i++)
            {
                m_MaterialList[i].renderQueue = _renderQueue;
            }
        }
    }


#if !UNITY_EDITOR
 
 
        private void OnDestroy()
        {
            if (m_MaterialList.Count > 0)
            {
                for (int i = (m_MaterialList.Count - 1); i >= 0; i--)
                {
                    Destroy(m_MaterialList[i]);
                }
            }
        }
 
 
#endif


    private void LoadMaterial()
    {
        var render = GetComponent<Renderer>();

        if (render == null)
        {
            var particleSystem = GetComponent<ParticleSystem>();

            if (particleSystem != null)
            {
                render = particleSystem.GetComponent<Renderer>();
            }
        }

        if (render != null)
        {
            var material = new Material(render.sharedMaterial);
            material.renderQueue = RenderQueue;
            render.material = material;

            m_MaterialList.Add(material);
        }
    }


    private void LoadChildrenMaterial()
    {
        var render = GetComponent<Renderer>();

        foreach (var child in gameObject.GetComponentsInChildren<Transform>())
        {
            render = child.GetComponent<Renderer>();

            if (render == null)
            {
                var particleSystem = child.GetComponent<ParticleSystem>();

                if (particleSystem != null)
                {
                    render = particleSystem.GetComponent<Renderer>();
                }
            }

            

            if (render != null)
            {
#warning That is temporary! My Player prefab is broken, I can't change the material of the hidden bones :(
                if (!render.name.StartsWith("hiddenBone"))
                {
                    var material = new Material(render.sharedMaterial);
                    material.renderQueue = RenderQueue;
                    render.material = material;

                    m_MaterialList.Add(material);
                }
            }
        }
    }
}