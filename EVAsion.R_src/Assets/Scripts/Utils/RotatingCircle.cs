﻿/**********************************************************************
* RotatingCircle.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCircle : MonoBehaviour
{
    public float TimeToMakeACompleteRotation;
    public bool AdjustRotationToDirection = false;
    public float zRotation = 360;

    private float _timer;
    private float _startTime;
    private bool _isRotating = false;
    private int _direction = 1;
    private Quaternion _baseRotation;

    public void MakeCompleteRotation(float duration, int direction)
    {
        _timer = Time.time;
        _startTime = Time.time;
        _isRotating = true;
        TimeToMakeACompleteRotation = duration;
        _direction = direction;
        if (AdjustRotationToDirection)
        {
            _baseRotation = Quaternion.Euler(new Vector3(0, 0, direction * 90));
        }
    }

    private void Update()
    {
        if (_isRotating)
        {
            _timer = Time.time - _startTime;
            transform.localRotation = Quaternion.Euler(Vector3.Lerp(_baseRotation.eulerAngles, new Vector3(transform.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, zRotation * _direction + _baseRotation.eulerAngles.z), _timer / TimeToMakeACompleteRotation));
            if (_timer > TimeToMakeACompleteRotation)
                _isRotating = false;
        }
    }
}
