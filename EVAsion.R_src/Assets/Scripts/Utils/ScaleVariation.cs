﻿/**********************************************************************
* ScaleVariation.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleVariation : MonoBehaviour
{
    public Vector2 ScaleXRange;
    public Vector2 ScaleYRange;
    public bool IsProportionnal = false;

    void Start()
    {
        if (IsProportionnal)
        {   
            float scale = Random.Range(ScaleXRange.x, ScaleXRange.y);
            transform.localScale = new Vector2(scale, scale);
        }
        else
        {
            float scaleX = Random.Range(ScaleXRange.x, ScaleXRange.y);
            float scaleY = Random.Range(ScaleYRange.x, ScaleYRange.y);
            Vector2 newScale = new Vector2(scaleX, scaleY);
            transform.localScale = newScale;
        }
    }

}
