﻿/**********************************************************************
* FollowOnXAxis.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowOnXAxis : MonoBehaviour
{
    public Transform Target;
    public float Damping;

    private Vector2 _offset = Vector2.zero;

    void Update()
    {
        // If the game status is APPARITION_PLAYER, we don't wxant the camera to move
        if (RunnerMgmt.Instance.IsGameRunning())
        {
            if (_offset == Vector2.zero)
            {
                _offset = transform.position - Target.transform.position;
            }

            transform.position = Vector2.Lerp(transform.position, new Vector2(Target.transform.position.x + _offset.x, transform.position.y), Damping * Time.deltaTime);
        }
    }
}
