﻿/**********************************************************************
* CustomUISpriteAnimation.cs
* Created at 30-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;

public class CustomUISpriteAnimation : UISpriteAnimation
{
    public bool IgnoreTimeScale { get { return _ignoreTimeScale; } set { _ignoreTimeScale = value; } }

    [HideInInspector] [SerializeField] protected bool _ignoreTimeScale;

	// Update is called once per frame
	protected override void Update ()
    {
        if ((!IgnoreTimeScale && Time.timeScale != 0) || IgnoreTimeScale)
        {
            base.Update();
        }
	}
}
