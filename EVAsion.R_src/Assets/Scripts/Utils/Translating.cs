﻿/**********************************************************************
* Translating.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translating : MonoBehaviour {

    public Vector3 Translation;
    public float Speed;

	void Update () {
        transform.localPosition += Translation * Speed * Time.deltaTime;
	}
}
