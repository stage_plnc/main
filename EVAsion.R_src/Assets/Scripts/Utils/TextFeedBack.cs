﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextFeedBack : MonoBehaviour
{
    public Font LabelFont;
    public int FontSize = 32;

    [Header("Global Scale")]
    public Vector3 GFrom;
    public Vector3 GTo;
    public float GDuration;
    public AnimationCurve GCurve;
    public Color CaracteresColor;

    [Header("Caracteres motions")]
    public float CaracteresSpacing = 2.0f;
    public float CaracteresFlying = 128.0f;
    public float CaracteresPlaceFactor = 1.0f;
    public float CaracteresRotation = 7.5f;
    public float CaracteresOutline = 4.0f;
    public Vector3 CaracteresScaleTo;
    public AnimationCurve AlphaCurve;
    public AnimationCurve PositionRepartition;

    public void Init(string str)
    {
        char[] charArray = str.ToCharArray();

        int idx = 0;
        foreach (char c in charArray)
        {
            GameObject tmpGO = NGUITools.AddChild(this.gameObject);
            CreateLabel(tmpGO, c, idx, str.Length);
            idx++;
        }

        AddGeneralScale();

    }

    void CreateLabel(GameObject go, char c, int idx, int strLenght)
    {
        UILabel label = go.AddComponent<UILabel>();
        Vector3 pos = Vector3.zero;

        label.trueTypeFont = LabelFont;
        label.text = "" + c;
        label.name = "C_" + c;
        label.fontSize = FontSize;
        label.effectStyle = UILabel.Effect.Outline;
        label.effectDistance = new Vector2(1, 1);
        //label.effectDistance = Vector2.one * CaracteresOutline;
        label.color = CaracteresColor;

        pos.x = idx * FontSize - (strLenght * FontSize) / 2;
        label.transform.localPosition = pos;

        label.depth = 500;

        AddCaractereTweenPos(go, idx, strLenght);
        AddCaractereTweenRot(go, idx, strLenght);
        AddCaractereTweenScale(go, idx, strLenght);
        AddCaractereTweenAlpha(go, idx, strLenght);
    }

    void AddGeneralScale()
    {
        TweenScale tmpT = this.gameObject.AddComponent<TweenScale>();
        tmpT.from = GFrom;
        tmpT.to = GTo;
        tmpT.duration = GDuration;
        tmpT.animationCurve = GCurve;
    }

    void AddCaractereTweenPos(GameObject go, int idx, int strLength)
    {
        float tr = (strLength % 2 == 0) ? idx + 0.5f : idx;

        if (strLength == 1)
            tr = 0.5f;

        float factor = Mathf.Abs((tr) - (strLength / 2));

        if (strLength == 1)
            factor = 0;

        TweenPosition tweenP = go.AddComponent<TweenPosition>();
        Vector3 to = go.transform.localPosition * CaracteresSpacing;

        if (strLength == 1)
            to = Vector3.zero;

        factor = ((strLength / 2) - factor) / ((strLength - 0.5f) / 2);
        factor = PositionRepartition.Evaluate(factor);

        if (strLength == 1)
            factor = 0;

        to.y += (factor * CaracteresFlying);
        tweenP.SetStartToCurrentValue();
        tweenP.to = to;
        tweenP.duration = GDuration;
        tweenP.animationCurve = GCurve;
    }

    void AddCaractereTweenRot(GameObject go, int idx, int strLength)
    {
        float tr = (strLength % 2 == 0) ? idx + 0.5f : idx;
        float factor = (tr) - (strLength / 2);
        Vector3 eulerTo = Vector3.zero;
        TweenRotation tmpT = go.AddComponent<TweenRotation>();

        eulerTo.z = -factor * CaracteresRotation;
        tmpT.SetStartToCurrentValue();
        tmpT.to = eulerTo;
        tmpT.duration = GDuration;
        tmpT.animationCurve = GCurve;
    }
    void AddCaractereTweenScale(GameObject go, int idx, int strLength)
    {
        TweenScale tweenS = go.AddComponent<TweenScale>();
        tweenS.SetStartToCurrentValue();
        tweenS.to = CaracteresScaleTo;
        tweenS.duration = GDuration;
        tweenS.animationCurve = GCurve;
    }

    void AddCaractereTweenAlpha(GameObject go, int idx, int strLength)
    {
        TweenAlpha tweenA = go.AddComponent<TweenAlpha>();
        tweenA.SetStartToCurrentValue();
        tweenA.to = 0.0f;
        tweenA.duration = GDuration;
        tweenA.animationCurve = AlphaCurve;
    }
}
