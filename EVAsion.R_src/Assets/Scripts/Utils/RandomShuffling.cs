﻿/**********************************************************************
* RandomShuffling.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using System;

public class RandomShuffling
{

    public static T[] Shuffle<T>(T[] OriginalArray, Random random = null)
    {
        SortedList matrix = new SortedList();

        if (random == null)
            random = new Random();

        for (var x = 0; x <= OriginalArray.GetUpperBound(0); x++)
        {
            var i = random.Next();

            while (matrix.ContainsKey(i))
            {
                i = random.Next();
            }

            matrix.Add(i, OriginalArray[x]);
        }

        var OutputArray = new T[OriginalArray.Length];

        matrix.Values.CopyTo(OutputArray, 0);

        return OutputArray;
    }

    public static T[] Shuffle2<T>(T[] array, Random random = null)
    {
        if (random == null)
            random = new Random();

        int n = array.Length;
        while (n > 1)
        {
            int k = random.Next(n--);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }

        return array;
    }

}
