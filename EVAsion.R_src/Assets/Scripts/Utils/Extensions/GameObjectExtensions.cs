﻿/**********************************************************************
* GameObjectExtensions.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions {

    public static T GetRequiredComponent<T>(this GameObject obj) where T : MonoBehaviour
    {
        T component = obj.GetComponent<T>();

        if (component == null)
        {
            Debug.LogError("Expected to find component of type "
               + typeof(T) + " but found none", obj);
        }

        return component;
    }

    public static T GetRequiredComponentInChildren<T>(this GameObject obj) where T : MonoBehaviour
    {
        T component = obj.GetComponentInChildren<T>();

        if (component == null)
        {
            Debug.LogError("Expected to find component of type "
               + typeof(T) + " in children but found none", obj);
        }

        return component;
    }
}
