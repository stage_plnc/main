﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CellInfo))]
public class LetterEgg : MonoBehaviour
{
    public enum LetterEggStatus
    {
        NOT_APPEAREAD,
        WILL_APPEAR,
        MOVING,
        ARRIVED_AT_DESTINATION,
        WILL_DISAPPEAR,
        DISAPPEARED
    };

    [Header("Graphics")]
    public UISprite Sprite;
    public UILabel Label;

    [Header("Status & infos")]
    public LetterEggStatus Status = LetterEggStatus.NOT_APPEAREAD;
    public int TurnApparition;
    public int MaxDepth = 100;
    public int MaxTurnBlocked = 3;
    public bool IsGoodCombination;

    [Header("Feedback System")]
    public GameObject FeedbackBadPrefab;
    public GameObject FeedbackGoodPrefab;
    public GameObject FeedbackMissedPrefab;

    [Header("TweenAlphas")]
    public TweenAlpha TweenAlphaApparition;
    public TweenAlpha TweenAlphaObstacle;

    private CellInfo _cellInfo;

    private List<GridCell> _path = new List<GridCell>();
    private int _currentPathIdx;

    private TweenPosition _tweenPositionJump;
    private bool _isMissed = false;

    private int _isBlockedByObstacleSinceTurn = 0;
    private bool _isBlockedByLetter = false;
    private bool _willRestartBecauseBlockedByEgg = false;

    public int Speed
    {
        get { return (int)_cellInfo.Speed; }
        set { _cellInfo.Speed = (CellInfo.TypeSpeed)value; }
    }

    private void Awake()
    {
        _cellInfo = GetComponent<CellInfo>();
        _tweenPositionJump = GetComponent<TweenPosition>();

        _currentPathIdx = 0;
        _isBlockedByObstacleSinceTurn = -1;

        _cellInfo.Speed = (CellInfo.TypeSpeed)UnityEngine.Random.Range(1, 4);
    }

    public void ReadyToMove()
    {
        // We suppose that the path is set
        _cellInfo.SetInitialCell(_path[0]);
        _cellInfo.PrepareMoveTo(_path[1]);

        TowerDefenseMgmt.Instance.OnNextTurn += OnNextTurn;
        TowerDefenseMgmt.Instance.OnEndCycle += HideAndDestroy;
    }

    private void OnEnable()
    {
        // Subscribe to events moved to start.
    }

    private void OnDisable()
    {
        // Unsuscribe events
        TowerDefenseMgmt.Instance.OnNextTurn -= OnNextTurn;
        TowerDefenseMgmt.Instance.OnEndCycle -= HideAndDestroy;

        TowerDefenseMgmt.Instance.ShowLetters -= ShowLetter;
        TowerDefenseMgmt.Instance.HideLetters -= HideLetter;
    }


    private void Start()
    {
        TowerDefenseMgmt.Instance.ShowLetters += ShowLetter;
        TowerDefenseMgmt.Instance.HideLetters += HideLetter;
    }

    private void HideLetter()
    {
        Label.alpha = 0.0f;
    }

    public void ShowLetter()
    {
        Label.alpha = 1.0f;
    }

    private void OnNextTurn()
    {
        Sprite.depth = MaxDepth - (_path.Count - _currentPathIdx) * 2;
        Label.depth = MaxDepth - (_path.Count - _currentPathIdx) * 2 + 1;

        if (Status == LetterEggStatus.NOT_APPEAREAD)
        {
            if (TowerDefenseMgmt.Instance.CurrentTurn == TurnApparition)
            {
                ShowSprite();

                Status = LetterEggStatus.MOVING;
            }
        }

        else if (Status == LetterEggStatus.MOVING)
        {
            // He's not at the end of its path
            if (_currentPathIdx + 1 < _path.Count)
            {
                GridCell nextCell = _path[_currentPathIdx + 1];

                //if (_isBlockedByLetter)
                //{
                //    _isBlockedByLetter = false;
                //}
                //else
                {
                    // There is nothing in front of the letter
                    if (TowerDefenseMgmt.Instance.MapMgmt.HasObstacleAt(nextCell) == false && _isBlockedByLetter == false)
                    {
                        // If the letter was blocked by an obstacle
                        if (_isBlockedByObstacleSinceTurn >= 0)
                        {
                            StopBlockedAnimation();
                            _isBlockedByObstacleSinceTurn = -1;
                        }

                        _cellInfo.AdvanceInCell();

                        // Returns true if the cell is walkable
                        if (_cellInfo.CheckCellProgression() == true && _isBlockedByLetter == false)
                        {
                            TowerDefenseMgmt.Instance.MapMgmt.LetterHasLeft(_path[_currentPathIdx]);

                            _currentPathIdx++;

                            if (_currentPathIdx + 1 < _path.Count)
                            {
                                _cellInfo.PrepareMoveTo(_path[_currentPathIdx + 1]);
                            }
                            else
                            {
                                Status = LetterEggStatus.ARRIVED_AT_DESTINATION;
                            }

                            TowerDefenseMgmt.Instance.MapMgmt.LetterIsAt(_path[_currentPathIdx]);
                        }
                    }
                    else
                    {
                        // Hoho, obstacle ! Is it useful to test this ?
                        if (nextCell.IsObstacle || nextCell.IsOccupiedByLetter)
                        {
                            // So, this is the first turn the letter is blocked
                            if (_isBlockedByObstacleSinceTurn == -1)
                            {
                                
                                _isBlockedByObstacleSinceTurn = 0;

                                if (nextCell.IsObstacle)
                                {
                                    _cellInfo.MoveNextToObstacle(nextCell);
                                    _isBlockedByLetter = false;

                                }
                                else if (nextCell.IsOccupiedByLetter)
                                {
                                    _isBlockedByLetter = true;
                                }

                                PlayBlockedAnimation();
                            }
                            else
                            {
                                _isBlockedByObstacleSinceTurn++;

                                // If the next cell is obstacle, we apply a counter to make it disappear
                                // If the next cell is an egg, the counter does not apply.
                                if (nextCell.IsObstacle)
                                {
                                    if (_isBlockedByObstacleSinceTurn >= MaxTurnBlocked)
                                    {
                                        _isMissed = true;
                                        TowerDefenseMgmt.Instance.LetterHasBeenDestroyed(Label.text);

                                        Status = LetterEggStatus.DISAPPEARED;
                                        HideAndDestroy();
                                    }
                                }
                            }
                        }
                        else
                        {
                            _isBlockedByLetter = false;
                            StopBlockedAnimation();
                        }

                    }
                }
            }
            else
            {
                Status = LetterEggStatus.WILL_DISAPPEAR;
            }
        }
        else if (Status == LetterEggStatus.ARRIVED_AT_DESTINATION)
        {
            if (TowerDefenseMgmt.Instance.ObstaclesMgmt.IsGateOpen(_path[_currentPathIdx]))
            {
                _isMissed = false;

                GameObject feedbackPrefab = IsGoodCombination ? FeedbackGoodPrefab : FeedbackBadPrefab;
                GameObject feedback = NGUITools.AddChild(transform.parent.gameObject, feedbackPrefab);
                feedback.transform.position = transform.position;
                feedback.GetComponent<TextFeedBack>().Init(Label.text);

                Destroy(feedback, feedback.GetComponent<TextFeedBack>().GDuration + 0.25f);

                TowerDefenseMgmt.Instance.LetterHasReachedCastle(Label.text);
                TowerDefenseMgmt.Instance.ObstaclesMgmt.SetGateClosed(_path[_currentPathIdx]);

                HideAndDestroy();
            }
            else
            {
                if (_isBlockedByObstacleSinceTurn == -1)
                {
                    PlayBlockedAnimation();
                    _cellInfo.IsBlockedByObstacle();

                    _isBlockedByObstacleSinceTurn = 0;
                }
                else
                {
                    _isBlockedByObstacleSinceTurn++;

                    if (_isBlockedByObstacleSinceTurn >= MaxTurnBlocked)
                    {
                        _isMissed = true;
                        Status = LetterEggStatus.DISAPPEARED;

                        TowerDefenseMgmt.Instance.LetterHasBeenDestroyed(Label.text);

                        HideAndDestroy();
                    }
                }
            }
        }
    }

    private void PlayBlockedAnimation()
    {
        _cellInfo.IsBlockedByObstacle();

        _tweenPositionJump.from = _cellInfo.transform.localPosition;
        _tweenPositionJump.to = _cellInfo.transform.localPosition + Vector3.up * 35.0f;

        _tweenPositionJump.PlayForward();

        TweenAlphaObstacle.PlayForward();
    }

    private void StopBlockedAnimation()
    {
        _cellInfo.LeaveObstacle();

        _tweenPositionJump.enabled = false;
        TweenAlphaObstacle.enabled = false;

        Sprite.alpha = 1.0f;
    }

    private void ShowSprite()
    {
        TweenAlphaApparition.PlayForward();
    }

    private void HideAndDestroy()
    {
        if (_isMissed && IsGoodCombination)
        {
            GameObject feedback = NGUITools.AddChild(transform.parent.gameObject, FeedbackMissedPrefab);
            feedback.transform.position = transform.position;
            feedback.GetComponent<TextFeedBack>().Init(Label.text);

            Destroy(feedback, feedback.GetComponent<TextFeedBack>().GDuration + 0.25f);
        }

        TowerDefenseMgmt.Instance.MapMgmt.LetterHasLeft(_path[_currentPathIdx]);

        TweenAlphaApparition.SetOnFinished(delegate () { Destroy(gameObject); });
        TweenAlphaApparition.PlayReverse();

    }

    public void SetSprite(string spriteName)
    {
        Sprite.spriteName = spriteName;
    }

    public void SetPath(List<GridCell> path)
    {
        _path = path;
    }

    public void SetLabel(string text)
    {
        Label.text = text;
    }
}
