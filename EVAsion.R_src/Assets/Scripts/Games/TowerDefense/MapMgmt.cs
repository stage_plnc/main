﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MapMgmt : MonoBehaviour
{
    public class TwoPoints
    {
        public Vector2 Beg;
        public Vector2 End;

        public TwoPoints(Vector2 beg, Vector2 end)
        {
            this.Beg = beg;
            this.End = end;
        }

        public TwoPoints(int begX, int begY, int endX, int endY)
        {
            Beg = new Vector2(begX, begY);
            End = new Vector2(endX, endY);
        }
    };

    public enum TypeCell
    {
        Empty = 0,
        Path = 1,
        PathAndObstacle = 2,
        Castle = 3
    };

   
    [Header("Map")]
    public Texture2D MapTexture;
    public GridCell[,] Map;

    private Vector2 _topLeftGrid = new Vector2(-1534.0f + 96.0f, 1152.0f - 96.0f);

    private Dictionary<Color, TypeCell> _colorToTypeCell;

    // So this is a nice thing
    // This is a 4-cell array. 4 because we have 4 viewports
    // There are 4 idctionary. The key is the starting cell of a letter, and the value a list of end cells.
    private Dictionary<Vector2, List<Vector2>>[] _begAndDestPoints;

    // From the asset store
    private AStarSolver<GridCell, System.Object> _aStarSolver;

    private ShuffleBag<Vector2> _startPositions;
    private UIWidget.Pivot _currentViewport;

    private void Awake()
    {
        _begAndDestPoints = new Dictionary<Vector2, List<Vector2>>[Constants.TowerDefense.NbrViewports];
    }
    void Start()
    {
        LoadMap();

        LoadBeginningPoints();
    }

    public bool OccupiedByLetter(int x, int y)
    {
        return Map[x, y].IsOccupiedByLetter;
    }

    // This function is called once
    // It defines all the possibles startings points and their destination, for each viewports
    // IMPORTANT : This is an array.
    // The index 0 if the bottom left
    // The index 1 is the top left
    // The index 2 is the top right
    // The index 3 is the bottom right
    // See Constants.Cs

    private void LoadBeginningPoints()
    {
        _begAndDestPoints[0] = new Dictionary<Vector2, List<Vector2>>();

        // Bottom left
        _begAndDestPoints[0][new Vector2(0, 5)] = new List<Vector2>()
        {
            new Vector2(6, 6),
        };

        _begAndDestPoints[0][new Vector2(0, 1)] = new List<Vector2>()
        {
            new Vector2(7, 4),
            new Vector2(6, 6)
        };
        _begAndDestPoints[0][new Vector2(1, 0)] = new List<Vector2>()
        {
            new Vector2(7, 4),
            new Vector2(6, 6)
        };
        _begAndDestPoints[0][new Vector2(5, 0)] = new List<Vector2>()
        {
            new Vector2(7, 4)
        };

        _begAndDestPoints[0][new Vector2(9, 0)] = new List<Vector2>()
        {
            new Vector2(8, 4)
        };

        // Top left
        _begAndDestPoints[1] = new Dictionary<Vector2, List<Vector2>>();
        _begAndDestPoints[1][new Vector2(0, 8)] = new List<Vector2>()
        {
            new Vector2(6, 6),
            new Vector2(6, 7)
        };

        _begAndDestPoints[1][new Vector2(0, 11)] = new List<Vector2>()
        {
            new Vector2(6, 6),
            new Vector2(6, 7)
        };

        _begAndDestPoints[1][new Vector2(2, 12)] = new List<Vector2>()
        {
            new Vector2(6, 6),
            new Vector2(6, 7)
        };

        _begAndDestPoints[1][new Vector2(4, 12)] = new List<Vector2>()
        {
            new Vector2(6, 7)
        };

        _begAndDestPoints[1][new Vector2(8, 12)] = new List<Vector2>()
        {
            new Vector2(8, 8)
        };

        // Top right
        _begAndDestPoints[2] = new Dictionary<Vector2, List<Vector2>>();
        
        _begAndDestPoints[2][new Vector2(8, 12)] = new List<Vector2>()
        {
            new Vector2(8, 8)
        };

        _begAndDestPoints[2][new Vector2(13, 12)] = new List<Vector2>()
        {
            new Vector2(10, 8)
        };

        _begAndDestPoints[2][new Vector2(16, 11)] = new List<Vector2>()
        {
            new Vector2(10, 8)
        };

        _begAndDestPoints[2][new Vector2(16, 9)] = new List<Vector2>()
        {
            new Vector2(10, 6)
        };

        // Bottom right
        _begAndDestPoints[3] = new Dictionary<Vector2, List<Vector2>>();

        _begAndDestPoints[3][new Vector2(16, 3)] = new List<Vector2>()
        {
            new Vector2(10, 6), 
            new Vector2(10, 4)
        };

        _begAndDestPoints[3][new Vector2(16, 1)] = new List<Vector2>()
        {
            new Vector2(10, 4)
        };

        _begAndDestPoints[3][new Vector2(13, 0)] = new List<Vector2>()
        {
            new Vector2(10, 4), 
            new Vector2(8, 4)
        };

        _begAndDestPoints[3][new Vector2(9, 0)] = new List<Vector2>()
        {
            new Vector2(8, 4)
        };
    }

    // INFO
    // We want to access the map by using Map[x, y], x is the X coord, y is the y coord.
    // The starting point is at the bottom left
    private void LoadMap()
    {
        Map = new GridCell[MapTexture.width, MapTexture.height];
        Color[] allColors = MapTexture.GetPixels();

        for (int i = 0; i < MapTexture.height * MapTexture.width; i++)
        {
            int x = i % MapTexture.width;
            int y = i / MapTexture.width;

            Map[x, y] = new GridCell()
            {
                X = x,
                Y = y
            };

            if (allColors[i] != Color.red && 
                allColors[i] != Color.black && 
                allColors[i] != Color.blue)
            {
                Map[x, y].IsRoad = false;

                // White is the castle, so we dont care
                if (allColors[i] != Color.white)
                {
                    Map[x, y].SetDecorationType(allColors[i]);
                }
            }
            else
            {
                Map[x, y].IsRoad = true;

                if (allColors[i] == Color.red)
                {
                    Map[x, y].IsObstacle = true;
                    Map[x, y].IsObstacleActivated = false;
                }
            }

            Map[x, y].IsOccupiedByLetter = false;
        }

        for (int y = 0; y < MapTexture.height; y++)
        {
            for (int x = 0; x < MapTexture.width; x++)
            {
                if (Map[x, y].IsObstacle)
                {
                    TowerDefenseMgmt.Instance.ObstaclesMgmt.AddObstacle(Map, x, y);
                }
            }
        }

            //StringBuilder sb = new StringBuilder();
            //for (int y = 0; y < MapTexture.height; y++)
            //{
            //    for (int x = 0; x < MapTexture.width; x++)
            //    {
            //        sb.Append(Map[x, y].IsRoad == true ? '1' : '0');
            //    }

            //    Debug.Log(sb.ToString());
            //    sb.Length = 0;
            //}

            _aStarSolver = new AStarSolver<GridCell, System.Object>(Map);
    }

    public void ResetMap()
    {
        for (int i = 0; i < Map.GetLength(0); i++)
        {
            for (int j = 0; j < Map.GetLength(1); j++)
            {
                Map[i, j].IsOccupiedByLetter = false;

                //if (Map[i, j].IsObstacle)
                //    Map[i, j].IsObstacleActivated = true;
            }
        }
    }

    public void LetterIsAt(GridCell cell)
    {
        Map[cell.X, cell.Y].IsOccupiedByLetter = true;
    }

    public void LetterHasLeft(GridCell cell)
    {
        Map[cell.X, cell.Y].IsOccupiedByLetter = false;
    }

    public void ClickOnBlockingElement(int x, int y, bool isVisible)
    {
        Map[x, y].IsObstacleActivated = isVisible;
    }

    public bool HasObstacleAt(GridCell nextCell)
    {
        return Map[nextCell.X, nextCell.Y].IsObstacleActivated 
            || Map[nextCell.X, nextCell.Y].IsOccupiedByLetter;
    }

    public void InitMap(UIWidget.Pivot currentViewport)
    {
        Dictionary<Vector2, List<Vector2>> possiblePaths = 
            _begAndDestPoints[Constants.TowerDefense._viewportToIdx[currentViewport]];

        Debug.Log("Nombre of possible paths = " + possiblePaths.Keys.Count);

        _startPositions = new ShuffleBag<Vector2>(new List<Vector2>(possiblePaths.Keys));
        _currentViewport = currentViewport;
    }

    public List<GridCell> GeneratePath()
    {
        List<GridCell> listPath = null;
        Vector2 start = _startPositions.Next();
        Vector2 end = GetDestination(start);
        // Thx to the asset store!
        IEnumerable<GridCell> path = _aStarSolver.Search(start, end, null);

        if (path != null)
        {
            listPath = new List<GridCell>(path);

            if (end == new Vector2(6, 6) && 
                (start == new Vector2(0, 8) || 
                 start == new Vector2(0, 11) ||
                 start == new Vector2(2, 12)))
            {
                listPath = GetCorrectPath(start, end);
            }

            //foreach (GridCell cell in listPath)
            //{
            //    Debug.Log(cell.ToString());
            //}
        }
        else
        {
            Debug.LogError("No path found from " + start + " to " + end);
        }

        return listPath;
    }

    private List<GridCell> GetCorrectPath(Vector2 start, Vector2 end)
    {
        if (start == new Vector2(0, 8))
        {
            return new List<GridCell>()
            {
                Map[0, 8],
                Map[1, 8],
                Map[1, 9],
                Map[2, 9],
                Map[3, 9],
                Map[4, 9],
                Map[4, 8],
                Map[4, 7],
                Map[4, 6],
                Map[5, 6],
                Map[6, 6]
            };
        }
        else if (start == new Vector2(0, 11))
        {
            return new List<GridCell>()
            {
                Map[0, 11],
                Map[1, 11],
                Map[1, 10],
                Map[1, 9],
                Map[2, 9],
                Map[3, 9],
                Map[4, 9],
                Map[4, 8],
                Map[4, 7],
                Map[4, 6],
                Map[5, 6],
                Map[6, 6]
            };
        }
        else if (start == new Vector2(2, 12))
        {
            return new List<GridCell>()
            {
                Map[2, 12],
                Map[2, 11],
                Map[1, 11],
                Map[1, 10],
                Map[1, 9],
                Map[2, 9],
                Map[3, 9],
                Map[4, 9],
                Map[4, 8],
                Map[4, 7],
                Map[4, 6],
                Map[5, 6],
                Map[6, 6]
            };
        }
        else
        {
            return null;
        }
    }

    public void MapObstacleHasAppeared(int x, int y)
    {
        Map[x, y].IsObstacleActivated = true;
    }

    private Vector2 GetDestination(Vector2 start)
    {
        int idxViewport = Constants.TowerDefense._viewportToIdx[_currentViewport];
        int idxCellEnd = UnityEngine.Random.Range(0, _begAndDestPoints[idxViewport][start].Count);

        return _begAndDestPoints[idxViewport][start][idxCellEnd];
    }

    public Vector2 MapToWorldCoord(int x, int y)
    {
        Vector2 coord = new Vector2(_topLeftGrid.x, _topLeftGrid.y);

        coord.x += 192.0f * x;
        coord.y += 192.0f * y;

        return coord;
    }
}
