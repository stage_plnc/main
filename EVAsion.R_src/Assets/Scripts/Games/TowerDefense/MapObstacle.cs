﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObstacle : MonoBehaviour
{
    public bool IsVisible;

    private UISprite _sprite;
    private TweenAlpha _tweenAlpha;
    private CellInfo _cellInfo;

    private int _turnApparition;

    private void Awake()
    {
        IsVisible = false;

        _sprite = GetComponent<UISprite>();
        _tweenAlpha = GetComponent<TweenAlpha>();
        _cellInfo = GetComponent<CellInfo>();
    }

    void Start ()
    {
        UIEventListener.Get(gameObject).onClick = ClickOnBlockingElement;
    }

    private void ClickOnBlockingElement(GameObject go)
    {
        if (IsVisible)
        {
            IsVisible = !IsVisible;

            HideAndUnregister();

            TowerDefenseMgmt.Instance.MapMgmt.ClickOnBlockingElement(_cellInfo.X, _cellInfo.Y, IsVisible);
        }
    }

    public void HideAndUnregister()
    {
        _tweenAlpha.PlayReverse();

        TowerDefenseMgmt.Instance.OnNextTurn -= OnNextTurn;
    }


    public void SetSprite(ObstaclesMgmt.TypeDecorationField typeDecorationField)
    {
        string spriteName =
            Array.Find(TowerDefenseMgmt.Instance.ObstaclesMgmt.DecorationsByColor,
                                    x => x.Type == typeDecorationField).SpriteName;

        // spritename is formatted like this : OBSTACLE_TypeXX, XX = 01 ou XX = 02
        _sprite.spriteName = string.Format(spriteName, UnityEngine.Random.Range(1, 3).ToString("00"));
    }

    public void OnNextTurn()
    {
        if (TowerDefenseMgmt.Instance.CurrentTurn >= _turnApparition)
        {
            if (!TowerDefenseMgmt.Instance.MapMgmt.OccupiedByLetter(_cellInfo.X, _cellInfo.Y))
            {
                _tweenAlpha.PlayForward();
                IsVisible = true;

                TowerDefenseMgmt.Instance.MapMgmt.MapObstacleHasAppeared(_cellInfo.X, _cellInfo.Y);
                TowerDefenseMgmt.Instance.OnNextTurn -= OnNextTurn;
            }
        }
    }

    public void SetApparitionAt(int turnApparition)
    {
        _turnApparition = turnApparition;
        TowerDefenseMgmt.Instance.OnNextTurn += OnNextTurn;
    }
}
