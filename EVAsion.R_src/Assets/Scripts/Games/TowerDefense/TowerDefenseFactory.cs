﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TowerDefenseFactory : CycleFactory
{
    [Header("TowerDefenseFactory attributes")]
    private TowerDefenseDifficultyData _difficultyData;

    [Header("Prefab")]
    public GameObject LetterEggPrefab;

    [Header("Parenting")]
    public GameObject EggsParentGo;

    public bool DEBUG = false;

    private ShuffleBag<string> _eggLettersSpriteName;

    protected override void Awake()
    {
        base.Awake();

        _eggLettersSpriteName =
            new ShuffleBag<string>(new List<string>(LetterEggPrefab.GetComponent<UISprite>().atlas.GetListOfSprites().ToArray()));
    }

    public override CycleData InitNextCycle(LPNC_DataExport parsevalDT)
    {
        base.InitNextCycle(parsevalDT);

        HandleDifficultyData(parsevalDT, Constants.DifficultyMethod);

        TowerDefenseMgmt.Instance.MapMgmt.InitMap(TowerDefenseMgmt.Instance.CurrentViewport);

        _goodCombination = GenerateGoodCombination();
        _wrongCombination = GenerateWrongCombination();

        _currentCycle++;

        // Apparently, we have to show the # if there is one 
        return new TowerDefenseCycleData(_goodCombination,
            _difficultyData.nbrTurns, _difficultyData.nbrGoodCombinations, _difficultyData.nbrWrongCombinations);
    }

    private void HandleDifficultyData(LPNC_DataExport parsevalDt, string difficulty)
    {
        Debug.Log("Difficulty = " + difficulty);

        if (difficulty == "random")
        {
            parsevalDt.d01 = _random.Next(0, 6);
            parsevalDt.d02 = _random.Next(0, 6);
            parsevalDt.d03 = _random.Next(0, 6);
        }

        // Not sure...
        else if (difficulty == "linear")
        {
            if (_currentCycle >= 1)
            {
                parsevalDt.d01 = (int)Mathf.Round(_initialParsevalDT.d01 + _factLinearDifficultyIncrease[0] * _currentCycle);
                parsevalDt.d02 = (int)Mathf.Round(_initialParsevalDT.d02 + _factLinearDifficultyIncrease[1] * _currentCycle);
                parsevalDt.d03 = (int)Mathf.Round(_initialParsevalDT.d03 + _factLinearDifficultyIncrease[2] * _currentCycle);
            }
            else
            {
                parsevalDt.d01 = (int)Constants.LinearDifficultyStartBound.x;
                parsevalDt.d02 = (int)Constants.LinearDifficultyStartBound.y;
                parsevalDt.d03 = (int)Constants.LinearDifficultyStartBound.z;

                _factLinearDifficultyIncrease[0] = (float)((int)Constants.LinearDifficultyEndBound.x - parsevalDt.d01) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[1] = (float)((int)Constants.LinearDifficultyEndBound.y - parsevalDt.d02) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[2] = (float)((int)Constants.LinearDifficultyEndBound.z - parsevalDt.d03) / (Constants.NbrTotalCycles - 1);

                _initialParsevalDT = parsevalDt;
            }
        }

        Debug.LogWarningFormat("d01 = {0}, d02 = {1}, d03 = {2}", parsevalDt.d01, parsevalDt.d02, parsevalDt.d03);

        _parsevalDT = parsevalDt;

        _difficultyData = GetDifficultyData(parsevalDt);
    }

    public override void GenerateNextCycle()
    {
        GameObject[] eggLetters = GenerateEggLetters();

        PrepareObstacles();
    }

    private void PrepareObstacles()
    {
        int idxViewport = TowerDefenseMgmt.Instance.CurrentViewportIdx;
        List<GameObject> obstacles = TowerDefenseMgmt.Instance.ObstaclesMgmt.GetObstacles(idxViewport);
        List<int> turnsApparitionsList = new List<int>();
        int maxApparition = (int)(_difficultyData.nbrTurns / 2.0f);

        for (int i = 1; i < maxApparition; i++)
        {
            turnsApparitionsList.Add(i);
        }

        ShuffleBag<int> turnsApparitions = new ShuffleBag<int>(turnsApparitionsList);

        foreach (GameObject obstacle in obstacles)
        {
            MapObstacle obstacleBehaviour = obstacle.GetComponent<MapObstacle>();
            obstacleBehaviour.SetApparitionAt(turnsApparitions.Next());
        }
    }

    private GameObject[] GenerateEggLetters()
    {
        GameObject[] letterEggs;

        letterEggs = new GameObject[_difficultyData.nbrGoodCombinations + _difficultyData.nbrWrongCombinations];
        // Lets suppose that the last letter appears halfway through the gameCycle.

        int maxApparition = (int)(_difficultyData.nbrTurns / 2.0f);
        List<int> turnsApparitionsList = new List<int>();

        for (int i = 2; i < maxApparition; i++)
        {
            if (i % 2 == 1)
                turnsApparitionsList.Add(i);
        }

        ShuffleBag<int> turnsApparitions = new ShuffleBag<int>(turnsApparitionsList);

        for (int i = 0; i < letterEggs.Length; i++)
        {
            LetterEgg letterEgg;
            List<GridCell> path = TowerDefenseMgmt.Instance.MapMgmt.GeneratePath();

            letterEggs[i] = NGUITools.AddChild(EggsParentGo, LetterEggPrefab);
            letterEgg = letterEggs[i].GetComponent<LetterEgg>();

            letterEgg.SetSprite(_eggLettersSpriteName.Next());

            if (i < _difficultyData.nbrGoodCombinations)
            {
                letterEgg.SetLabel(_goodCombination);
                letterEgg.IsGoodCombination = true;
            }
            else
            {
                letterEgg.SetLabel(_wrongCombination);
                letterEgg.IsGoodCombination = false;
            }

            letterEgg.SetPath(path);

            if (i == 0)
            {
                letterEgg.TurnApparition = 1;
            }
            else
            {
                letterEgg.TurnApparition = turnsApparitions.Next();
            }

            // We have to check if the letter can reach the end with the sufficiant amount of turns
            // If it is too slow, we're accelerating it. 
            // If it is still too slow, we decrease the turn of the letter apparition

            while (letterEgg.TurnApparition + path.Count * letterEgg.Speed + 1 > _difficultyData.nbrTurns)
            {
                if (letterEgg.Speed != 1)
                {
                    letterEgg.Speed--;
                }
                // That means the speed is already at one...
                else
                {
                    letterEgg.TurnApparition--;

                    if (letterEgg.TurnApparition == 0)
                    {
                        Debug.LogError("ERRROR + " + path.Count);
                        Debug.LogError("Result = " + (letterEgg.TurnApparition + path.Count * letterEgg.Speed));
                        Debug.LogError("VS = " + (_difficultyData.nbrTurns));
                        break;
                    }
                }
            }
            letterEgg.ReadyToMove();
        }


        return letterEggs;
    }

    private string GenerateGoodCombination()
    {
        string goodCombination = string.Empty;

        if (_difficultyData.DifficultyTarget >= 3)
        {
            _indexNewPattern = TargetGeneratorMgmt.Instance.GetRandomIndex(_random, _difficultyData.DifficultyTarget);

            int cran = -1;
            if (_difficultyData.DifficultyTarget >= 3 && _difficultyData.DifficultyTarget <= 4)
                cran = 1;
            if (_difficultyData.DifficultyTarget >= 5 && _difficultyData.DifficultyTarget <= 6)
                cran = 3;

            string[] array = TargetGeneratorMgmt.Instance.GetCombinationAt(_random, _indexNewPattern, cran);
            _leftPartCombination = array[0].Trim();
            _rightPartCombination = array[1].Trim();

            if (_upperCaseSession)
            {
                _leftPartCombination = _leftPartCombination.ToUpper();
                _rightPartCombination = _rightPartCombination.ToUpper();
            }
            else
            {
                _leftPartCombination = _leftPartCombination.ToLower();
                _rightPartCombination = _rightPartCombination.ToLower();
            }

            // Hack, must be changed
            if (_difficultyData.DifficultyTarget % 2 == 0)
            {
                _leftPartCombination = "#" + _leftPartCombination;
                _rightPartCombination = _rightPartCombination + "#";
            }

            goodCombination = _leftPartCombination + _rightPartCombination;
        }
        else
        {
            // Generate only one random letter... Forget leftPart and rightPart.
            char character = (char)(_random.Next(0, 26) + 'A');
            goodCombination = character.ToString();

            if (_difficultyData.DifficultyTarget == 2)
            {
                goodCombination = '#' + goodCombination + '#';
            }

            _leftPartCombination = _rightPartCombination = string.Empty;

            if (DEBUG)
                goodCombination = "y";
        }

        if (_upperCaseSession)
        {
            goodCombination = goodCombination.ToUpper();
        }
        else
        {
            goodCombination = goodCombination.ToLower();
        }

        return goodCombination;
    }

    private string GenerateWrongCombination()
    {
        string wrongCombination = string.Empty;

        // We dont need to generate a wrong combination based on a good combination
        if (_difficultyData.DifficultyTarget >= 3)
        {
            int nbrOfLetters = _difficultyData.GetNumberOfLetters();

            wrongCombination = DistractorsMgmt.Instance.GenerateWrongCombinationRunnerTD(
                _goodCombination, nbrOfLetters, _difficultyData.DifficultyDistractor);
        }
        else
        {
            StringBuilder wrongCombinationSB = new StringBuilder(_goodCombination);

            for (int i = 0; i < wrongCombinationSB.Length; i++)
            {
                if (Char.IsLetter(wrongCombinationSB[i]))
                {
                    char closeCharacter = DistractorsMgmt.Instance.GetDistractor(_random, wrongCombinationSB[i], _difficultyData.DifficultyDistractor);

                    wrongCombinationSB[i] = closeCharacter;
                }
            }

            wrongCombination = wrongCombinationSB.ToString();
        }

        if (_upperCaseSession)
        {
            wrongCombination = wrongCombination.ToUpper();
        }
        else
        {
            wrongCombination = wrongCombination.ToLower();
        }

        return wrongCombination;
    }

    //#region DifficultyData
    private TowerDefenseDifficultyData GetDifficultyData(LPNC_DataExport parsevalDT)
    {
        int difficultyDistractor = 1;
        int nbrTurns = 20;
        int nbrGoodCombinations = 3;
        int nbrWrongCombinations = 3;

        int difficultyTarget = 1;

        try
        {
            difficultyTarget = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("tower_defense", "difficulty_target", parsevalDT.d01));

            if (difficultyTarget < 1 || difficultyTarget > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de tower_defense.json : la dimension difficultyTarget de la difficulté " + parsevalDT.d01 +
                    "doit etre comprise entre 1 et 6");

            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de tower_defense.json a la dimension combination_pattern de la difficulté " + parsevalDT.d01);
        }
        try
        {
            difficultyDistractor = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("tower_defense", "difficulty_distractor", parsevalDT.d02));

            if (difficultyDistractor < 1 || difficultyDistractor > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de tower_defense.json : la dimension ratio_similarity de la difficulté " + parsevalDT.d02 +
                    "doit etre comprise entre 1 et 6");

            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de tower_defense.json a la dimension difficulty_similarity de la difficulté " + parsevalDT.d02);
        }
        try
        {
            string[] targetInfos = DifficultyMgmt.Instance.GetDifficultyValue("tower_defense", "turns;good_combin;bad_combin", parsevalDT.d03).Split(';');

            nbrTurns = int.Parse(targetInfos[0]);
            nbrGoodCombinations = int.Parse(targetInfos[1]);
            nbrWrongCombinations = int.Parse(targetInfos[2]);
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Error while reading tower_defense.json file on combinations dimension from difficulty " + parsevalDT.d03 + ".\nExpected format \"string1,string2,string3...\" but receive : "
                + DifficultyMgmt.Instance.GetDifficultyValue("tower_defense", "turns;good_combin;bad_combin", parsevalDT.d03));
        }

        if (DEBUG)
            difficultyTarget = difficultyDistractor = 1;

        return new TowerDefenseDifficultyData(difficultyTarget, difficultyDistractor, nbrTurns, nbrGoodCombinations, nbrWrongCombinations);
    }

    private class TowerDefenseDifficultyData : DifficultyData
    {
        public TowerDefenseDifficultyData(int difficultyTarget, int difficultyDistractor,
            int nbrTurns, int nbrGoodCombinations, int nbrWrongCombinations) :
                base(difficultyTarget, difficultyDistractor)
        {
            this.nbrTurns = nbrTurns;
            this.nbrGoodCombinations = nbrGoodCombinations;
            this.nbrWrongCombinations = nbrWrongCombinations;
        }

        public int nbrTurns;
        public int nbrGoodCombinations;
        public int nbrWrongCombinations;

    }
    //#endregion
}
