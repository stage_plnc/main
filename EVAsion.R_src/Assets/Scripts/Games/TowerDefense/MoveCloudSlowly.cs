﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCloudSlowly : MonoBehaviour
{
    public float Speed;

    private int _direction;

    private void Awake()
    {
        if (UnityEngine.Random.value > 0.5f)
        {
            _direction = 1;
        }
        else
        {
            _direction = -1;
        }
    }

    void Start ()
    {

		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.localPosition += Vector3.right * _direction * Speed * Time.deltaTime;
		
	}
}
