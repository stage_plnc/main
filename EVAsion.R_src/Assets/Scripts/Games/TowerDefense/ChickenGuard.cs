﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenGuard : MonoBehaviour
{
    public BoxCollider BoxColliderParent;

    public bool IsVisible;

    private UISprite _sprite;

    public TweenAlpha TweenAlphaDisparition;
    public TweenAlpha TweenAlphaReapparation;

    private bool _isDoingBlinking = false;

    private void Awake()
    {
        IsVisible = true;

        _sprite = GetComponent<UISprite>();
        TweenAlphaDisparition = GetComponent<TweenAlpha>();

        BoxColliderParent.size = _sprite.localSize;

        
    }

    void Start ()
    {
        TowerDefenseMgmt.Instance.OnEndCycle += OnEndCycle;

        UIEventListener.Get(BoxColliderParent.gameObject).onClick = ClickOnBlockingElement;
	}

    private void ClickOnBlockingElement(GameObject go)
    {
        if (_isDoingBlinking == false)
        {
            if (IsVisible)
                TweenAlphaDisparition.PlayForward();
            else
            {
                //Invoke("StopBlinking", TweenAlphaReapparation.duration * 2.0f + TweenAlphaReapparation.delay);
                TweenAlphaDisparition.PlayReverse();
            }

            IsVisible = !IsVisible;
        }
    }

    public void SetVisible()
    {
        TweenAlphaReapparation.PlayForward();
        IsVisible = true;

        _isDoingBlinking = true;
        Invoke("StopBlinking", TweenAlphaReapparation.duration * 2.0f + TweenAlphaReapparation.delay);
    }

    public void OnEndCycle()
    {
        if (!IsVisible)
        {
            TweenAlphaDisparition.PlayReverse();
            IsVisible = true;
        }
    }

    public void StopBlinking()
    {
        TweenAlphaDisparition.PlayReverse();
        TweenAlphaReapparation.enabled = false;
        _isDoingBlinking = false;
    }
}
