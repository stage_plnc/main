﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDefenseMgmt : GameMgmt
{
    [Header("TowerDefenseMgmt attributes")]
    public TowerDefenseFactory Factory;
    public Camera MainCamera;

    [Header("Mgmt")]
    public MapMgmt MapMgmt;
    public ObstaclesMgmt ObstaclesMgmt;

    private static TowerDefenseMgmt _instance;
    public static TowerDefenseMgmt Instance
    {
        get { return _instance; }
    }

    [HideInInspector]
    public int CurrentTurn;

    public delegate void OnNextTurnAction();
    public event OnNextTurnAction OnNextTurn;
    public event OnNextTurnAction OnEndCycle;

    private Dictionary<UIWidget.Pivot, Vector2> _viewports = new Dictionary<UIWidget.Pivot, Vector2>();
    private int _currentIdxViewport;
    private UIWidget.Pivot _currentViewport;

    private TowerDefenseCycleData _towerDefenseCycleData;
    private float _durationAction = 0.0f;

    private TweenPosition _cameraTweenPosition;

    public UIWidget.Pivot CurrentViewport { get { return _currentViewport; } }
    public int CurrentViewportIdx { get { return _currentIdxViewport; } }

    public AudioClip SoundSuccess;
    public AudioClip SoundFailure;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogError("[TowerDefenseMgmt] Instance de TowerDefenseMgmt already exists)");
        }

        _gameName = "tower_defense";
    }

    public new void OnDestroy()
    {
        base.OnDestroy();

        _instance = null;
    }


    public override void Start()
    {
        base.Start();

        _factory = Factory;
        _cameraTweenPosition = MainCamera.GetComponent<TweenPosition>();

        _viewports[UIWidget.Pivot.BottomLeft] = new Vector2(-736.0f, -480.0f);
        _viewports[UIWidget.Pivot.TopLeft] = new Vector2(-736.0f, +480.0f);
        _viewports[UIWidget.Pivot.TopRight] = new Vector2(+736.0f, +480.0f);
        _viewports[UIWidget.Pivot.BottomRight] = new Vector2(+736.0f, -480.0f);

        Invoke("StartGame", 2.5f);
    }

    public override void StartInterlude()
    {
        if (_nbrElapsedCycles == 0)
        {
            _currentIdxViewport = 3;
        }
        else
        {
            _currentIdxViewport = (_currentIdxViewport + 1) % Constants.TowerDefense.NbrViewports;
        }

        Debug.LogWarning("Moving the camera to " + _currentIdxViewport);
        MoveCameraToViewport(_currentIdxViewport);

        base.StartInterlude();

        Invoke("EndInterlude", Constants.TowerDefense.InterludeDuration);
    }

    public override void StartGameCycle()
    {
        base.StartGameCycle();

        _towerDefenseCycleData = (TowerDefenseCycleData)_cycleData;

        _durationAction = Constants.MatchMaker.CycleDuration / _towerDefenseCycleData.nbrTurns;
    }

    public override void EndGameCycle()
    {
        CurrentTurn = 0;

        // Fade and destroy the ghosts
        if (OnEndCycle != null)
        {
            OnEndCycle();
        }

        MapMgmt.ResetMap();

        base.EndGameCycle();
    }

    public void LetterHasBeenDestroyed(string label)
    {
        if (label == _cycleData.Combination)
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
            }
            else
            {
                // What to do ?
                _cycleSuccessAndFailure.NbrTargetFailure++;
            }
        }
    }

    public void LetterHasReachedCastle(string label)
    {
        if (label == _cycleData.Combination)
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 1.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetSuccess++;
            }

            _nbrTotalSuccess++;
            SoundMgmt.Instance.PlayOneShotFXSound(SoundSuccess, 1.0f);
        }
        else
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetFailure++;
            }

            SoundMgmt.Instance.PlayOneShotFXSound(SoundFailure, 1.0f);
            _nbrTotalFailure++;
        }
    }

    public override void EndTutorial()
    {

    }

    public override float CycleScore()
    {
        return 1.0f;
    }

    private void MoveCameraToViewport(int idxViewport)
    {
        _currentIdxViewport = idxViewport;
        _currentViewport = Constants.TowerDefense._idxToViewport[_currentIdxViewport];

        _cameraTweenPosition.from = MainCamera.transform.localPosition;
        _cameraTweenPosition.to = _viewports[_currentViewport];

        _cameraTweenPosition.ResetToBeginning();

        _cameraTweenPosition.PlayForward();
    }

    protected override void Update()
    {
        base.Update();

        if (Status == GameStatus.GAME_CYCLE /*&& CurrentTurn < _nbrTurnsForCurrentCycle*/)
        {
            float elapsedTime = Time.time - _cycleStartDate;
            float timeSinceLastTurn = elapsedTime / (CurrentTurn + 1);

            if (timeSinceLastTurn > _durationAction)
            {
                CurrentTurn++;

                if (OnNextTurn != null)
                {
                    OnNextTurn();
                }
                else if (!IsInvoking("EndGameCycle"))
                {
                    Invoke("EndGameCycle", 1.5f);
                }
            }
        }
    }
}
