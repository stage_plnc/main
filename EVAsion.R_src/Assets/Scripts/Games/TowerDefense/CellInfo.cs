﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CellInfo : MonoBehaviour
{
    public enum TypeSpeed
    {
        Low = 3, 
        Medium = 2, 
        High = 1
    };

    public Vector2 BottomLeft;
    public int SizeCell;

    public int X;
    public int Y;

    public TypeSpeed Speed;

    public bool IsTweening = false;

    private GridCell _currentCell;
    private GridCell _nextCell;

    private UIWidget.Pivot _currentDirection;
    private float _distX;
    private float _distY;
    private int _progressionInCell = 0;

    private void Awake()
    {
        _distX = _distY = 0;

        Speed = (TypeSpeed) UnityEngine.Random.Range(1, 3 + 1);
    }

    public void SetInitialCell(GridCell cell)
    {
        X = cell.X;
        Y = cell.Y;

        _currentCell = cell;
    }

    public void MoveTo(GridCell cell)
    {
        X = cell.X;
        Y = cell.Y;
    }

    public void AdvanceInCell()
    {
        _progressionInCell++;

        switch (_currentDirection)
        {
            case UIWidget.Pivot.Left:
                _distX -= SizeCell / (int) Speed;
                break;
            case UIWidget.Pivot.Right:
                _distX += SizeCell / (int)Speed;
                break;
            case UIWidget.Pivot.Top:
                _distY += SizeCell / (int)Speed;
                break;
            case UIWidget.Pivot.Bottom:
                _distY -= SizeCell / (int)Speed;
                break;
        }
    }

    public bool CheckCellProgression()
    {
        bool isInTheCell = _progressionInCell == (int) Speed;

        if (isInTheCell)
        {
            _currentCell = _nextCell;
            X = _currentCell.X;
            Y = _currentCell.Y;
            _distX = _distY = 0;
        }

        return isInTheCell;
    }

    public void MoveTo(int x, int y)
    {
        X = x;
        Y = y;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsTweening)
        {
            transform.localPosition = new Vector2(BottomLeft.x + SizeCell * X + _distX, BottomLeft.y + SizeCell * Y + _distY);
        }
        else
        {

        }
    }

    public void PrepareMoveTo(GridCell cell)
    {
        _nextCell = cell;

        if (_nextCell.X > _currentCell.X)
            _currentDirection = UIWidget.Pivot.Right;
        else if (_nextCell.X < _currentCell.X)
            _currentDirection = UIWidget.Pivot.Left;
        else if (_nextCell.Y > _currentCell.Y)
            _currentDirection = UIWidget.Pivot.Top;
        else if (_nextCell.Y < _currentCell.Y)
            _currentDirection = UIWidget.Pivot.Bottom;
        else
            Debug.LogError("WHAT ? Are nextCell and the currentCell the same one ?");

        //X = _currentCell.X;
        //Y = _currentCell.Y;

        _progressionInCell = 0;
    }

    public void MoveNextToObstacle(GridCell nextCell)
    {
        UIWidget.Pivot direction = UIWidget.Pivot.Bottom;

        if (nextCell.X > _currentCell.X)
            direction = UIWidget.Pivot.Right;
        else if (nextCell.X < _currentCell.X)
            direction = UIWidget.Pivot.Left;
        else if (nextCell.Y > _currentCell.Y)
            direction = UIWidget.Pivot.Top;
        else if (nextCell.Y < _currentCell.Y)
            direction = UIWidget.Pivot.Bottom;
        else
            Debug.LogError("WHAT ? Are nextCell and the currentCell the same one ?");

        switch (direction)
        {
            case UIWidget.Pivot.Left:
                _distX = -SizeCell / 2.0f;
                break;
            case UIWidget.Pivot.Right:
                _distX = SizeCell / 2.0f;
                break;
            case UIWidget.Pivot.Top:
                _distY = SizeCell / 2.0f;
                break;
            case UIWidget.Pivot.Bottom:
                _distY = -SizeCell / 2.0f;
                break;
        }

        // We have to update the transform in order for the tween position (boucing) to work
        transform.localPosition = new Vector2(BottomLeft.x + SizeCell * X + _distX, BottomLeft.y + SizeCell * Y + _distY);
    }

    public void IsBlockedByObstacle()
    {
        IsTweening = true;
    }

    public void LeaveObstacle()
    {
        _distX = _distY = 0;
        IsTweening = false;
    }
}
