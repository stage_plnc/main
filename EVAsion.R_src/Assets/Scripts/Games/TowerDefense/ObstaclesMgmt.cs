﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesMgmt : MonoBehaviour
{
    public enum TypeDecorationField
    {
        None = 0,
        Cows = 1,
        Bulls = 2,
        Pigs = 4,
        Trees = 8,
        BundlesOfStraw = 16
    }

    [System.Serializable]
    public class DecorationByColor
    {
        public Color Color;
        public TypeDecorationField Type;
        public string SpriteName;
    }

    [Header("Obstacles")]
    public GameObject ObstaclePrefab;
    public GameObject ObstacleParentGo;
    public DecorationByColor[] DecorationsByColor;

    [Header("Chickens")]
    public ChickenGuard[] ChickenGuards;

    private List<GameObject>[] _obstaclesByViewports;
    private Dictionary<Vector2, ChickenGuard> _guardsByCell;

    private void Awake()
    {
        _guardsByCell = new Dictionary<Vector2, ChickenGuard>();

        _guardsByCell[new Vector2(8, 4)] = ChickenGuards[0];
        _guardsByCell[new Vector2(10, 4)] = ChickenGuards[1];
        _guardsByCell[new Vector2(10, 6)] = ChickenGuards[2];
        _guardsByCell[new Vector2(10, 8)] = ChickenGuards[3];
        _guardsByCell[new Vector2(8, 8)] = ChickenGuards[4];
        _guardsByCell[new Vector2(6, 7)] = ChickenGuards[5];
        _guardsByCell[new Vector2(6, 6)] = ChickenGuards[6];
        _guardsByCell[new Vector2(7, 4)] = ChickenGuards[7];

        _obstaclesByViewports = new List<GameObject>[Constants.TowerDefense.NbrViewports];
        for (int i = 0; i < _obstaclesByViewports.Length; i++)
        {
            _obstaclesByViewports[i] = new List<GameObject>();
        }
    }

    public void AddObstacle(GridCell[ , ] map, int x, int y)
    {
        List<TypeDecorationField> typeFieldsAround = new List<TypeDecorationField>();

        GameObject obstacle = NGUITools.AddChild(ObstacleParentGo, ObstaclePrefab);
        MapObstacle obstacleBehaviour = obstacle.GetComponent<MapObstacle>();

        // Look around !
        if (x - 1 >= 0 && map[x - 1, y].TypeDecoration != TypeDecorationField.None)
        {
            typeFieldsAround.Add(map[x - 1, y].TypeDecoration);
        }
        if (x + 1 < map.GetLength(0) && map[x + 1, y].TypeDecoration != TypeDecorationField.None)
        {
            typeFieldsAround.Add(map[x + 1, y].TypeDecoration);
        }
        if (y - 1 >= 0 && map[x, y - 1].TypeDecoration != TypeDecorationField.None)
        {
            typeFieldsAround.Add(map[x, y - 1].TypeDecoration);
        }
        if (y + 1 < map.GetLength(1) && map[x, y + 1].TypeDecoration != TypeDecorationField.None)
        {
            typeFieldsAround.Add(map[x, y + 1].TypeDecoration);
        }
       
        // Set the sprite and position
        obstacleBehaviour.SetSprite(typeFieldsAround[UnityEngine.Random.Range(0, typeFieldsAround.Count)]);
        obstacle.GetComponent<CellInfo>().MoveTo(x, y);

        // And store it
        if (x < 9 && y < 7)
        {
            _obstaclesByViewports[0].Add(obstacle);
        }
        if (x < 9 && y > 5)
        {
            _obstaclesByViewports[1].Add(obstacle);
        }
        if (x > 6 && y > 5)
        {
            _obstaclesByViewports[2].Add(obstacle);
        }
        if (x > 6 && y < 7)
        {
            _obstaclesByViewports[3].Add(obstacle);
        }

    }

    public List<GameObject> GetObstacles(int idxViewport)
    {
        return _obstaclesByViewports[idxViewport];
    }

    public bool IsGateOpen(GridCell cell)
    {
        Vector2 pos = new Vector2(cell.X, cell.Y);

        if (_guardsByCell.ContainsKey(pos))
        {
            return !_guardsByCell[pos].IsVisible;
        }

        return false;
    }

    public void SetGateClosed(GridCell cell)
    {
        Vector2 pos = new Vector2(cell.X, cell.Y);

        if (_guardsByCell.ContainsKey(pos))
        {
            _guardsByCell[pos].SetVisible();
        }

    }
}
