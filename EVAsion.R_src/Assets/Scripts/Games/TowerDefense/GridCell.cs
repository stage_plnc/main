﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GridCell : SettlersEngine.IPathNode<System.Object>
{
    public int X { get; set; }
    public int Y { get; set; }
    public bool IsRoad { get; set; }
    public bool IsObstacle { get; set; }
    public bool IsObstacleActivated { get; set; }
    public bool IsOccupiedByLetter { get; set; }

    // 0 left, 1 top, 2 right, 3 bottom
    public ObstaclesMgmt.TypeDecorationField TypeDecoration;
    public ObstaclesMgmt.TypeDecorationField[] DecorationsAround;

    public bool IsWalkable(System.Object unused)
    {
        return IsRoad;
    }

    public new string ToString()
    {
        return string.Format("({0};{1})", X, Y).ToString();
    }

    public void SetDecorationType(Color color)
    {
        TypeDecoration = Array.Find(TowerDefenseMgmt.Instance.ObstaclesMgmt.DecorationsByColor, 
                                    x => x.Color == color).Type;
    }
}
