﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDefenseCycleData : CycleData
{
    public int nbrTurns;
    public int nbrGoodCombinations;
    public int nbrWrongCombinations;

    public TowerDefenseCycleData(string combination, int nbrTurns, int nbrGoodCombinations, int nbrWrongCombinations) 
        : base(combination, nbrGoodCombinations)
    {
        this.nbrTurns = nbrTurns;
        this.nbrGoodCombinations = nbrGoodCombinations;
        this.nbrWrongCombinations = nbrWrongCombinations;
    }
}
