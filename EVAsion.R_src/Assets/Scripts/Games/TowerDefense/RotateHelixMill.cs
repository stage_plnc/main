﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateHelixMill : MonoBehaviour {

    public float MinSpeed;
    public float MaxSpeed;

    private float _speed;

    void Start ()
    {
        _speed = UnityEngine.Random.Range(MinSpeed, MaxSpeed);
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(Vector3.forward * Time.deltaTime * _speed); 
		
	}
}
