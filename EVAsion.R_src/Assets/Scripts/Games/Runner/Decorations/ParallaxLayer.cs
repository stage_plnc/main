﻿/**********************************************************************
* ParallaxLayer.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;
using System;

public class ParallaxLayer : MonoBehaviour
{
    public enum LayerType
    {
        FOREGROUND_COLLINNETTES,
        LANE,
        BACKGROUND_HILL,
        OTHER
    }

    [Header("Layer type")]
    public LayerType Type;

    [Header("Lane informations")]
    public int LaneNumber = 0;

    [Header("Collinettes infos")]
    public int MinGapBetweenCollinettes;

    [Header("Speed")]
    public int InitialSpeed;
    public int CurrentSpeed;

    [Header("Bricks")]
    public GameObject[] BricksDecoration;
    public int Depth;
    public int Superposition = 64;

    public int NbrLayersEndToEnd = 3;
    public int WidthBrick = 2048;

    [Header("Ponctual Elements")]
    public GameObject[] PonctualElementsPrefab;
    // The frequence that a ponctuel element will appear. 0 is none, 1 is always
    [Range(0, 1)]
    public float FreqSpawningPonctualElements;

    [Range(0, 20)]
    public int NbrMinPonctualElementByBrick;
    [Range(0, 20)]
    public int NbrPonctualElementsMaxByBricks;

    public int YMin = -1;
    public int YMax = -1;

    private int _count = 0; // To calcul the position of the bricks

    private Queue<GameObject> _activeBricks;
    private Vector2 _lastPositionWithChange;

    private void Start()
    {
        float offsetStartX = (Constants.WidthScreen - WidthBrick) / 2.0f;
        _activeBricks = new Queue<GameObject>();

        // If the size of the layer if inferior the the screen width, we must add a start offset
        transform.localPosition = Vector3.left * offsetStartX;

        for (int i = 0; i < NbrLayersEndToEnd; i++)
        {
            GameObject brick = CreateBrick();

            _activeBricks.Enqueue(brick);
            RandomShuffling.Shuffle(BricksDecoration);

            _count++;
        }

        _lastPositionWithChange = transform.localPosition;

        Assert.IsTrue(BricksDecoration.Length > 0);
    }

    void Update()
    {
        if (RunnerMgmt.Instance.IsGameRunning())
        {
            // Move the layer to the left
            transform.localPosition += (Vector3.left * (CurrentSpeed * Time.deltaTime));

            // If the last brick created has gone along the screen, create another
            if (Vector2.Distance(_lastPositionWithChange, transform.localPosition) >= WidthBrick - Superposition)
            {
                GameObject brick = CreateBrick();

                _activeBricks.Enqueue(brick);

                // Destroy old brick when we can't see it
                if (_activeBricks.Count > NbrLayersEndToEnd)
                {
                    Destroy(_activeBricks.Dequeue());
                }

                _lastPositionWithChange += Vector2.left * (WidthBrick - Superposition);

                _count++;
            }
        }
    }


    private GameObject CreateBrick()
    {
        GameObject brick = NGUITools.AddChild(gameObject, BricksDecoration[_count % BricksDecoration.Length]);
        UIWidget brickWidget = brick.GetComponent<UIWidget>();

        brick.name += _count;
        brick.transform.localScale = Vector3.one;

        brick.transform.localPosition = BricksDecoration[_count % BricksDecoration.Length].transform.localPosition + Vector3.right * (_count * (WidthBrick - Superposition));

        if (Type == LayerType.BACKGROUND_HILL)
        {
            brickWidget.depth = -(_count % 2);
        }

        // The difference between the creation of the collinettes and a normal ponctual element is this one :
        // The X position of a ponctual element doesnt really matter
        // Otherwise, we're trying to make sure that the collinettes are "compacted" inside the brick, so they don't overlap
        if (Type == LayerType.FOREGROUND_COLLINNETTES)
        {
            CreateCollinettes(brickWidget);
        }

        // If there is a ponctual element to add
        else if (PonctualElementsPrefab.Length > 0)
        {
            float random = UnityEngine.Random.Range(0.0f, 1.0f);

            // Randomize the creation of a ponctual element
            if (random < FreqSpawningPonctualElements)
            {
                int nbrOfElementsToSpawn = UnityEngine.Random.Range(NbrMinPonctualElementByBrick, NbrPonctualElementsMaxByBricks);
                int indexPreviousElement = -1;

                float[] xPositions = GenerateXPositions(nbrOfElementsToSpawn, brick.GetComponent<UIWidget>().width);

                for (int i = 0; i < nbrOfElementsToSpawn; i++)
                {
                    int indexElement = UnityEngine.Random.Range(0, PonctualElementsPrefab.Length);

                    // UGLY but effective
                    while (indexElement == indexPreviousElement)
                    {
                        indexElement = UnityEngine.Random.Range(0, PonctualElementsPrefab.Length);
                    }

                    AddPonctualElement(brick, PonctualElementsPrefab[indexElement], xPositions[i]);

                    indexPreviousElement = indexElement;
                }
            }
        }
        return brick;
    }

    #region Generic bricks
    private float[] GenerateXPositions(int nbrOfElementsToSpawn, int width)
    {
        float[] xPositions = new float[nbrOfElementsToSpawn];
        float gap = width / (nbrOfElementsToSpawn + 1);

        for (int i = 0; i < nbrOfElementsToSpawn; i++)
        {
            xPositions[i] = -width / 2.0f + gap * (i + 1);
        }

        return xPositions;
    }

    private void AddPonctualElement(GameObject brick, GameObject PonctualElementPrefab, float x, bool isTopLane = false)
    {
        GameObject ponctualElement = NGUITools.AddChild(brick, PonctualElementPrefab);
        UIWidget ponctualElementWidget = ponctualElement.GetComponent<UIWidget>();
        UITexture brickTexture = brick.GetComponent<UITexture>();

        x += UnityEngine.Random.Range(-ponctualElementWidget.width, -ponctualElementWidget.width / 2.0f);

        // We never know
        if (x >= brickTexture.width / 2.0f - ponctualElementWidget.width)
        {
            x = UnityEngine.Random.Range(-brickTexture.width / 2.0f + ponctualElementWidget.width, brickTexture.width / 2.0f - ponctualElementWidget.width);
        }

        float y = 0.0f;

        if ((Type == LayerType.LANE && (LaneNumber == 0 || LaneNumber == 1)) /*|| Type == LayerType.BACKGROUND_HILL*/)
        {
             y = -brickTexture.height / 2.0f;

            //if (Type == LayerType.BACKGROUND_HILL)
            //    y -= 20.0f;
        }
        else if (YMin == -1 && YMax == -1)
        {
            y = UnityEngine.Random.Range(-brickTexture.height / 2.0f + ponctualElementWidget.height, brickTexture.height / 2.0f - ponctualElementWidget.height);
        }
        else
        {
            y = UnityEngine.Random.Range(YMin, YMax);
        }

        ponctualElement.transform.localPosition = new Vector2(x, y);
        if (ponctualElement.GetComponent<ScaleVariation>() == null)
            ponctualElement.transform.localScale = PonctualElementPrefab.transform.localScale;

#warning We choose 300 artbitrarly...
        ponctualElementWidget.depth = 300 - (int)(y - ponctualElementWidget.height / 2.0f);
    }
    #endregion

    #region Collinettes
    private void CreateCollinettes(UIWidget brickWidget)
    {
        UIWidget previousPonctualElement = null;
        int indexPreviousElement = -1;
        float x = float.MaxValue;

        // So, we're creating the collinettes according to the right side of the brick
        // The goal is to go from the right to the left while we "leave" the brick
        while (x > -brickWidget.width / 2.0f)
        {
            int indexElement = UnityEngine.Random.Range(0, PonctualElementsPrefab.Length);

            // UGLY but effective
            while (indexElement == indexPreviousElement)
            {
                indexElement = UnityEngine.Random.Range(0, PonctualElementsPrefab.Length);
            }

            previousPonctualElement = AddPonctualElementCollinette(brickWidget, PonctualElementsPrefab[indexElement], previousPonctualElement);

            indexPreviousElement = indexElement;

            x = previousPonctualElement.transform.localPosition.x - (previousPonctualElement.width / 2.0f);
        }

    }

    private UIWidget AddPonctualElementCollinette(UIWidget brickWidget, GameObject PonctualElementPrefab, UIWidget previousPonctualElement)
    {
        GameObject ponctualElement = NGUITools.AddChild(brickWidget.gameObject, PonctualElementPrefab);
        UIWidget ponctualElementWidget = ponctualElement.GetComponent<UIWidget>();
        float x = 0.0f, y = 0.0f;

        if (previousPonctualElement == null)
        {
            x = (brickWidget.width - ponctualElementWidget.width) / 2.0f;
        }
        else
        {
            x = previousPonctualElement.transform.localPosition.x -
                  (previousPonctualElement.width + ponctualElementWidget.width) / 2.0f - MinGapBetweenCollinettes;
        }

        if (YMin == -1 && YMax == -1)
        {
            y = UnityEngine.Random.Range(-brickWidget.height / 2.0f + ponctualElementWidget.height, brickWidget.height / 2.0f - ponctualElementWidget.height);
        }
        else
        {
            y = UnityEngine.Random.Range(YMin, YMax);
        }

        ponctualElement.transform.localPosition = new Vector2(x, y);
        ponctualElement.transform.localScale = PonctualElementPrefab.transform.localScale;
        ponctualElementWidget.depth = 300 - (int)(y);

        return ponctualElementWidget;
    }
    #endregion

}
