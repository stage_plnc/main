﻿/**********************************************************************
* ParallaxMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxMgmt : MonoBehaviour
{
    public GameObject[] LayerPrefabs;

    private List<ParallaxLayer> _decorationsLayers;

    // Use this for initialization
    void Awake()
    {
        _decorationsLayers = new List<ParallaxLayer>();
    }

    void Start()
    {
        foreach (GameObject layerPrefab in LayerPrefabs)
        {
            GameObject layer = NGUITools.AddChild(gameObject, layerPrefab);

            layer.AddComponent<UIPanel>().depth = layer.GetComponent<ParallaxLayer>().Depth;

            ParallaxLayer parallaxLayer = layer.GetComponent<ParallaxLayer>();

            // Useful if the brick of the width is not equal to the widthscreen (2048)
            layer.transform.localPosition = Vector3.right * (parallaxLayer.WidthBrick - Constants.WidthScreen) / 2.0f;

            _decorationsLayers.Add(parallaxLayer);

        }
    }

    public void Update()
    {
        if (RunnerMgmt.Instance.Controller.NextSpeed != RunnerMgmt.Instance.Controller.SpeedPerS)
        {
            if (RunnerMgmt.Instance.Controller.SpeedPerS != 0.0f)
            {
                ApplyPlayerSpeedUpdate(RunnerMgmt.Instance.Controller.SpeedPerS / RunnerMgmt.Instance.Controller.InitialSpeed);
            }
        }

    }

    public void ApplyPlayerSpeedUpdate(float factor)
    {
        foreach (ParallaxLayer layer in _decorationsLayers)
        {
            layer.CurrentSpeed = (int)(RunnerMgmt.Instance.Controller.SpeedPerS * (layer.InitialSpeed / RunnerMgmt.Instance.Controller.InitialSpeed));
        }
    }

}
