﻿/**********************************************************************
* RunnerMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using System.Text;
using System.IO;
using System;

public class RunnerMgmt : GameMgmt
{
    [Header("RunnerMgmt attributes")]
    public RunnerFactory Factory;
    public RunnerCtrl Controller;

    public AvatarController Avatar;

    public PoolingMgmt PoolingMgmt;

    [Header("Lanes")]
    // In this array, the lanes[0] is the "top" lane, the one far from ther player
    public GameObject[] Lanes;

    public GameObject CurrentLane
    {
        get { return Lanes[_currentLane]; }
    }


    public float[] ScaleByLane = { 0.5f, 0.75f, 1.0f };
    public Transform[] PlayerPositionByLane;
    public Transform[] LetterPositionByLane;

    private static RunnerMgmt _instance;
    public static RunnerMgmt Instance
    {
        get { return _instance; }
    }

    public float DurationPlayerOutOfScreen = 3.0f;

    private int _currentLane = 1;
    private float _gameEndedStartTime;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogError("[RunnerMgmt] Instance de RunnerMgmt already exists)");
        }

        _gameName = "runner";
    }

    public override void OnDestroy()
    {
        base.OnDestroy();

        _instance = null;
    }


    public override void Start()
    {
        base.Start();

        _factory = Factory;

        Controller.OnUpSwipe += MoveUpCharacter;
        Controller.OnDownSwipe += MoveDownCharacter;

        Avatar.SetStartLane(_currentLane);
        Avatar.StartRunningAnimation();

        Controller.SetInitialSpeed();

        StartApparitionPlayer();
        // Invoke("StartApparitionPlayer", Constants.Runner.DurationTutorialPopUp);
    }

    public void StartApparitionPlayer()
    {
        Status = GameStatus.APPARITION_PLAYER;

        // Invoke("StartTutorial", GameStartCountdown);
        Invoke("StartGame", Constants.Runner.GameStartCountdown);
    }

    public override void StartGameCycle()
    {
        Invoke("EndGameCycle", Constants.Runner.CycleDuration);
    }

    public override void StartInterlude()
    {
        base.StartInterlude();

        Invoke("EndInterlude", Constants.Runner.InterludeDuration);
    }

    //public override void StartTutorial()
    //{
    //    base.StartTutorial();

    //    Controller.SetTutorialSpeed();

    //    RunnerHUD.ShowFirstArrow();
    //}

    public override void EndTutorial()
    {
        Invoke("StartGame", 1.0f);
    }

    public bool PlayerCollidesWithTarget(RunningLetter runnerTarget)
    {
        string runnerTargetString = runnerTarget.Text;

        if (runnerTarget.IsGoodCombination)
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 1.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetSuccess++;
            }

            _nbrTotalSuccess++;
        }
        else
        {
            if (Constants.HardFeedParseval)
            {
                Debug.Log("IS WRONG");
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetFailure++;
            }
            _nbrTotalFailure++;
        }

        return runnerTarget.IsGoodCombination;
    }

    public override void EndGameCycle()
    {
        PoolingMgmt.ResetPool();

        base.EndGameCycle();

        _cycleSuccessAndFailure.Clear();
    }

    public override float CycleScore()
    {
        float score = 0.0f;
        int nbrTargetToGet = ((RunnerCycleData)_cycleData).NumTargotToGet;
        int nbrBadTarget = ((RunnerCycleData)_cycleData).NumBadTarget;

        float success = Mathf.Max(0.0f,
            _cycleSuccessAndFailure.NbrTargetSuccess / (float)nbrTargetToGet -
            _cycleSuccessAndFailure.NbrTargetFailure / (float)nbrBadTarget);

        if (success >= UPL)
        {
            score = 1.0f;
        }
        else
        {
            score = 0.0f;
        }

        return score;
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            MoveUpCharacter();
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            MoveDownCharacter();
        }

        // When the game is finished, the player is leaving the screen
        if (Status == GameStatus.GAME_IS_FINISHED)
        {
            Avatar.PlayerSpriteParent.transform.localPosition += Vector3.right * Time.deltaTime * 425.0f;
        }
    }

    public void PlayerHasMissedGoodLetter()
    {
        Debug.Log("Is missed !");

        _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
    }

    public override void GameEnded()
    {
        base.GameEnded();

        Invoke("StartAnimatiqueEnd", DurationPlayerOutOfScreen);

    }

    private void MoveUpCharacter()
    {
        if (_currentLane > 0)
        {
            _currentLane--;
            Avatar.MoveToLane(_currentLane);

            //if (RunnerHUD != null && RunnerHUD.ArrowAppearance == 1)
            //{
            //    RunnerHUD.ShowSecondArrow();
            //}
        }
    }

    private void MoveDownCharacter()
    {
        if (_currentLane < Lanes.Length - 1)
        {
            _currentLane++;
            Avatar.MoveToLane(_currentLane);

            //if (RunnerHUD != null && RunnerHUD.ArrowAppearance == 2)
            //{
            //    RunnerHUD.HideSecondArrow();
            //}
        }
    }
}
