﻿/**********************************************************************
* PanelTutorialBehaviour.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelTutorialBehaviour : MonoBehaviour
{
    public int DurationDisparition;

	void Start ()
    {
        // In DurationTutorialPopUp seconds, the panel will disappear
        Invoke("FadeOut", Constants.HUD.DurationTutorialPopUp - DurationDisparition);

        // And its childs will also be destroyed
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject, Constants.HUD.DurationTutorialPopUp);
        }
	}

    private void FadeOut()
    {
        TweenAlpha.Begin(gameObject, DurationDisparition, 0.0f);
        Destroy(gameObject, DurationDisparition);
    }
}
