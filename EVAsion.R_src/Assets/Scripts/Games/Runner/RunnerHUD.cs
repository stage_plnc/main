﻿/**********************************************************************
* RunnerHUD.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerHUD : GameHUD
{
    [Header("Tutorial")]
    public int ArrowAppearance = 0;

    public PanelTutorialBehaviour PanelTutorialBehaviour;

    public GameObject ArrowUp;
    public GameObject ArrowDown;

    // When the player swipes up, this arrow will disappear
    public void ShowFirstArrow()
    {
        ArrowAppearance = 1;

        TweenAlpha.Begin(ArrowUp, 1.0f, 1.0f);
    }

    // .. and this one will appear
    public void ShowSecondArrow()
    {
        ArrowAppearance = 2;

        TweenAlpha.Begin(ArrowUp, 1.0f, 0.0f);
        TweenAlpha.Begin(ArrowDown, 1.0f, 1.0f);
    }

    // When the player swipes down, the second arrow disappear, and the game can start
    public void HideSecondArrow()
    {
        TweenAlpha.Begin(ArrowDown, 1.0f, 0.0f);

        ArrowAppearance = 0;

        RunnerMgmt.Instance.EndTutorial();
    }
}
