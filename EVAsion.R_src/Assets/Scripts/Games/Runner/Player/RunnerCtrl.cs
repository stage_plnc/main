﻿/**********************************************************************
* RunnerCtrl.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class RunnerCtrl : SwipeControler
{
    /*
     *  Player controles for the runner
     */
    public float InitialSpeed;

    public int OutOfScreenX = (int) -(Constants.WidthScreen / 2.0f) - 128;
    public int GamePositionX = -700;

    private float _speedperS;
    public float SpeedPerS
    {
        get
        {
            return _speedperS;
        }
    }

    private float _nextSpeed; 
    public float NextSpeed
    {
        get
        {
            return _nextSpeed;
        }
    }

    private float _initialSpeedChange;
    private float _durationChangeSpeed = 0.0f;
    private bool _changeSpeed = false;

    private float _time = 0.0f;

    protected override void CtrlUpdate()
    {
        // Do the first move, when the game appears from the left of the screen
        if (RunnerMgmt.Instance.Status == GameMgmt.GameStatus.APPARITION_PLAYER)
        {
            _time += Time.deltaTime / Constants.Runner.GameStartCountdown;
            transform.localPosition = Vector3.Lerp(new Vector3(OutOfScreenX, 0.0f), new Vector3(GamePositionX, 0.0f), _time);
        }
        // If we are during the game, move the player to the right
        else if (RunnerMgmt.Instance.IsGameRunning())
        {
            base.CtrlUpdate();

            this.transform.localPosition += Vector3.right * SpeedPerS * Time.deltaTime;

            if (_changeSpeed)
            {
                _durationChangeSpeed += Time.deltaTime;

                _speedperS = Mathf.Lerp(_initialSpeedChange, _nextSpeed, _durationChangeSpeed / Constants.Runner.InterludeDuration);

                if (_durationChangeSpeed >= Constants.Runner.InterludeDuration)
                {
                    _changeSpeed = false;
                    _speedperS = _nextSpeed;
                }
            }
        }

 
    }

    public void SetInitialSpeed()
    {
        _speedperS = InitialSpeed;
    }

    public void SetTutorialSpeed()
    {
        SetInitialSpeed();
    }

    public void ChangeSpeed(float factor)
    {
        _initialSpeedChange = SpeedPerS;
        _nextSpeed = InitialSpeed * factor;

        _durationChangeSpeed = 0.0f;

        _changeSpeed = true;
    }
}
