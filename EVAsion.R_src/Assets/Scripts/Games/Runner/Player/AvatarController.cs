﻿/**********************************************************************
* AvatarController.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

[RequireComponent(typeof(SetRenderQueue))]
public class AvatarController : MonoBehaviour
{
    /**
     * Control the animations of the avatar and its movement.
     */

    public GameObject GameObjectToMove;

    public GameObject PlayerSpriteParent;
    public Animator PlayerAnimator;

    public float TweenPosTime = 1;

    public float SpeedAnimationFactor = 2.0f;

    private float _initialScale;
    private SetRenderQueue _setRenderQueue;

    private void Awake()
    {
        _setRenderQueue = GetComponent<SetRenderQueue>();

        _initialScale = PlayerSpriteParent.transform.localScale.x;
    }

    
    public void StartRunningAnimation()
    {
        PlayerAnimator.SetBool("running", true);
    }

    public void StopRunningAnimation()
    {
        PlayerAnimator.SetBool("running", false);
    }

    public void Update()
    {
        if (RunnerMgmt.Instance.Status != GameMgmt.GameStatus.GAME_IS_FINISHED)
        {
            if (RunnerMgmt.Instance.Controller.NextSpeed != RunnerMgmt.Instance.Controller.SpeedPerS)
            {
                if (RunnerMgmt.Instance.Controller.SpeedPerS != 0.0f)
                {
                    PlayerAnimator.speed = (RunnerMgmt.Instance.Controller.SpeedPerS / RunnerMgmt.Instance.Controller.InitialSpeed) * SpeedAnimationFactor;
                }
            }
        }
    }


    public void SetStartLane(int indexLane)
    {
        GameObject lane = RunnerMgmt.Instance.Lanes[indexLane];

        _setRenderQueue.RenderQueue = lane.GetComponent<UIPanel>().startingRenderQueue + 1;

        GameObjectToMove.transform.localPosition = RunnerMgmt.Instance.PlayerPositionByLane[indexLane].localPosition;
        PlayerSpriteParent.transform.localScale = Vector3.one * RunnerMgmt.Instance.ScaleByLane[indexLane] * _initialScale;
    }

    public void MoveToLane(int indexLane)
    {
        GameObject lane = RunnerMgmt.Instance.Lanes[indexLane];
        Vector2 newPosition = RunnerMgmt.Instance.PlayerPositionByLane[indexLane].localPosition;

        _setRenderQueue.RenderQueue = lane.GetComponent<UIPanel>().startingRenderQueue + 1;
        
        TweenPosition.Begin(GameObjectToMove, TweenPosTime, newPosition);
        TweenScale.Begin(PlayerSpriteParent, TweenPosTime, Vector3.one * RunnerMgmt.Instance.ScaleByLane[indexLane] * _initialScale);

        // TODO : Temporary value of the volume
        // SoundMgmt.Instance.PlayOneShotFXSound(MoveToLaneSound, 0.3f);
    }
}
