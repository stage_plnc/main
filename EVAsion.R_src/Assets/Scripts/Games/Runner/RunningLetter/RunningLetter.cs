﻿/**********************************************************************
* RunningLetter.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class RunningLetter : MonoBehaviour
{
    [Header("Label")]
    public UIPanel PanelParent;
    public UILabel Label;
    public TweenAlpha TweenAlphaLabel;

    [Header("Misc")]
    public bool IsGoodCombination = false;
    public int MaxSortingLayers = 5;

    [Header("Sounds")]
    public AudioClip SoundSuccess;
    public AudioClip SoundFailure;

    [Header("Feedback System")]
    public GameObject FeedbackBadPrefab;
    public GameObject FeedbackGoodPrefab;

    [Header("Animation")]
    public Animator Animator;

    [Header("Scale")]
    public static float[] ScaleByLane = { 0.5f, 0.8f, 1.0f };

    [Header("Materials")]
    public Renderer TroncRenderer;
    public Material TroncMaterialBase;
    public float[] ShadesOfGray = { 1.0f, 0.9f, 0.8f, 0.7f, 0.6f };
    public float verticalShifting = 15.0f;

    private GameObject _lane;
    private Material[] _troncsMaterial;
    private Dictionary<Renderer, int> _sortingOrderByRenderer;
    private List<RunningLetter> _associatedCharacters;
    private List<Renderer> _allRenderers;

    private string _fullCombination;
    private bool _hasCollidedWithFakeCollider = false;
    public bool _isHead = false;

    public string Text
    {
        get
        {
            return Label.text;
        }
    }

    private void Awake()
    {
        _associatedCharacters = new List<RunningLetter>();
        _sortingOrderByRenderer = new Dictionary<Renderer, int>();
        _troncsMaterial = new Material[ShadesOfGray.Length];
        _allRenderers = new List<Renderer>();

        for (int i = 0; i < ShadesOfGray.Length; i++)
        {
            _troncsMaterial[i] = Instantiate(TroncMaterialBase);
            _troncsMaterial[i].color = new Color(ShadesOfGray[i], ShadesOfGray[i], ShadesOfGray[i]);
        }

        LoadSortingOrders();
    }

    private void Start()
    {
        RunnerMgmt.Instance.ShowLetters += ShowLetter;
        RunnerMgmt.Instance.HideLetters += HideLetter;
    }

    private void OnDestroy()
    {
        if (RunnerMgmt.Instance != null)
        {
            RunnerMgmt.Instance.ShowLetters -= ShowLetter;
            RunnerMgmt.Instance.HideLetters -= HideLetter;
        }
    }

    private void LoadSortingOrders()
    {
        // We store the base sorting order for each part of the letter
        foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>())
        {
            _sortingOrderByRenderer[renderer] = renderer.sortingOrder;

            _allRenderers.Add(renderer);
        }
    }

    public void Init(RunningLetterData runningLetterData, char character)
    {
        _lane = RunnerMgmt.Instance.Lanes[runningLetterData.LaneIndex];
        _fullCombination = runningLetterData.TextPut;

        Label.text = character.ToString();
        IsGoodCombination = runningLetterData.IsGoodCombination;

        Animator.Play(Constants.Runner.RunAnimationId, -1, runningLetterData.DelayAnimation);
    }

    public void AddAssociatedCharacter(RunningLetter runningLetter)
    {
        _associatedCharacters.Add(runningLetter);
    }

    public void ClearNeighbours()
    {
        _associatedCharacters.Clear();
    }

    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Character") && RunnerMgmt.Instance.CurrentLane == _lane)
        {
            bool goodTarget = RunnerMgmt.Instance.PlayerCollidesWithTarget(this);
            GameObject feedbackPrefab = goodTarget ? FeedbackGoodPrefab : FeedbackBadPrefab;

            if (goodTarget)
            {
                SoundMgmt.Instance.PlayOneShotFXSound(SoundSuccess, 1.0f);
                // PRSVL_PlayerMgnt.Instance.CurrentPlayer.AddCurrentResult(PRSVL_PlayerMgnt.Instance.CurrentPlayer.LastExercice, PRSVL_PlayerMgnt.Instance.GamesNames[0], 1);
            }
            else
            {
                SoundMgmt.Instance.PlayOneShotFXSound(SoundFailure, 1.0f);
                // PRSVL_PlayerMgnt.Instance.CurrentPlayer.AddCurrentResult(PRSVL_PlayerMgnt.Instance.CurrentPlayer.LastExercice, PRSVL_PlayerMgnt.Instance.GamesNames[0], 0);

            }

            GameObject go = NGUITools.AddChild(transform.parent.gameObject, feedbackPrefab);
            go.transform.position = transform.position;
            go.GetComponent<TextFeedBack>().Init(_fullCombination);

            Destroy(go, go.GetComponent<TextFeedBack>().GDuration + 0.25f);

            // We're iterating to reveal the particle of the associated characters
            //foreach (RunningLetter associatedLetter in _associatedCharacters)
            //{
            //    RevealFeedBackParticle(associatedLetter.Label.text[0], associatedLetter.transform.parent.gameObject, particlePrefab);
            //}
            //RevealFeedBackParticle(Label.text[0], this.transform.parent.gameObject, particlePrefab);

            RunnerMgmt.Instance.PoolingMgmt.HideGameObject(gameObject);

            // Iterate through the list of the associated letters, and destroy them... (I mean, hide them)
            for (int i = 0; i < _associatedCharacters.Count; i++)
            {
                RunnerMgmt.Instance.PoolingMgmt.HideGameObject(_associatedCharacters[i].gameObject);
            }

            _associatedCharacters.Clear();
        }
        else if (Constants.HardFeedParseval && other.CompareTag("PRSVL_FakeCollider"))
        {
            if (_isHead && _hasCollidedWithFakeCollider == false && IsGoodCombination)
            {
                RunnerMgmt.Instance.PlayerHasMissedGoodLetter();
                _hasCollidedWithFakeCollider = true;
            }
        }
    }

    //void RevealFeedBackParticle(char letter, GameObject parent, GameObject prefab)
    //{
    //    // Since the Label is just one letter, we're getting the index 0
    //    // We substract the value of 'A' to get a number between 0 and 26, corresponding to the index of the letter
    //    // And also! We can have lower case, so we upper the letter
    //    int indexLetter = Char.ToUpper(letter) - 'A';

    //    // TODO What to do in other cases?
    //    if (indexLetter >= 0 && indexLetter <= Constants.NbrOfLetterLatinAlphabet - 1)
    //    {
    //        // Can't use GetRequiredComponent because ParticleSystem is not a Monobehaviour
    //        ParticleSystem particles = NGUITools.AddChild(parent, prefab).GetComponent<ParticleSystem>();
    //        particles.transform.position = this.transform.position;

    //        // By default, the texture sprite sheet is only uppercases. If the letter is lower case, we're changing the material
    //        if (Char.IsLower(letter))
    //        {
    //            particles.GetComponent<Renderer>().material = LowercaseLettersTextureSheet; 
    //        }

    //        // See The TextureSheetAnimation inside the ParticleSystem.
    //        // textureSheetAnimationModule.rowIndex = indexLetter set the letter that will be shown.
    //        ParticleSystem.TextureSheetAnimationModule textureSheetAnimationModule = particles.textureSheetAnimation;
    //        textureSheetAnimationModule.rowIndex = indexLetter;

    //        Destroy(particles.gameObject, particles.main.duration);
    //    }
    //}

    public void SetOrder(int position)
    {
        _isHead = position == 0;
        // First, we take care of the he sorting layer of ALL the materials
        foreach (Renderer renderer in _allRenderers)
        {
            renderer.sortingOrder = _sortingOrderByRenderer[renderer] + (MaxSortingLayers - position) * MaxSortingLayers;
        }

        TroncRenderer.material = _troncsMaterial[position];

        PanelParent.sortingOrder = (MaxSortingLayers - position + 1) * MaxSortingLayers;

        // Then, we shift the sprite a little up to make a QUINCONCE effect
        transform.localPosition += Vector3.up * (verticalShifting * position);
    }

    public void HideLetter()
    {
        TweenAlphaLabel.PlayForward();
    }

    public void ShowLetter()
    {
        TweenAlphaLabel.PlayReverse();
    }
}
