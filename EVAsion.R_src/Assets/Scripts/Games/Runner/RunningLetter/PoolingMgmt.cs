﻿/**********************************************************************
* PoolingMgmt.cs
* Created at 10-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoolingMgmt : MonoBehaviour
{
    public GameObject RunningLetterPrefab;

    public int AmountToPool;

    // TODO : Why a dictionary ?
    //private Dictionary<int, GameObject> _pooledObjects;
    //private Dictionary<int, bool> _usedInScene;

    private List<GameObject> _pooledObjects;
    private List<bool> _usedInScene;


    void Awake()
    {
        _pooledObjects = new List<GameObject>();
        _usedInScene = new List<bool>();
    }

    void Start()
    {
        for (int i = 0; i < AmountToPool; i++)
        {
            GameObject obj = CreateRunningLetter();

            _pooledObjects.Add(obj);
            _usedInScene.Add(false);
        }

    }

    public GameObject GetPooledObject()
    {
        GameObject returnedObject = null;

        for (int i = 0; i < _pooledObjects.Count; i++)
        {
            if (!_usedInScene[i])
            {
                _usedInScene[i] = true;
                returnedObject = _pooledObjects[i];

                break;
            }
        }

        if (returnedObject == null)
        {
            // If there is not enough objects, we create a new one
            returnedObject = CreateRunningLetter();

            _pooledObjects.Add(returnedObject);
            _usedInScene.Add(true);
        }

        return returnedObject;
    }

    public void HideGameObject(GameObject go)
    {
        // Constants.HeigthScreen * 2.0f because we want to set the position of the letter out of the screen
        go.transform.localPosition = new Vector2(0, Constants.HeigthScreen * 2.0f);
    }

    public void ResetPool()
    {
        for (int i = 0; i < _pooledObjects.Count; i++)
        {
            if (_usedInScene[i])
            {
                _pooledObjects[i].transform.SetParent(gameObject.transform, false);
                _usedInScene[i] = false;

                HideGameObject(_pooledObjects[i]);

                _pooledObjects[i].GetComponent<RunningLetter>().ClearNeighbours();
            }
        }
    }

    private GameObject CreateRunningLetter()
    {
        GameObject obj = NGUITools.AddChild(gameObject, RunningLetterPrefab);

        HideGameObject(obj);

        return obj;
    }
}
