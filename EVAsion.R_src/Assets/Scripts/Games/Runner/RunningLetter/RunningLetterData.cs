﻿/**********************************************************************
* RunningLetterData.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct RunningLetterData
{
    public bool IsGoodCombination;
    public int LaneIndex;
    public string TextPut;
    public float DelayAnimation;

    public RunningLetterData(int laneIndex, string textPut, bool isGoodCombiation)
    {
        LaneIndex = laneIndex;
        TextPut = textPut;
        IsGoodCombination = isGoodCombiation;

        DelayAnimation = 0.0f;
    }
}
