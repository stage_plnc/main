﻿/**********************************************************************
* RunnerCycleData.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerCycleData : CycleData
{
    public int NumTargotToGet;
    public int NumBadTarget;

    public RunnerCycleData(string combination, int numTargotToGet, int numBadTarget) : base(combination, numTargotToGet)
    {
        this.NumTargotToGet = numTargotToGet;
        this.NumBadTarget = numBadTarget;
    }
}
