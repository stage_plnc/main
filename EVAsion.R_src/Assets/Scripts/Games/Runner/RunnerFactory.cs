﻿/**********************************************************************
* RunnerFactory.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

using System.Text;
using System;
using System.Linq;

[RequireComponent(typeof(RunnerMgmt))]
public class RunnerFactory : CycleFactory
{
    [Header("RunnerFactory attributes")]
    public GameObject Player;

    public int GapBetweenRunningLetter = 60;

    public GameObject[] CycleGOByLane;

    private float _distancePerCycle;
    private float _distancePerInterdule;
    private float _sizeOfLaneColumn;

    private RunnerDifficultyData _difficultyData;

    private enum CaseStatus
    {
        EMPTY = 0,
        GOOD_TARGET = 1,
        BAD_TARGET = 2
    };

    protected override void Awake()
    {
        base.Awake();
    }

    public override CycleData InitNextCycle(LPNC_DataExport parsevalDT)
    {
        base.InitNextCycle(parsevalDT);

        float acceleration = 0.0f;

        HandleDifficultyData(parsevalDT, Constants.DifficultyMethod);

        RunnerMgmt.Instance.Controller.ChangeSpeed(_difficultyData.speed);

        // During the game cycle, the speed doesnt change, so is the distance
        _distancePerCycle = Constants.Runner.CycleDuration * RunnerMgmt.Instance.Controller.NextSpeed;

        // Because we linearly decreases/increases the speed of the player during an interlude, this formula calcualte the distance
        acceleration = (RunnerMgmt.Instance.Controller.NextSpeed - RunnerMgmt.Instance.Controller.SpeedPerS) / Constants.Runner.InterludeDuration;

        _distancePerInterdule =
            RunnerMgmt.Instance.Controller.SpeedPerS * Constants.Runner.InterludeDuration
            + 0.5f * acceleration * (Constants.Runner.InterludeDuration * Constants.Runner.InterludeDuration);

        // We're substracting a little offset, to make sure that the last letter is out of screen
        _sizeOfLaneColumn = ((_distancePerCycle + RunnerMgmt.Instance.Controller.GamePositionX) / (_difficultyData.numCol + 1));

        ShiftCycleGameObjects();

        _goodCombination = GenerateGoodCombination();
        _wrongCombination = GenerateWrongCombination();

        Debug.Log("Diff target = " + _difficultyData.DifficultyTarget + ", DifficultyDistractor = " + _difficultyData.DifficultyDistractor);
        Debug.Log("Good combination = " + _goodCombination + ". Wrong combination = " + _wrongCombination);

        _currentCycle++;

        // Apparently, we have to show the # if there is one 
        return new RunnerCycleData(_goodCombination, _difficultyData.numTargetToGet, _difficultyData.numBadTarget);
    }

    private void HandleDifficultyData(LPNC_DataExport parsevalDt, string difficulty)
    {
        Debug.Log("Difficulty = " + difficulty);

        if (difficulty == "random")
        {
            parsevalDt.d01 = _random.Next(0, 6);
            parsevalDt.d02 = _random.Next(0, 6);
            parsevalDt.d03 = _random.Next(0, 6);
        }

        // Not sure...
        else if (difficulty == "linear")
        {
            if (_currentCycle >= 1)
            {
                parsevalDt.d01 = (int) Mathf.Round(_initialParsevalDT.d01 + _factLinearDifficultyIncrease[0] * _currentCycle);
                parsevalDt.d02 = (int) Mathf.Round(_initialParsevalDT.d02 + _factLinearDifficultyIncrease[1] * _currentCycle);
                parsevalDt.d03 = (int) Mathf.Round(_initialParsevalDT.d03 + _factLinearDifficultyIncrease[2] * _currentCycle);
            }
            else
            {
                parsevalDt.d01 = (int) Constants.LinearDifficultyStartBound.x;
                parsevalDt.d02 = (int) Constants.LinearDifficultyStartBound.y;
                parsevalDt.d03 = (int) Constants.LinearDifficultyStartBound.z;

                _factLinearDifficultyIncrease[0] = (float) ((int) Constants.LinearDifficultyEndBound.x - parsevalDt.d01) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[1] = (float) ((int)Constants.LinearDifficultyEndBound.y - parsevalDt.d02) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[2] = (float) ((int) Constants.LinearDifficultyEndBound.z - parsevalDt.d03) / (Constants.NbrTotalCycles - 1);

                _initialParsevalDT = parsevalDt;
            }
        }

        Debug.LogWarningFormat("d01 = {0}, d02 = {1}, d03 = {2}", parsevalDt.d01, parsevalDt.d02, parsevalDt.d03);

       _parsevalDT = parsevalDt;

        _difficultyData = GetDifficultyData(parsevalDt);
    }

    public override void GenerateNextCycle()
    {
        GenerateRunningLetters(_difficultyData);
    }

    private void ShiftCycleGameObjects()
    {
        // We want to shift the first running letter out of the screen, so we're deciding : 
        // either the distance per interlude is long enough, or we're using the screen size
        int offsetX = (int)Mathf.Max(_distancePerInterdule,
            Constants.WidthScreen / 2.0f + RunnerMgmt.Instance.Controller.GamePositionX);

        for (int i = 0; i < CycleGOByLane.Length; i++)
        {
            CycleGOByLane[i].name = CurrentSeed;
            CycleGOByLane[i].transform.localPosition = new Vector3(Player.transform.localPosition.x + offsetX, 0, 0);
        }
    }

    #region GenerationFunctions
    private string GenerateGoodCombination()
    {
        string goodCombination = string.Empty; 

        if (_difficultyData.DifficultyTarget >= 3)
        {
            _indexNewPattern = TargetGeneratorMgmt.Instance.GetRandomIndex(_random, _difficultyData.DifficultyTarget);

            int cran = -1;
            if (_difficultyData.DifficultyTarget >= 3 && _difficultyData.DifficultyTarget <= 4)
                cran = 1;
            if (_difficultyData.DifficultyTarget >= 5 && _difficultyData.DifficultyTarget <= 6)
                cran = 3;

            string[] array = TargetGeneratorMgmt.Instance.GetCombinationAt(_random, _indexNewPattern, cran);
            _leftPartCombination = array[0].Trim();
            _rightPartCombination = array[1].Trim();

            if (_upperCaseSession)
            {
                _leftPartCombination = _leftPartCombination.ToUpper();
                _rightPartCombination = _rightPartCombination.ToUpper();
            }
            else
            {
                _leftPartCombination = _leftPartCombination.ToLower();
                _rightPartCombination = _rightPartCombination.ToLower();
            }

            // Hack, must be changed
            if (_difficultyData.DifficultyTarget % 2 == 0)
            {
                _leftPartCombination = "#" + _leftPartCombination;
                _rightPartCombination = _rightPartCombination + "#";
            }

            goodCombination = _leftPartCombination + _rightPartCombination;
        }
        else
        {
            // Generate only one random letter... Forget leftPart and rightPart.
            char character = (char) (_random.Next(0, 26) + 'A');

            goodCombination = character.ToString();

            if (_difficultyData.DifficultyTarget == 2)
            {
                goodCombination = '#' + goodCombination + '#';
            }

            _leftPartCombination = _rightPartCombination = string.Empty;
        }

        if (_upperCaseSession)
        {
            goodCombination = goodCombination.ToUpper();
        }
        else
        {
            goodCombination = goodCombination.ToLower();
        }

        return goodCombination;
    }

    private string GenerateWrongCombination()
    {
        string wrongCombination = string.Empty;

        // We dont need to generate a wrong combination based on a good combination
        int nbrOfletters = _difficultyData.GetNumberOfLetters();

        if (_difficultyData.DifficultyTarget >= 3)
        {
            wrongCombination = DistractorsMgmt.Instance.GenerateWrongCombinationRunnerTD(
                _goodCombination, nbrOfletters, _difficultyData.DifficultyDistractor);
        }
        else
        {
            StringBuilder wrongCombinationSB = new StringBuilder(_goodCombination);

            for (int i = 0; i < wrongCombinationSB.Length; i++)
            {
                if (Char.IsLetter(wrongCombinationSB[i]))
                {
                    char closeCharacter = DistractorsMgmt.Instance.GetDistractor(_random, wrongCombinationSB[i], _difficultyData.DifficultyDistractor);

                    wrongCombinationSB[i] = closeCharacter;
                }
            }

            wrongCombination = wrongCombinationSB.ToString();
        }

        if (_upperCaseSession)
        {
            wrongCombination = wrongCombination.ToUpper();
        }
        else
        {
            wrongCombination = wrongCombination.ToLower();
        }

        return wrongCombination;
    }

    private void GenerateRunningLetters(RunnerDifficultyData difficultyData)
    {
        // Array containing each letter lane and text
        RunningLetterData[] lettersData;
        // This array contains the content (empty, good target or bad target) of the game cycle
        CaseStatus[] columns = GenerateLettersColumns(difficultyData);
        // Set the target data lane and letter
        lettersData = FillLettersData(columns);

        for (int i = 0; i < lettersData.Length; i++)
        {
            if (columns[i] != CaseStatus.EMPTY)
            {
                GenerateRunningLetter(i, lettersData[i]);
            }
        }
    }

    private CaseStatus[] GenerateLettersColumns(RunnerDifficultyData difficultyData)
    {
        CaseStatus[] columns = new CaseStatus[difficultyData.numCol]; // Contains 0 if no target on the nth case, 1 if bad target, 2 if good target

        // Fill columns
        int numGoodTargetPut = 0;
        int numBadTargetPut = 0;
        for (int i = 0; i < difficultyData.numCol; i++)
        {
            Assert.IsTrue(difficultyData.numCol - i >= difficultyData.numBadTarget - numBadTargetPut + difficultyData.numTargetToGet - numGoodTargetPut);
            // Array to make probabilities
            // The probability that a target is set on the column evoluate relative to the numTargetPut and the current column
            CaseStatus[] probs = new CaseStatus[difficultyData.numCol - i];

            // Fill with 0
            for (int j = 0; j < probs.Length; j++)
            {
                probs[j] = CaseStatus.EMPTY;
            }
            // Added 1 for bad targets
            for (int j = 0; j < difficultyData.numBadTarget - numBadTargetPut; j++)
            {
                probs[j] = CaseStatus.GOOD_TARGET;
            }
            // Added 2 for good targets
            for (int j = 0; j < difficultyData.numTargetToGet - numGoodTargetPut; j++)
            {
                probs[difficultyData.numBadTarget - numBadTargetPut + j] = CaseStatus.BAD_TARGET;
            }
            columns[i] = probs[_random.Next(0, probs.Length)];
            if (columns[i] == CaseStatus.GOOD_TARGET)
            {
                numBadTargetPut++;
            }
            else if (columns[i] == CaseStatus.BAD_TARGET)
            {
                numGoodTargetPut++;
            }
        }

        return columns;

    }

    private RunningLetterData[] FillLettersData(CaseStatus[] columns)
    {
        RunningLetterData[] runningLettersData = new RunningLetterData[columns.Length];
        RunningLetterData previousRunningLetterData = new RunningLetterData();

        for (int i = 0; i < columns.Length; i++)
        {
            CaseStatus caseStatus = columns[i];

            if (caseStatus == CaseStatus.GOOD_TARGET)
            {
                // If it should be a good target, put the string to get
                runningLettersData[i].TextPut = _goodCombination;
                runningLettersData[i].IsGoodCombination = true;
            }
            else if (caseStatus == CaseStatus.BAD_TARGET)
            {
                runningLettersData[i].TextPut = _wrongCombination;
                runningLettersData[i].IsGoodCombination = false;
            }

            #region Choose the lane relative to the precedent lane
            if (caseStatus != CaseStatus.EMPTY && i > 0)
            {
                int laneIndex = -1;

                // If the previous letter is the same as this one
                if (previousRunningLetterData.IsGoodCombination && runningLettersData[i].IsGoodCombination)
                {
                    // We're setting the lane of this one with a maximum of one "distance"
                    // Note : _random.Next(min, max) returns a value including min but excluding max
                    if (previousRunningLetterData.LaneIndex == 0)
                    {
                        // 0 or 1 
                        laneIndex = _random.Next(0, 2);
                    }
                    else if (previousRunningLetterData.LaneIndex == 1)
                    {
                        // 0, 1 or 2
                        laneIndex = _random.Next(0, 3);
                    }
                    else
                    {
                        // 1 or 2
                        laneIndex = _random.Next(1, 3);
                    }
                }
                else
                {
                    laneIndex = _random.Next(0, RunnerMgmt.Instance.Lanes.Length);
                }

                runningLettersData[i].LaneIndex = laneIndex;

                previousRunningLetterData = runningLettersData[i];
            }


            #endregion
        }

        return runningLettersData;
    }


    /* Generate a target ont the idx column, with all its data contained inside targetData (lane and letter)
    * There is an offsetX according to the speed of the last target (see above)
    */
    private void GenerateRunningLetter(int idx, RunningLetterData runningLetterData)
    {
        // Each letter has to run with the same animation. 
        runningLetterData.DelayAnimation = (float)_random.NextDouble();
        List<RunningLetter> listOfRunningLetters = new List<RunningLetter>();

        for (int i = 0; i < runningLetterData.TextPut.Length; i++)
        {
            RunningLetter runningLetter;
            GameObject tmpGO = null;
            float widthLetter = -1;

            // Add the target to the lane with the good position
            tmpGO = RunnerMgmt.Instance.PoolingMgmt.GetPooledObject();

            runningLetter = tmpGO.GetRequiredComponent<RunningLetter>();
            runningLetter.Init(runningLetterData, runningLetterData.TextPut[i]);

            tmpGO.transform.localScale = Vector3.one;

            tmpGO.transform.SetParent(CycleGOByLane[runningLetterData.LaneIndex].transform);
            // NGUITools.MarkParentAsChanged(tmpGO);

            tmpGO.transform.localScale =
                RunnerMgmt.Instance.PoolingMgmt.RunningLetterPrefab.transform.localScale * RunnerMgmt.Instance.ScaleByLane[runningLetterData.LaneIndex];

            widthLetter = GapBetweenRunningLetter * tmpGO.transform.localScale.x;

            // Little hack
            //if (runningLetterData.TextPut[i] == '#')
            //{
            //    Vector3 scale = tmpGO.transform.localScale;
            //    scale.x *= 0.7f;

            //    tmpGO.transform.localScale = scale;

            //}

            {
                tmpGO.transform.localPosition =
                    new Vector2((idx + 1) * _sizeOfLaneColumn + i * widthLetter, RunnerMgmt.Instance.LetterPositionByLane[runningLetterData.LaneIndex].localPosition.y);
            }
            runningLetter.SetOrder(i);

            listOfRunningLetters.Add(runningLetter);
        }

        // After that, we're associating each letter with one another
        for (int i = 0; i < listOfRunningLetters.Count; i++)
        {
            for (int j = 0; j < listOfRunningLetters.Count; j++)
            {
                if (i != j)
                {
                    listOfRunningLetters[i].AddAssociatedCharacter(listOfRunningLetters[j]);
                }
            }
        }
    }

    #endregion


    #region DifficultyData
    private RunnerDifficultyData GetDifficultyData(LPNC_DataExport parsevalDT)
    {
        float speed = 1;
        int numTargetToGet = 1;
        int numBadTarget = 1;
        int numCol = 2;

        int difficultyDistractor = 1;
        int difficultyTarget = 1;

        try
        {
            difficultyTarget = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("runner", "difficulty_target", parsevalDT.d01));

            if (difficultyTarget < 1 || difficultyTarget > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de runner.json : la dimension difficulty_target de la difficulté " + parsevalDT.d01 +
                    "doit etre comprise entre 1 et 6");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de runner.json a la dimension combination_pattern de la difficulté " + parsevalDT.d01);
        }
        try
        {
            difficultyDistractor = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("runner", "difficulty_distractor", parsevalDT.d02));

            if (difficultyDistractor < 1 || difficultyDistractor > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de runner.json : la dimension ratio_similarity de la difficulté " + parsevalDT.d02 +
                    "doit etre comprise entre 1 et 6");

            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de runner.json a la dimension difficulty_similarity de la difficulté " + parsevalDT.d02);
        }
        try
        {
            string[] targetInfos = DifficultyMgmt.Instance.GetDifficultyValue("runner", "speed_and_target_quantity", parsevalDT.d03).Split(';');

            speed = float.Parse(targetInfos[0]);
            numTargetToGet = int.Parse(targetInfos[1]);
            numBadTarget = int.Parse(targetInfos[2]);
            numCol = int.Parse(targetInfos[3]);

            if (speed <= 0)
            {
                PrintToScreen.Print("Error while reading runner.json file on speed_and_target_quantity dimension from difficulty " + parsevalDT.d03 +
                    ".\n speed shloud be > 0");
            }
            if (numBadTarget + numTargetToGet > numCol)
            {
                PrintToScreen.Print("Error while reading runner.json file on speed_and_target_quantity dimension from difficulty " + parsevalDT.d03 + ".\n numBadTarget + numGoodTarget should be <= to numTargetLocations.");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Error while reading runner.json file on combinations dimension from difficulty " + parsevalDT.d03 + ".\nExpected format \"string1,string2,string3...\" but receive : "
                + DifficultyMgmt.Instance.GetDifficultyValue("runner", "combinations", parsevalDT.d03));
        }

        //if (Constants.NbrEvents != -1)
        //{
        //    numTargetToGet = Constants.NbrEvents;
        //}

        return new RunnerDifficultyData(difficultyTarget, difficultyDistractor, numCol, numBadTarget, numTargetToGet, speed);
    }

    private class RunnerDifficultyData : DifficultyData
    {
        public RunnerDifficultyData(int difficultyTarget, int difficultyDistractor, int numCol, int numBadTarget, int numTargetToGet, float speed)
            : base(difficultyTarget, difficultyDistractor)
        { 
            this.numCol = numCol;
            this.numBadTarget = numBadTarget;
            this.numTargetToGet = numTargetToGet;
            this.speed = speed;
        }

        public int numCol;
        public int numBadTarget;
        public int numTargetToGet;
        public float speed;
    }
    #endregion
}