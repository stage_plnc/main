﻿/**********************************************************************
* Map.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 3D reprensentation of our forest using a matrix.
/// </summary>
public class Map
{
    #region properties

    public int Width
    {
        get
        {
            return _width;
        }
    }

    public int Height
    {
        get
        {
            return _height;
        }
    }

    public int Depth
    {
        get
        {
            return _depth;
        }
    }

    #endregion
  
    public Map (int width, int height, int depth, Vector3 worldPosition, int pixelWidth, int pixelHeight)
    {
        _width = width;
        _height = height;
        _depth = depth;

        //_worldPosition = worldPosition;
        //_pixelHeight = pixelHeight;
        //_pixelWidth = pixelWidth;

        _matrix = new MapCell[width, height, depth];

        // Fill the matrix with empty cases.
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int z = 0; z < depth; z++)
                {
                    _matrix[x, y, z] = new MapCell(new Vector3(x, y, z));
                }
            }
        }
    }

    public MapCell GetCell(int x, int y, int z)
    {
        return _matrix[x, y, z];
    }

    public MapCell GetCell(Vector3 mapPosition)
    {
        return _matrix[(int)mapPosition.x, (int)mapPosition.y, (int)mapPosition.z];
    }

    public void SetCellVisible(int x, int y, int z, bool visible)
    {
        _matrix[x, y, z].IsVisible = visible;
    }

    /// <summary>
    /// Is the given position in bounds ? (it's a cell of the grid)
    /// </summary>
    /// <param name="x">x position</param>
    /// <param name="y">y position</param>
    /// <param name="z">z position</</param>
    /// <returns>If the given position is in bounds</returns>
    public bool IsCell(int x, int y, int z)
    {
        return x >= 0 && x < Width && y >= 0 && y < Height && z >= 0 && z < Depth;
    }

    /// <summary>
    /// Fill the matrix with empty cases.
    /// </summary>
    public void Reset()
    {
        // Fill the matrix with empty cases.
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                for (int z = 0; z < _depth; z++)
                {
                    _matrix[x, y, z].IsVisible = true;
                }
            }
        }
    }

    /// <summary>
    /// A cell is visible if there is nothing in front of it from t z = 0 point of view.
    /// </summary>
    /// <param name="posX"></param>
    /// <param name="posY"></param>
    /// <param name="posZ"></param>
    /// <returns>True if the cell is visible.</returns>
    public bool IsCellVisible(int posX, int posY, int posZ)
    {
        for (int z = 0; z < posZ; z++)
        {
            if (!_matrix[posX, posY, z].IsVisible)
            {
                return false;
            }
        }
        return true;
    }

    public bool IsCellVisible(Vector3 position)
    {
        return IsCellVisible((int) position.x, (int) position.y, (int) position.z);
    }

    /// <summary>
    /// Returns the position in pixels of the cell at the given position in the map.
    /// </summary>
    /// <param name="mapPosition">The position of the cell in the map</param>
    public Vector3 MapPositionToWorldPosition(Vector3 mapPosition)
    {
        return GetCell((int)mapPosition.x, (int)mapPosition.y, (int)mapPosition.z).WorldPosition;
    }

    public List<MapCell> GetAdjacentCell(Vector3 mapPosition)
    {
        int x = (int)mapPosition.x;
        int y = (int)mapPosition.y;
        int z = (int)mapPosition.z;
        List<MapCell> adjacentCells = new List<MapCell>();

        // Left and right
        if (IsCell(x + 1, y, z))
            adjacentCells.Add(GetCell(x + 1, y, z));
        if (IsCell(x - 1, y, z))
            adjacentCells.Add(GetCell(x - 1, y, z));
        // Up and down
        if (IsCell(x, y + 1, z))
            adjacentCells.Add(GetCell(x, y + 1, z));
        if (IsCell(x, y - 1, z))
            adjacentCells.Add(GetCell(x, y - 1, z));
        // Forward and backward
        if (IsCell(x, y, z + 1))
            adjacentCells.Add(GetCell(x, y, z + 1));
        if (IsCell(x, y, z - 1))
            adjacentCells.Add(GetCell(x, y, z - 1));

        return adjacentCells;
    }

    public List<MapCell> GetVisibleCell()
    {
        List<MapCell> visibleCell = new List<MapCell>();
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                for (int z = 0; z < _depth; z++)
                {
                    if (_matrix[x, y, z].IsVisible)
                        visibleCell.Add(_matrix[x, y, z]);
                }
            }
        }
        return visibleCell;
    }

    private MapCell[,,] _matrix;
    private int _width;
    private int _height;
    private int _depth;
    private Vector3 _worldPosition;
    private int _pixelWidth;
    private int _pixelHeight;
}
