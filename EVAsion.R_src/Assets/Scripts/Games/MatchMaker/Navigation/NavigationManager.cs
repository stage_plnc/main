﻿/**********************************************************************
* NavigationManager.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;

/// <summary>
/// Manage the map where the ghosts will move.
/// </summary>
public class NavigationManager : MonoBehaviour
{
    public Transform[] PlaceHolders; //< The three first layers position from the closer one to the farthest

    public Transform MapTablesParent;
    public GameObject MapTablePrefab;
    public GameObject CellPrefab;

    public float YOffset = 0;

    [Header("Map properties")]
    public int MapWidth;
    public int MapHeight;
    public int MapDepth = 3;
    public Transform MapObject;

    /// <summary>
    /// Generate a map with the attributes.
    /// The different layers of the map adjust their y scale, 
    /// but keep the x scale so that ghost can move on the whole screen.
    /// </summary>
    /// <returns></returns>
    public Map GenerateMap()
    {
        // Create the map object
        _map = new Map(MapWidth, MapHeight, MapDepth, MapObject.transform.localPosition, Constants.WidthScreen, Constants.MatchMaker.HeigthMapGhosts);

        // Adjust the cell prefab with the map dimensions
        CellPrefab.GetRequiredComponent<UIWidget>().width = MapObject.GetRequiredComponent<UIWidget>().width / _map.Width - (int)MapTablePrefab.GetRequiredComponent<UITable>().padding.x;
        CellPrefab.GetRequiredComponent<UIWidget>().height = MapObject.GetRequiredComponent<UIWidget>().height / _map.Height - (int)MapTablePrefab.GetRequiredComponent<UITable>().padding.y;

        // Create a map base on positions and scales of the placeholders
        for (int j = 0; j < Mathf.Min(_map.Depth, PlaceHolders.Length); j++)
        {
            Transform placeHolder = PlaceHolders[j];
            GameObject mapTable = NGUITools.AddChild(MapTablesParent.gameObject, MapTablePrefab);
            mapTable.gameObject.transform.localPosition = new Vector3(MapObject.localPosition.x, MapObject.localPosition.y + YOffset, MapObject.localPosition.z);
            mapTable.GetRequiredComponent<UITable>().columns = _map.Width;
            // Keep the x scale so that the ghost can move on all the screen
            mapTable.transform.localScale = new Vector3(PlaceHolders[0].transform.localScale.x, placeHolder.localScale.y, 0);

            // Insert the cells
            GameObject[] cells = new GameObject[_map.Width * _map.Height];
            for (int i = 0; i < cells.Length; i++)
            {
                GameObject cell = NGUITools.AddChild(mapTable.gameObject, CellPrefab);
                cells[i] = cell;
                cell.name = "Cell " + i; ;
            }

            // Force to reposition the cells
            mapTable.GetRequiredComponent<UITable>().Reposition();

            // Save the cells's positions
            for (int i = 0; i < cells.Length; i++)
            {
                GameObject cell = cells[i];
                _map.GetCell(i % _map.Width, i / _map.Width, j).WorldPosition = cell.transform.position;
                cell.GetRequiredComponent<Cell>().MapPosition = new Vector3(i % _map.Width, i / _map.Width, j);
            }
        }
        return _map;
    }

    private Map _map;
}
