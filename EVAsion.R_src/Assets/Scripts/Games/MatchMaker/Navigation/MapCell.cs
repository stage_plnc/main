﻿/**********************************************************************
* MapCell.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections.Generic;
using UnityEngine;

public class MapCell
{
    public Vector3 MapPosition;
    public Vector3 WorldPosition; //< To set after instanciation by the navigation manager

    public bool IsVisible = true;

    public MapCell(Vector3 mapPosition)
    {
        MapPosition = mapPosition;
    }
    public Ghost Ghost; // Can be null
    public bool HasGhost
    {
        get
        {
            return Ghost != null;
        }
    }
}
