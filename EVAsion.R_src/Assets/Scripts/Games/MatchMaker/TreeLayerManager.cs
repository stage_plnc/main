﻿/**********************************************************************
* TreeLayerManager.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;

/// <summary>
/// Manage the creation and evolution of the layers.
/// </summary>
public class TreeLayerManager : MonoBehaviour
{
    #region Layers attributes
    [Header("Layers")]
    [Tooltip("PlaceHolders from the closer one to the farthest. " +
        "Will be used to create and move the tree layers.")]
    public GameObject[] TreeLayerPlaceHolders;
    [Tooltip("The prefab of the tree layer that will be created.")]
    public GameObject TreeLayerPrefab;
    [Tooltip("Templates that will be randomly chosen during creation.")]
    public LayerTreeTemplateData[] DataTemplates;
    [Tooltip("The depth of the fog.")]
    public int FogDepthRange = 0;
    [Tooltip("The depth that will take the ghost when behind the layer.")]
    public int GhostDepthRangeBehindLayerObjects = -10;

    [Tooltip("The max value that can be removed from each color component (rgb) of sprites.")]
    [Range(0, 255)]
    public int ShadowValue;
    #endregion Layers properties

    #region Trees generation attributes
    [Header("Trees generation")]
    [Tooltip("Prefabs of trees. Will be randomly chosen during creation.")]
    public GameObject[] TreePrefabs;
    [Tooltip("The depth of the trees.")]
    public int TreeDepthRange = 100;
    [Tooltip("The scale of the trees.")]
    public float TreeScale = 0.5f;
    [Tooltip("The minimum distance between two decoration trees.")]
    [Range(0, Constants.WidthScreen)]
    public int MinDistanceBetweenDecoTrees = 250;
    [Tooltip("The maximum distance between two decoration trees.")]
    [Range(0, Constants.WidthScreen)]
    public int MaxDistanceBetweenDecoTrees = 800;
    [Tooltip("The number of trees per layer.")]
    public int NbTreesDecorations = 70;
    #endregion Trees generation attributes

    #region Ground decoration generation attributes
    [Header("Ground decoration generation")]
    [Tooltip("Prefabs of ground decorations. Will be randomly chosen during creation.")]
    public GameObject[] GroundDecorationPrefabs;
    [Tooltip("The widget in which we will create ground decorations (For side decorations, will just used the Y dimension).")]
    public UIWidget DecorsZoneDimensions;
    [Tooltip("The depth of the decors.")]
    public int DecorsDepthRange = 200;
    [Tooltip("The minimum number of decorations put on the main part of a layer.")]
    public int NbMinDecorsOnMainTrees = 2;
    [Tooltip("The maximum number of decorations put on the main part of a layer.")]
    public int NbMaxDecorsOnMainTrees = 4;
    [Tooltip("The minimum distance between two ground decorations.")]
    [Range(0, Constants.WidthScreen)]
    public int MinDistanceBetweenDeco = 250;
    [Tooltip("The maximum distance between two ground decorations.")]
    [Range(0, Constants.WidthScreen)]
    public int MaxDistanceBetweenDeco = 1500;
    [Tooltip("The distance between the zone where decorations are created on the main zone of a layer," +
        " and decorations that are on decorations zones.")]
    [Range(0, Constants.WidthScreen)]
    public int DistanceBetweenDecorsZoneDimensionsAndDecosDecorations = 100;
    #endregion Ground decoration generation attributes

    #region Fly circle generation
    [Header("Flying generation")]
    public FlyCircle CirclePrefab;
    public Vector2 MinMaxRangeNumberFlyCircle;
    public float FlyCircleDepth;
    public Vector2 RangeYPosition;
    #endregion Fly circle generation

    [Header("Destruction Info")]
    public int BottomLeftLimitX;
    public int TopRightLimitX;

    /// <summary>
    /// The tree layers from the foreground layer to the background layer.
    /// </summary>
    public GameObject[] MainTreeLayers
    {
        get
        {
            return _mainTreesLayers;
        }
    }

    /// <summary>
    /// Create all tree layers using the placeholders as models.
    /// Should be called once.
    /// </summary>
    public void ConstructTreeLayers()
    {
        // Build tree layers from place holder
        _treesLayers = new GameObject[TreeLayerPlaceHolders.Length];
        for (int i = 0; i < _treesLayers.Length; i++)
        {
            _treesLayers[i] = CreateTreeLayer(i);
        }

        // We don't want to see the foreground
        _treesLayers[0].SetActive(false);

        CleanNonVisibleDecorsObjects();
        RefreshMainTreesLayer();
    }

    /// <summary>
    /// Advance each layer one step more to the foreground by doing some tweens.
    /// </summary>
    /// <param name="timeToAdvance">The time given to advance all layers.</param>
    public void AdvanceLayers(float timeToAdvance)
    {
        // Move and rescale each layer to advance them 
        // Ignore the first layer (the foreground) because he wont move and will be destroyed.
        for (int i = 1; i < _treesLayers.Length; i++)
        {
            _treesLayers[i].GetRequiredComponent<TreesLayer>().LayerNumber--;

            TweenPositionTo(_treesLayers[i], timeToAdvance, TreeLayerPlaceHolders[i - 1].transform.localPosition);
            TweenScaleTo(_treesLayers[i], timeToAdvance, TreeLayerPlaceHolders[i - 1].transform.localScale);
        }

        // Destroy the foreground layer
        Destroy(_treesLayers[0]);

        // Move the layers in the array to advance them
        for (int i = 0; i < _treesLayers.Length - 1; i++)
        {
            _treesLayers[i] = _treesLayers[i + 1];
        }

        // Fade out the foreground
        TweenAlpha.Begin(_treesLayers[0], timeToAdvance, 0.0f);
        _treesLayers[0].GetComponent<TweenAlpha>().method = UITweener.Method.EaseInOut;

        // Create a new layer to add at the back
        _treesLayers[_treesLayers.Length - 1] = CreateTreeLayer(_treesLayers.Length - 1);

        // Fade in it and make it appear from scale 0 to the good scale.
        TweenAlpha.Begin(_treesLayers[_treesLayers.Length - 1], timeToAdvance / 4.0f, 1.0f, 5f);
        _treesLayers[_treesLayers.Length - 1].GetComponent<TweenAlpha>().method = UITweener.Method.EaseInOut;
        _treesLayers[_treesLayers.Length - 1].transform.localScale = Vector3.zero;
        TweenScale.Begin(_treesLayers[_treesLayers.Length - 1], timeToAdvance, TreeLayerPlaceHolders[TreeLayerPlaceHolders.Length - 1].transform.localScale);
        _treesLayers[_treesLayers.Length - 1].GetComponent<TweenScale>().method = UITweener.Method.EaseInOut;

        CleanNonVisibleDecorsObjects();
        RefreshMainTreesLayer();
    }

    public void AddGhostToGoodLayer(GameObject ghost)
    {
        Transform goodLayer = _mainTreesLayers[(int)ghost.GetComponent<Ghost>().MapPosition.z].transform;
        Ghost tmpGhost = ghost.GetComponent<Ghost>();
        if (tmpGhost.CurrentLayer != goodLayer)
        {
            tmpGhost.CurrentLayer = goodLayer;
            ghost.transform.parent = goodLayer;
            NGUITools.MarkParentAsChanged(ghost);
            goodLayer.GetRequiredComponent<TreesLayer>().NumGhostPassed++;
            tmpGhost.ActualFogColor = _mainTreesLayers[(int)ghost.GetComponent<Ghost>().MapPosition.z].GetComponent<TreesLayer>().Fog.color;
            if (tmpGhost.Status != Ghost.GhostStatus.HAS_FUSIONNED && tmpGhost.Status != Ghost.GhostStatus.HAS_JUST_FUSIONNED)
                tmpGhost.ResetColor();
            foreach (UIWidget uiWidgets in ghost.GetComponentsInChildren<UIWidget>())
            {
                if (uiWidgets.depth < ghost.GetComponent<Ghost>().AssignedDepth)
                    uiWidgets.depth += ghost.GetComponent<Ghost>().AssignedDepth;
            }
        }
    }

    public void TweenPositionTo(GameObject treeLayer, float timeToAdvance, Vector3 to)
    {
        TweenPosition tweenPosition = treeLayer.GetComponent<TweenPosition>();

        if (tweenPosition == null)
        {
            tweenPosition = treeLayer.AddComponent<TweenPosition>();
        }

        tweenPosition.from = treeLayer.transform.localPosition;
        tweenPosition.to = to;
        tweenPosition.duration = timeToAdvance;
        tweenPosition.method = UITweener.Method.EaseInOut;
        tweenPosition.ignoreTimeScale = false;

        tweenPosition.ResetToBeginning();

        tweenPosition.PlayForward();

    }

    public void TweenScaleTo(GameObject treeLayer, float timeToAdvance, Vector3 to)
    {
        TweenScale tweenScale = treeLayer.GetComponent<TweenScale>();

        if (tweenScale == null)
        {
            tweenScale = treeLayer.AddComponent<TweenScale>();
        }

        tweenScale.from = treeLayer.transform.localScale;
        tweenScale.to = to;
        tweenScale.duration = timeToAdvance;
        tweenScale.method = UITweener.Method.EaseInOut;
        tweenScale.ignoreTimeScale = false;

        tweenScale.ResetToBeginning();

        tweenScale.PlayForward();
    }

    public void PutGhostBehindCurrentLayer(Ghost ghost)
    {
        foreach (UIWidget uiWidgets in ghost.GetComponentsInChildren<UIWidget>())
        {
            uiWidgets.depth -= ghost.AssignedDepth;
        }
    }

    public GameObject GetLayerForMapDepth(int depth)
    {
        return _mainTreesLayers[depth];
    }

    #region private

    private GameObject[] _treesLayers; //< From the foreground layer to the background layer
    private List<GameObject> _gameObjectsDecorsSpawned = new List<GameObject>(100);
    private GameObject[] _mainTreesLayers = new GameObject[3];
    private ShuffleBag<GameObject> _treePrefabsBag;
    private ShuffleBag<GameObject> _groundDecorationPrefabsBag;
    private ShuffleBag<LayerTreeTemplateData> _templateDataBag;

    private void Awake()
    {
        // Disable place holders
        foreach (GameObject placeHolder in TreeLayerPlaceHolders)
        {
            placeHolder.SetActive(false);
        }

        // Set the good depth on tree prefabs
        foreach (GameObject tree in TreePrefabs)
        {
            foreach (UIWidget widget in tree.GetComponentsInChildren<UIWidget>())
            {
                if (widget.depth < TreeDepthRange)
                    widget.depth += TreeDepthRange;
            }
        }

        // Set the good depth on decors prefabs
        foreach (GameObject decors in GroundDecorationPrefabs)
        {
            foreach (UIWidget widget in decors.GetComponentsInChildren<UIWidget>())
            {
                if (widget.depth < DecorsDepthRange)
                    widget.depth += DecorsDepthRange;
            }
        }
    }

    private void Start()
    {
        // Create the shuffle bags
        // In the start because we use the factory
        System.Random random = MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator;
        _treePrefabsBag = new ShuffleBag<GameObject>(TreePrefabs, random);
        _groundDecorationPrefabsBag = new ShuffleBag<GameObject>(GroundDecorationPrefabs, random);
        _templateDataBag = new ShuffleBag<LayerTreeTemplateData>(DataTemplates, random);
    }

    /// <summary>
    /// Destroy all gameObjects that we can't see.
    /// </summary>
    private void CleanNonVisibleDecorsObjects()
    {
        // Clean if the gameObject was destroyed
        for (int i = 0; i < _gameObjectsDecorsSpawned.Count; i++)
        {
            GameObject go = _gameObjectsDecorsSpawned[i];
            if (go == null)
            {
                _gameObjectsDecorsSpawned.RemoveAt(i);
                i--;
            }
        }
        // TODO 2 Optimisation is possible
        for (int i = 0; i < _gameObjectsDecorsSpawned.Count; i++)
        {
            GameObject go = _gameObjectsDecorsSpawned[i];
            if (go.transform.position.x < BottomLeftLimitX || go.transform.position.x > TopRightLimitX)
            {
                Destroy(go);
                _gameObjectsDecorsSpawned.RemoveAt(i);
                i--;
            }
        }
    }

    /// <summary>
    /// Refresh the _mainTreesLayers.
    /// </summary>
    private void RefreshMainTreesLayer()
    {
        for (int i = 0; i < _mainTreesLayers.Length; i++)
        {
            //  1 + i because the 0th layer is the foreground and we ignore it
            _mainTreesLayers[i] = _treesLayers[1 + i];
        }
    }

    private GameObject CreateTree(GameObject parent)
    {
        GameObject go = NGUITools.AddChild(parent, _treePrefabsBag.Next());
        foreach (UIWidget widget in go.GetComponentsInChildren<UIWidget>())
        {
            if (widget.depth < TreeDepthRange)
                widget.depth += TreeDepthRange;
        }
        return go;
    }

    /// <summary>
    /// Create on tree layer and add it as children of this.
    /// </summary>
    /// <param name="layerNumber">The number of the layer.</param>
    /// <returns></returns>
    private GameObject CreateTreeLayer(int layerNumber)
    {
        GameObject treeLayer = NGUITools.AddChild(gameObject, TreeLayerPrefab);

        // Use the corresponding place holder in the PlaceHolders, the layer tree take the positions and scales of the place holder
        treeLayer.transform.localScale = TreeLayerPlaceHolders[layerNumber].transform.localScale;
        treeLayer.transform.localPosition = TreeLayerPlaceHolders[layerNumber].transform.localPosition;

        // Modify the layer behavior
        TreesLayer layerTreeBehavior = treeLayer.GetComponent<TreesLayer>();
        // Modify the size of the fog so that it takes the whole screen
        layerTreeBehavior.Fog.width = (int)(layerTreeBehavior.Fog.width / treeLayer.transform.localScale.x);
        layerTreeBehavior.Fog.GetComponent<UITexture>().depth += FogDepthRange;
        // Get a random template for this layer
        layerTreeBehavior.TemplateData = _templateDataBag.Next();
        // Adjust the layer number and set the good depth on the panel
        layerTreeBehavior.LayerNumber = layerNumber;
        treeLayer.GetComponent<UIPanel>().depth = -layerNumber;

        CreateMainTrees(layerTreeBehavior);

        // Create decors
        float mostRightTreePosX = layerTreeBehavior.TreesHorizontalPlacement[layerTreeBehavior.TreesHorizontalPlacement.Length - 1] * (Constants.WidthScreen / Constants.MatchMaker.TreesWidthSeparation);
        float mostLeftTreePosX = layerTreeBehavior.TreesHorizontalPlacement[0] * (Constants.WidthScreen / Constants.MatchMaker.TreesWidthSeparation);
        CreateDecorsTrees(layerTreeBehavior, mostLeftTreePosX, mostRightTreePosX);
        CreateGroundDecorations(layerTreeBehavior);

        CreateFlyCircles(layerTreeBehavior);

        return treeLayer;
    }

    /// <summary>
    /// Randomly shadow all uiwidget of the given GameObject.
    /// </summary>
    /// <param name="go"></param>
    private void RandomlyShadowObject(GameObject go)
    {
        foreach (UISprite widget in go.GetComponentsInChildren<UISprite>())
        {
            float value = (255 - Random.Range(0, ShadowValue)) / 255.0f;
            widget.color = new Color(value, value, value);
        }
    }

    #region Creation methods
    #region Trees
    /// <summary>
    /// Create the trees that will be seen on the nearest layer.
    /// </summary>
    /// <param name="layerTreeBehavior">The behavior of the layer.</param>
    private void CreateMainTrees(TreesLayer layerTreeBehavior)
    {
        for (int i = 0; i < layerTreeBehavior.TreesHorizontalPlacement.Length; i++)
        {
            GameObject tree = CreateTree(layerTreeBehavior.TreesParent);
            RandomlyShadowObject(tree);
            tree.transform.localPosition =
                new Vector3(layerTreeBehavior.TreesHorizontalPlacement[i] * (Constants.WidthScreen / Constants.MatchMaker.TreesWidthSeparation), 0.0f, 0.0f);
            tree.transform.localScale = Vector3.one * TreeScale;
        }
    }

    /// <summary>
    /// Create decors trees on both sides of the given layer.
    /// </summary>
    /// <param name="layerTreeBehavior">The behavior of the layer.</param>
    /// <param name="mostLeftTreePosX">The position where to start creating trees on the left.</param>
    /// <param name="mostRightTreePosX">The position where to start creating trees on the right.</param>
    private void CreateDecorsTrees(TreesLayer layerTreeBehavior, float mostLeftTreePosX, float mostRightTreePosX)
    {
        // Create on the trees on the left
        CreateDecorsTreesOnSide(layerTreeBehavior, mostLeftTreePosX, -1);

        // Create on the trees on the right
        CreateDecorsTreesOnSide(layerTreeBehavior, mostRightTreePosX, 1);
    }

    /// <summary>
    /// Create decors trees on one side.
    /// </summary>
    /// <param name="layerTreeBehavior">The behavior of the layer.</param>
    /// <param name="lastTreePosX">The pos from where it will start create trees.</param>
    /// <param name="rightOrLeft">Should be equal to -1 if left, 1 otherwise.</param>
    private void CreateDecorsTreesOnSide(TreesLayer layerTreeBehavior, float lastTreePosX, int rightOrLeft)
    {
        Assert.IsTrue(rightOrLeft == 1 || rightOrLeft == -1);
        for (int i = 0; i < NbTreesDecorations; i++)
        {
            GameObject treeAdded = CreateTree(layerTreeBehavior.TreesParent);
            RandomlyShadowObject(treeAdded);
            treeAdded.transform.localPosition =
                new Vector3(lastTreePosX + rightOrLeft * MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator.Next(MinDistanceBetweenDecoTrees, MaxDistanceBetweenDecoTrees), 0.0f, 0.0f);
            lastTreePosX = treeAdded.transform.localPosition.x;
            treeAdded.transform.localScale = Vector3.one * TreeScale;
            _gameObjectsDecorsSpawned.Add(treeAdded);
        }
    }
    #endregion Trees;

    #region Ground Decorations
    /// <summary>
    /// Create ground decorations on the given layer.
    /// </summary>
    /// <param name="layerTreeBehavior"></param>
    private void CreateGroundDecorations(TreesLayer layerTreeBehavior)
    {
        // Add deco between main trees
        // Does not respect exactly the decors zone, it might overcome the right border
        Vector2 bottomLeft = new Vector2(DecorsZoneDimensions.transform.localPosition.x - DecorsZoneDimensions.localSize.x / 2,
            DecorsZoneDimensions.transform.localPosition.y - DecorsZoneDimensions.localSize.y / 2);
        Vector3 topRight = new Vector2(DecorsZoneDimensions.transform.localPosition.x + DecorsZoneDimensions.localSize.x / 2,
            DecorsZoneDimensions.transform.localPosition.y + DecorsZoneDimensions.localSize.y / 2);
        float lastDecoPosX = bottomLeft.x;

        for (int i = 0; i < MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator.Next(NbMinDecorsOnMainTrees, NbMaxDecorsOnMainTrees); i++)
        {
            GameObject decoAddedToTheRight = NGUITools.AddChild(layerTreeBehavior.DecosParent, _groundDecorationPrefabsBag.Next());
            float posX = lastDecoPosX + MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator.Next(MinDistanceBetweenDeco, MaxDistanceBetweenDeco);
            float posY = MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator.Next((int)bottomLeft.y, (int)topRight.y);

            foreach (UIWidget widget in decoAddedToTheRight.GetComponentsInChildren<UIWidget>())
            {
                widget.depth += DecorsDepthRange - (int)posY;
            }
            RandomlyShadowObject(decoAddedToTheRight);
            decoAddedToTheRight.transform.localPosition = new Vector3(posX, posY, 0.0f);
            lastDecoPosX = decoAddedToTheRight.transform.localPosition.x;
        }

        // Adjust the scale of the zone to the present layer
        DecorsZoneDimensions.transform.localScale = layerTreeBehavior.gameObject.transform.localScale;

        // Create ground decorations on the left
        CreateGroundDecorationsOnSide(layerTreeBehavior, bottomLeft, topRight, -1);

        // Create ground decorations on the right
        CreateGroundDecorationsOnSide(layerTreeBehavior, bottomLeft, topRight, 1);

        // Reput it to one
        DecorsZoneDimensions.transform.localScale = Vector3.one;
    }

    /// <summary>
    /// Create ground decoration on one side.
    /// </summary>
    /// <param name="layerTreeBehavior">The behavior of the layer.</param>
    /// <param name="bottomLeft">The bottom left of the spawn zone.</param>
    /// <param name="topRight">The top right of the spawn zone.</param>
    /// <param name="rightOrLeft">Should be equal to -1 if left, 1 otherwise.</param>
    private void CreateGroundDecorationsOnSide(TreesLayer layerTreeBehavior, Vector2 bottomLeft, Vector2 topRight, int rightOrLeft)
    {
        float lastDecoPosX;
        if (rightOrLeft == -1)
        {
            lastDecoPosX = bottomLeft.x;
        }
        else
        {
            lastDecoPosX = topRight.x;
        }
        for (int i = 0; i < NbTreesDecorations; i++)
        {
            GameObject decoAddedToTheRight = NGUITools.AddChild(layerTreeBehavior.DecosParent, _groundDecorationPrefabsBag.Next());
            float posX = lastDecoPosX + rightOrLeft * MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator.Next(MinDistanceBetweenDeco, MaxDistanceBetweenDeco);
            float posY = MatchMakerMgmt.Instance.Factory.FactoryRandomGenerator.Next((int)bottomLeft.y, (int)topRight.y);
            foreach (UIWidget widget in decoAddedToTheRight.GetComponentsInChildren<UIWidget>())
            {
                widget.depth += DecorsDepthRange - (int)posY;
            }
            RandomlyShadowObject(decoAddedToTheRight);
            decoAddedToTheRight.transform.localPosition =
                new Vector3(posX, posY, 0.0f);
            lastDecoPosX = decoAddedToTheRight.transform.localPosition.x;
            _gameObjectsDecorsSpawned.Add(decoAddedToTheRight);
        }
    }
    #endregion Ground Decorations

    private void CreateFlyCircles(TreesLayer layerTreeBehavior)
    {
        for (int i = 0; i < Random.Range(MinMaxRangeNumberFlyCircle.x, MinMaxRangeNumberFlyCircle.y); i++)
        {
            float posX = Random.Range(-Constants.WidthScreen / 2, Constants.WidthScreen / 2);
            GameObject go = NGUITools.AddChild(layerTreeBehavior.FlyCirclesParent, CirclePrefab.gameObject);
            float posY = Random.Range(RangeYPosition.x, RangeYPosition.y);
            go.transform.localPosition = new Vector3(posX, posY, 0);
            go.GetRequiredComponent<FlyCircle>().MinMaxRangeDistanceFromCenter.x = Mathf.Max(posY, go.GetRequiredComponent<FlyCircle>().MinMaxRangeDistanceFromCenter.x);
            foreach (UIWidget widget in go.GetComponentsInChildren<UIWidget>())
            {
                widget.depth += (int)FlyCircleDepth;
            }
        }
    }
    #endregion Creation methods
    #endregion Private
}
