﻿/**********************************************************************
* MatchMakerDebugUI.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchMakerDebugUI : MonoBehaviour {

    public UILabel CurrentTurnLabel;

	void Update () {
        CurrentTurnLabel.text = "Turn : " + MatchMakerMgmt.Instance.CurrentTurn;
	}
}
