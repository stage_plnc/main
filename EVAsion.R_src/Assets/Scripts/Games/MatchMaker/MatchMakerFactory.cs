﻿/**********************************************************************
* MatchMakerFactory.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;

/// <summary>
/// Will only generate the ghosts and fusions on the map of the Manager.
/// </summary>
public class MatchMakerFactory : CycleFactory
{
    public GameObject[] GhostPrefabs;
    [Tooltip("The depth of ghosts.")]
    public int GhostDepthRange = 5;
    [Tooltip("The depth that will be added between each ghost.")]
    public int DepthBetweenGhost = 10;
    public int NbGhostPerFusion = 2;
    public int NumberLayersWhereFusion = 2;
    public int LastTurnOffset = 4; // Won't create between offset and last turn

    // The data to use for generation
    private MatchMakerDifficultyData _difficultyData;

    private int _numberGhostThisCycle = 0;
    private string _leftPartWrongCombination, _rightPartWrongCombination;

    public System.Random FactoryRandomGenerator
    {
        get
        {
            return _random;
        }
    }

    public override CycleData InitNextCycle(LPNC_DataExport parsevalDT)
    {
        base.InitNextCycle(parsevalDT);

        HandleDifficultyData(parsevalDT, Constants.DifficultyMethod);

        _goodCombination = GenerateGoodCombination();
        _wrongCombination = GenerateWrongCombination();

        Couple.LifeTimeForBadCouple = Couple.LifeTimeForGoodCouple = _difficultyData.durationCombination;

        MatchMakerMgmt.Instance.NbTurnsForCurrentCycle = _difficultyData.nbTurns;

        _currentCycle++;

        return new MatchMakerCycleData(_leftPartCombination, _rightPartCombination, _difficultyData.nbTurns, _difficultyData.durationCombination, _difficultyData.numberGoodCombinations);
    }

    private void HandleDifficultyData(LPNC_DataExport parsevalDt, string difficulty)
    {
        Debug.Log("Difficulty = " + difficulty);

        if (difficulty == "random")
        {
            parsevalDt.d01 = _random.Next(0, 6);
            parsevalDt.d02 = _random.Next(0, 6);
            parsevalDt.d03 = _random.Next(0, 6);
        }

        // Not sure...
        else if (difficulty == "linear")
        {
            if (_currentCycle >= 1)
            {
                parsevalDt.d01 = (int)Mathf.Round(_initialParsevalDT.d01 + _factLinearDifficultyIncrease[0] * _currentCycle);
                parsevalDt.d02 = (int)Mathf.Round(_initialParsevalDT.d02 + _factLinearDifficultyIncrease[1] * _currentCycle);
                parsevalDt.d03 = (int)Mathf.Round(_initialParsevalDT.d03 + _factLinearDifficultyIncrease[2] * _currentCycle);
            }
            else
            {
                parsevalDt.d01 = (int)Constants.LinearDifficultyStartBound.x;
                parsevalDt.d02 = (int)Constants.LinearDifficultyStartBound.y;
                parsevalDt.d03 = (int)Constants.LinearDifficultyStartBound.z;

                _factLinearDifficultyIncrease[0] = (float)((int)Constants.LinearDifficultyEndBound.x - parsevalDt.d01) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[1] = (float)((int)Constants.LinearDifficultyEndBound.y - parsevalDt.d02) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[2] = (float)((int)Constants.LinearDifficultyEndBound.z - parsevalDt.d03) / (Constants.NbrTotalCycles - 1);

                _initialParsevalDT = parsevalDt;
            }
        }

        Debug.LogWarningFormat("d01 = {0}, d02 = {1}, d03 = {2}", parsevalDt.d01, parsevalDt.d02, parsevalDt.d03);

        _parsevalDT = parsevalDt;

        _difficultyData = GetDifficultyData(parsevalDt);
    }

    public override void GenerateNextCycle()
    {
        _numberGhostThisCycle = 0;
        Ghost[] ghostCouples = GenerateGhostCouples();
        _numberGhostThisCycle = ghostCouples.Length;
        GenerateFusionPoints(ghostCouples);
        SetGhostsPositionAndScale(ghostCouples);
        SetDisapearAndAppearTurns(ghostCouples);
    }

    public void ResetGhostDepthInFrontOfCurrentLayer(UIWidget ghost)
    {
        ghost.depth += GhostDepthRange + (DepthBetweenGhost * _numberGhostThisCycle);
    }

    #region private


    /// <summary>
    /// Create fusion points on visible positions.
    /// Only one fusion max per turn.
    /// Limit the generation : no fusion on the first and on LastTurnOffset
    /// Create fusion only on NumberLayersWhereFusion first layers.
    /// </summary>
    /// <param name="ghostCouples"></param>
    private void GenerateFusionPoints(Ghost[] ghostCouples)
    {
        int nbTotalCombinations = _difficultyData.numberGoodCombinations + _difficultyData.numberBadCombinations;
        List<MapCell> visibleCases = MatchMakerMgmt.Instance.Map.GetVisibleCell();
        int nbTurns = MatchMakerMgmt.Instance.NbTurnsForCurrentCycle;

        List<int> availableTurns = new List<int>();
        // Limit the generation : no fusion on the last turn
        int iterator = nbTurns / nbTotalCombinations;
        int LastMaxIndex = iterator;
        int LastMinIndex = 0;
        for (int i = 0; i < nbTotalCombinations; i++)
        {
            availableTurns.Add(_random.Next(LastMinIndex, LastMaxIndex - 1));
            LastMinIndex += iterator;
            LastMaxIndex += iterator;
        }

        // Remove cells from layers we don't want
        visibleCases.RemoveAll(cell => cell.MapPosition.z > NumberLayersWhereFusion - 1);
        Assert.IsTrue(nbTurns >= nbTotalCombinations);

        for (int i = 0; i < nbTotalCombinations * NbGhostPerFusion; i += NbGhostPerFusion)
        {
            // Choose a visible cell, and a free turn
            MapCell fusionCase = visibleCases[_random.Next(0, visibleCases.Count)];
            visibleCases.Remove(fusionCase);
            fusionCase.IsVisible = false;
            int index = _random.Next(0, availableTurns.Count);
            int fusionTurn = availableTurns[index];
            availableTurns.RemoveAt(index);

            // Say to the ghost that he will fusion
            for (int j = 0; j < NbGhostPerFusion; j++)
            {
                ghostCouples[i + j].FusionTargetMapPosition = fusionCase.MapPosition;
                ghostCouples[i + j].Status = Ghost.GhostStatus.WILL_BE_FUSIONNED;
                ghostCouples[i + j].FusionTurn = fusionTurn;
                ghostCouples[i + j].Partner = ghostCouples[i + 1].gameObject;
            }
        }
    }

    private void SetDisapearAndAppearTurns(Ghost[] ghostScripts)
    {
        for (int i = 0; i < ghostScripts.Length; i++)
        {
            if (ghostScripts[i].IsInAGoodCouple)
            {
                ghostScripts[i].TurnToAppear = _random.Next(1, Mathf.Max(1, ghostScripts[i].FusionTurn - 1)); // appear before the turn of fusion
            }
            else
            {
                ghostScripts[i].TurnToAppear = _random.Next(1, MatchMakerMgmt.Instance.NbTurnsForCurrentCycle);
                ghostScripts[i].TurnRateDisapear = _random.Next(0, 4);
            }
        }
    }

    private string GenerateGoodCombination()
    {
        string goodCombination = string.Empty;

        _indexNewPattern = TargetGeneratorMgmt.Instance.GetRandomIndex(_random, _difficultyData.DifficultyTarget);

        string[] array = TargetGeneratorMgmt.Instance.GetCombinationAt(_random, _indexNewPattern, _difficultyData.DifficultyTarget);
        _leftPartCombination = array[0].Trim();
        _rightPartCombination = array[1].Trim();

        if (_upperCaseSession)
        {
            _leftPartCombination = _leftPartCombination.ToUpper();
            _rightPartCombination = _rightPartCombination.ToUpper();
        }
        else
        {
            _leftPartCombination = _leftPartCombination.ToLower();
            _rightPartCombination = _rightPartCombination.ToLower();
        }

        goodCombination = _leftPartCombination + _rightPartCombination;

        return goodCombination;
    }

    private string GenerateWrongCombination()
    {
        //string wrongCombination = string.Empty;
        string[] array = new string[2];

        string wrongCombination = DistractorsMgmt.Instance.GenerateWrongCombinationMMAndChase(
            _goodCombination, _difficultyData.DifficultyTarget);

        if (wrongCombination.Length == 2 || wrongCombination.Length == 4)
        {
            _leftPartWrongCombination = wrongCombination.Substring(0, wrongCombination.Length / 2);
            _rightPartWrongCombination = wrongCombination.Substring(wrongCombination.Length / 2, wrongCombination.Length / 2);
        }
        else
        {
            int middle = UnityEngine.Random.value > 0.5f ? 1 : 2;
            _leftPartWrongCombination = wrongCombination.Substring(0, middle);
            _rightPartWrongCombination = wrongCombination.Substring(middle, wrongCombination.Length - middle);
        }

        if (_upperCaseSession)
        {
            _leftPartWrongCombination = _leftPartWrongCombination.ToUpper();
            _rightPartWrongCombination = _rightPartWrongCombination.ToUpper();
        }
        else
        {
            _leftPartWrongCombination = _leftPartWrongCombination.ToLower();
            _rightPartWrongCombination = _rightPartWrongCombination.ToLower();
        }

        return wrongCombination;
    }

    /**
     * Set the position of each ghost.
     * Put the ghost with a fusion target near it.
     */
    private void SetGhostsPositionAndScale(Ghost[] ghostScripts)
    {
        Map Map = MatchMakerMgmt.Instance.Map;
        List<MapCell> visibleCases = MatchMakerMgmt.Instance.Map.GetVisibleCell();

        for (int i = 0; i < ghostScripts.Length; i++)
        {
            Vector3 mapPosition = new Vector3();

            // If the ghost will be fusioned, set it on an adjacent cell of the future fusion.
            if (ghostScripts[i].Status == Ghost.GhostStatus.WILL_BE_FUSIONNED)
            {
                List<MapCell> adjacentCases = Map.GetAdjacentCell(ghostScripts[i].FusionTargetMapPosition);
                int adjacentCasesIndex = _random.Next(0, adjacentCases.Count);
                mapPosition = adjacentCases[adjacentCasesIndex].MapPosition;
                visibleCases.Remove(adjacentCases[adjacentCasesIndex]);
            }
            // else, put it on an empty cell.
            else
            {
                int visibleCaseIndex = _random.Next(0, visibleCases.Count);
                mapPosition = visibleCases[visibleCaseIndex].MapPosition;
                visibleCases.Remove(visibleCases[visibleCaseIndex]);
            }

            ghostScripts[i].MapPosition = mapPosition;

            ghostScripts[i].transform.position = Map.MapPositionToWorldPosition(mapPosition);
            MatchMakerMgmt.Instance.TreeManager.AddGhostToGoodLayer(ghostScripts[i].gameObject);
            ghostScripts[i].AssignedDepth = GhostDepthRange + (DepthBetweenGhost * i);

            // Set the depth
            foreach (UIWidget widget in ghostScripts[i].GetComponentsInChildren<UIWidget>())
            {
                widget.depth += GhostDepthRange + (DepthBetweenGhost * i);
            }
        }
    }

    /**
     * Return an array with ghost couples. The inth ghost is in couple with the i+1nth one.
     * The numberGoodCombinations first couples are good ones, other bad ones.
     * If odd number of ghost, the last is alone.
     */
    private Ghost[] GenerateGhostCouples()
    {
        Ghost[] ghostScripts = new Ghost[_difficultyData.numberGhosts];

        // Create a ShuffleBag to randomly pick ghost prefabs
        ShuffleBag<GameObject> ghostsPrefabsBag = new ShuffleBag<GameObject>(GhostPrefabs, _random);

        // Fill with good combinations
        for (int i = 0; i < _difficultyData.numberGoodCombinations * NbGhostPerFusion; i += NbGhostPerFusion)
        {

            // Create two ghosts, one for each letter of the combination
            ghostScripts[i] = NGUITools.AddChild(gameObject, ghostsPrefabsBag.Next()).GetRequiredComponent<Ghost>();
            ghostScripts[i].Init(_leftPartCombination, true);
            ghostScripts[i].RightInCouple = false;
            ghostScripts[i + 1] = NGUITools.AddChild(gameObject, ghostsPrefabsBag.Next()).GetRequiredComponent<Ghost>();
            ghostScripts[i + 1].Init(_rightPartCombination, true);
            ghostScripts[i + 1].RightInCouple = true;
        }

        // Fill with bad combinations
        for (int i = _difficultyData.numberGoodCombinations * NbGhostPerFusion; i < ghostScripts.Length - 1; i += NbGhostPerFusion)
        {
            // Create two ghosts, one for each letter of the combination
            ghostScripts[i] = NGUITools.AddChild(gameObject, ghostsPrefabsBag.Next()).GetRequiredComponent<Ghost>();
            ghostScripts[i].Init(_leftPartWrongCombination, false);
            ghostScripts[i].RightInCouple = false;
            ghostScripts[i + 1] = NGUITools.AddChild(gameObject, ghostsPrefabsBag.Next()).GetRequiredComponent<Ghost>();
            ghostScripts[i + 1].Init(_rightPartWrongCombination, false);
            ghostScripts[i + 1].RightInCouple = true;
        }

        // If odd number of ghost, add the last (but not the least)
        if (_difficultyData.numberGhosts % NbGhostPerFusion != 0)
        {
            for (int i = 0; i < _difficultyData.numberGhosts % NbGhostPerFusion; i++)
            {
                // Choose a random combination
                string combination = UnityEngine.Random.value > 0.5f ? _leftPartWrongCombination : _rightPartWrongCombination;

                ghostScripts[ghostScripts.Length - 1 - i] = NGUITools.AddChild(gameObject, ghostsPrefabsBag.Next()).GetRequiredComponent<Ghost>();
                ghostScripts[ghostScripts.Length - 1 - i].Init(combination, false);
            }
        }

        return ghostScripts;
    }



    #region DifficultyData

    private MatchMakerDifficultyData GetDifficultyData(LPNC_DataExport parsevalDT)
    {
        int nbTurns = 20;
        float durationCombination = 2.0f;

        int numberGhosts = 4;
        int numberGoodCombinations = 1;
        int numberBadCombinations = 1;

        int difficultyTarget = 1;
        int difficultyDistractor = 1;

        try
        {
            difficultyTarget = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("match_maker", "difficulty_target", parsevalDT.d01));

            if (difficultyTarget < 1 || difficultyTarget > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de match_maker.json : la dimension difficulty_target de la difficulté " + parsevalDT.d01 +
                    "doit etre comprise entre 1 et 6");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de match_maker.json a la dimension difficulty de la difficulté " + parsevalDT.d01);
        }

        try
        {
            difficultyDistractor = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("match_maker", "difficulty_distractor", parsevalDT.d02));

            if (difficultyDistractor < 1 || difficultyDistractor > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de match_maker.json : la dimension difficulty_distractor de la difficulté " + parsevalDT.d02 +
                    "doit etre comprise entre 1 et 6");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de match_maker.json a la dimension difficulty_distractor de la difficulté " + parsevalDT.d02);
        }

        try
        {
            string[] gameValues =
                DifficultyMgmt.Instance.GetDifficultyValue("match_maker", "turns;fusion_duration;ghosts;good_combin;bad_combin", parsevalDT.d03).Split(';');

            nbTurns = int.Parse(gameValues[0]);
            durationCombination = int.Parse(gameValues[1]);
            numberGhosts = int.Parse(gameValues[2]);
            numberGoodCombinations = int.Parse(gameValues[3]);
            numberBadCombinations = int.Parse(gameValues[4]);

            if (numberGhosts / 2 < numberGoodCombinations + numberBadCombinations)
            {
                PrintToScreen.Print("Erreur lors de la lecture de match_maker.json à la dimension match_maker de la difficulté " + parsevalDT.d03 +
                    " : ghosts / 2 doit etre supérieur à good_combin + bad_combin ");
            }
        }
        catch (System.FormatException e)
        {
            PrintToScreen.Print("Erreur lors de la lecture de match_maker.json a la " + 
                "dimension turns;fusion_duration;ghosts;good_combin;bad_combin de la difficulté " + parsevalDT.d03);

        }

        //if (Constants.NbrEvents != -1)
        //{
        //    numberGoodCombinations = Constants.NbrEvents;
        //    numberGoodCombinations = numberGoodCombinations + numberBadCombinations;
        //}

        return new MatchMakerDifficultyData(difficultyTarget, difficultyDistractor, 
            nbTurns, durationCombination, numberGhosts, numberGoodCombinations, numberBadCombinations);
    }

    private class MatchMakerDifficultyData : DifficultyData
    {
        public int nbTurns;
        public int durationCombination;

        public int numberGhosts;
        public int numberGoodCombinations;
        public int numberBadCombinations;

        public MatchMakerDifficultyData(int difficultyTarget, int difficultyDistractor, int nbTurns, float durationCombination, int numberGhosts, int numberGoodCombinations, int numberBadCombinations)
            : base(difficultyTarget, difficultyDistractor)
        {
            this.nbTurns = nbTurns;
            this.durationCombination = (int)durationCombination;
            this.numberGhosts = numberGhosts;
            this.numberGoodCombinations = numberGoodCombinations;
            this.numberBadCombinations = numberBadCombinations;
        }
    }

    #endregion
    #endregion private
}