﻿/**********************************************************************
* MatchMakerCycleData.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;
using System.Collections;

public class MatchMakerCycleData : CycleData
{
    public int NbTurns;
    public float DurationCombination;
    public int NumberGoodCombinations;
    public string LeftPartCombination;
    public string RightPartCombination;

    public MatchMakerCycleData(string leftPartCombination, string rightPartCombination, int nbTurns, float durationCombination, int numberGoodCombinations) 
        : base(leftPartCombination + rightPartCombination, numberGoodCombinations)
    {
        LeftPartCombination = leftPartCombination;
        RightPartCombination = rightPartCombination;
        NbTurns = nbTurns;
        DurationCombination = durationCombination;
        NumberGoodCombinations = numberGoodCombinations;
    }
}
