﻿/**********************************************************************
* LayerTreeTemplateData.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;

/// <summary>
/// Contains the positions of the trees on a line (to use in a layer).
/// </summary>
[CreateAssetMenu(fileName = "TreeTemplate", menuName = "TreeTemplate", order = 1)]
public class LayerTreeTemplateData : ScriptableObject
{
    public int[] TreesHorizontalPlacement;
}
