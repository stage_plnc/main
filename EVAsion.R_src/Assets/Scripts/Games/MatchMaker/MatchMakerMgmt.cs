﻿/**********************************************************************
* MatchMakerMgmt.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class MatchMakerMgmt : GameMgmt
{
    public MatchMakerFactory Factory;
    public GameObject SceneryContainer;
    public GameObject GhostFusionPrefab;
    public TreeLayerManager TreeManager;
    public NavigationManager NavigationManager;
    public float TimeToAdvanceLayer;

    [HideInInspector]
    public Map Map;
    [HideInInspector]
    public int NbTurnsForCurrentCycle;
    [HideInInspector]
    public int CurrentTurn;

    #region properties
    public static MatchMakerMgmt Instance
    {
        get { return _instance; }
    }

    public GameObject[] LayersTrees
    {
        get
        {
            return TreeManager.MainTreeLayers;
        }
    }

    public float DurationAction
    {
        get
        {
            return _durationAction;
        }
    }
    public float DurationActionBase
    {
        get
        {
            return _durationActionBase;
        }
    }
    #endregion

    public delegate void OnNextTurnAction();
    public event OnNextTurnAction OnNextTurn;
    public event OnNextTurnAction OnEndCycle;

    private static MatchMakerMgmt _instance = null;
    private float _durationAction = 0.0f;
    private readonly float _durationActionBase = 1.5f;
    private MatchMakerCycleData _matchMakerCycleData;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogWarning("MatchMakerMgmt: Tried to instanciate a second MatchMakerMgmt.");
            Destroy(this.gameObject);

        }

        Map = NavigationManager.GenerateMap();

        _gameName = "match_maker";
        _cycleSuccessAndFailure = new CycleSuccessAndFailure();
    }

    new void OnDestroy()
    {
        base.OnDestroy();

        _instance = null;
    }

    public override void Start()
    {
        base.Start();

        _factory = Factory;

        TreeManager.ConstructTreeLayers();

        Invoke("StartGame", 2.0f);
    }

    public override void StartGame()
    {
        base.StartGame();
    }

    public override void StartGameCycle()
    {
        base.StartGameCycle();

        _matchMakerCycleData = (MatchMakerCycleData)_cycleData;
        _durationAction = Constants.MatchMaker.CycleDuration / NbTurnsForCurrentCycle;

        //Debug.Log("Nombre de tours du cycle : " + NbTurnsForCurrentCycle);

        Invoke("EndGameCycle", Constants.MatchMaker.CycleDuration + 3.0f);
    }

    public override void StartInterlude()
    {
        base.StartInterlude();

        Invoke("EndInterlude", Constants.MatchMaker.InterludeDuration);
    }

    public override void EndGameCycle()
    {
        TreeManager.AdvanceLayers(TimeToAdvanceLayer);

        CurrentTurn = 0;

        // Fade and destroy the ghosts
        if (OnEndCycle != null)
        {
            OnEndCycle();
        }

        // Each layer is changed, so we reset the map
        Map.Reset();

        base.EndGameCycle();

        //Debug.Log("Success : " + _cycleSuccessAndFailure.NbrTargetSuccess + " / " + _matchMakerCycleData.NumberGoodCombinations);
        //Debug.Log("Failure : " + _cycleSuccessAndFailure.NbrTargetFailure);

        _cycleSuccessAndFailure.Clear();
    }

    public void ClickedOnGhost(bool IsGoodCombination)
    {
        if (IsGoodCombination)
        {
            //HACK asked by LPNC
            //Debug.Log("=> --------------");
            //Debug.Log("=> " + _currentParsevalDt.d01);
            //Debug.Log("=> " + _currentParsevalDt.d02);
            //Debug.Log("=> " + _currentParsevalDt.d03);
            //_pPlayer.AddCurrentResult(_currentParsevalDt, _gameName, 1.0f);

            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 1.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetSuccess++;
            }

            _nbrTotalSuccess++;
        }
        else
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetFailure++;
            }
            _nbrTotalFailure++;
        }
    }


    protected override void Update()
    {
        base.Update();

        if (Status == GameStatus.GAME_CYCLE && CurrentTurn < NbTurnsForCurrentCycle)
        {
            float elapsedTime = Time.time - _cycleStartDate;
            float timeSinceLastTurn = elapsedTime / (CurrentTurn + 1);

            if (timeSinceLastTurn > _durationAction)
            {
                CurrentTurn++;

                if (OnNextTurn != null)
                {
                    OnNextTurn();
                }
                //else if (!IsInvoking("EndGameCycle"))
                //{
                //    Invoke("EndGameCycle", 1.5f);
                //}
            }
        }
    }

    public override void EndTutorial()
    {

    }

    public override float CycleScore()
    {
        float score = 0.0f;
        int nbrTargetToGet = ((MatchMakerCycleData)_cycleData).NumberGoodCombinations;

        float success = Mathf.Max(0.0f,
            _cycleSuccessAndFailure.NbrTargetSuccess / (float)nbrTargetToGet -
            _cycleSuccessAndFailure.NbrTargetFailure / nbrTargetToGet);
        if (success >= UPL)
        {
            score = 1.0f;
        }
        else
        {
            score = 0.0f;
        }

        return score;
    }

    public void PlayerHasMissedCouple()
    {
        _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
    }
}
