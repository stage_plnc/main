﻿/**********************************************************************
* TreesLayer.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using UnityEngine;

/// <summary>
/// Represent a layer (with a depth so) of trees in the game.
/// Just store some info, do nothing more.
/// </summary>
public class TreesLayer : MonoBehaviour
{
    [Tooltip("The parent of the future trees.")]
    public GameObject TreesParent;
    [Tooltip("The parent of the future decorations.")]
    public GameObject DecosParent;
    public GameObject FlyCirclesParent;
    [Tooltip("The texture of the layer fog.")]
    public UITexture Fog;
    [Tooltip("The template of this layer (to create trees).")]
    public LayerTreeTemplateData TemplateData;

    /// <summary>
    /// The number of the layer. From 0 to n, with 0 the foreground layer.
    /// </summary>
    public int LayerNumber
    {
        get
        {
            return _layerNumber;
        }
        set
        {
            _layerNumber = value;
            SetDepth(-_layerNumber);
        }
    }

    /// <summary>
    /// The array with the positions of the trees in the layer.
    /// </summary>
    public int[] TreesHorizontalPlacement
    {
        get
        {
            return TemplateData.TreesHorizontalPlacement;
        }
    }

    public int NumGhostPassed = 0;

    /// <summary>
    /// Reset the depth of the layer panel component.
    /// </summary>
    /// <param name="depth"></param>
    public void SetDepth(int depth)
    {
        if (_panel == null)
            _panel = gameObject.GetRequiredComponent<UIPanel>();
        _panel.depth = depth;
    }

    private UIPanel _panel; //< The panel of the layer.
    private int _layerNumber; //< The number of the layer. From 0 to n, with 0 the foreground layer.
}
