﻿/**********************************************************************
* Ghost.cs
* Created at 9-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

[RequireComponent(typeof(UIWidget))]
/// <summary>
/// Ghost behavior, will move the game object on a map.
/// </summary>
public class Ghost : MonoBehaviour
{
    #region Public Attributes
    public UIWidget HandsMarker;
    public Couple CouplePrefab;
    public Color GhostColor = Color.white;
    [HideInInspector] public Color ActualFogColor = Color.white;
    private Vector3 _scaleToKeep = Vector3.one;
    public BoxCollider Collider;
    public GhostHeadController HeadController;

    [Header("Dying")]
    public float ToScaleWhenGoodClick;
    public float TimeToReachHigherScale;
    public float TimeToDisapear;
    public float TimeForParticlePrefab;
    public Vector2 ParticleRotationRange;

    [Header("Feedback System")]
    public GameObject FeedbackBadPrefab;
    public GameObject FeedbackGoodPrefab;
    public GameObject FeedbackMissedPrefab;

    [Header("Movement")]
    public TweenPosition SpriteTweenPosition;
    public float TimeToFadeInAtAwake = 1.0f;
    public float FlappingMaxYRange;
    public static float OneFlappingDuration = 0.5f;
    public AnimationCurve GhostMovementCurve;
    public UISpriteAnimation TailSpriteAnimation;
    public int TailMinSpeed = 20;
    public int TailMaxSpeed = 25;
    public UISprite Tail;
    public GameObject SpriteParent;
    public UIWidget SizeUIWidget;

    [Header("Label")]
    public TweenAlpha TweenAlphaLabel;

    [HideInInspector] public enum GhostSide { Left, Right };
    [HideInInspector] public GhostSide CurrentSide;

    [HideInInspector] public bool IsInAGoodCouple;
    [HideInInspector] public bool CoupleSet = false; // Put to true if one of the ghost take care of making the couple.
    [HideInInspector] public bool RightInCouple = false; // True if the ghost is left one in the couple
    [HideInInspector] public Transform CurrentLayer = null;
    [HideInInspector] public Vector3 MapPosition;
    [HideInInspector] public Vector3 FusionTargetMapPosition;
    [HideInInspector] public GameObject Partner;
    [HideInInspector] public int FusionTurn;
    [HideInInspector] public GhostStatus Status;
    [HideInInspector] public int AssignedDepth;
    [HideInInspector] public int TurnToAppear;
    [HideInInspector] public int TurnRateDisapear; // disapear or appear each turnRateDisapear;
    #endregion Public Attributes

    private static float _YOffsetFromCellCenterWhenCoupling = 0.05f; // Where on the y axis will be the ghosts hands
    private Vector3 _spriteOriginPosition;
    //private Vector3 _spriteOriginScale;
    private Vector3 _lastMapPosition = Vector3.zero;
    private MapCell _nextCaseToGo;
    private UILabel _label;
    private Vector3 _offsetWithHands;
    private bool _isAppeared = false;
    private int _turnToDisappear = -1;

    private void OnDisable()
    {
        // Unsuscribe events
        MatchMakerMgmt.Instance.OnNextTurn -= OnNextTurn;
        MatchMakerMgmt.Instance.OnEndCycle -= DestroyGhost;
        MatchMakerMgmt.Instance.ShowLetters -= ShowLetter;
        MatchMakerMgmt.Instance.HideLetters -= HideLetter;
    }

    private void OnEnable()
    {
        // Subscribe to events.
        MatchMakerMgmt.Instance.OnNextTurn += OnNextTurn;
        MatchMakerMgmt.Instance.OnEndCycle += DestroyGhost;
        MatchMakerMgmt.Instance.ShowLetters += ShowLetter;
        MatchMakerMgmt.Instance.HideLetters += HideLetter;
    }

    private void Awake()
    {
        _label = this.GetRequiredComponentInChildren<UILabel>();
    }

    private void Start()
    {

        _spriteOriginPosition = SpriteTweenPosition.transform.localPosition;
        //_spriteOriginScale = SpriteTweenPosition.transform.localScale;
        SpriteParent.transform.localScale = Vector3.zero;

        SpriteTweenPosition.ignoreTimeScale = false;
        SpriteTweenPosition.delay = Random.Range(0f, 1f);
        SpriteTweenPosition.from = SpriteTweenPosition.transform.localPosition;
        SpriteTweenPosition.to = SpriteTweenPosition.transform.localPosition + Vector3.up * FlappingMaxYRange;


        // Put the good color
        ResetColor();

        TailSpriteAnimation.framesPerSecond = Random.Range(TailMinSpeed, TailMaxSpeed);
        Invoke("StartMovingTail", Random.Range(.0f, 0.15f));
    }

    public string Letter
    {
        get
        {
            return _label.text;
        }
    }

    /// <summary>
    /// Representen the different status for a ghost.
    /// </summary>
    public enum GhostStatus
    {
        UNDEFINED = 0,
        WILL_BE_FUSIONNED,
        HAS_JUST_FUSIONNED,
        HAS_DISAPPEARED,
        HAS_FUSIONNED,
        NO_FUSION
    };

    public void Init(string text, bool isGoodCombination, float contentScale = 1.0f)
    {
        _label.text = text;
        IsInAGoodCouple = isGoodCombination;
    }

    #region private

    private void StartMovingTail()
    {
        TailSpriteAnimation.Play();
    }

    /// <summary>
    /// Returns a random cell in adjacent cell, or an adjacent cell that go toward the fusion cell if the ghost has to fusion.
    /// </summary>
    /// <param name="adjacentCases">The adjacent cells to choose from.</param>
    /// <returns></returns>
    private MapCell ChooseNextCase(List<MapCell> adjacentCases)
    {
        adjacentCases = FilterCells(adjacentCases);
        MapCell nextCase = adjacentCases[Random.Range(0, adjacentCases.Count)];

        if (Status == GhostStatus.WILL_BE_FUSIONNED)
        {
            int distOnX = Mathf.Abs((int)FusionTargetMapPosition.x - (int)MapPosition.x);
            int distOnY = Mathf.Abs((int)FusionTargetMapPosition.y - (int)MapPosition.y);
            int distOnZ = Mathf.Abs((int)FusionTargetMapPosition.z - (int)MapPosition.z);
            int distWithFusionTarget = distOnX + distOnY + distOnZ;

            List<MapCell> availableCells = new List<MapCell>();

            if (MatchMakerMgmt.Instance.CurrentTurn + distWithFusionTarget >= FusionTurn - 1)
            {
                if (FusionTargetMapPosition.x - MapPosition.x > 0)
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x + 1, (int)MapPosition.y, (int)MapPosition.z));
                }
                else if (FusionTargetMapPosition.x - MapPosition.x < 0)
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x - 1, (int)MapPosition.y, (int)MapPosition.z));
                }
                else if (FusionTargetMapPosition.y - MapPosition.y < 0)
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x, (int)MapPosition.y - 1, (int)MapPosition.z));
                }
                else if (FusionTargetMapPosition.y - MapPosition.y > 0)
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x, (int)MapPosition.y + 1, (int)MapPosition.z));
                }
                else if (FusionTargetMapPosition.z - MapPosition.z > 0)
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x, (int)MapPosition.y, (int)MapPosition.z + 1));
                }
                else if (FusionTargetMapPosition.z - MapPosition.z < 0)
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x, (int)MapPosition.y, (int)MapPosition.z - 1));
                }
                else
                {
                    availableCells.Add(MatchMakerMgmt.Instance.Map.GetCell((int)MapPosition.x, (int)MapPosition.y, (int)MapPosition.z));
                }
                availableCells = FilterCells(availableCells);
                nextCase = availableCells[Random.Range(0, availableCells.Count)];
            }
        }
        MatchMakerMgmt.Instance.Map.GetCell(MapPosition).Ghost = null;
        nextCase.Ghost = this;
        _lastMapPosition = nextCase.MapPosition;
        return nextCase;
    }

    /// <summary>
    /// Filter the cells given.
    /// Try to remove the last cell used.
    /// Try to remove cells where a ghost is.
    /// If cannot, return available cells.
    /// </summary>
    /// <param name="availableCells"></param>
    /// <returns></returns>
    private List<MapCell> FilterCells(List<MapCell> availableCells)
    {
        Assert.IsTrue(availableCells.Count > 0);
        if (_lastMapPosition != Vector3.zero && availableCells.Count > 1)
        {
            availableCells.RemoveAll(cell => cell.MapPosition == _lastMapPosition);
        }
        List<MapCell> cellsWithoutLastPosition = new List<MapCell>(availableCells);
        cellsWithoutLastPosition.RemoveAll(cell => cell.HasGhost);
        if (cellsWithoutLastPosition.Count > 0)
        {
            availableCells = cellsWithoutLastPosition;
        }
        return availableCells;
    }

    /// <summary>
    /// Executed on the next turn call.
    /// Will move the ghost. If the ghost change layer, it's layer will change on half the duration time.
    /// </summary>
    private void OnNextTurn()
    {
        if (Status == GhostStatus.HAS_DISAPPEARED || Status == GhostStatus.HAS_FUSIONNED)
            return;
        if (MatchMakerMgmt.Instance.CurrentTurn >= TurnToAppear && !_isAppeared)
        {
            GhostAppear(TimeToFadeInAtAwake);
            _turnToDisappear = TurnToAppear + TurnRateDisapear;
        }
        if (MatchMakerMgmt.Instance.CurrentTurn >= _turnToDisappear && _isAppeared && Status == GhostStatus.UNDEFINED)
        {
            GhostDisapear();
            _isAppeared = false;
            TurnToAppear = _turnToDisappear + TurnRateDisapear;
        }
        if (Status != GhostStatus.HAS_FUSIONNED && Status != GhostStatus.HAS_JUST_FUSIONNED)
            ResetColor();
        MatchMakerMgmt.Instance.TreeManager.AddGhostToGoodLayer(gameObject);
        TweenScale.Begin(gameObject, MatchMakerMgmt.Instance.DurationAction / 2, _scaleToKeep); // Bug if in same time...
        // If it is the turn just before fusion, form the couple on the fusion cell
        if (Status == GhostStatus.WILL_BE_FUSIONNED && MatchMakerMgmt.Instance.CurrentTurn >= FusionTurn - 1)
        {
            Status = GhostStatus.HAS_JUST_FUSIONNED;


            ResetColor(Color.white);
            // Tell the ghost to go on the fusion cell, whatever his current position
            _nextCaseToGo = MatchMakerMgmt.Instance.Map.GetCell(FusionTargetMapPosition);
            AdjustSide();

            // Change the position of the ghost relative to the hand marker and it's position
            float handWidth = GetHandsDataAndCreateCouple();

            // Hand markers of both ghost should be aligned, and on the center of the cell
            float handMarkerFinalXPosition = _nextCaseToGo.WorldPosition.x;
            float handMarkerFinalYPosition = _nextCaseToGo.WorldPosition.y + _YOffsetFromCellCenterWhenCoupling;
            if (RightInCouple)
            {
                handMarkerFinalXPosition = _nextCaseToGo.WorldPosition.x + handWidth / 2;
            }
            else
            {
                handMarkerFinalXPosition = _nextCaseToGo.WorldPosition.x - handWidth / 2;
            }
            Vector3 handMarkerFinalPosition = new Vector3(handMarkerFinalXPosition, handMarkerFinalYPosition, _nextCaseToGo.WorldPosition.z);
            StopFlapping();
            MoveTo(handMarkerFinalPosition - _offsetWithHands, (int)_nextCaseToGo.MapPosition.z);
            Collider.enabled = false;
        }
        // else if the ghost has not fusionned yet, choose a cell and move to it
        else if (Status != GhostStatus.HAS_JUST_FUSIONNED && Status != GhostStatus.HAS_FUSIONNED)
        {
            List<MapCell> adjacentCases = MatchMakerMgmt.Instance.Map.GetAdjacentCell(MapPosition);

            _nextCaseToGo = ChooseNextCase(adjacentCases);
            MoveTo(_nextCaseToGo.WorldPosition, (int)_nextCaseToGo.MapPosition.z);
        }
        else if (Status == GhostStatus.HAS_JUST_FUSIONNED)
        {
            StartFlapping();

            HeadController.BeInCouple();
            Status = GhostStatus.HAS_FUSIONNED;
        }
    }

    private void PutInFrontOfCurrentLayer()
    {
        MatchMakerMgmt.Instance.Factory.ResetGhostDepthInFrontOfCurrentLayer(GetComponent<UIWidget>());
    }

    /// <summary>
    /// I change the parent temporaly to calculate the offset if the ghost has to change his scale.
    /// </summary>
    /// <returns></returns>
    private float GetHandsDataAndCreateCouple()
    {
        Vector3 oldSpritePosition = SpriteTweenPosition.transform.localPosition;
        SpriteTweenPosition.transform.localPosition = _spriteOriginPosition;
        Vector3 oldMapPosition = MapPosition;
        MapPosition = _nextCaseToGo.MapPosition;
        Vector3 oldParentScale = SpriteParent.transform.localScale;
        SpriteParent.transform.localScale = Vector3.one;
        MatchMakerMgmt.Instance.TreeManager.AddGhostToGoodLayer(gameObject);
        transform.localScale = _scaleToKeep;
        float handWidth = HandsMarker.worldCorners[2].x - HandsMarker.worldCorners[0].x;
        _offsetWithHands = HandsMarker.transform.position - transform.position;
        if (!CoupleSet)
        {
            CreateCouple(_nextCaseToGo.WorldPosition);
            CoupleSet = true;
            Partner.GetComponent<Ghost>().CoupleSet = true;
        }
        MapPosition = oldMapPosition;
        SpriteParent.transform.localScale = oldParentScale;
        MatchMakerMgmt.Instance.TreeManager.AddGhostToGoodLayer(gameObject);
        SpriteTweenPosition.transform.localPosition = oldSpritePosition;
        transform.localScale = _scaleToKeep;
        SpriteParent.transform.localScale = Vector3.one;
        return Mathf.Abs(handWidth);
    }

    private void CreateCouple(Vector3 position)
    {
        GameObject couple = NGUITools.AddChild(transform.parent.gameObject, CouplePrefab.gameObject);
        couple.transform.position = position;
        couple.GetComponent<Couple>().Initialize(this, Partner.GetComponent<Ghost>());
    }

    private void AdjustSide()
    {
        if (_nextCaseToGo.MapPosition.x < MapPosition.x && CurrentSide == GhostSide.Right)
        {
            CurrentSide = GhostSide.Left;
            FlipSprites();
        }
        else if (_nextCaseToGo.MapPosition.x > MapPosition.x && CurrentSide == GhostSide.Left)
        {
            CurrentSide = GhostSide.Right;
            FlipSprites();
        }
    }

    private void MoveTo(Vector3 worldPosition, int zPosition)
    {
        AdjustSide();
        if (_isAppeared && transform.localScale == Vector3.zero)
        {
            TweenScale.Begin(gameObject, MatchMakerMgmt.Instance.DurationAction - .2f, _scaleToKeep); // Bug if in same time...
        }

        // The ghost moves in depth
        if (zPosition != MapPosition.z)
        {
            float oldZ = MapPosition.z;
            MapPosition = _nextCaseToGo.MapPosition;

            // If we go deeper
            if (zPosition > oldZ)
            {
                MatchMakerMgmt.Instance.TreeManager.PutGhostBehindCurrentLayer(this);
                if (_isAppeared)
                {
                    Vector3 newParentScale = MatchMakerMgmt.Instance.TreeManager.GetLayerForMapDepth((int)MapPosition.z).transform.localScale;
                    float parentScale = CurrentLayer.transform.localScale.x;
                    Vector3 newScale = newParentScale / (_scaleToKeep.x * parentScale);

                    TweenScale tweenScale = gameObject.AddComponent<TweenScale>();
                    tweenScale.duration = MatchMakerMgmt.Instance.DurationAction - .2f; // Bug if in same time...
                    tweenScale.from = transform.localScale;
                    tweenScale.to = newScale;
                    tweenScale.ignoreTimeScale = false;
                    tweenScale.method = UITweener.Method.EaseInOut;

                    tweenScale.PlayForward();
                }
                ChangeGhostColorForNextFog(ActualFogColor);
            }
            else // Else if we comme back to foreground
            {
                if (_isAppeared)
                {
                    TweenScale.Begin(gameObject, MatchMakerMgmt.Instance.DurationAction - .2f, _scaleToKeep); // Bug if in same time...
                    GetComponent<TweenScale>().method = UITweener.Method.EaseInOut;
                }
            }
        }

        // He's moving througout the x-axis or the y-axis
        TweenPositionTo(worldPosition);

        // GetComponent<TweenPosition>().animationCurve = GhostMovementCurve;
        MapPosition = _nextCaseToGo.MapPosition;
    }

    private void FlipSprites()
    {
        SpriteTweenPosition.transform.localScale = new Vector2(SpriteTweenPosition.transform.localScale.x * -1, SpriteTweenPosition.transform.localScale.y);
        _label.transform.localScale = new Vector2(_label.transform.localScale.x * -1, _label.transform.localScale.y);

        //_spriteOriginScale = SpriteTweenPosition.transform.localScale;
    }


    public void TweenPositionTo(Vector3 to)
    {
        TweenPosition tweenPosition = gameObject.AddComponent<TweenPosition>();

        tweenPosition.from = gameObject.transform.position;
        tweenPosition.to = to;
        tweenPosition.duration = MatchMakerMgmt.Instance.DurationAction;
        tweenPosition.ignoreTimeScale = false;
        tweenPosition.worldSpace = true;
        tweenPosition.animationCurve = GhostMovementCurve;

        tweenPosition.PlayForward();
    }

    public void ResetColor()
    {
        foreach (UIWidget uiWidgets in GetComponentsInChildren<UIWidget>())
        {
            uiWidgets.color = GhostColor;
        }
    }

    public void ResetColor(Color newColor)
    {
        foreach (UIWidget uiWidgets in GetComponentsInChildren<UIWidget>())
        {
            uiWidgets.color = newColor;
        }
    }

    #region Fade Functions

    private void FadeOut()
    {
        FadeOutDuring(MatchMakerMgmt.Instance.DurationAction / 4.0f, MatchMakerMgmt.Instance.DurationAction / 4.0f);
    }

    private void FadeIn()
    {
        FadeInDuring(0, MatchMakerMgmt.Instance.DurationAction / 4.0f);
    }

    private void Fade()
    {
        Invoke("FadeOut", 0);
        Invoke("FadeIn", MatchMakerMgmt.Instance.DurationAction / 2.0f);
    }

    private void FadeInDuring(float delay, float duration)
    {
        TweenAlpha.Begin(gameObject, duration, 1, delay);
    }

    private void FadeOutDuring(float delay, float duration)
    {
        TweenAlpha.Begin(gameObject, duration, 0.5f, delay);
    }
    #endregion Fade Functions

    public void GhostDisapear()
    {
        if (_isAppeared == true)
        {
            TweenScale.Begin(SpriteParent, TimeToDisapear, Vector3.zero);
            _isAppeared = false;
        }
    }

    public void GhostAppear(float duration)
    {
        _isAppeared = true;
        // transform.localScale = ScaleToKeep;
        TweenScale.Begin(SpriteParent, duration, Vector3.one);
    }

    public void DestroyGhost()
    {
        DestroyGhost(false);
    }

    public void DestroyGhost(bool isClicked = true)
    {
        if (isClicked)
        {
            if (IsInAGoodCouple)
            {
                TweenScale.Begin(gameObject, TimeToReachHigherScale, Vector3.one * ToScaleWhenGoodClick);
                Invoke("GhostDisapear", TimeToReachHigherScale);
                HeadController.BeHappy();
                Destroy(gameObject, TimeToReachHigherScale + TimeToDisapear + TimeForParticlePrefab);
                //Invoke("CreateFeedbackPrefab", TimeToDisapear + TimeToReachHigherScale);

                //PRSVL_PlayerMgnt.Instance.CurrentPlayer.AddCurrentResult(
                //    PRSVL_PlayerMgnt.Instance.CurrentPlayer.LastExercice, PRSVL_PlayerMgnt.Instance.GamesNames[1], 1);
            }
            else
            {
                GhostDisapear();
                //Invoke("CreateFeedbackPrefab", TimeToDisapear);
                HeadController.BeHungry();
                Destroy(gameObject, TimeToDisapear + TimeForParticlePrefab);

                //PRSVL_PlayerMgnt.Instance.CurrentPlayer.AddCurrentResult(
                //    PRSVL_PlayerMgnt.Instance.CurrentPlayer.LastExercice, PRSVL_PlayerMgnt.Instance.GamesNames[1], 0);
            }
        }
        else
        {
            GhostDisapear();
            HeadController.BeHungry();
            Destroy(gameObject, TimeToDisapear + TimeForParticlePrefab);
        }
    }

    public void CreateFeedbackPrefab()
    {
        GameObject feedbackPrefab = IsInAGoodCouple ? FeedbackGoodPrefab : FeedbackBadPrefab;

        GameObject go = NGUITools.AddChild(MatchMakerMgmt.Instance.gameObject, feedbackPrefab);
        go.transform.position = transform.position;
        go.GetComponent<TextFeedBack>().Init(_label.text);
        //UISprite sprite = go.GetComponent<UISprite>();
        //sprite.depth = GetComponent<UIWidget>().depth;
        //transform.rotation = Quaternion.Euler(0, 0, Random.Range(ParticleRotationRange.x, ParticleRotationRange.y));
        //go.GetComponent<TextFeedBack>().ini
        //Destroy(go.gameObject, 20.0f);
    }

    private void StopFlapping()
    {
        SpriteTweenPosition.from = SpriteTweenPosition.transform.localPosition;
        SpriteTweenPosition.to = _spriteOriginPosition;
        SpriteTweenPosition.style = UITweener.Style.Once;
        SpriteTweenPosition.duration = .2f;
        SpriteTweenPosition.PlayForward();
    }

    private void StartFlapping()
    {

        SpriteTweenPosition.delay = 0;
        SpriteTweenPosition.from = SpriteTweenPosition.transform.localPosition + Vector3.up * FlappingMaxYRange;
        SpriteTweenPosition.to = SpriteTweenPosition.transform.localPosition;
        SpriteTweenPosition.style = UITweener.Style.PingPong;
        SpriteTweenPosition.duration = OneFlappingDuration;
        SpriteTweenPosition.PlayReverse();
    }

    private void ChangeGhostColorForNextFog(Color fogColor)
    {
        foreach (UIWidget uiWidgets in GetComponentsInChildren<UIWidget>())
        {
            // From the Diquelou theorem
            float rValue = uiWidgets.color.r * (1 - fogColor.a) + fogColor.r * fogColor.a;
            float gValue = uiWidgets.color.g * (1 - fogColor.a) + fogColor.g * fogColor.a;
            float bValue = uiWidgets.color.b * (1 - fogColor.a) + fogColor.b * fogColor.a;
            Color newGhostColor = new Color(rValue, gValue, bValue);
            TweenColor.Begin(uiWidgets.gameObject, MatchMakerMgmt.Instance.DurationAction - .1f, newGhostColor);
        }
    }

    private void ChangeGhostColorForCurrentFogInstant(Color fogColor)
    {
        //Dead code
        foreach (UIWidget uiWidgets in GetComponentsInChildren<UIWidget>())
        {
            // From the Diquelou theorem
            float rValue = uiWidgets.color.r * (1 - fogColor.a) + fogColor.r * fogColor.a;
            float gValue = uiWidgets.color.g * (1 - fogColor.a) + fogColor.g * fogColor.a;
            float bValue = uiWidgets.color.b * (1 - fogColor.a) + fogColor.b * fogColor.a;
            Color newGhostColor = new Color(rValue, gValue, bValue);
            uiWidgets.color = newGhostColor;
        }
    }

    private void AddToGoodLayer()
    {
        MatchMakerMgmt.Instance.TreeManager.AddGhostToGoodLayer(gameObject);
    }

    private void ShowLetter()
    {
        TweenAlphaLabel.PlayReverse();
    }

    private void HideLetter()
    {
        TweenAlphaLabel.PlayForward();
    }



    #endregion private
}
