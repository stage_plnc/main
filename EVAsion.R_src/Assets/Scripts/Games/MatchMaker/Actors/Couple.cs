﻿/**********************************************************************
* Couple.cs
* Created at 11-5-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UIWidget))]
public class Couple : MonoBehaviour
{
    [Header("Sounds")]
    public AudioClip SoundSuccess;
    public AudioClip SoundFailure;

    [Header("Feedback System")]
    public GameObject FeedbackBadPrefab;
    public GameObject FeedbackGoodPrefab;
    public GameObject FeedbackMissedPrefab;

    [Tooltip("Lifetime ine turns")]
    public static int LifeTimeForGoodCouple;
    [Tooltip("Lifetime ine turns")]
    public static int LifeTimeForBadCouple;

    private Ghost _ghost1;
    private Ghost _ghost2;
    private int _nbTurn = 0;
    private bool _clicked = false;

    // Status = 0 : clicked and wrong.
    // Status = 1 : missed
    // Status = 2 = good
    private int _status;

    public void Initialize(Ghost ghost1, Ghost ghost2)
    {
        _ghost1 = ghost1;
        _ghost2 = ghost2;
        float ghost1SizeX = ghost1.GetRequiredComponent<Ghost>().SizeUIWidget.localSize.x * ghost1.transform.localScale.x;
        float ghost1SizeY = ghost1.GetRequiredComponent<Ghost>().SizeUIWidget.localSize.y * ghost1.transform.localScale.y;
        float ghost2SizeX = ghost2.GetRequiredComponent<Ghost>().SizeUIWidget.localSize.x * ghost2.transform.localScale.x;
        float ghost2SizeY = ghost2.GetRequiredComponent<Ghost>().SizeUIWidget.localSize.y * ghost2.transform.localScale.y;
        Vector2 size = new Vector2(ghost1SizeX + ghost2SizeX, Mathf.Max(ghost1SizeY, ghost2SizeY));
        this.GetRequiredComponent<UIWidget>().autoResizeBoxCollider = true;
        this.GetRequiredComponent<UIWidget>().SetDimensions((int)size.x, (int)size.y);
    }

    public void OnClick()
    {
        if (!_clicked)
        {
            if (_ghost1.IsInAGoodCouple)
            {
                _status = 2;
                SoundMgmt.Instance.PlayOneShotFXSound(SoundSuccess, 1.0f);
            }
            else
            {
                _status = 0;
                SoundMgmt.Instance.PlayOneShotFXSound(SoundFailure, 1.0f);
            }

            Invoke("CreateFeedbackPrefab", _ghost1.TimeToDisapear);

            _clicked = true;
            _ghost1.DestroyGhost(true);
            _ghost2.DestroyGhost(true);

            MatchMakerMgmt.Instance.ClickedOnGhost(_ghost1.IsInAGoodCouple);
        }
    }

    public void CreateFeedbackPrefab()
    {
        GameObject feedbackPrefab = null;
        
        switch (_status)
        {
            case 0:
                feedbackPrefab = FeedbackBadPrefab;
                break;
            case 1:
                feedbackPrefab = FeedbackMissedPrefab;
                break;
            case 2:
                feedbackPrefab = FeedbackGoodPrefab;
                break;
        }


        GameObject go = NGUITools.AddChild(MatchMakerMgmt.Instance.gameObject, feedbackPrefab);
        go.transform.position = transform.position;
        go.GetComponent<TextFeedBack>().Init(_ghost1.Letter + _ghost2.Letter);

        Destroy(go, go.GetComponent<TextFeedBack>().GDuration + 0.25f);
    }

    private void OnEnable()
    {
        MatchMakerMgmt.Instance.OnEndCycle += DestroyCouple;
        MatchMakerMgmt.Instance.OnNextTurn += GoNextTurn;
    }

    private void OnDisable()
    {
        MatchMakerMgmt.Instance.OnEndCycle -= DestroyCouple;
        MatchMakerMgmt.Instance.OnNextTurn -= GoNextTurn;
    }

    private void DestroyCouple()
    {
        Debug.Log("Destroying a couple ! A good couple ? " + _ghost1.IsInAGoodCouple);
        if (Constants.HardFeedParseval && _ghost1.IsInAGoodCouple && !_clicked)
        {
            MatchMakerMgmt.Instance.PlayerHasMissedCouple();

            _status = 1;
            CreateFeedbackPrefab();
        }

        Destroy(gameObject);
    }

    private void GoNextTurn()
    {
        _nbTurn++;
        if ((_ghost1.IsInAGoodCouple && _nbTurn > LifeTimeForGoodCouple) || (!_ghost1.IsInAGoodCouple && _nbTurn > LifeTimeForBadCouple))
        {
            _ghost1.DestroyGhost(false);
            _ghost2.DestroyGhost(false);

            DestroyCouple();
        }
    }
}
