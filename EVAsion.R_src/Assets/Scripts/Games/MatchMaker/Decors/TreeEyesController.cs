﻿/**********************************************************************
* TreeEyesController.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeEyesController : MonoBehaviour {

    public UISpriteAnimation SpriteAnimation;
    //public UISprite Sprite;
    public Vector2 MinMaxTimeRange;

    private float _timer;
    private float _timeRange;

    private void Start()
    {
        UpdateTimers();
    }

    private void Update()
    {
        if (Time.time - _timer >= _timeRange)
        {
            UpdateTimers();
            SpriteAnimation.Play();
        }
    }

    private void UpdateTimers()
    {
        _timer = Time.time;
        _timeRange = Random.Range(MinMaxTimeRange.x, MinMaxTimeRange.y);
    }
}
