﻿/**********************************************************************
* HoleGenerator.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleGenerator : MonoBehaviour {

    public GameObject HolePrefab;
    public UIWidget HoleContainer;
    public float GenerationProbability;

    private void Awake()
    {
        if (Random.Range(0f, 1f) < GenerationProbability)
        {
            GameObject hole = NGUITools.AddChild(HoleContainer.gameObject, HolePrefab);
            float yPosition = Random.Range(HoleContainer.worldCorners[0].y, HoleContainer.worldCorners[1].y);
            Vector3 position = new Vector3(HoleContainer.transform.position.x, yPosition, 1);
            hole.transform.position = position;
            hole.GetRequiredComponent<UIWidget>().depth = HoleContainer.depth;
            hole.transform.localRotation = HoleContainer.transform.localRotation;
        }
    }
}
