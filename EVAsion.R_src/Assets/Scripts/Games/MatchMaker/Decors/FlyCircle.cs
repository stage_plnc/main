﻿/**********************************************************************
* FlyCircle.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RotatingCircle))]
public class FlyCircle : MonoBehaviour
{
    public GameObject[] FlyingThingPrefabs;
    public Vector2 MinMaxRangeDistanceFromCenter;
    public Vector2 MinMaxRangeDelay;
    public Vector2 MinMaxRangeTimeBetweenRotation;
    public Vector2 MinMaxTimeToCompleteShortRotation;
    public float ShortRotation = 200f;
    public Vector3 PrefabRotation;
    public bool FlyingObjectRotateAroundItself = false;
    public float AngleRotation;

    private GameObject _flyingObject;
    private RotatingCircle _rotatingCircle;
    private bool _isStarted = false;
    private float _startDelay;
    private float _startTime;
    private float _timeBetweenRotation;
    private float _distanceFromCenter;
    private float _timeToCompleteShortRotation;

    private float _originalXScale;

    private void Awake()
    {
        _rotatingCircle = GetComponent<RotatingCircle>();
        _startTime = Time.time;
        _startDelay = Random.Range(MinMaxRangeDelay.x, MinMaxRangeDelay.y);
        _timeBetweenRotation = 0;
        _distanceFromCenter = 0;
        _originalXScale = transform.localScale.x;
    }

    private void Update()
    {
        if (!_isStarted && Time.time - _startTime >= _startDelay)
        {
            _startTime = Time.time;
            _isStarted = true;
        }
        if (_isStarted && Time.time - _startTime >= _timeBetweenRotation)
        {
            MakeANewTurn();
        }
    }

    private void MakeANewTurn()
    {
        // Create the prefab
        if (_flyingObject != null)
        {
            Destroy(_flyingObject);
        }
        _flyingObject = NGUITools.AddChild(gameObject, FlyingThingPrefabs[Random.Range(0, FlyingThingPrefabs.Length)]);
        _flyingObject.layer = gameObject.layer;
        Color noAlpha = _flyingObject.GetRequiredComponentInChildren<UIWidget>().color;
        noAlpha.a = 0;
        _flyingObject.GetRequiredComponent<UIWidget>().color = noAlpha;
        _flyingObject.transform.rotation = Quaternion.Euler(PrefabRotation);

        _startTime = Time.time;
        _distanceFromCenter = Random.Range(MinMaxRangeDistanceFromCenter.x, MinMaxRangeDistanceFromCenter.y);
        _timeToCompleteShortRotation = Random.Range(MinMaxTimeToCompleteShortRotation.x, MinMaxTimeToCompleteShortRotation.y);
        float timeToCompleteThisRotation = (_distanceFromCenter / ShortRotation) * _timeToCompleteShortRotation;

        TweenAlpha.Begin(_flyingObject, 2f / 10f * timeToCompleteThisRotation, 1);
        // To avoid start a new rotation before finishing it
        _timeBetweenRotation = Random.Range(Mathf.Max(MinMaxRangeTimeBetweenRotation.x, timeToCompleteThisRotation), Mathf.Max(MinMaxRangeTimeBetweenRotation.y, timeToCompleteThisRotation));
        _flyingObject.transform.localPosition = new Vector3(0, _distanceFromCenter, 0);
        int direction = (Random.Range(0f, 1f) < 0.5f) ? -1 : 1;
        _flyingObject.transform.localScale = new Vector3(direction * _originalXScale, _flyingObject.transform.localScale.y, _flyingObject.transform.localScale.z);
        _rotatingCircle.MakeCompleteRotation(timeToCompleteThisRotation, direction);
        StartCoroutine(DisapearDelayed(_flyingObject, 7.5f / 10f * timeToCompleteThisRotation, .5f / 10f * timeToCompleteThisRotation));
        if (FlyingObjectRotateAroundItself)
        {
            RotatingCircle rotationScript = _flyingObject.GetRequiredComponent<RotatingCircle>();
            rotationScript.zRotation = AngleRotation;
            rotationScript.MakeCompleteRotation(timeToCompleteThisRotation, direction);
            //TweenRotation.Begin(_flyingObject, timeToCompleteThisRotation, Quaternion.Euler(_flyingObject.transform.localRotation.x, _flyingObject.transform.localRotation.y, _flyingObject.transform.localRotation.z + AngleRotation));
        }
    }

    private IEnumerator DisapearDelayed(GameObject go, float delay, float timeToDisapear)
    {
        yield return new WaitForSeconds(delay);
        TweenAlpha.Begin(go, timeToDisapear, 0);
    }
}
