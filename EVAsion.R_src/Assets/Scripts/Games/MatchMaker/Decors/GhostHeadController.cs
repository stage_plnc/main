﻿/**********************************************************************
* GhostHeadController.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostHeadController : MonoBehaviour {

    public Ghost TheGhost;
    public UISprite HeadSprite;
    public UISpriteAnimation SpriteAnimation;

    [Header("Sprite names")]
    public string IdleSpriteName;
    public string WinkSpriteName;

    [Header("Prefix sprite animations")]
    public string HappyPrefix;
    public string HangryPrefix;
    public string InCouplePrefix;

    [Header("Animations Data")]
    public int HappyFPS;
    public int HangryFPS;
    public int InCoupleFPS;

    public Vector2 TimeBetweenWink;
    public Vector2 WinkDuration;

    public enum GhostAnimationState { Idle, Happy, InCouple, Hangry }

    private float _timer;

    private GhostAnimationState _currentState;

    // Idle
    private float _timeBetweenWink;
    private float _winkDuration;
    private bool _isBlinking;

    private void Awake()
    {
        Idle();
    }

    void Update () {
		switch (_currentState)
        {
            case GhostAnimationState.Idle:
                DoIdle();
                break;
        }
	}

    private void DoIdle()
    {
        if (!_isBlinking && Time.time - _timer >= _timeBetweenWink)
        {
            HeadSprite.spriteName = WinkSpriteName;
            _timer = Time.time;
            _isBlinking = true;
        }
        else if (_isBlinking && Time.time - _timer >= _winkDuration)
        {
            _timer = Time.time;
            HeadSprite.spriteName = IdleSpriteName;
            _isBlinking = false;
        }
    }

    public void Idle()
    {
        _winkDuration = Random.Range(WinkDuration.x, WinkDuration.y);
        _timeBetweenWink = Random.Range(TimeBetweenWink.x, TimeBetweenWink.y);
        _currentState = GhostAnimationState.Idle;
        _timer = Time.time;
        HeadSprite.spriteName = IdleSpriteName;
        SpriteAnimation.enabled = false;
    }

    public void BeHappy()
    {
        _currentState = GhostAnimationState.Happy;
        ChangeAnimation(HappyPrefix, HappyFPS);
    }

    public void BeHungry()
    {
        _currentState = GhostAnimationState.Hangry;
        ChangeAnimation(HangryPrefix, HangryFPS);
    }

    public void BeInCouple()
    {
        _currentState = GhostAnimationState.InCouple;
        ChangeAnimation(InCouplePrefix, InCoupleFPS);
    }

    private void ChangeAnimation(string namePrefix, int fps)
    {
        SpriteAnimation.enabled = true;
        SpriteAnimation.namePrefix = namePrefix;
        SpriteAnimation.framesPerSecond = fps;
        SpriteAnimation.loop = true;
    }
}
