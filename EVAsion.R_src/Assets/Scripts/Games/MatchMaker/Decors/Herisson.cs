﻿/**********************************************************************
* Herisson.cs
* Created at 19-6-2017 
* Copyright (c) 2017 
* XIWEN Studio
**********************************************************************/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Translating))]
public class Herisson : MonoBehaviour {
    public Vector2 MinMaxRangeSpeed;

	// Use this for initialization
	void Start () {
        float speed = Random.Range(MinMaxRangeSpeed.x, MinMaxRangeSpeed.y);
        int direction = (Random.Range(0f, 1f) < 0.5f) ? 1 : -1;
        transform.localScale = new Vector3(transform.localScale.x * direction, transform.localScale.y, transform.localScale.z);
        GetComponent<Translating>().Translation = new Vector3(direction, 0, 0);
        GetComponent<Translating>().Speed = speed;
    }
	
}
