﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseCycleData : CycleData
{ 
    public int NbrDoors;
    public int NbrTurns;
    public float DurationTurn;
    public int NumBadTarget;

    public ChaseCycleData(string combination, int nbrDoors, int nbrTurns) : base(combination, nbrTurns)
    {
        this.NbrDoors = nbrDoors;
        this.NbrTurns = nbrTurns;

        this.DurationTurn = Constants.Chase.CycleDuration / (float) nbrTurns;
    }
}
