﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorBehaviour : MonoBehaviour {

    [Header("Borders")]
    public UISprite LeftBorderMountain;
    public UISprite RightBorderMountain;

    public string[] LeftBorderSpriteNames;
    public string[] RightBorderSpriteNames;

    [Header("Top Floor")]
    public UITexture TopFloorTexture;

    private static int _floorGeneratedCounter = 0;

	// Use this for initialization
	void Start ()
    {
        LeftBorderMountain.spriteName = LeftBorderSpriteNames[_floorGeneratedCounter % 3];
        RightBorderMountain.spriteName = RightBorderSpriteNames[_floorGeneratedCounter % 3];

        _floorGeneratedCounter++;

        if (_floorGeneratedCounter % 2 == 0)
        {
            TopFloorTexture.flip = UIBasicSprite.Flip.Horizontally;
        }
    }
}
