﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseMgmt : GameMgmt
{
    [Header("ChaseMgmt attributes")]
    public ChaseFactory Factory;
    public ChaseLevelMgmt LevelMgmt;

    private static ChaseMgmt _instance;
    public static ChaseMgmt Instance
    {
        get { return _instance; }
    }

    public GameObject BlockCollisionPanel;

    private int _currentTurn = 0;
    private float _startTurn;

    public bool FirstCycle { get { return _nbrElapsedCycles == 0; } }
    public bool SecondCycle { get { return _nbrElapsedCycles == 1; } }

    public bool WillChangeLevel = false;
    public bool FirstFloorStarted = false;
    public bool ClickOnDoor = false;

    public AudioClip SoundSuccess;
    public AudioClip SoundFailure;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Debug.LogError("[ChaseMgmt] Instance de ChaseMgmt already exists)");
        }

        _gameName = "chase";
    }

    public new void OnDestroy()
    {
        base.OnDestroy();

        _instance = null;
    }


    public override void Start()
    {
        base.Start();

        _factory = Factory;

        Invoke("StartGame", 1.0f);
    }

    public override void StartGameCycle()
    {
        base.StartGameCycle();

        _currentTurn = 0;
        _startTurn = Time.time;

        FirstFloorStarted = true;

        LevelMgmt.ShowFirstFloorLabels();

        SetBlockCollisionInactive();
    }

    public override void StartInterlude()
    {
        base.StartInterlude();

        Invoke("EndInterlude", Constants.Chase.InterludeDuration);
    }

    public override void EndGameCycle()
    {
        FirstFloorStarted = false;
        LevelMgmt.ClearFloors();

        SetBlockCollisionActive();

        base.EndGameCycle();
    }

    public override void GameEnded()
    {
        base.GameEnded();
    }

    public override void EndTutorial()
    {
        throw new NotImplementedException();
    }

    public void SetBlockCollisionActive()
    {
        BlockCollisionPanel.SetActive(true);
    }

    public void SetBlockCollisionInactive()
    {
        BlockCollisionPanel.SetActive(false);
    }

    public void HandleClickOnDoor(string label)
    {
        if (label == _cycleData.Combination)
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 1.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetSuccess++;
            }

            SoundMgmt.Instance.PlayOneShotFXSound(SoundSuccess, 1.0f);
            _nbrTotalSuccess++;
        }
        else
        {
            if (Constants.HardFeedParseval)
            {
                _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
            }
            else
            {
                _cycleSuccessAndFailure.NbrTargetFailure++;
            }

            SoundMgmt.Instance.PlayOneShotFXSound(SoundFailure, 1.0f);
            _nbrTotalFailure++;
        }

        WillChangeLevel = true;
        ClickOnDoor = true;
        SetBlockCollisionActive();

        Invoke("ShiftToNextFloor", 1.0f);
    }

    public override float CycleScore()
    {
        return 1.0f;
    }

    private void ShiftToNextFloor()
    {
        int nbrTurns = ((ChaseCycleData)_cycleData).NbrTurns;

        _currentTurn++;

        SetBlockCollisionActive();

        if (Constants.HardFeedParseval && !ClickOnDoor)
        {
            _pPlayer.AddCurrentResult(_currentParsevalDT, _gameName, 0.0f);
        }

        if (_currentTurn == nbrTurns)
        {
            EndGameCycle();
        }
        else
        {
            _startTurn = Time.time;

            LevelMgmt.ShiftLevels();
        }



        WillChangeLevel = false;
        ClickOnDoor = false;
    }

    protected override void Update()
    {
        base.Update();

        if (Status == GameStatus.GAME_CYCLE && !WillChangeLevel)
        {
            float durationTurn = ((ChaseCycleData)(_cycleData)).DurationTurn;
            float elapsedTime = Time.time - _startTurn;

            if (elapsedTime > durationTurn)
            {
                ShiftToNextFloor();
            }
        }
    }


}
