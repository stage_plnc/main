﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlooringDecoration : BaseDecoration
{
    public override void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood)
    {
        // If this is the first cycle, and the cell is at the very first floor, we're adding flooring
        if (row == Constants.Chase.NbrVisibleFloors - 1 && ChaseMgmt.Instance.FirstCycle)
        {
            cellBehaviour.SetFlooring();
        }

        if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY)
        {
            if (neighbourhood[(int)CardinalPoint.S] != null && neighbourhood[(int)CardinalPoint.S].Type == ChaseCell.TypeCell.EMPTY)
            {
                cellBehaviour.SetFlooring();
            }
            else if (UnityEngine.Random.value < 0.5f)
            {
                cellBehaviour.SetFlooring();
            }
        }
        else
        {
            cellBehaviour.SetFlooring();
        }

    }
}
