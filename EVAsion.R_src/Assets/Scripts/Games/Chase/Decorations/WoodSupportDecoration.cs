﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodSupportDecoration : BaseDecoration
{
    public override void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood)
    {
        // First condition : this cell must have a floor
        if (cellBehaviour.HasFlooring)
        {
            // If the cell below has flooring
            if (row == 0 || neighbourhood[(int)CardinalPoint.N].HasFlooring)
            {
                // We randomly add a wood border
                if (UnityEngine.Random.value < 0.5f)
                {
                    cellBehaviour.SetLeftWoodBorder();
                }

                if (UnityEngine.Random.value < 0.5f)
                {
                    cellBehaviour.SetRightWoodBorder();
                }
            }
        }
    }
}
