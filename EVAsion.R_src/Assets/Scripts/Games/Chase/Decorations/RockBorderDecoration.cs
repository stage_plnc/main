﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBorderDecoration : BaseDecoration {

    public override void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood)
    {
        // Thses are the extremes (far left and far right) of the map
        if (col == 0 && !cellBehaviour.HasLeftWoodSupport)
        {
            if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY)
            {
                cellBehaviour.SetLeftRockBorder(true);
            }
        }
        if (col == Constants.Chase.NbrColumns - 1 && cellBehaviour.Type != ChaseCell.TypeCell.EMPTY && !cellBehaviour.HasRightWoodSupport)
        {
            if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY)
            {
                cellBehaviour.SetRightRockBorder(false);
            }
        }

        // If the sprite at the left as a right wood support
        if (neighbourhood[(int)CardinalPoint.W] != null)
        {
            if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY && neighbourhood[(int)CardinalPoint.W].HasRightWoodSupport)
            {
                cellBehaviour.SetLeftRockBorder(true);
            }
            else if (cellBehaviour.Type == ChaseCell.TypeCell.EMPTY && neighbourhood[(int)CardinalPoint.W].Type != ChaseCell.TypeCell.EMPTY)
            {
                if (!neighbourhood[(int)CardinalPoint.W].HasRightWoodSupport && !neighbourhood[(int)CardinalPoint.W].HasRightRockBorder)
                    cellBehaviour.SetLeftRockBorder(false);
            }
        }

        // If the sprite at the right as a left wood support
        if (neighbourhood[(int)CardinalPoint.E] != null)
        {
            if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY && neighbourhood[(int)CardinalPoint.E].HasLeftWoodSupport)
            {
                cellBehaviour.SetRightRockBorder(false);
            }
            else if (cellBehaviour.Type == ChaseCell.TypeCell.EMPTY && neighbourhood[(int)CardinalPoint.E].Type != ChaseCell.TypeCell.EMPTY)
            {
                if (!neighbourhood[(int)CardinalPoint.E].HasLeftWoodSupport && !neighbourhood[(int)CardinalPoint.E].HasLeftRockBorder)
                    cellBehaviour.SetRightRockBorder(true);
            }
        }

    }
}
