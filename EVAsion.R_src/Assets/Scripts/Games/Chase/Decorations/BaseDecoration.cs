﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseDecoration
{

    // The neighbourhood is defined this way : it is an 8-length array
    // 0 1 2
    // 7   3
    // 6 5 4

    public enum CardinalPoint
    {
        NW = 0,
        N = 1,
        NE = 2,
        E = 3,
        SE = 4,
        S = 5,
        SW = 6,
        W = 7,
    };

    [Range(0, 1)]
    public float ProbabilityWoodSupport = 0.5f;

    public abstract void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood) ;
}
