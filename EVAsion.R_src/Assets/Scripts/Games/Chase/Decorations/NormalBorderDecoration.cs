﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalBorderDecoration : BaseDecoration
{
    public override void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood)
    {
        if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY)
        {
            if (!cellBehaviour.HasLeftWoodSupport && !cellBehaviour.HasLeftRockBorder)
            {
                cellBehaviour.SetLeftNormalBorder();
            }

            if (!cellBehaviour.HasRightWoodSupport && !cellBehaviour.HasRightRockBorder)
            {
                cellBehaviour.SetRightNormalBorder();
            }
        }

    }
}
