﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowRandomly : MonoBehaviour
{
    [Range(0, 1)]
    public float Probability;

    private void Awake()
    {
        if (UnityEngine.Random.value <= Probability)
        {
            GetComponent<UISprite>().enabled = true;
        }
    }

}
