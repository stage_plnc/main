﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PonctualElementsDecoration : BaseDecoration
{
    public override void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood)
    {
        if (cellBehaviour.HasFlooring)
        {
            if (UnityEngine.Random.value < 0.5f)
            {
                cellBehaviour.AddPonctualElement();
            }
        }
    }
}
