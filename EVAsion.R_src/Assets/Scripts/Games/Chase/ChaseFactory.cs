﻿using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;

using System.Text;
using System;
using System.Linq;

[RequireComponent(typeof(ChaseMgmt))]
public class ChaseFactory : CycleFactory
{
    [Header("ChaseFactory attributes")]
    [Range(0, 1)]
    public float ProbaWall;

    private ChaseDifficultyData _difficultyData;

    [Header("Prefab")]
    public GameObject[] WallPrefabs;
    public GameObject DoorPrefab;
    public GameObject EmptyPrefab;

    private ChaseCell[][] _currentMap;

    //public TypeCell[][] CurrentMap { get { return _currentMap; } }

    protected override void Awake()
    {
        base.Awake();

        _currentMap = new ChaseCell[Constants.Chase.NbrVisibleFloors][];

        for (int i = 0; i < Constants.Chase.NbrVisibleFloors; i++)
        {
            _currentMap[i] = new ChaseCell[Constants.Chase.NbrColumns];
        }
    }

    public override CycleData InitNextCycle(LPNC_DataExport parsevalDT)
    {
        base.InitNextCycle(parsevalDT);

        HandleDifficultyData(parsevalDT, Constants.DifficultyMethod);

        _goodCombination = GenerateGoodCombination();
        _wrongCombination = GenerateWrongCombination();

        _currentCycle++;

        return new ChaseCycleData(_goodCombination, _difficultyData.nbrDoors, _difficultyData.nbrTurns);
    }

    private void HandleDifficultyData(LPNC_DataExport parsevalDt, string difficulty)
    {
        Debug.Log("Difficulty = " + difficulty);

        if (difficulty == "random")
        {
            parsevalDt.d01 = _random.Next(0, 6);
            parsevalDt.d02 = _random.Next(0, 6);
            parsevalDt.d03 = _random.Next(0, 6);
        }

        // Not sure...
        else if (difficulty == "linear")
        {
            if (_currentCycle >= 1)
            {
                parsevalDt.d01 = (int)Mathf.Round(_initialParsevalDT.d01 + _factLinearDifficultyIncrease[0] * _currentCycle);
                parsevalDt.d02 = (int)Mathf.Round(_initialParsevalDT.d02 + _factLinearDifficultyIncrease[1] * _currentCycle);
                parsevalDt.d03 = (int)Mathf.Round(_initialParsevalDT.d03 + _factLinearDifficultyIncrease[2] * _currentCycle);
            }
            else
            {
                parsevalDt.d01 = (int)Constants.LinearDifficultyStartBound.x;
                parsevalDt.d02 = (int)Constants.LinearDifficultyStartBound.y;
                parsevalDt.d03 = (int)Constants.LinearDifficultyStartBound.z;

                _factLinearDifficultyIncrease[0] = (float)((int)Constants.LinearDifficultyEndBound.x - parsevalDt.d01) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[1] = (float)((int)Constants.LinearDifficultyEndBound.y - parsevalDt.d02) / (Constants.NbrTotalCycles - 1);
                _factLinearDifficultyIncrease[2] = (float)((int)Constants.LinearDifficultyEndBound.z - parsevalDt.d03) / (Constants.NbrTotalCycles - 1);

                _initialParsevalDT = parsevalDt;
            }
        }

        Debug.LogWarningFormat("d01 = {0}, d02 = {1}, d03 = {2}", parsevalDt.d01, parsevalDt.d02, parsevalDt.d03);

        _parsevalDT = parsevalDt;

        _difficultyData = GetDifficultyData(parsevalDt);
    }


    public override void GenerateNextCycle()
    {
        if (!ChaseMgmt.Instance.FirstCycle)
        {
            ChaseMgmt.Instance.LevelMgmt.ShiftToNextLevelCycle();
        }

        ChaseMgmt.Instance.LevelMgmt.PrepareFloors(_difficultyData.nbrTurns);

        for (int i = 0; i < _difficultyData.nbrTurns; i++)
        {
            ChaseCell[][] map = GenerateNewMap(i);

            ChaseMgmt.Instance.LevelMgmt.GenerateLevel(map);
        }
    }

    public ChaseCell[][] GenerateNewMap(int idxMap)
    {
        int[] randomIndexesDoors = GenerateRandomIndexes(_difficultyData.nbrDoors);
        int indexGoodDoor = _random.Next(randomIndexesDoors.Length);

        ClearMap();

        for (int i = 0; i < randomIndexesDoors.Length; i++)
        {
            int row = (int)Mathf.Floor(randomIndexesDoors[i] / Constants.Chase.NbrColumns);
            int col = randomIndexesDoors[i] % Constants.Chase.NbrColumns;

            if (i == indexGoodDoor)
            {
                _currentMap[row][col] =
                    new ChaseDoor(DoorPrefab, _goodCombination, true, idxMap == 0);
            }
            else
            {
                _currentMap[row][col] =
                    new ChaseDoor(DoorPrefab, _wrongCombination, false, idxMap == 0);
            }

        }

        for (int i = 0; i < _currentMap.Length; i++)
        {
            for (int j = 0; j < _currentMap[i].Length; j++)
            {
                if (_currentMap[i][j] == null)
                {
                    _currentMap[i][j] = GetRandomCell();
                }
            }
        }

        return _currentMap;
        // PrintMap();
    }

    private ChaseCell GetRandomCell()
    {
        ChaseCell chaseCell = null;
        float random = (float)_random.NextDouble();

        if (random < ProbaWall)
        {
            chaseCell = new ChaseWall(
                WallPrefabs[_random.Next(0, WallPrefabs.Length)]);
        }
        else
        {
            chaseCell = new ChaseEmpty(EmptyPrefab);
        }

        return chaseCell;
    }

    private int[] GenerateRandomIndexes(int nbrIndexes)
    {
        int[] allRandomIndexes = new int[Constants.Chase.NbrColumns * Constants.Chase.NbrVisibleFloors];
        int[] randomIndexes = new int[nbrIndexes];

        for (int i = 0; i < allRandomIndexes.Length; i++)
        {
            allRandomIndexes[i] = i;
        }

        allRandomIndexes = RandomShuffling.Shuffle(allRandomIndexes, _random);

        for (int i = 0; i < nbrIndexes; i++)
        {
            randomIndexes[i] = allRandomIndexes[i];
        }

        return randomIndexes;
    }

    private void ClearMap()
    {
        for (int i = 0; i < _currentMap.Length; i++)
        {
            for (int j = 0; j < _currentMap[i].Length; j++)
            {
                _currentMap[i][j] = null;
            }
        }
    }

    private string GenerateGoodCombination()
    {
        string goodCombination = string.Empty;

        _indexNewPattern = TargetGeneratorMgmt.Instance.GetRandomIndex(_random, _difficultyData.DifficultyTarget);

        string[] array = TargetGeneratorMgmt.Instance.GetCombinationAt(_random, _indexNewPattern, _difficultyData.DifficultyTarget);
        _leftPartCombination = array[0].Trim();
        _rightPartCombination = array[1].Trim();

        if (_upperCaseSession)
        {
            _leftPartCombination = _leftPartCombination.ToUpper();
            _rightPartCombination = _rightPartCombination.ToUpper();
        }
        else
        {
            _leftPartCombination = _leftPartCombination.ToLower();
            _rightPartCombination = _rightPartCombination.ToLower();
        }

        goodCombination = _leftPartCombination + _rightPartCombination;

        return goodCombination;
    }

    private string GenerateWrongCombination()
    {
        string[] array = new string[2];

        string wrongCombination = DistractorsMgmt.Instance.GenerateWrongCombinationMMAndChase(
            _goodCombination, _difficultyData.DifficultyTarget);

        if (_upperCaseSession)
        {
            wrongCombination = wrongCombination.ToUpper();
        }
        else
        {
            wrongCombination = wrongCombination.ToLower();
        }

        return wrongCombination;
    }



    #region DifficultyData
    private ChaseDifficultyData GetDifficultyData(LPNC_DataExport parsevalDT)
    {
        string combinationPattern = "A";

        int nbrDoors = 3;
        int nbrTurns = 3;

        int difficultyDistractor = 1;
        int difficultyTarget = 1;

        try
        {
            difficultyTarget = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("chase", "difficulty_target", parsevalDT.d01));

            if (difficultyDistractor < 1 || difficultyDistractor > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de chase.json : la dimension difficulty de la difficulté " + parsevalDT.d01 +
                    "doit etre comprise entre 1 et 6");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de chase.json a la dimension difficulty_similarity de la difficulté " + parsevalDT.d01);
        }

        try
        {
            difficultyDistractor = int.Parse(DifficultyMgmt.Instance.GetDifficultyValue("chase", "difficulty_distractor", parsevalDT.d02));

            if (difficultyDistractor < 1 || difficultyDistractor > 6)
            {
                PrintToScreen.Print("Erreur lors de la lecture de chase.json : la dimension difficulty de la difficulté " + parsevalDT.d02 +
                    "doit etre comprise entre 1 et 6");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de chase.json a la dimension difficulty_similarity de la difficulté " + parsevalDT.d02);
        }

        try
        {
            string[] gameValues =
                 DifficultyMgmt.Instance.GetDifficultyValue("chase", "nbr_doors;nbr_turns", parsevalDT.d03).Split(';');

            nbrDoors = int.Parse(gameValues[0]);
            nbrTurns = int.Parse(gameValues[1]);


            if (nbrTurns < 1 || nbrTurns > 9)
            {
                PrintToScreen.Print("Erreur lors de la lecture de chase.json : la dimension nbr_turns de la difficulté " + parsevalDT.d03 +
                 ".\n : nbrTurns doit etre compris entre 2 et 8");
            }
        }
        catch (System.FormatException)
        {
            PrintToScreen.Print("Erreur lors de la lecture de chase.json a la dimension nbr_doors_and_nbr_turns de la difficulté " + parsevalDT.d03);

        }

        //if (Constants.NbrEvents != -1)
        //{
        //    nbrTurns = Constants.NbrEvents;
        //}

        return new ChaseDifficultyData(difficultyTarget, difficultyDistractor, nbrDoors, combinationPattern, nbrTurns);
    }

    private class ChaseDifficultyData : DifficultyData
    {
        public int nbrDoors;
        public int nbrTurns;
        public int firstPartCombination;
        public int secondPartCombination;

        public ChaseDifficultyData(int difficultyTarget, int difficultyDistractor, int nbrDoors, string combinationPattern, int nbrTurns) :
            base(difficultyTarget, difficultyDistractor)
        {
            this.nbrDoors = nbrDoors;
            this.nbrTurns = nbrTurns;
        }


    }
    #endregion
}
