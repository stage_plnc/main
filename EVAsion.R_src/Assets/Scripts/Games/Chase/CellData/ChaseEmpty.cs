﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseEmpty : ChaseCell
{
    public ChaseEmpty(GameObject prefab) : base(prefab)
    {
        Type = TypeCell.EMPTY;
    }

    public override GameObject GenerateGO(GameObject parent)
    {
        GameObject go = NGUITools.AddChild(parent, CellPrefab);

        return go;
    }
}
