﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseDoor : ChaseCell
{
    public bool IsLabelHidden;
    public string Label;
    public bool IsGoodCombination;
    
    public ChaseDoor(GameObject prefab, string label, bool isGoodCombination, bool isLabelHidden) : base(prefab)
    {
        Type = TypeCell.DOOR;

        Label = label;
        IsLabelHidden = isLabelHidden;
        IsGoodCombination = isGoodCombination;
    }

    public override GameObject GenerateGO(GameObject parent)
    {
        GameObject go = NGUITools.AddChild(parent, CellPrefab);
        DoorBehaviour doorBehaviour = go.GetComponent<DoorBehaviour>();

        doorBehaviour.Init(Label, IsGoodCombination, IsLabelHidden);

        return go;
    }

    //public GameObject GenerateGO()
    //{

    //}
}
