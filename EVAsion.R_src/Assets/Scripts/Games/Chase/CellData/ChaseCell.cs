﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ChaseCell
{
    public enum TypeCell
    {
        UNDEFINED = 0,
        EMPTY = 1,
        WALL = 2,
        DOOR = 3

    };

    public GameObject CellPrefab;
    public TypeCell Type;

    public ChaseCell(GameObject prefab)
    {
        CellPrefab = prefab;
    }

    public abstract GameObject GenerateGO(GameObject parent);
}