﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseWall : ChaseCell
{
    public ChaseWall(GameObject prefab) : base(prefab)
    {
        Type = TypeCell.WALL;
    }

    public override GameObject GenerateGO(GameObject parent)
    {
        GameObject go = NGUITools.AddChild(parent, CellPrefab);

        return go;
    }

}
