﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehaviour : CellBehaviour
{
    public static ShuffleBag<string> BackgroundSpriteNames = null;

    [Header("Door attributes")]
    [Header("Sprite names")]
    public string ClosedDoorSpriteName = "Porte{0}";
    public string OpenedDoorSpriteName = "Porte{0}OUVERTE";
    public string LittleGirlSpriteName = "Enfant_PORTE";
    public string FoxSpriteName = "Renard_PORTE";

    [Header("Feedback System")]
    public GameObject FeedbackBadPrefab;
    public GameObject FeedbackGoodPrefab;

    [Header("Sprites")]
    public int NbrDoorsAvailable = 6;
    public UISprite ClosedDoorSprite;
    public UISprite OpenedDoorSprite;
    public UISprite BehindDoorSprite;
    public UILabel DoorLabel;

    private TweenAlpha _labelTweenAlpha;
    private int _indexDoor;

    private string _label;
    private bool _isGoodCombination;

    protected override void Awake()
    {
        base.Awake();

        Type = ChaseCell.TypeCell.DOOR;

        _labelTweenAlpha = DoorLabel.gameObject.GetComponent<TweenAlpha>();

        BackgroundSprite.spriteName = GetBackgroundSprite(BackgroundSprite);
    }


    private void OnEnable()
    {
        ChaseMgmt.Instance.ShowLetters += ShowLetter;
        ChaseMgmt.Instance.HideLetters += HideLetter;
    }

    private void OnDisable()
    {
        ChaseMgmt.Instance.ShowLetters -= ShowLetter;
        ChaseMgmt.Instance.HideLetters -= HideLetter;
    }


    protected override void Start()
    {
        base.Start();

        _indexDoor = UnityEngine.Random.Range(1, NbrDoorsAvailable);

        ClosedDoorSprite.spriteName = String.Format(ClosedDoorSpriteName, _indexDoor.ToString("00"));
        OpenedDoorSprite.spriteName = String.Format(OpenedDoorSpriteName, _indexDoor.ToString("00"));

        if (UnityEngine.Random.value < 0.5f)
        {
            ClosedDoorSprite.flip = OpenedDoorSprite.flip = UIBasicSprite.Flip.Horizontally;
        }
    }

    public void Init(string label, bool isGoodCombination, bool isLabelHidden = false)
    {
        _label = DoorLabel.text = label;
        _isGoodCombination = isGoodCombination;

        if (isLabelHidden)
        {
            DoorLabel.alpha = 0.0f;
        }

        UIEventListener.Get(gameObject).onClick = HandleClickOnDoor;
    }

    public static string GetBackgroundSprite(UISprite sprite)
    {
        if (BackgroundSpriteNames == null)
        {
            BackgroundSpriteNames = new ShuffleBag<string>(new List<string>(sprite.atlas.GetListOfSprites().ToArray()));
        }

        return BackgroundSpriteNames.Next();
    }

    private void HideLetter()
    {
        _labelTweenAlpha.ResetToBeginning();
        _labelTweenAlpha.PlayReverse();
    }

    public void ShowLetter()
    {
        if (ChaseMgmt.Instance.FirstFloorStarted && ChaseMgmt.Instance.LevelMgmt.CurrentLevel == IndexFloor)
        {
            _labelTweenAlpha.ResetToBeginning();
            _labelTweenAlpha.PlayForward();
        }
    }

    private void HandleClickOnDoor(GameObject go)
    {
        ClosedDoorSprite.alpha = 0.0f;

        GameObject feedbackPrefab = _isGoodCombination ? FeedbackGoodPrefab : FeedbackBadPrefab;
        GameObject feedback = NGUITools.AddChild(gameObject, feedbackPrefab);
        feedback.transform.position = transform.position;
        feedback.GetComponent<TextFeedBack>().Init(_label);

        Destroy(feedback, feedback.GetComponent<TextFeedBack>().GDuration + 0.25f);

        if (_isGoodCombination)
        {
            BehindDoorSprite.spriteName = LittleGirlSpriteName;
        }
        else
        {
            BehindDoorSprite.spriteName = FoxSpriteName;
        }

        OpenedDoorSprite.alpha = 1.0f;
        BehindDoorSprite.alpha = 1.0f;

        ChaseMgmt.Instance.HandleClickOnDoor(_label);
    }

    public override void AddPonctualElement()
    {
        int MinX, MaxX, indexPrefab;
        indexPrefab = UnityEngine.Random.Range(0, PonctualElementPrefab.Length);

        GameObject ponctualElement
            = NGUITools.AddChild(gameObject, PonctualElementPrefab[indexPrefab]);
        UISprite ponctualElementSprite = ponctualElement.GetComponent<UISprite>();

        MinX = (int)((ClosedDoorSprite.width + ponctualElementSprite.width) / 2.0f);
        MaxX = (int)(BackgroundSprite.width / 2.0f);

        ponctualElement.transform.localPosition
             = PonctualElementPrefab[indexPrefab].transform.localPosition
             + UnityEngine.Random.Range(MinX, MaxX) * (UnityEngine.Random.value > 0.5f ? Vector3.right : Vector3.left);



    }
}
