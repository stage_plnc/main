﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyBehaviour : CellBehaviour
{
    public override void AddPonctualElement()
    {
       
    }

    protected override void Awake()
    {
        base.Awake();

        Type = ChaseCell.TypeCell.EMPTY;
    }
}
