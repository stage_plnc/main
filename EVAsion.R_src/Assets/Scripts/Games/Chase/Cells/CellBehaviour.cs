﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CellBehaviour : MonoBehaviour
{
    public enum ElementConstraint
    {
        NONE, 
        CAN, 
        MUST
    };

    public ChaseCell.TypeCell Type;


    [Header("Atlas infos")]
    public string RockBorderSpriteName = "RockBorder";
    public string NormalBorderSpriteName = "Bord";
    public string WoodBorderSpriteName = "WoodBorder";

    [Header("Prefab")]
    public GameObject[] PonctualElementPrefab;

    [Header("Sprites")]
    public UISprite BackgroundSprite;
    public UISprite LeftEdge;
    public UISprite RightEdge;
    public UISprite FlooringSprite;

    [Header("Index floor")]
    public int IndexFloor;

    public bool HasFlooring { get { return FlooringSprite.isActiveAndEnabled; } }

    public bool HasLeftWoodSupport { get { return LeftEdge.spriteName.StartsWith(WoodBorderSpriteName); } }
    public bool HasRightWoodSupport { get { return RightEdge.spriteName.StartsWith(WoodBorderSpriteName); } }
    public bool HasLeftRockBorder { get { return LeftEdge.spriteName.StartsWith(RockBorderSpriteName); } }
    public bool HasRightRockBorder { get { return RightEdge.spriteName.StartsWith(RockBorderSpriteName); } }

    protected virtual void Awake()
    {
        if (UnityEngine.Random.value < 0.5f)
        {
            BackgroundSprite.flip = UIBasicSprite.Flip.Horizontally;
        }
    }
 
    protected virtual void Start()
    {
        
        //LeftEdge.spriteName = GetRandomSprite(LeftEdge);
        //RightEdge.spriteName = GetRandomSprite(RightEdge);


    }

    public static string GetRandomSprite(UISprite sprite, string prefix = "")
    {
        BetterList<string> spritesList = sprite.atlas.GetListOfSprites();

        // Since we're deleting elements, we start from the last
        if (prefix != "")
        {
            for (int i = spritesList.size - 1; i >= 0; i--)
            {
                if (!spritesList[i].StartsWith(prefix))
                {
                    spritesList.RemoveAt(i);
                }
            }
        }

        return spritesList[UnityEngine.Random.Range(0, spritesList.size)];
    }

    public void SetFlooring()
    {
        FlooringSprite.enabled = true;
    }

    public void SetLeftWoodBorder()
    {
        LeftEdge.spriteName = GetRandomSprite(LeftEdge, WoodBorderSpriteName);
        LeftEdge.flip = UIBasicSprite.Flip.Horizontally;
    }

    public void SetRightWoodBorder()
    {
        RightEdge.spriteName = GetRandomSprite(LeftEdge, WoodBorderSpriteName);
    }

    public void SetLeftRockBorder(bool flip)
    {
        LeftEdge.spriteName = GetRandomSprite(LeftEdge, RockBorderSpriteName);

        if (flip)
        {
            LeftEdge.flip = UIBasicSprite.Flip.Horizontally;
        }
    }

    public void SetRightRockBorder(bool flip)
    {
        RightEdge.spriteName = GetRandomSprite(RightEdge, RockBorderSpriteName);

        if (flip)
        {
            RightEdge.flip = UIBasicSprite.Flip.Horizontally;
        }
    }

    public void SetLeftNormalBorder()
    {
        LeftEdge.spriteName = GetRandomSprite(LeftEdge, NormalBorderSpriteName);
    }

    public void SetRightNormalBorder()
    {
        RightEdge.spriteName = GetRandomSprite(RightEdge, NormalBorderSpriteName);
    }

    public abstract void AddPonctualElement();
}
