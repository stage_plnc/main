﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBehaviour : CellBehaviour
{
    public static ShuffleBag<string> BackgroundSpriteNames = null;

    protected override void Awake()
    {
        base.Awake();

        Type = ChaseCell.TypeCell.WALL;

        // BackgroundSprite.spriteName = GetBackgroundSprite(BackgroundSprite);
    }

    protected override void Start()
    {
        base.Start();
    }

    public static string GetBackgroundSprite(UISprite sprite)
    {
        if (BackgroundSpriteNames == null)
        {
            BackgroundSpriteNames = new ShuffleBag<string>(new List<string>(sprite.atlas.GetListOfSprites().ToArray()));
        }

        return BackgroundSpriteNames.Next();
    }

    public override void AddPonctualElement()
    {
        int MinX, MaxX, indexPrefab;
        indexPrefab = UnityEngine.Random.Range(0, PonctualElementPrefab.Length);

        GameObject ponctualElement = 
            NGUITools.AddChild(gameObject, PonctualElementPrefab[indexPrefab]);
        UISprite ponctualElementSprite = ponctualElement.GetComponent<UISprite>();

        MinX = 0;
        MaxX = (int) (BackgroundSprite.width / 2.0f);

        ponctualElement.transform.localPosition
             = PonctualElementPrefab[indexPrefab].transform.localPosition
             + UnityEngine.Random.Range(MinX, MaxX) * (UnityEngine.Random.value > 0.5f ? Vector3.right : Vector3.left);


    }
}
