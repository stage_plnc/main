﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowFoxDecoration : MonoBehaviour {

    public GameObject[] Foxes;

    [Range(0, 1)]
    public float Probability;

    private UISprite _spriteBackground;

    public bool MustFlip;
    private bool _flipChanged = false;

    public void Awake()
    {
        _spriteBackground = GetComponent<UISprite>();
    }

    private void Start()
    {
        if (Foxes.Length > 0)
        {
            if (UnityEngine.Random.value <= Probability)
            {
                GameObject selectedFox = Foxes[UnityEngine.Random.Range(0, Foxes.Length)];
                selectedFox.SetActive(true);

                selectedFox.GetComponent<TweenPosition>().delay = UnityEngine.Random.Range(0.0f, 4.0f);
            }
        }
    }

    public void Update()
    {
        if (Foxes.Length > 0)
        {
            if (!_flipChanged && _spriteBackground.flip == UIBasicSprite.Flip.Horizontally)
            {
                TweenPosition[] tweenPositions = GetComponentsInChildren<TweenPosition>();

                foreach (TweenPosition tweenPosition in tweenPositions)
                {
                    tweenPosition.from = new Vector3(-tweenPosition.from.x, tweenPosition.from.y, 0.0f);
                    tweenPosition.to = new Vector3(-tweenPosition.to.x, tweenPosition.to.y, 0.0f);
                }

                _flipChanged = true;
            }
        }

    }
}
