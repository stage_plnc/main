﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChaseLevelMgmt : MonoBehaviour
{
    [Header("Floor")]
    public GameObject FloorPrefab;
    public int FloorHeigth = 1600;

    [Header("Tween position")]
    public TweenPosition FloorsTweenPosition;

    private GameObject[][] _cells;

    private GameObject[] _floors;
    private GameObject[] _floorsContent;
    private CellBehaviour[][][] _neighbourhoods;
    private List<BaseDecoration> _listDecorationActions;

    // This is a copy of the last floor of a game cycle
    private GameObject _previousTransitionFloor = null;
    private GameObject _nextTransitionFloor = null;

    private int _floorsGenerated = 0;
    private int _currentLevel = 0;

    public int CurrentLevel { get { return _currentLevel; } }

    private void Awake()
    {
        _listDecorationActions = new List<BaseDecoration>
        {
            new FlooringDecoration(),
            new WoodSupportDecoration(),
            new RockBorderDecoration(),
            new NormalBorderDecoration(), 
            new FlipBackground(), 
            new PonctualElementsDecoration()
        };

        _cells = new GameObject[Constants.Chase.NbrVisibleFloors][];
        _neighbourhoods = new CellBehaviour[Constants.Chase.NbrVisibleFloors][][];

        for (int i = 0; i < _cells.Length; i++)
        {
            _cells[i] = new GameObject[Constants.Chase.NbrColumns];
            _neighbourhoods[i] = new CellBehaviour[Constants.Chase.NbrColumns][];
        }
    }

    public void GenerateLevel(ChaseCell[][] currentMap)
    {
        GameObject currentFloor = _floorsContent[_floorsGenerated];
        UIGrid grid = currentFloor.GetComponent<UIGrid>();

        // So the cells are created here according to their type
        for (int i = 0; i < currentMap.Length; i++)
        {
            for (int j = 0; j < currentMap[i].Length; j++)
            {
                _cells[i][j] = currentMap[i][j].GenerateGO(currentFloor);
                _cells[i][j].GetComponent<CellBehaviour>().IndexFloor = _floorsGenerated;
            }
        }

        grid.Reposition();

        for (int i = 0; i < currentMap.Length; i++)
        {
            for (int j = 0; j < currentMap[i].Length; j++)
            {
                _neighbourhoods[i][j] = GenerateNeighbourhood(i, j, currentMap);
            }
        }

        foreach (BaseDecoration decoration in _listDecorationActions)
        {
            for (int i = 0; i < currentMap.Length; i++)
            {
                for (int j = 0; j < currentMap[i].Length; j++)
                {
                    decoration.Decorate(_cells[i][j].GetComponent<CellBehaviour>(), i, j, _neighbourhoods[i][j]);
                }
            }
        }

        _floorsGenerated++;
    }

    public void ShowFirstFloorLabels()
    {
        foreach (DoorBehaviour doorBehaviour in _floorsContent[0].GetComponentsInChildren<DoorBehaviour>())
        {
            doorBehaviour.ShowLetter();
        } 
    }

    public void ShowCurrentFloorDoorsLabel()
    {
        foreach (DoorBehaviour doorBehaviour in _floorsContent[_currentLevel].GetComponentsInChildren<DoorBehaviour>())
        {
            doorBehaviour.ShowLetter();
        }
    }

    public void PrepareFloors(int nbrFloorByGameCycle)
    {
        _floors = new GameObject[nbrFloorByGameCycle];
        _floorsContent = new GameObject[nbrFloorByGameCycle];

        if (!ChaseMgmt.Instance.FirstCycle && !ChaseMgmt.Instance.SecondCycle)
        {
            Destroy(_previousTransitionFloor);
        }

        _previousTransitionFloor = _nextTransitionFloor;

        for (int i = 0; i < nbrFloorByGameCycle; i++)
        {
            _floors[i] = NGUITools.AddChild(gameObject, FloorPrefab);
            _floors[i].name = "Floor" + i;

            // That means this is the first cycle
            if (ChaseMgmt.Instance.FirstCycle)
            {
                _floors[i].transform.localPosition = Vector3.up * FloorHeigth * i;
            }
            else
            {
                _floors[i].transform.localPosition = _previousTransitionFloor.transform.localPosition + Vector3.up * FloorHeigth * (i + 1);
            }

            _floorsContent[i] = _floors[i].transform.GetChild(0).gameObject;

            // Is this the last floor ?
            if (i == nbrFloorByGameCycle - 1)
            {
                _nextTransitionFloor = _floors[i];
            }
        }

        _floorsGenerated = _currentLevel = 0;

        
    }

    public void ShiftToNextLevelCycle()
    {
        EventDelegate.Remove(FloorsTweenPosition.onFinished, ChaseMgmt.Instance.SetBlockCollisionInactive);

        FloorsTweenPosition.from = transform.localPosition;
        FloorsTweenPosition.to = FloorsTweenPosition.from + Vector3.down * FloorHeigth;

        FloorsTweenPosition.PlayForward();

        FloorsTweenPosition.ResetToBeginning();

        _currentLevel = 0;
    }

    public void ShiftLevels()
    {
        if (_currentLevel == 0)
        {
            EventDelegate.Add(FloorsTweenPosition.onFinished, ChaseMgmt.Instance.SetBlockCollisionInactive);
            EventDelegate.Add(FloorsTweenPosition.onFinished, ShowCurrentFloorDoorsLabel);
        }

        FloorsTweenPosition.from = transform.localPosition;
        FloorsTweenPosition.to = FloorsTweenPosition.from + Vector3.down * FloorHeigth;

        FloorsTweenPosition.PlayForward();

        FloorsTweenPosition.ResetToBeginning();

        _currentLevel++;
    }


    // The neighbourhood is defined this way : it is an 8-length array
    // 0 1 2
    // 7   3
    // 6 5 4

    private CellBehaviour[] GenerateNeighbourhood(int row, int col, ChaseCell[][] currentMap)
    {
        CellBehaviour[] neighbourhood = new CellBehaviour[8];

        for (int i = 0; i < neighbourhood.Length; i++)
        {
            neighbourhood[i] = null;
        }

        // NW
        if (row - 1 >= 0 && col - 1 >= 0)
        {
            neighbourhood[0] = _cells[row - 1][col - 1].GetComponent<CellBehaviour>();
        }
        // N
        if (row - 1 >= 0)
        {
            neighbourhood[1] = _cells[row - 1][col].GetComponent<CellBehaviour>();
        }

        // NE
        if (row - 1 >= 0 && col + 1 < Constants.Chase.NbrColumns)
        {
            neighbourhood[2] = _cells[row - 1][col + 1].GetComponent<CellBehaviour>();
        }

        // E
        if (col + 1 < Constants.Chase.NbrColumns)
        {
            neighbourhood[3] = _cells[row][col + 1].GetComponent<CellBehaviour>();
        }

        // SE
        if (col + 1 < Constants.Chase.NbrColumns && row + 1 < Constants.Chase.NbrVisibleFloors)
        {
            neighbourhood[4] = _cells[row + 1][col + 1].GetComponent<CellBehaviour>();
        }

        // S
        if (row + 1 < Constants.Chase.NbrVisibleFloors)
        {
            neighbourhood[5] = _cells[row + 1][col].GetComponent<CellBehaviour>();
        }

        // SW 
        if (row + 1 < Constants.Chase.NbrVisibleFloors && col - 1 >= 0)
        {
            neighbourhood[6] = _cells[row + 1][col - 1].GetComponent<CellBehaviour>();
        }

        // W
        if (col - 1 >= 0)
        {
            neighbourhood[7] = _cells[row][col - 1].GetComponent<CellBehaviour>();
        }

        return neighbourhood;
    }

    public void ClearFloors()
    {
        for (int i = 0; i < _floors.Length - 1; i++)
        {
            //while (_floorsContent[i].transform.childCount > 0)
            //{
            //    NGUITools.Destroy(_floorsContent[i].transform.GetChild(0).gameObject);
            //}

            NGUITools.Destroy(_floors[i]);
        }
    }

    private void DestroyFloor(GameObject floor)
    {
        NGUITools.Destroy(floor);
    }
}
