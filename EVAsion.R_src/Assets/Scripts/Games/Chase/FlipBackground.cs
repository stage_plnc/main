﻿using System;
using UnityEngine;

internal class FlipBackground : BaseDecoration
{
    public override void Decorate(CellBehaviour cellBehaviour, int row, int col, CellBehaviour[] neighbourhood)
    {
        if (cellBehaviour.Type != ChaseCell.TypeCell.EMPTY)
        {
            CellBehaviour leftCell = neighbourhood[(int)CardinalPoint.W];

            if (leftCell != null)
            {
                if (cellBehaviour.BackgroundSprite.spriteName == leftCell.BackgroundSprite.spriteName
                 && cellBehaviour.BackgroundSprite.flip == leftCell.BackgroundSprite.flip)
                {
                    if (leftCell.BackgroundSprite.flip == UIBasicSprite.Flip.Horizontally)
                    {
                        cellBehaviour.BackgroundSprite.flip = UIBasicSprite.Flip.Nothing;
                    }
                    else
                    {
                        cellBehaviour.BackgroundSprite.flip = UIBasicSprite.Flip.Horizontally;
                    }

                    cellBehaviour.BackgroundSprite.MarkAsChanged();
                }
            }
        }
    }
}