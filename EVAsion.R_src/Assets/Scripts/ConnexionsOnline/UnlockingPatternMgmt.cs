﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnlockingPatternMgmt : MonoBehaviour
{
    [Header("Mgmt")]
    //public ChoosePictogramMgmt ChoosePictogramMgmtI;
    public ChoosePictogramOfflineMgmt ChoosePictogramMgmtI;

    [Header("Camera")]
    public Camera MainCamera;

    [Header("Sprite")]
    public UISprite GroupSprite;
    public UISprite GroupSpriteBackground;
    public UISprite LearnerSprite;
    public UISprite LearnerBackground;

    [Header("Points")]
    public GameObject DigitsParent;
    public GameObject[] Digits;
    public int PointRadius;

    [Header("Parents")]
    public GameObject Pattern;

    [Header("Line")]
    public GameObject LinePrefab;
    public GameObject CurrentLine;

    [Header("Status")]
    public bool IsVisible = false;

    private List<GameObject> _drawnLines;
    private List<GameObject> _selectedDigits;

    // The key is a string "[num]-[num]" to an int, which is the digit
    private Dictionary<string, int> _specialLinks;

    private GameObject _lastDigitSelected = null;
    private Vector2 _lastDigitSelectedPos;
    private TweenAlpha _tweenAlpha;

    private int[] _patternToMatch;

    private void Awake()
    {
        _drawnLines = new List<GameObject>();
        _selectedDigits = new List<GameObject>();

        foreach (GameObject digit in Digits)
        {
            UIEventListener.Get(digit).onDragStart = StartDrag;
            UIEventListener.Get(digit).onDrag = DoDrag;
            UIEventListener.Get(digit).onDragEnd = EndDrag;
        }

        _specialLinks = new Dictionary<string, int>();

        _specialLinks["1-3"] = 2;
        _specialLinks["1-7"] = 4;
        _specialLinks["1-9"] = 5;
        _specialLinks["2-8"] = 5;
        _specialLinks["3-7"] = 5;
        _specialLinks["3-9"] = 6;
        _specialLinks["4-6"] = 5;
        _specialLinks["7-9"] = 8;

        _tweenAlpha = GetComponent<TweenAlpha>();
    }

    private void StartDrag(GameObject digit)
    {
        // Useful if we're looking at a wrong pattern   
        CancelInvoke();
        ResetPattern();

        _lastDigitSelected = digit;
        _lastDigitSelectedPos = transform.InverseTransformPoint(digit.transform.position);

        digit.GetComponent<DigitPattern>().SetSelected();
        _selectedDigits.Add(digit);

        CurrentLine.transform.localPosition = _lastDigitSelectedPos;
    }

    public void DoDrag(GameObject digit, Vector2 delta)
    {
        if (IsVisible)
        {
            Vector2 mousePosition = transform.InverseTransformPoint(MainCamera.ScreenToWorldPoint(Input.mousePosition));

            if (_lastDigitSelectedPos != mousePosition)
            {
                float oppose = mousePosition.y - _lastDigitSelectedPos.y;
                float adj = mousePosition.x - _lastDigitSelectedPos.x;
                UISprite sprite = CurrentLine.GetComponent<UISprite>();

                // Draw line according to previous point
                float angle = Mathf.Atan(oppose / adj) * Mathf.Rad2Deg;

                if (adj < 0.0f)
                {
                    angle += 180.0f;
                }

                CurrentLine.transform.eulerAngles = new Vector3(0.0f, 0.0f, angle);

                sprite.width = (int)Mathf.Sqrt((float)(oppose * oppose + adj * adj));

                CheckIfOnOtherDigit(mousePosition);
            }
        }
    }

    private void EndDrag(GameObject digit)
    {
        if (CheckPattern() == true)
        {
            ChoosePictogramMgmtI.HasCompletePattern();
        }
        else
        {
            Debug.LogWarning("Wrong match...");
            
            SetWrongPattern();

            Invoke("ResetPattern", 1.0f);
        }

        ResetCurrentLine();
    }

    public void Unlock(string pattern)
    {
        _patternToMatch = new int[pattern.Length];

        for (int i = 0; i < _patternToMatch.Length; i++)
        {
            _patternToMatch[i] = (int)Char.GetNumericValue(pattern[i]);
        }

        DigitsParent.SetActive(true);
        _tweenAlpha.ResetToBeginning();
        _tweenAlpha.onFinished.Clear();

        _tweenAlpha.PlayForward();

        IsVisible = true;

        Debug.LogError("The pattern is " + pattern);
    }

    public void Lock()
    {
        _tweenAlpha.SetOnFinished(delegate () { DigitsParent.SetActive(false); });
        _tweenAlpha.PlayReverse();
        
        IsVisible = false;
    }

    public void CheckIfOnOtherDigit(Vector2 mousePosition)
    {
        foreach (GameObject digit in Digits)
        {
            if (_selectedDigits.FindIndex(x => x.GetInstanceID() == digit.GetInstanceID()) == -1)
            {
                Vector2 pointPosition = transform.InverseTransformPoint(digit.transform.position);

                if (mousePosition.x > pointPosition.x - PointRadius && mousePosition.x < pointPosition.x + PointRadius
                 && mousePosition.y > pointPosition.y - PointRadius && mousePosition.y < pointPosition.y + PointRadius)
                {
                    HandleNewLink(digit);

                    break;
                }
            }
        }
    }

    private void HandleNewLink(GameObject newPoint)
    {
        string link1 = String.Format("{0}-{1}", _lastDigitSelected.name, newPoint.name);
        string link2 = String.Format("{0}-{1}", newPoint.name, _lastDigitSelected.name);
        int possibleMiddlePoint = -1;

        if (_specialLinks.ContainsKey(link1))
        {
            possibleMiddlePoint = _specialLinks[link1];
        }

        if (_specialLinks.ContainsKey(link2))
        {
            possibleMiddlePoint = _specialLinks[link2];
        }

        if (possibleMiddlePoint != -1)
        {
            CreateNewLine(Digits[possibleMiddlePoint - 1]);
            CreateNewLine(newPoint);
        }
        else
        {
            CreateNewLine(newPoint);
        }
    }

    public void CreateNewLine(GameObject digit)
    {
        Vector2 digitPosition = transform.InverseTransformPoint(digit.transform.position);

        DrawLine(_lastDigitSelectedPos, digitPosition);

        ResetCurrentLine();

        _lastDigitSelected = digit;
        _lastDigitSelectedPos = digitPosition;
        CurrentLine.transform.localPosition = _lastDigitSelectedPos;

        digit.GetComponent<DigitPattern>().SetSelected();

        _selectedDigits.Add(digit);
    }

    private void ResetCurrentLine()
    {
        CurrentLine.transform.eulerAngles = Vector3.zero;

        CurrentLine.GetComponent<UISprite>().width = 2;

        // Out of screen
        CurrentLine.transform.localPosition = new Vector3(0.0f, 2048.0f, 0.0f);
    }



    private void SetWrongPattern()
    {
        foreach (GameObject digit in _selectedDigits)
        {
            digit.GetComponent<DigitPattern>().SetWrong();
        }
    }

    private bool CheckPattern()
    {
        bool areMatching = true;

        if (_selectedDigits.Count == _patternToMatch.Length)
        {
            for (int i = 0; i < _selectedDigits.Count; i++)
            {
                if (int.Parse(_selectedDigits[i].name) != _patternToMatch[i])
                {
                    areMatching = false;
                    break;
                }
            }
        }
        else
        {
            areMatching = false;
        }

        return areMatching;
    }

    private void ResetPattern()
    {
        foreach (GameObject line in _drawnLines)
        {
            Destroy(line);
        }
        _drawnLines.Clear();

        foreach (GameObject digit in _selectedDigits)
        {
            digit.GetComponent<DigitPattern>().Reset();
        }
        _selectedDigits.Clear();


        ResetCurrentLine();
    }

    public void DrawLine(Vector2 beg, Vector2 end)
    {
        GameObject line = NGUITools.AddChild(Pattern, LinePrefab);
        UISprite sprite = line.GetComponent<UISprite>();

        line.transform.localPosition = beg;

        float oppose = end.y - beg.y;
        float adj = end.x - beg.x;

        // Draw line according to previous point
        float angle = Mathf.Atan(oppose / adj) * Mathf.Rad2Deg;

        if (adj < 0.0f)
        {
            angle += 180.0f;
        }

        line.transform.eulerAngles = new Vector3(0.0f, 0.0f, angle);

        sprite.width = (int)Mathf.Sqrt((float)(oppose * oppose + adj * adj));
        _drawnLines.Add(line);
    }
}
