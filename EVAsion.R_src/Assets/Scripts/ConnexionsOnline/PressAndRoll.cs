﻿using UnityEngine;
using System.Collections;
using System;

/*
**Script created by M3n4d3s
*/

public class PressAndRoll : MonoBehaviour
{
    public GameObject Base;
    public Camera cameraS;
    public WheelOffline Wheel;

    [Range(0, 1)]
    public float AngleThreshold;
    float startAngle;

    private bool _doSnap = true;

    public void Start()
    {
        UIEventListener.Get(Base).onDrag += BasePressed;
        UIEventListener.Get(Base).onDragEnd += SnapPictos;
    }

    private void SnapPictos(GameObject go)
    {
        if (_doSnap)
        {
            //Wheel.SnapPictos();
        }
    }

    public void OnDragStart()
    {
        Vector3 worldPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        worldPos = cameraS.ScreenToWorldPoint(worldPos);

        Vector3 dir = worldPos - transform.localPosition;
        startAngle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        _doSnap = true;
    }

    public void BasePressed(GameObject go, Vector2 delta)
    {
        Vector3 worldPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        worldPos = cameraS.ScreenToWorldPoint(worldPos);

        Vector3 dir = worldPos - transform.localPosition;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        float diff = (angle - startAngle);
        if (diff > 180.0f)
            diff -= 360.0f;

        bool hasMoved = Wheel.UpdateAngles(diff * AngleThreshold);
        _doSnap = hasMoved;

        startAngle = angle;
    }

}

