﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetStudentList : MonoBehaviour {

    [Header("API")]
    [Header("Auth")]
    public string AuthUrl;
    public string IdEvasion;

    [Header("Teacher Url")]
    public string TeacherUrl;

    [Header("Students Url")]
    public string GetStudentsUrl;
}
