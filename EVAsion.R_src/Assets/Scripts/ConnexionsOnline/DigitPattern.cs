﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigitPattern : MonoBehaviour
{
    public UISprite BigCircleSprite;

    public Color InitialColor;
    public Color WrongColor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetSelected()
    {
        BigCircleSprite.gameObject.SetActive(true);
    }

    public void SetWrong()
    {
        BigCircleSprite.color = WrongColor;
    }

    public void Reset()
    {
        BigCircleSprite.color = InitialColor;
        BigCircleSprite.gameObject.SetActive(false);
    }
}
