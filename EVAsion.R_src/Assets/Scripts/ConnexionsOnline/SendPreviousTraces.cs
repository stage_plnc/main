﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SendPreviousTraces : MonoBehaviour
{
    public string SendUrl = "log-event";

    public void Send()
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath);

        foreach (FileInfo fileInfo in directoryInfo.GetFiles())
        {
            if (fileInfo.Name.StartsWith("traces"))
            {
                Debug.Log(fileInfo.FullName);
                string contentJson = File.ReadAllText(fileInfo.FullName);

                StartCoroutine(PostTraces(contentJson, fileInfo));
            }
        }

    }

    IEnumerator PostTraces(string contentJson, FileInfo tracesFile)
    {
        string accessToken = PlayerPrefs.GetString("access_token");
        string url = string.Format("{0}{1}", Constants.BaseURLApi, SendUrl);

        Dictionary<string, string> headers = new Dictionary<string, string>();

        Debug.Log(url + "; I got the token => " + accessToken);

        headers.Add("Content-Type", "application/json");
        headers.Add("Authorization", "bearer " + accessToken);

        byte[] pData = System.Text.Encoding.UTF8.GetBytes(contentJson.ToCharArray());

        WWW www = new WWW(url, pData, headers);

        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Debug.LogError("Traces sent ! => " + www.text);
            tracesFile.Delete();
        }
        else
        {
            Debug.LogError("Error : " + www.error + " => " + www.text);
            
        }
    }

}
