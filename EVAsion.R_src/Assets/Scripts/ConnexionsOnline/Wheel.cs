﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Wheel : MonoBehaviour
{
    [Header("Mgmt")]
    public ChoosePictogramMgmt ChoosePictogramMgmtI;

    [Header("Values")]
    public int NbrInitialPictos;

    public float AngleAreaSelectedPicto;
    public float AngleAreaHidden;

    public float MaxAngleRotation;
    public int NbrMaxVisiblePictos;
    public int NbrPictos;

    [Header("Pictogram")]
    public Transform PictogramParent;
    public GameObject PictogramPrefab;

    [Header("Status")]
    public bool IsLocked;

    [Header("Pictograms")]
    public UIAtlas AtlasPictos;
    public int NbrPictosPossible;

    [HideInInspector] public JSONObject SelectedJson = null;
    public Pictogram SelectedPictogram = null;

    private List<Pictogram> _visiblePictos;
    private List<JSONObject> _jsonObjects;
    private Pictogram _firstOne;
    private Pictogram _lastOne;

    private float _gapBetweenPictos;
    private int _nextFirstOneIdx;
    private int _nextLastOneIdx;

    private bool _learnerWheel;

    private SphereCollider _sphereCollider;

    private Dictionary<GameObject, JSONObject> _jsonByGameObject;

    void Awake()
    {
        _jsonByGameObject = new Dictionary<GameObject, JSONObject>();

        _visiblePictos = new List<Pictogram>();

        _sphereCollider = GetComponent<SphereCollider>();
    }

    public void Clean()
    {
        foreach (GameObject picto in _jsonByGameObject.Keys)
        {
            Destroy(picto);
        }

        _jsonByGameObject.Clear();
        _visiblePictos.Clear();
    }

    public void FillPictograms(List<JSONObject> jsonObjects, bool learner = false)
    {
        bool mustBeLocked = false;

        _jsonObjects = jsonObjects;
        NbrPictos = _jsonObjects.Count;

        _nextFirstOneIdx = NbrPictos - 1;
        _nextLastOneIdx = NbrInitialPictos;

        _gapBetweenPictos = Mathf.Abs(AngleAreaHidden - AngleAreaSelectedPicto) / NbrInitialPictos;

        if (NbrInitialPictos > _jsonObjects.Count)
        {
            NbrInitialPictos = _jsonObjects.Count;
            mustBeLocked = true;
        }

        _learnerWheel = learner;

        for (int i = 0; i < NbrInitialPictos; i++)
        {
            GameObject pictogramGo = NGUITools.AddChild(PictogramParent.gameObject, PictogramPrefab);
            Pictogram pictogram = pictogramGo.GetComponent<Pictogram>();

            int indexSprite = 0;
            if (jsonObjects[i].GetField("avatar").IsNull == false)
            {
                indexSprite = int.Parse(jsonObjects[i].GetField("avatar").str) - 1;

                if (indexSprite == -1)
                {
                    indexSprite = 0;
                    Debug.LogError("WTF the avatars ???");
                }
            }

            float angle = AngleAreaSelectedPicto + i * _gapBetweenPictos;

            pictogram.Init(angle, AtlasPictos, indexSprite % NbrPictosPossible, (int)(indexSprite / (float)NbrPictosPossible), _learnerWheel);
            _visiblePictos.Add(pictogram);

            _jsonByGameObject[pictogramGo] = jsonObjects[i];

            UIEventListener.Get(pictogramGo).onClick = ClickOnPictogram;
        }

        _firstOne = _visiblePictos[0];
        _lastOne = _visiblePictos[_visiblePictos.Count - 1];

        if (mustBeLocked)
        {
            Lock();
        }
    }
    
    private void ClickOnPictogram(GameObject go)
    {
        SelectedJson = _jsonByGameObject[go];
        SelectedPictogram = go.GetComponent<Pictogram>();

        ChoosePictogramMgmtI.PictogramHasBeenClicked(SelectedJson);
    }

    public bool UpdateAngles(float angle)
    {
        bool hasMoved = false;
        bool canRotate = true;

        angle = Mathf.Clamp(angle, -MaxAngleRotation, MaxAngleRotation);

        if (angle > 0 && _visiblePictos.Count == 1)
        {
            canRotate = false;
        }
        if (angle > 0 && _visiblePictos.Count < NbrMaxVisiblePictos)
        {
            canRotate = false;
        }

        if (_visiblePictos.Count == _jsonObjects.Count && AllPictosAreScaled())
        {
            canRotate = false;
        }
        // angle > 0 = counter clockwise

        // We dont go here if :
        // - We have one picto and we go to the right
        // - We have NbrPictos pictos
        if (canRotate)
        {
            for (int i = 0; i < _visiblePictos.Count; i++)
            {
                _visiblePictos[i].Angle += angle;
                //_pictos[i].Angle = Mathf.Round(_pictos[i].Angle);

                if (_visiblePictos[i].Angle < 0.0f)
                    _visiblePictos[i].Angle = 360.0f;
                else if (_visiblePictos[i].Angle >= 360.0f)
                    _visiblePictos[i].Angle = 0.0f;
            }

            hasMoved = true;
        }

        return hasMoved;
    }

    private bool AllPictosAreScaled()
    {
        bool allScaled = true;

        foreach (Pictogram picto in _visiblePictos)
        {
            // Little bias
            if (picto.Scale < 0.95f)
            {
                allScaled = false;
                break;
            }
        }

        return allScaled;
    }

    public void Unlock()
    {
        _sphereCollider.enabled = true;
        IsLocked = false;
    }

    public void Lock()
    {
        _sphereCollider.enabled = false;

        IsLocked = true;
    }

    private void CheckIfNewPictogram()
    {
        // That means we're rotating COUNTER CLOCKWISE
        // We're checking if the first picto is near the right side of the button. We don't check until we have destroyed one in the right
        // (Meaning we have reached this position)

        // Mouvement de doigt de la droite vers la gauche
        if (_firstOne != null && _firstOne.Angle > AngleAreaHidden + _gapBetweenPictos && _visiblePictos.Count >= NbrMaxVisiblePictos)
        {
            // Its time to create a new one
            GameObject pictogramGo = NGUITools.AddChild(PictogramParent.gameObject, PictogramPrefab);
            Pictogram pictogram = pictogramGo.GetComponent<Pictogram>();

            _jsonByGameObject[pictogramGo] = _jsonObjects[_nextFirstOneIdx];

            pictogram.Init(AngleAreaHidden + 3.5f, AtlasPictos,
                _nextFirstOneIdx % NbrPictosPossible, (int)(_nextFirstOneIdx / (float)NbrPictosPossible), _learnerWheel);

            _visiblePictos.Add(pictogram);

            _firstOne = pictogram;

            _nextFirstOneIdx--;

            if (_nextFirstOneIdx < 0)
                _nextFirstOneIdx = NbrPictos - 1;

            UIEventListener.Get(pictogramGo).onClick = ClickOnPictogram;

        }

        // That means we're rotating CLOCKWISE
        // We're checking if the last pico (left) has reached an amount of distance to the button

        if (_lastOne && _lastOne.Angle < AngleAreaHidden - _gapBetweenPictos)
        {
            if (_visiblePictos.Count < _jsonObjects.Count)
            {
                GameObject pictogramGo = NGUITools.AddChild(PictogramParent.gameObject, PictogramPrefab);
                Pictogram pictogram = pictogramGo.GetComponent<Pictogram>();

                pictogram.Init(AngleAreaHidden - 3.5f, AtlasPictos,
                    _nextLastOneIdx % NbrPictosPossible, (int)(_nextLastOneIdx / (float)NbrPictosPossible), _learnerWheel);
                _visiblePictos.Add(pictogram);

                _jsonByGameObject[pictogramGo] = _jsonObjects[_nextLastOneIdx];

                _lastOne = pictogram;

                _nextLastOneIdx++;

                if (_nextLastOneIdx >= NbrPictos)
                    _nextLastOneIdx = 0;

                UIEventListener.Get(pictogramGo).onClick = ClickOnPictogram;
            }
        }
    }

    private Pictogram FindLastOne()
    {
        Pictogram lastOne = null;
        float maxAngle = float.MinValue;

        foreach (Pictogram picto in _visiblePictos)
        {
            if (picto.Angle < AngleAreaHidden && picto.Angle > maxAngle)
            {
                maxAngle = picto.Angle;
                lastOne = picto;
            }
        }

        return lastOne;
    }

    public void SnapPictos()
    {
        if (_visiblePictos.Count == 10)
        {
            _visiblePictos.Sort(delegate (Pictogram x, Pictogram y)
            {
                return x.Angle.CompareTo(y.Angle);
            });

            for (int i = 0; i < _visiblePictos.Count; i++)
            {
                _visiblePictos[i].SnapTo(18.0f + i * _gapBetweenPictos);
            }
        }
        else
        {
            // Store picto angle, position => Pictogram.

            // Create a new list of Pictogram, based of the position
            // If there is position 10, it goes to 9...
            List<KeyValuePair<Pictogram, int>> posByPictos = new List<KeyValuePair<Pictogram, int>>();
            List<int> positions = new List<int>();

            foreach (Pictogram picto in _visiblePictos)
            {
                // Really proud of this trick haha ! If we have positionF .5, we decide that the position of the thing is the next one
                float positionF = (picto.Angle - 18.0f) / _gapBetweenPictos;
                int position = (int)Mathf.Round(positionF);

                if (positionF - position == 0.5f)
                    position++;

                // ERK, but effective
                if (position == 10)
                    position = 9;


                picto.SnapTo(18.0f + position * _gapBetweenPictos);

                posByPictos.Add(new KeyValuePair<Pictogram, int>(picto, position));
                positions.Add(position);
            }

            // Sort posByPictos by positions
            if (positions.Count != positions.Distinct().Count())
            {
                // Duplicates exist
                posByPictos.Sort(delegate (KeyValuePair<Pictogram, int> x, KeyValuePair<Pictogram, int> y)
                {
                    return x.Key.Angle.CompareTo(y.Key.Angle);
                });

                for (int i = 0; i < posByPictos.Count; i++)
                {

                }
            }
        }
    }

    private Pictogram FindFirstOne()
    {
        Pictogram firstOne = _visiblePictos[0];
        float minAngle = firstOne.Angle;

        for (int i = 1; i < _visiblePictos.Count; i++)
        {
            //if (picto.Angle < minAngle)
            //{
            //    minAngle = picto.Angle;
            //    firstOne = picto;
            //}

            if (_visiblePictos[i].Angle > AngleAreaHidden)
            {
                if (minAngle < AngleAreaHidden || _visiblePictos[i].Angle < minAngle)
                {
                    minAngle = _visiblePictos[i].Angle;
                    firstOne = _visiblePictos[i];
                }
            }
            else
            {
                if (minAngle < AngleAreaHidden && _visiblePictos[i].Angle < minAngle)
                {
                    minAngle = _visiblePictos[i].Angle;
                    firstOne = _visiblePictos[i];
                }
            }
        }

        return firstOne;
    }

    public void Update()
    {
        if (_visiblePictos.Count > 0)
        {
            for (int i = _visiblePictos.Count - 1; i >= 0; i--)
            {
                // That means the pictogram is located in the area that's supposed to scale the button; or even remove it
                if (_visiblePictos[i].Angle >= AngleAreaHidden - _gapBetweenPictos && _visiblePictos[i].Angle <= AngleAreaHidden + _gapBetweenPictos)
                {
                    // This is the RIGHT of the button
                    if (_visiblePictos[i].Angle > AngleAreaHidden && _visiblePictos[i].Angle <= AngleAreaHidden + _gapBetweenPictos)
                    {
                        _visiblePictos[i].SetScale((_visiblePictos[i].Angle - AngleAreaHidden) / _gapBetweenPictos);
                    }

                    // This is the LEFT of the button
                    else if (_visiblePictos[i].Angle < AngleAreaHidden && _visiblePictos[i].Angle >= AngleAreaHidden - _gapBetweenPictos)
                    {
                        _visiblePictos[i].SetScale((AngleAreaHidden - _visiblePictos[i].Angle) / _gapBetweenPictos);
                    }
                }
                else
                {
                    _visiblePictos[i].SetScale(1.0f);
                }
            }

            // We're checking if we must destroy the picto
            if (_firstOne && _firstOne.Angle >= AngleAreaHidden - 3.0f + Mathf.Epsilon
             && _firstOne.Angle <= AngleAreaHidden + 3.0f - Mathf.Epsilon)
            {
                _nextFirstOneIdx++;

                if (_nextFirstOneIdx >= NbrPictos)
                    _nextFirstOneIdx = 0;

                Destroy(_firstOne.gameObject);
                _visiblePictos.Remove(_firstOne);

                _firstOne = FindFirstOne();

                Debug.LogError("Destroying first one ");
            }

            if (_lastOne && _lastOne.Angle >= AngleAreaHidden - 3.0f + Mathf.Epsilon
             && _lastOne.Angle <= AngleAreaHidden + 3.0f - Mathf.Epsilon)
            {
                _nextLastOneIdx--;

                if (_nextLastOneIdx < 0)
                    _nextLastOneIdx = NbrPictos - 1;

                Destroy(_lastOne.gameObject);
                _visiblePictos.Remove(_lastOne);

                _lastOne = FindLastOne();

                Debug.LogError("Destroying last one");
            }

            CheckIfNewPictogram();
        }
    }
}
