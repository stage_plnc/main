﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class DataSender : MonoBehaviour
{
    public TextAsset TempTextAsset;

    public string SendUrl = "log-event";

    private static DataSender _instance;
    public static DataSender Instance
    {
        get { return _instance; }
    }

    private JSONObject _global;

    private JSONObject _jsonParameters;

    private JSONObject _exercices;

    private JSONObject _currentExercice;
    private JSONObject _events;
    private JSONObject _actions;

    public int GetTimestamp()
    {
        DateTime time = DateTime.UtcNow;
        DateTime date1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        int timespamp = (int)(time.Subtract(date1970).TotalSeconds);

        return timespamp;

    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            Init();
        }
        else
        {
            Debug.LogWarning("[DataSender] Instance already exists");
        }
    }

    void Init()
    {
        _global = new JSONObject(JSONObject.Type.OBJECT);
        _jsonParameters = new JSONObject(JSONObject.Type.OBJECT);
        _exercices = new JSONObject(JSONObject.Type.ARRAY);

        int fluenceId = 25005;

        if (PlayerPrefs.HasKey("learnerId"))
        {
            fluenceId = PlayerPrefs.GetInt("learnerId");
        }

        _global.AddField("fluenceId", fluenceId);
        _global.AddField("fluenceType", 1);
        _global.AddField("genericConfigId", 50);
        _global.AddField("hasMedia", false);

        _global.AddField("jsonParameters", _jsonParameters);

        _jsonParameters.AddField("sessionBegin", GetTimestamp());
        _jsonParameters.AddField("exercices", _exercices);

        if (Constants.SendTraces)
        {
            SendPreviousTraces();
        }
    }

    public void SendPreviousTraces()
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.persistentDataPath);

        foreach (FileInfo fileInfo in directoryInfo.GetFiles())
        {
            if (fileInfo.Name.StartsWith("traces"))
            {
                string contentJson = File.ReadAllText(fileInfo.FullName);

                StartCoroutine(PostTraces(contentJson, fileInfo));
            }
        }

    }

    public void SetLearnerId(int id)
    {
        _global.SetField("fluenceId", id);
    }

    public void CreateNewExercice(int gamePlayed)
    {

        _currentExercice = new JSONObject(JSONObject.Type.OBJECT);
        _events = new JSONObject(JSONObject.Type.ARRAY);
        _actions = new JSONObject(JSONObject.Type.ARRAY);

        _currentExercice.AddField("exerciceBegin", GetTimestamp());
        _currentExercice.AddField("jeuId", gamePlayed);
        _currentExercice.AddField("evenements", _events);
        _currentExercice.AddField("actions", _actions);


        _exercices.Add(_currentExercice);

    }

    // Event = new parseval session
    public void AddNewEvent(int param1, int param2, int param3, float surfaceDiff, bool hasWon)
    {

        JSONObject gameEvent = new JSONObject(JSONObject.Type.OBJECT);

        gameEvent.AddField("evenementDate", GetTimestamp());
        gameEvent.AddField("paramDiff1", param1);
        gameEvent.AddField("paramDiff2", param2);
        gameEvent.AddField("paramDiff3", param3);
        gameEvent.AddField("surfaceDiff", surfaceDiff);
        gameEvent.AddField("reussite", hasWon);

        _events.Add(gameEvent);

        // Debug.Log(_events);

    }

    public void AddNewEvent(LPNC_DataExport parsevalInstance, float surfaceDiff, bool hasWon)
    {
        AddNewEvent(parsevalInstance.d01, parsevalInstance.d02, parsevalInstance.d03, surfaceDiff, hasWon);
    }

    public void AddNewAction(string actionType, string actionDetail = null)
    {
        JSONObject gameAction = new JSONObject(JSONObject.Type.OBJECT);

        int timeSpamp = GetTimestamp();

        gameAction.AddField("actionType", actionType);
        gameAction.AddField("actionDate", GetTimestamp());

        if (actionDetail != null)
        {
            gameAction.AddField("actionDetail", actionDetail);
        }

        _actions.Add(gameAction);

    }

    public void SetExerciceEnd()
    {
        _currentExercice.AddField("exerciceEnd", GetTimestamp());
    }

    public void OnApplicationQuit()
    {
        if (Constants.SendTraces)
        {
            if (_currentExercice != null && !_currentExercice.HasField("exerciceEnd"))
            {
                _currentExercice.AddField("exerciceEnd", GetTimestamp());
            }

            _jsonParameters.AddField("sessionEnd", GetTimestamp());

            if (Constants.OnlineVersion)
            {
                WriteTracesLocal();
            }
        }
    }

    public void WriteTracesLocal()
    {
        int sessionBeg = (int)_jsonParameters.GetField("sessionBegin").n;
        string path = Path.Combine(Application.persistentDataPath, string.Format("traces_{0}.json", sessionBeg));

        File.WriteAllText(path, _global.ToString(true));

    }

    IEnumerator PostTraces(string contentJson, FileInfo tracesFile)
    {
        string accessToken = PlayerPrefs.GetString("access_token");
        string url = string.Format("{0}{1}", Constants.BaseURLApi, SendUrl);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/json");
        headers.Add("Authorization", "bearer " + accessToken);
        byte[] pData = System.Text.Encoding.ASCII.GetBytes(contentJson.ToCharArray());

        WWW www = new WWW(url, pData, headers);

        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
            Debug.LogError("Traces sent ! => " + www.text);
            tracesFile.Delete();
        }
        else
        {
            Debug.LogError("Error : " + www.error);
        }
    }
}
