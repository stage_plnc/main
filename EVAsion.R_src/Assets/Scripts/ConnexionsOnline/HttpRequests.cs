﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HttpRequests : MonoBehaviour
{

    public enum RequestType
    {
        Authentification,
        GetEstablishments,
        GetGroups,
        GetGroupsFromEstablishment,
        GetStudents
    };

    [Header("Mgmt")]
    public SendPreviousTraces SendPreviousTracesI;
    public SchoolChoiceMgmt EstablishmentChoiceMgmtI;
    public ChoosePictogramMgmt GroupPictogramMgmt;
    public ChoosePictogramMgmt LearnerPictogramMgmt;

    [Header("Label")]
    public UILabel ConnexionErrorLabel;


    [Header("Auth")]
    public string AuthUrl;
    public string IdEvasion;

    [Header("Establishments")]
    public string EstablishmentsURL;

    [Header("Groups Url")]
    public string GroupURL;

    [Header("Students Url")]
    public string GetStudentsUrl;

    [Header("Write in local")]
    public string EstablishmentJsonFilename;

    public bool Verbose;
    private RequestType _currentRequest;

    // Useful datas
    private string _accessToken;
    private List<string> _establishmentsList;

    private List<JSONObject> _groupsJson;
    private List<Learner> _jsonStudents;

    public List<JSONObject> GroupsInSelectedEstablishment;

    private Dictionary<RequestType, Action<string>> _requestHandler;

    private string _establishmentContentFilepath;

    private void Awake()
    {
        _currentRequest = RequestType.Authentification;
        _requestHandler = new Dictionary<RequestType, Action<string>>();

        _establishmentsList = new List<string>();
        _groupsJson = new List<JSONObject>();
        _jsonStudents = new List<Learner>();

        _requestHandler[RequestType.Authentification] = (s) => HandleAuthentificationRequest(s);
        _requestHandler[RequestType.GetEstablishments] = (s) => HandleEstablishmentsListRequest(s);
        _requestHandler[RequestType.GetGroupsFromEstablishment] = (s) => HandleGroupListFromEstablishmentRequest(s);

        _establishmentContentFilepath = Path.Combine(Application.persistentDataPath, EstablishmentJsonFilename);

        ReadURLPath();
    }

    private void ReadURLPath()
    {
//        string path = string.Empty;

//#if UNITY_EDITOR
//        path = "file:" + Application.dataPath + "/StreamingAssets/";
//#elif UNITY_ANDROID
//             path = "jar:file://"+ Application.dataPath + "!/assets/;
//#elif UNITY_IOS
//             path = "file://" + Application.dataPath + "/Raw/";
//#else
//             //Desktop (Mac OS or Windows)
//             path = "file:"+ Application.dataPath + "/StreamingAssets/";
//#endif

//        path += "URL.txt";

//        StartCoroutine(GetAPIURL(path));
    }

    //private IEnumerator GetAPIURL(string path)
    //{
    //    WWW data = new WWW(path);
    //    yield return data;

    //    Constants.BaseURLApi = data.text;

    //    Request(CreateAccessTokenRequest(), RequestType.Authentification);

    //    UIEventListener.Get(EstablishmentChoiceMgmtI.OKButton).onClick = ShowGroupsWheel;
    //}

    void Start()
    {
        //if (PlayerPrefs.HasKey("SendTraces"))
        //{
        //    if (Constants.SendTraces)
        //    {
        //        SendPreviousTracesI.Send();
        //    }

        //    PlayerPrefs.DeleteKey("SendTraces");
        //}

        Request(CreateAccessTokenRequest(), RequestType.Authentification);

        UIEventListener.Get(EstablishmentChoiceMgmtI.OKButton).onClick = ShowGroupsWheel;
    }

    // Called by the establishment "scebe"
    private void ShowGroupsWheel(GameObject go = null)
    {
        EstablishmentChoiceMgmtI.OKButton.GetComponent<BoxCollider>().enabled = false;

        PlayerPrefs.SetString("establishment", EstablishmentChoiceMgmtI.SelectedEstablishment);

        Request(CreateGroupListFromEstablishmentRequest(), RequestType.GetGroupsFromEstablishment);
    }

    // Called by the group "scene"
    public void GoBackToGroupChoice()
    {
        LearnerPictogramMgmt._tweenAlpha.PlayReverse();
        GroupPictogramMgmt._tweenAlpha.PlayForward();
    }

    // Called by the "group" scene
    public void ShowLearnersWheel(GameObject go = null)
    {
        // Request(CreateStudentsListRequest(), RequestType.GetStudents);

        List<JSONObject> learners = GroupPictogramMgmt.SelectedJSON.GetField("learners").list;
        LearnerPictogramMgmt.FillPictograms(learners, true);

        LearnerPictogramMgmt._tweenAlpha.PlayForward();
        GroupPictogramMgmt._tweenAlpha.PlayReverse();
    }

    private void Request(WWW www, RequestType requestType)
    {
        _currentRequest = requestType;
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        if (Verbose) Debug.Log("HttpRequests : request [" + www.url + "]");
        yield return www;

        // check for errors
        if (www.error == null)
        {
            if (Verbose) Debug.Log("HttpRequests : message retrieved [" + www.text + "]");

            // Handle the http reponse
            _requestHandler[_currentRequest].Invoke(www.text);

            if (ConnexionErrorLabel.gameObject.activeInHierarchy)
            {
                ConnexionErrorLabel.gameObject.SetActive(false);
            }

            if (_currentRequest == RequestType.Authentification)
            {
                if (Constants.SendTraces)
                {
                    SendPreviousTracesI.Send();
                }

                if (!PlayerPrefs.HasKey("establishment"))
                {
                    Request(CreateEstablishmentsListRequest(), RequestType.GetEstablishments);
                }
                else if (PlayerPrefs.HasKey("establishment"))
                {
                    if (Verbose) Debug.Log("HttpRequests : a key have been found");

                    Request(CreateGroupListFromEstablishmentRequest(), RequestType.GetGroupsFromEstablishment);
                }
            }
            else if (_currentRequest == RequestType.GetEstablishments)
            {

            }
            else if (_currentRequest == RequestType.GetGroupsFromEstablishment)
            {

            }
        }
        else
        {
            ManageConnexionError();
            Debug.LogError("HttpRequests : WWW Error : " + www.error);
        }
    }

    private void ManageConnexionError()
    {
        ConnexionErrorLabel.text = "Erreur de connexion, nouvel essai dans 5 secondes";
        ConnexionErrorLabel.gameObject.SetActive(true);

        Invoke("ShowAttemptConnexionLabel", 4.0f);
        Invoke("RetryConnexion", 5.0f);
    }

    public void ShowAttemptConnexionLabel()
    {
        ConnexionErrorLabel.text = "Tentative de connexion...";
        ConnexionErrorLabel.color = Color.black;
    }

    public void RetryConnexion()
    {
        if (_currentRequest == RequestType.Authentification)
        {
            if (PlayerPrefs.HasKey("establishment") && File.Exists(_establishmentContentFilepath))
            {
                string establishmentJson = File.ReadAllText(_establishmentContentFilepath);

                HandleGroupListFromEstablishmentRequest(establishmentJson);

                ConnexionErrorLabel.gameObject.SetActive(false);
            }
            else
            {
                Request(CreateAccessTokenRequest(), RequestType.Authentification);
            }
        }
        else if (_currentRequest == RequestType.GetEstablishments)
        {
            if (File.Exists(_establishmentContentFilepath))
            {

            }
            else
            {
                Request(CreateEstablishmentsListRequest(), RequestType.GetEstablishments);
            }
        }
        else if (_currentRequest == RequestType.GetGroupsFromEstablishment)
        {
            if (File.Exists(_establishmentContentFilepath))
            {
                string establishmentJson = File.ReadAllText(_establishmentContentFilepath);

                HandleGroupListFromEstablishmentRequest(establishmentJson);

                ConnexionErrorLabel.gameObject.SetActive(false);
            }
            else
            {
                Request(CreateGroupListFromEstablishmentRequest(), RequestType.GetGroupsFromEstablishment);
            }
        }
    }
    public void HandleAuthentificationRequest(string reponse)
    {
        JSONObject jsonObject = new JSONObject(reponse);
        _accessToken = jsonObject["access_token"].str;

        PlayerPrefs.SetString("access_token", _accessToken);
    }

    private void HandleEstablishmentsListRequest(string reponse)
    {
        JSONObject jsonObject = new JSONObject(reponse);
        List<JSONObject> jsonList = jsonObject.list;

        foreach (JSONObject establishmentJSON in jsonList)
        {
            _establishmentsList.Add(establishmentJSON.str);
        }

        if (Verbose) Debug.Log("HttpRequests : establishments count " + _establishmentsList.Count);

        EstablishmentChoiceMgmtI.ConstructSchoolList(_establishmentsList);
        EstablishmentChoiceMgmtI.TweenAlpha.PlayForward();
    }
    private void HandleGroupListFromEstablishmentRequest(string response)
    {
        JSONObject responseJson = new JSONObject(response);
        GroupsInSelectedEstablishment = responseJson.list;

        if (GroupsInSelectedEstablishment != null)
        {
            foreach (JSONObject groupJson in GroupsInSelectedEstablishment)
            {
                _groupsJson.Add(groupJson);
            }

            if (PlayerPrefs.HasKey("establishment") && File.Exists(_establishmentContentFilepath))
            {
                string jsonContent = File.ReadAllText(_establishmentContentFilepath);

                if (jsonContent != response)
                {
                    File.WriteAllText(_establishmentContentFilepath, response);
                }
            }
            else
            {
                File.WriteAllText(_establishmentContentFilepath, response);
            }


            GroupPictogramMgmt.FillPictograms(_groupsJson);
            GroupPictogramMgmt._tweenAlpha.PlayForward();
        }
        else
        {
            ConnexionErrorLabel.text = "Pas de groupe dans cet établissement...";
            ConnexionErrorLabel.gameObject.SetActive(true);
        }
    }


    public WWW CreateAccessTokenRequest()
    {
        string url = string.Format("{0}{1}", Constants.BaseURLApi, AuthUrl);

        WWWForm wwwForm = new WWWForm();
        Dictionary<string, string> headers = wwwForm.headers;

        char[] userAndPassword = string.Format("{0}:{1}", "evasion", IdEvasion).ToCharArray();
        headers["Authorization"] = "Basic " +
            System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(userAndPassword));

        wwwForm.AddField("grant_type", "client_credentials");

        WWW www = new WWW(url, wwwForm.data, headers);

        return www;
    }

    public WWW CreateGroupsListRequest()
    {
        string url = string.Format("{0}{1}", Constants.BaseURLApi, GroupURL);

        WWWForm wwwForm = new WWWForm();
        Dictionary<string, string> headers = wwwForm.headers;

        headers["Authorization"] = "bearer " + _accessToken;

        WWW www = new WWW(url, null, headers);

        return www;
    }

    public WWW CreateGroupListFromEstablishmentRequest()
    {
        string establishment = string.Empty;

        if (PlayerPrefs.HasKey("establishment"))
        {
            establishment = PlayerPrefs.GetString("establishment");
        }
        else
        {
            establishment = EstablishmentChoiceMgmtI.SelectedEstablishment;
        }

        string url = string.Format("{0}{1}?rne={2}", Constants.BaseURLApi, GroupURL, establishment);

        WWWForm wwwForm = new WWWForm();
        Dictionary<string, string> headers = wwwForm.headers;

        headers["Authorization"] = "bearer " + _accessToken;

        WWW www = new WWW(url, null, headers);

        return www;
    }

    public WWW CreateEstablishmentsListRequest()
    {
        string url = string.Format("{0}{1}", Constants.BaseURLApi, EstablishmentsURL);

        WWWForm wwwForm = new WWWForm();
        Dictionary<string, string> headers = wwwForm.headers;

        headers["Authorization"] = "bearer " + _accessToken;

        if (Verbose) Debug.Log("HttpRequests : bearer token : [" + headers["Authorization"] + "]");

        WWW www = new WWW(url, null, headers);

        return www;
    }

    public WWW CreateStudentsListRequest()
    {
        string studentUrl = GetStudentsUrl + GroupPictogramMgmt.SelectedJSON.ToString();
        string url = string.Format("{0}{1}", Constants.BaseURLApi, studentUrl);

        WWWForm wwwForm = new WWWForm();
        Dictionary<string, string> headers = wwwForm.headers;

        headers["Authorization"] = "bearer " + _accessToken;

        WWW www = new WWW(url, null, headers);

        return www;
    }
}
