﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pictogram : MonoBehaviour
{
    [Header("Sprite")]
    public UISprite Sprite;
    public UISprite BackgroundSprite;
    public UIAtlas LearnerBackgroundSpriteAtlas;

    [Header("Values")]
    public float RadiusWheel = 512.0f;
    public float TimeToLerp = 0.5f;
    public AnimationCurve AnimationCurve;

    [Range(0, 360)]
    public float Angle;

    public float Scale { get { return transform.localScale.x; } }

    private float _startAngle;
    private float _nextAngle;
    private float _startLerping;
    private bool _isLerping;

    public void Awake()
    {
        _isLerping = false;
    }

    public void Update()
    {
        if (_isLerping)
        {
            float elapsedTime = Time.time - _startLerping;
            Angle = Mathf.Lerp(_startAngle, _nextAngle, AnimationCurve.Evaluate(elapsedTime / TimeToLerp));

            if (elapsedTime > TimeToLerp)
            {
                _isLerping = false;    
            }
        }
    }

    public void LateUpdate()
    {
        float angleRad = Angle * Mathf.Deg2Rad;

        transform.localPosition = new Vector3(RadiusWheel * Mathf.Cos(angleRad), RadiusWheel * Mathf.Sin(angleRad), 0.0f);
    }

    public void SetScale(float scale)
    {
        transform.localScale = Vector3.one * scale;
    }

    public void Init(float angle, UIAtlas atlas, int indexSprite, int indexBackgroundColor, bool learner)
    {
        Angle = angle;

        Sprite.atlas = atlas;
        Sprite.spriteName = atlas.GetListOfSprites()[indexSprite];

        if (learner)
        {
            BackgroundSprite.atlas = LearnerBackgroundSpriteAtlas;
            BackgroundSprite.spriteName = BackgroundSprite.atlas.GetListOfSprites()[indexBackgroundColor];
        }
        
    }

    public void SnapTo(float nextAngle)
    {
        _nextAngle = nextAngle;
        _startLerping = Time.time;
        _isLerping = true;
        _startAngle = Angle;
    }



}
