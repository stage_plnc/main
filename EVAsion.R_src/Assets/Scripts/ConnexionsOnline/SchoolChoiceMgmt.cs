﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SchoolChoiceMgmt : MonoBehaviour
{
    [Header("Elements")]
    public GameObject DropdownButton;
    public UIScrollView ScrollView;
    public UILabel Label;
    public UIGrid Grid;
    public GameObject OKButton;
    public TweenAlpha TweenAlpha;

    [Header("Text")]
    public string SchoolsListFilename;

    [Header("Prefab")]
    public GameObject SchoolButton;

    private List<string> _allEstablishments;
    private string _establishmentChoice;

    public string SelectedEstablishment { get { return _establishmentChoice;  } }

    // Use this for initialization
    void Start()
    {
        // ConstructDropdown();
        UIEventListener.Get(DropdownButton).onClick = OpenDropdown;

        _establishmentChoice = null;

    }

    private void OpenDropdown(GameObject go)
    {
        ScrollView.gameObject.SetActive(true);
    }

    //private void ConstructDropdown()
    //{
    //    _allEstablishments = ReadTextFile();

    //    for (int i = 0; i < _allEstablishments.Count; i++)
    //    {
    //        GameObject schoolButton = NGUITools.AddChild(Grid.gameObject, SchoolButton);
    //        SchoolButtonBehaviour schoolButtonBehaviour = schoolButton.GetComponent<SchoolButtonBehaviour>();

    //        schoolButton.name = _allEstablishments[i];
    //        schoolButtonBehaviour.Init(_allEstablishments[i], this);
    //    }

    //    Grid.Reposition();
    //    ScrollView.ResetPosition();
    //}

    //private List<string> ReadTextFile()
    //{
    //    char[] separatorBackLine = { '\n' };
    //    string schoolsListFilePath = Path.Combine(Application.persistentDataPath, SchoolsListFilename);
    //    List<string> schoolsList = null;
    //    string content = null;

    //    if (File.Exists(schoolsListFilePath))
    //    {
    //        content = File.ReadAllText(schoolsListFilePath);
    //    }

    //    try
    //    {

    //        string[] schools = content.Split(separatorBackLine, StringSplitOptions.RemoveEmptyEntries);

    //        schoolsList = new List<string>(schools);
    //    }
    //    catch (Exception e)
    //    {
    //        Debug.LogError("[SchoolChoiceSceneMgmt] Error reading the text file : " + e.Message);
    //    }

    //    return schoolsList;
    //}

    public void UpdateLabel(string text)
    {
        PlayerPrefs.SetString("establishment", text);

        _establishmentChoice = Label.text = text;
        ScrollView.gameObject.SetActive(false);

        OKButton.GetComponent<BoxCollider>().enabled = true;
        OKButton.GetComponent<UIButton>().SetState(UIButtonColor.State.Normal, true);
    }

    public void Show()
    {
        ScrollView.gameObject.SetActive(true);
    }

    public void ConstructSchoolList(List<string> establishmentsList)
    {
        _allEstablishments = establishmentsList;

        for (int i = 0; i < _allEstablishments.Count; i++)
        {
            GameObject schoolButton = NGUITools.AddChild(Grid.gameObject, SchoolButton);
            SchoolButtonBehaviour schoolButtonBehaviour = schoolButton.GetComponent<SchoolButtonBehaviour>();

            schoolButton.name = _allEstablishments[i];
            schoolButtonBehaviour.Init(_allEstablishments[i], this);
        }

        Grid.Reposition();
        ScrollView.ResetPosition();
    }


}
