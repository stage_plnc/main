﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChoosePictogramMgmt : MonoBehaviour
{
    public enum TypeChoice
    {
        Learner,
        Group
    };

    [Header("Type")]
    public TypeChoice Type;

    [Header("Mgmt")]
    public HttpRequests ConnexionMgmtI;
    public UnlockingPatternMgmt UnlockingPatternMgmt;

    [Header("Button")]
    public GameObject ArrowButton;

    [Header("Wheels")]
    public Wheel Wheel;
    public TweenAlpha tweenAlphaPictograms;

    [Header("Text files")]
    public TextAsset JsonTempFile;
    public TextAsset PatternsFile;
    public string PictosOrderFilename;
    public string PatternsFilename;

    private string[] _pictogramsOrders;
    private Dictionary<string, string> _patternByPicto;
    private string _choosenPictogram = null;
    private string _patternToMatch = null;

    public TweenAlpha _tweenAlpha;

    public JSONObject SelectedJSON { get { return Wheel.SelectedJson; } }

    private void Awake()
    {
        _tweenAlpha = GetComponent<TweenAlpha>();
    }

    void Start()
    {
        if (ArrowButton != null)
        {
            UIEventListener.Get(ArrowButton).onClick = HandleArrowButton;
        }
    }

    private void HandleArrowButton(GameObject go)
    {
        if (Type == TypeChoice.Learner)
        {
            // We decided to choose another group
            if (!Wheel.IsLocked)
            {
                Wheel.Clean();
                ConnexionMgmtI.GoBackToGroupChoice();
            }
            // Else, we clicked on a pictogram but it wasnt the good one
            else
            {
                Wheel.Unlock();
                tweenAlphaPictograms.PlayReverse();
                UnlockingPatternMgmt.Lock();
            }
        }
    }

    private void ShowLearnersWheel(GameObject go = null)
    {
        ConnexionMgmtI.ShowLearnersWheel();
    }

    public void FillPictograms(List<JSONObject> jsonObjects, bool learners = false)
    {
        Wheel.FillPictograms(jsonObjects, learners);
    }

    public void LoadPatternByPicto()
    {
        char[] separator = { ':' };
        char[] separatorBackLine = { '\n' };
        string pictosOrderFilePath = Path.Combine(Application.persistentDataPath, PictosOrderFilename);
        string patternsFilePath = Path.Combine(Application.persistentDataPath, PatternsFilename);
        string content = null;

        if (File.Exists(patternsFilePath))
        {
            content = File.ReadAllText(patternsFilePath);
        }
        else
        {
            content = PatternsFile.text;
        }

        try
        {

            string[] eachPattern = content.Split(separatorBackLine, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < eachPattern.Length; i++)
            {
                string[] nameAndPattern = eachPattern[i].Split(separator, StringSplitOptions.None);
                nameAndPattern[1] = nameAndPattern[1].Trim();

                _patternByPicto[nameAndPattern[0]] = nameAndPattern[1];
            }
        }

        catch (Exception e)
        {
            Debug.LogError("[ChoosePictogramMgmt] Error reading the text file : " + e.Message);
        }

        // Read the file if it exist
        if (File.Exists(pictosOrderFilePath))
        {
            string orderByPicto = File.ReadAllText(pictosOrderFilePath);
            string[] eachLine = orderByPicto.Split(separatorBackLine, StringSplitOptions.RemoveEmptyEntries);

            _pictogramsOrders = new string[eachLine.Length];
            for (int i = 0; i < eachLine.Length; i++)
            {
                _pictogramsOrders[i] = eachLine[i].Trim();
            }
        }
        // Otherwise, we're creating it
        else
        {
            _pictogramsOrders = new string[_patternByPicto.Count];

            for (int i = 0; i < _pictogramsOrders.Length; i++)
            {
                _pictogramsOrders[i] = _patternByPicto.Keys.ElementAt(i);
            }

            File.WriteAllLines(pictosOrderFilePath, _pictogramsOrders);
        }

    }

    public void PictogramHasBeenClicked(JSONObject selectedJson)
    {
        if (Type == TypeChoice.Learner)
        {
            // Store the id of the learner
            OpenUnlockingPatternScene();
        }
        else if (Type == TypeChoice.Group)
        {
            ShowLearnersWheel();
        }
    }

    public void OpenUnlockingPatternScene(GameObject go = null)
    {
        JSONObject learner = Wheel.SelectedJson;
        _patternToMatch = learner.GetField("password").str;

        // Illogical, but lets move on
        UnlockingPatternMgmt.GroupSprite.spriteName = ConnexionMgmtI.GroupPictogramMgmt.Wheel.SelectedPictogram.Sprite.spriteName;
        //UnlockingPatternMgmt.GroupSpriteBackground.spriteName = ConnexionMgmtI.GroupPictogramMgmt.Wheel.SelectedPictogram.BackgroundSprite.spriteName;
        UnlockingPatternMgmt.LearnerSprite.spriteName = Wheel.SelectedPictogram.Sprite.spriteName;
        UnlockingPatternMgmt.LearnerBackground.spriteName = Wheel.SelectedPictogram.BackgroundSprite.spriteName;

        PlayerPrefs.SetString("group_sprite_name", ConnexionMgmtI.GroupPictogramMgmt.Wheel.SelectedPictogram.Sprite.spriteName);
        PlayerPrefs.SetString("leaner_sprite_name", Wheel.SelectedPictogram.Sprite.spriteName);
        PlayerPrefs.SetString("leaner_background_sprite_name", Wheel.SelectedPictogram.BackgroundSprite.spriteName);

        UnlockingPatternMgmt.Unlock(_patternToMatch);

        Wheel.Lock();
        tweenAlphaPictograms.PlayForward();
    }

    public void HasCompletePattern()
    {
        PlayerPrefs.SetInt("learnerId", int.Parse(Wheel.SelectedJson.GetField("learnerId").str));

        SceneManager.LoadScene("CinematicScene");

        //string pictosOrderFilePath = Path.Combine(Application.persistentDataPath, PictosOrderFilename);
        //int idxPictogram = Array.FindIndex(_pictogramsOrders, x => x == _choosenPictogram);

        ////for (int i = idxPictogram; i < _pictogramsOrders.Length - 1; i++)
        ////{
        ////    _pictogramsOrders[i] = _pictogramsOrders[i + 1];
        ////}

        //ArrayList tmpArrayList = new ArrayList(_pictogramsOrders);

        //tmpArrayList.Insert(0, _choosenPictogram);
        //tmpArrayList.RemoveAt(idxPictogram + 1);

        //for (int i = 0; i < tmpArrayList.Count; i++)
        //    _pictogramsOrders[i] = (string)tmpArrayList[i];

        ////_pictogramsOrders[idxPictogram] = _pictogramsOrders[0];
        ////_pictogramsOrders[0] = _choosenPictogram;

        //File.WriteAllLines(pictosOrderFilePath, _pictogramsOrders);
    }
}
