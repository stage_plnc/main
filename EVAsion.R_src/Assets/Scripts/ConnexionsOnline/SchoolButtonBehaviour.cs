﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchoolButtonBehaviour : MonoBehaviour
{
    [Header("Objects")]
    public SchoolChoiceMgmt DropdownBehaviour;
    public UILabel Label;

    public void Start()
    {
        UIEventListener.Get(gameObject).onClick = UpdateChoice;
    }

    private void UpdateChoice(GameObject go)
    {
        DropdownBehaviour.UpdateLabel(Label.text);
    }

    public void Init(string label, SchoolChoiceMgmt dropdownBehaviour)
    {
        Label.text = label;
        DropdownBehaviour = dropdownBehaviour;
    }
}
