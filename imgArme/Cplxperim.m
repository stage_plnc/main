clc;
clf;
clear all;

%%
%       Author : Pierre Brossy
%       Use for research only during internship at LPNC
%
%       This program goal is to compute the abslute complexity of an
%       alphabet an to store it in an array
%
%       Input : You need to have files that contains images (PNG format
%       type ) of the each letter of the alphabet plus a corresponding
%       directory with matching names of images of the contour of the
%       letter. There name must be numbers (from 1.png to XX.png)
%
%       Output : Array containing the letters complexity given the order
%       they are stored within the source directory
%       Images directory must be named respectively
%       "Contours" and "Letters" and must be in the same directory as this file.


%%
path= fileparts(mfilename('fullpath')); 
cntPath= strcat(path,'/Contours/');
letPath= strcat(path,'/Letters/');
savefile= strcat(path,'Results.txt');

%Checking that each letter as a corresponding contour
d1 = dir(cntPath);
d2 = dir(letPath);
l1 = length(find([d1.isdir]==0));
l2 = length(find([d2.isdir]==0));

% Exception raised
if (l1 ~= l2)
    print('ERROR :There are not matching elements in contour and letters directories.')

else
    results= ones(1,l1);
    %Img treatment + complexity computation
    for i=1:l1
    %   In this part we do a first image treatment , which will  binarize
    %   both the letter and contour image. They will be stored as 2 Binary
    %   matrix with 1 symbolizing white and 0 symbolizing black.
    
    imgCntName = strcat(cntPath, int2str(i));
    imgCntName = strcat(imgCntName, '.png');    
    imgLetName = strcat(letPath, int2str(i));
    imgLetName = strcat(imgLetName, '.png');
    

    imgCnt= imread(imgCntName);
    imgLet=imread(imgLetName);
    imgCntBW = im2bw(imgCnt,0.9);
    imgLetBW = im2bw(imgLet,0.9);
    
    
    %This part will go through both matrix and count the different black
    %parts. All the black pixels in the contour matrix symbolize the
    %image's perimeter. All the black pixels in the Letter Matrix will
    %symbolize the image's area.
    
    currentPerim=0;
    currentArea=0;
   
    for j=1:111
        for k=1:111
            if (imgLetBW(j,k) == 0)
                currentArea = currentArea + 1;
            end
            if (imgCntBW(j,k) == 0)
                currentPerim = currentPerim + 1;
            end
        end  
    end
 
    %Complexity computation p^2/area
    %Pelli et al. 2006
    results(i) = (currentPerim*currentPerim) / currentArea;

%     %% Uncomment this if you want to plot both images
         figure;
         subplot(1,2,1);
         imshow(imgCntBW);
         subplot(1,2,2);
         imshow(imgLetBW);
    end 
end




