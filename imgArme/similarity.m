clc;
clear all;

final = ones(36,36);
path= fileparts(mfilename('fullpath')); 
lettersPath= strcat(path,'/letter_checker/');

for k = 1:36
    for j = 1:36

        l1 = k;
        l2 = j;
        l1Name = strcat(lettersPath,int2str(l1));
        l2Name = strcat(lettersPath,int2str(l2));


        fid1 = fopen(l1Name);
        fid2 = fopen(l2Name);
        let = textscan(fid1, '%d');
        letters1 = let{1,1}; 
        let = textscan(fid2, '%d');
        letters2 = let{1,1};
        res=0;
        for i=1:36
            if letters1(i,1) ~= letters2(i,1)
                res = res + 1;
            end
        end
        final(k,j)=res;
        
    end
end